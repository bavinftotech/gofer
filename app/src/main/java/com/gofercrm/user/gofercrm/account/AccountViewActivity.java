package com.gofercrm.user.gofercrm.account;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.attachments.AttachmentActivity;
import com.gofercrm.user.gofercrm.entity.Address;
import com.gofercrm.user.gofercrm.entity.Comment;
import com.gofercrm.user.gofercrm.entity.Email;
import com.gofercrm.user.gofercrm.entity.Phone;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AccountViewActivity extends BaseActivity {

    ArrayList<AccountView> accountPage = new ArrayList<>();
    AccountList account_obj = new AccountList();
    List comments = new ArrayList<Comment>();
    String ACCOUNT_ID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_view);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        ACCOUNT_ID = intent.getStringExtra("ACCOUNT_ID");
        if (ACCOUNT_ID != null) {
            getAccountById(ACCOUNT_ID);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_account_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_attachment) {
            Intent intent = new Intent(getApplicationContext(), AttachmentActivity.class);
            intent.putExtra("ENTITY_ID", ACCOUNT_ID);
            intent.putExtra("ENTITY_TYPE", "account");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        if (id == R.id.action_delete) {
            new AlertDialog.Builder(this)
                    .setTitle(getResources().getString(R.string.delete))
                    .setMessage(getResources().getString(R.string.delete_confirmation_msg))
                    .setIcon(android.R.drawable.ic_menu_delete)
                    .setPositiveButton(getResources().getString(R.string.yes), (dialog, whichButton) -> deleteAccountById(ACCOUNT_ID))
                    .setNegativeButton(getResources().getString(R.string.no), null).show();
        }
        return super.onOptionsItemSelected(item);
    }

    public ArrayList<AccountView> getData() {
        return accountPage;
    }

    public List<Comment> getComments() {
        comments = account_obj.getComments();
        return comments;
    }

    public String getAccount_Id() {
        String account_id;
        account_id = ACCOUNT_ID != null ? ACCOUNT_ID : "";
        return account_id;
    }

    private void deleteAccountById(String account_id) {
        JSONObject postData = new JSONObject();
        try {
            postData.put("action", "delete");
            postData.put("account_id", account_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        NetworkManager.customJsonObjectRequest(
                AccountViewActivity.this, Constants.MANAGE_ACCOUNT_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processResult(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ACCOUNT PROCESS ERROR", message);
                    }

                }, true);
    }

    private void processResult(JSONObject response) {
        try {
            if (Integer.parseInt(response.getString("result")) == 1) {
                Util.onMessageTop(getApplicationContext(), getResources().getString(R.string.account_deleted));
                Intent intent = new Intent(getApplicationContext(), AccountListActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } else {
                Util.onMessageTop(getApplicationContext(), getResources().getString(R.string.account_deleted_failed));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void processData(JSONObject data) {
        try {
            if (Integer.parseInt(data.getString("result")) == 1) {

                JSONObject result_Data = data.getJSONObject("data");
                JSONArray leads = result_Data.getJSONArray("accounts");
                for (int i = 0; i < leads.length(); i++) {

                    JSONObject obj = leads.getJSONObject(i);
                    JSONArray emailArray = obj.getJSONArray("email_list");
                    List<Email> emails = new ArrayList<>();
                    for (int j = 0; j < emailArray.length(); j++) {
                        JSONObject emailObj = emailArray.getJSONObject(j);
                        emails.add(new Email(emailObj.getString("email"), emailObj.getString("type")));
                    }
                    JSONArray phoneArray = obj.getJSONArray("phone_list");
                    List<Phone> phones = new ArrayList<>();
                    for (int j = 0; j < phoneArray.length(); j++) {
                        JSONObject phoneObj = phoneArray.getJSONObject(j);
                        phones.add(new Phone(phoneObj.getString("phone"), phoneObj.getString("type")));
                    }
                    JSONArray addressArray = obj.getJSONArray("address_list");
                    List<Address> addresses = new ArrayList();
                    for (int j = 0; j < addressArray.length(); j++) {
                        JSONObject addressObj = addressArray.getJSONObject(j);
                        addresses.add(new Address(addressObj.getString("address"), addressObj.getString("type")));
                    }

//                    JSONArray commentsArray = obj.getJSONArray("comments");
//                    List<Comment> comments = new ArrayList();
//
//                    for (int j = 0; j < commentsArray.length(); j++) {
//                        JSONObject commentObj = commentsArray.getJSONObject(j);
//                        comments.add(new Comment(commentObj.getString("comment"), commentObj.getString("date")));
//                    }

                    List<Comment> comments_data = new ArrayList();
                    if (obj.has("comments")) {
                        Object comment_instance = obj.get("comments");
                        if (comment_instance instanceof JSONArray) {
                            JSONArray commentsArray = obj.getJSONArray("comments");

                            for (int j = 0; j < commentsArray.length(); j++) {
                                JSONObject commentObj = commentsArray.getJSONObject(j);
                                comments_data.add(new Comment(commentObj.getString("comment"), commentObj.getString("date"), commentObj.has("url") ? commentObj.getString("url") : "",
                                        commentObj.has("content_type") ? commentObj.getString("content_type") : ""));
                            }
                        }
                    }

//                    JSONObject lOObj = obj.getJSONObject("lead_owner");
//                    LeadOwner leadOwner = new LeadOwner(lOObj.getString("id"),
//                            lOObj.getString("name"));

                    account_obj = new AccountList(obj.getString("account_id"),
                            obj.getString("name"),
                            obj.getString("status"),
                            obj.getString("file_as"),
                            obj.getString("source"),
                            comments_data,
                            addresses,
                            emails,
                            phones,
                            obj.getString("name")
                    );
                    renderView();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getAccountById(String account_id) {
        JSONObject postData = new JSONObject();
        try {
            postData.put("action", "get");
            postData.put("account_id", account_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        NetworkManager.customJsonObjectRequest(
                AccountViewActivity.this, Constants.MANAGE_ACCOUNT_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processData(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ACCOUNT PROCESS ERROR", message);
                    }
                }, true);
    }

    private void renderView() {
        AccountView av;
        av = new AccountView(getResources().getString(R.string.name),
                account_obj.getTitle(), "", R.drawable.ic_account, 0);
        accountPage.add(av);
        for (int i = 0; i < account_obj.getEmails().size(); i++) {
            Email email = account_obj.getEmails().get(i);
            String et = getResources().getString(R.string.email);
            if (!email.getType().isEmpty()) {
                et = et + "(" + email.getType() + ")";
            }

            av = new AccountView(et, email.getEmail(), "EMAIL", R.drawable.ic_email, 0);
            accountPage.add(av);
        }

        for (int i = 0; i < account_obj.getPhones().size(); i++) {
            Phone phone = account_obj.getPhones().get(i);
            String pt = getResources().getString(R.string.phone);
            if (!phone.getType().isEmpty()) {
                pt = pt + "(" + phone.getType() + ")";
            }
            av = new AccountView(pt, phone.getPhone(), "PHONE", R.drawable.ic_call, R.drawable.ic_message);
            accountPage.add(av);
        }

        for (int i = 0; i < account_obj.getAddresses().size(); i++) {
            Address address = account_obj.getAddresses().get(i);
            String at = getResources().getString(R.string.address);
            if (!address.getType().isEmpty()) {
                at = at + "(" + address.getType() + ")";
            }
            av = new AccountView(at, address.getAddress(), "ADDRESS", R.drawable.ic_location, R.drawable.ic_directions);
            accountPage.add(av);
        }

//        cv = new AccountView("Lead Owner", client_obj.getLeadOwner().getName(), "", R.drawable.ic_account_circle, 0);
//        clientPage.add(cv);

        av = new AccountView(getResources().getString(R.string.source),
                account_obj.getLeadSource(), "", 0, 0);
        accountPage.add(av);
        av = new AccountView(getResources().getString(R.string.file_as),
                account_obj.getFileAs(), "", 0, 0);
        accountPage.add(av);

        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(account_obj.getTitle());
        AccountViewPagerAdapter mSectionsPagerAdapter = new AccountViewPagerAdapter(getSupportFragmentManager());
        ViewPager mViewPager = findViewById(R.id.leadView_viewPager_id);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        final FloatingActionButton fab = findViewById(R.id.lead_view_edit_id);
        fab.setOnClickListener(view -> {
            Intent leadIntent = new Intent(getApplicationContext(), AccountProcessActivity.class);
            leadIntent.putExtra("ACCOUNT_ID", account_obj.getId());
            leadIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(leadIntent);
        });

        TabLayout tabLayout = findViewById(R.id.leadView_tabs_id);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout) {
            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        fab.show();
                        break;
                    case 1:
                        fab.hide();
                        break;
                    case 2:
                        fab.hide();
                        break;

                }
            }
        });
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
    }
}