package com.gofercrm.user.gofercrm.websocket.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Payload {
    @SerializedName("call_type")
    @Expose
    private String callType;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("channel_name")
    @Expose
    private String channelName;

    public String getCallType() {
        return callType;
    }

    public void setCallType(String callType) {
        this.callType = callType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }
}