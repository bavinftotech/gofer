package com.gofercrm.user.gofercrm.task;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.InputType;
import android.text.Spanned;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.text.HtmlCompat;

import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.appointments.Contact;
import com.gofercrm.user.gofercrm.appointments.ContactAdapter;
import com.gofercrm.user.gofercrm.attachments.AttachmentActivity;
import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.SharedPreferenceData;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class NewTaskActivity extends BaseActivity {
    final Calendar myCalendar = Calendar.getInstance();
    LinearLayout due_date_layout;
    List<String> subject_type;
    ArrayAdapter subjectTypeAdapter;
    List<String> status;
    ArrayAdapter statusAdapter;
    List<String> action_type;
    ArrayAdapter actionAdapter;
    List<String> priority;
    ArrayAdapter priorityAdapter;
    AutoCompleteTextView subjectsChip;
    List<Contact> subjects = new ArrayList<>();
    ArrayAdapter<Contact> subjectAdapter;
    AutoCompleteTextView connectionsChip;
    List<Contact> connections = new ArrayList<>();
    ArrayAdapter<Contact> connectionsAdapter;
    List<Contact> selectedSubjectList = new ArrayList<>();
    List<Contact> selectedContactsList = new ArrayList<>();
    EditText task_name, action_text, task_desc, task_project_name, comments;
    EditText dueDate;
    Spinner subjectTypeSpinner;
    Spinner statusSpinner, prioritySpinner;
    LinearLayout event_timezone_layout_id, llContext;
    Spinner actionSpinner;
    String TASK_ID = "";
    String SUBJECT_ID;
    String EMAIL_ID, PHONE;
    String USER_ID;
    String LEAD_ID, CLIENT_ID, OPPORTUNITY_ID;
    //    ProgressDialog progressDialog;
    String PROCESSING_MESSAGE;
    String SUCCESS_MESSAGE;
    Toolbar toolbar;
    private int year, month, day;
    private int mYear, mMonth, mDay, mHour, mMinute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_task);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        PROCESSING_MESSAGE = getResources().getString(R.string.saving_task_);
        SUCCESS_MESSAGE = getResources().getString(R.string.task_save_success);

        dueDate = findViewById(R.id.task_due_date);
        task_name = findViewById(R.id.input_task_name);
        task_desc = findViewById(R.id.input_task_action_desc);
        task_project_name = findViewById(R.id.input_task_project);
        action_text = findViewById(R.id.input_task_action_text);
        comments = findViewById(R.id.input_task_action_comments);

        subject_type = new ArrayList<>();
        subject_type.addAll(Arrays.asList(getResources().getStringArray(R.array.arrSubject)));

        status = new ArrayList<>();
        status.addAll(Arrays.asList(getResources().getStringArray(R.array.arrStatus)));

        action_type = new ArrayList<>();
        action_type.addAll(Arrays.asList(getResources().getStringArray(R.array.arrAction)));

        priority = new ArrayList<>();
        priority.addAll(Arrays.asList(getResources().getStringArray(R.array.arrPriority)));

        statusSpinner = findViewById(R.id.task_status);
        statusAdapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_item, status);
        statusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        statusSpinner.setAdapter(statusAdapter);

        prioritySpinner = findViewById(R.id.task_priority);
        priorityAdapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_item, priority);
        priorityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        prioritySpinner.setAdapter(priorityAdapter);

        event_timezone_layout_id = findViewById(R.id.event_timezone_layout_id);
        llContext = findViewById(R.id.llContext);
        event_timezone_layout_id.setVisibility(SharedPreferenceData.getInstance(this).isFullAccessAccount() ?
                View.VISIBLE : View.GONE);
        llContext.setVisibility(SharedPreferenceData.getInstance(this).isFullAccessAccount() ?
                View.VISIBLE : View.GONE);
        subjectTypeSpinner = findViewById(R.id.task_subject_type);
        subjectTypeAdapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_item, subject_type);
        subjectTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        subjectTypeSpinner.setAdapter(subjectTypeAdapter);
        subjectTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selectedValue = getResources().getString(R.string.none);
                selectedValue = subject_type.get(i);
                if (getResources().getString(R.string.lead).equalsIgnoreCase(selectedValue)) {
                    getLeads(null);
                } else if (getResources().getString(R.string.contact).equalsIgnoreCase(selectedValue)) {
                    getContacts(null);
                } else if (getResources().getString(R.string.communities).equalsIgnoreCase(selectedValue)) {
                    getCommunities(null);
                } else if (getResources().getString(R.string.account).equalsIgnoreCase(selectedValue)) {
                    getAccounts(null);
                } else if (getResources().getString(R.string.opportunity).equalsIgnoreCase(selectedValue)) {
                    getOpportunities(null);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        final DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateDueDate();
        };

        dueDate.setOnClickListener(v -> new DatePickerDialog(NewTaskActivity.this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show());

        actionSpinner = findViewById(R.id.task_action_type);
        actionAdapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_item, action_type);
        actionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        actionSpinner.setAdapter(actionAdapter);
        actionSpinner.setSelection(0);
        actionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selectedValue = action_type.get(i);
                if (getResources().getString(R.string.call).equals(selectedValue)) {
                    action_text.setHint(getResources().getString(R.string.call_hint));
                    action_text.setText(PHONE);
                    action_text.setInputType(InputType.TYPE_CLASS_PHONE);
                } else if (getResources().getString(R.string.general).equals(selectedValue)) {
                    action_text.setHint(getResources().getString(R.string.general_hint));
                    action_text.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
                } else if (getResources().getString(R.string.email_task).equals(selectedValue)) {
                    action_text.setHint(getResources().getString(R.string.email_task_hint));
                    action_text.setText(EMAIL_ID);
                    action_text.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                } else if (getResources().getString(R.string.postcard).equals(selectedValue)) {
                    action_text.setHint(getResources().getString(R.string.postcard_hint));
                    action_text.setInputType(InputType.TYPE_TEXT_VARIATION_POSTAL_ADDRESS);
                } else if (getResources().getString(R.string.text).equals(selectedValue)) {
                    action_text.setHint(getResources().getString(R.string.text_hint));
                    action_text.setInputType(InputType.TYPE_CLASS_PHONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        Intent intent = getIntent();
        LEAD_ID = intent.getStringExtra("LEAD_ID");
        if (LEAD_ID != null) {
            getLeadById(LEAD_ID);
            subjectTypeSpinner.setSelection(1);
            subjectTypeSpinner.setEnabled(false);
        }
        CLIENT_ID = intent.getStringExtra("CLIENT_ID");
        if (CLIENT_ID != null) {
            getClientById(CLIENT_ID);
            subjectTypeSpinner.setSelection(3);
            subjectTypeSpinner.setEnabled(false);
        }

        OPPORTUNITY_ID = intent.getStringExtra("OPPORTUNITY_ID");
        if (OPPORTUNITY_ID != null) {
            getOpportunityById(OPPORTUNITY_ID);
            subjectTypeSpinner.setSelection(5);
            subjectTypeSpinner.setEnabled(false);
        }
        subjectAdapter = new ContactAdapter(this, R.layout.contact_ac_layout, subjects);
        subjectsChip = findViewById(R.id.leads_id);
        subjectsChip.setAdapter(subjectAdapter);
        subjectsChip.setOnItemClickListener((adapterView, view, i, l) -> {
            SUBJECT_ID = ((Contact) adapterView.getItemAtPosition(i)).getId();
            EMAIL_ID = ((Contact) adapterView.getItemAtPosition(i)).getEmail();
            PHONE = ((Contact) adapterView.getItemAtPosition(i)).getPhone();
        });

        connectionsAdapter = new ContactAdapter(this, R.layout.contact_ac_layout, connections);
        connectionsChip = findViewById(R.id.connections_id);
        connectionsChip.setAdapter(connectionsAdapter);
        connectionsChip.setOnItemClickListener((adapterView, view, i, l) -> USER_ID = ((Contact) adapterView.getItemAtPosition(i)).getId());

        TASK_ID = intent.getStringExtra("TASK_ID");
        if (TASK_ID != null) {
            if (getSupportActionBar() != null)
                getSupportActionBar().setTitle(getResources().getString(R.string.edit_task));
            comments.setVisibility(View.GONE);
            PROCESSING_MESSAGE = getResources().getString(R.string.updating_task);
            SUCCESS_MESSAGE = getResources().getString(R.string.task_update_success);
            getTasksById(TASK_ID);
        } else {
            if (getSupportActionBar() != null)
                getSupportActionBar().setTitle(getResources().getString(R.string.new_task));
            updateDueDate();
            getConnections(null);
        }
    }

    private void getLeadById(String lead_id) {
        JSONObject postData = new JSONObject();
        try {
            postData.put("action", "get");
            postData.put("lead_id", lead_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        NetworkManager.customJsonObjectRequest(
                NewTaskActivity.this, Constants.LEAD_LIST_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processLeads(response, null);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("LEAD PROCESS ERROR", message);
                    }
                }, true);
    }

    private void getLeads(final Task task) {
        NetworkManager.customJsonObjectRequest(
                NewTaskActivity.this, Constants.GET_LEADS_PICKLIST_URL, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processLeads(response, task);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ERRROR=", message);
                    }
                }, false);
    }

    private void processLeads(JSONObject data, Task task) {
        try {
            subjects.clear();
            String id = "";
            String name = "";
            String email = "";
            String phone = "";
            if (Integer.parseInt(data.getString("result")) == 1) {
                JSONObject result_Data = data.getJSONObject("data");
                JSONArray lead_contacts = result_Data.getJSONArray("lead_contacts");
                for (int i = 0; i < lead_contacts.length(); i++) {
                    JSONObject obj = lead_contacts.getJSONObject(i);
                    JSONArray emailArray = obj.getJSONArray("email_list");
                    for (int j = 0; j < emailArray.length(); j++) {
                        JSONObject emailObj = emailArray.getJSONObject(j);
                        email = emailObj.has("email") ? emailObj.getString("email") : "";
                    }
                    JSONArray phoneArray = obj.getJSONArray("phone_list");
                    for (int j = 0; j < phoneArray.length(); j++) {
                        JSONObject phoneObj = phoneArray.getJSONObject(j);
                        phone = phoneObj.has("phone") ? phoneObj.getString("phone") : "";
                    }
                    id = obj.getString("id");
                    name = obj.getString("name");
                    if (name != null) {
                        name = Util.capitalize(name);
                    }
                    if (task != null) {
                        if (task.getSubjectId().equals(id)) {
                            subjectsChip.setText(name);
                        }
                    }
                    if (LEAD_ID != null && LEAD_ID.equals(id)) {
                        toolbar.setSubtitle(getResources().getString(R.string.for_) + " " + name);
                        subjectsChip.setText(name);
                        subjectsChip.setEnabled(false);
                    }
                    Contact contact = new Contact(id, name, email, phone);
                    subjects.add(contact);
                }
                subjectAdapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getClientById(String client_id) {
        JSONObject postData = new JSONObject();
        try {
            postData.put("action", "get");
            postData.put("contact_id", client_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        NetworkManager.customJsonObjectRequest(
                NewTaskActivity.this, Constants.MANAGE_CONTACT_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processContacts(response, null);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("CLIENT PROCESS ERROR", message);
                    }
                }, true);
    }

    private void getContacts(final Task task) {
        NetworkManager.customJsonObjectRequest(
                NewTaskActivity.this, Constants.GET_CONTACTS_PICKLIST_URL, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processContacts(response, task);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ERRROR=", message);
                    }
                }, false);
    }

    private void processContacts(JSONObject data, Task task) {
        try {
            subjects.clear();
            String id = "";
            String name = "";
            String email = "", phone = "";
            if (Integer.parseInt(data.getString("result")) == 1) {
                JSONObject result_Data = data.getJSONObject("data");
                JSONArray leads = result_Data.getJSONArray("contacts");
                for (int i = 0; i < leads.length(); i++) {
                    JSONObject obj = leads.getJSONObject(i);
                    JSONArray emailArray = obj.getJSONArray("email_list");

                    for (int j = 0; j < emailArray.length(); j++) {
                        JSONObject emailObj = emailArray.getJSONObject(j);
                        email = emailObj.has("email") ? emailObj.getString("email") : "";
                    }

                    JSONArray phoneArray = obj.getJSONArray("phone_list");
                    for (int j = 0; j < phoneArray.length(); j++) {
                        JSONObject phoneObj = phoneArray.getJSONObject(j);
                        phone = phoneObj.has("phone") ? phoneObj.getString("phone") : "";
                    }
                    id = obj.getString("id");
                    name = obj.getString("name");
                    if (name != null) {
                        name = Util.capitalize(name);
                    }
                    if (task != null) {
                        if (task.getSubjectId().equals(id)) {
                            subjectsChip.setText(name);
                        }
                    }
                    if (CLIENT_ID != null && CLIENT_ID.equals(id)) {
                        toolbar.setSubtitle(getResources().getString(R.string.for_) + " " + name);
                        subjectsChip.setText(name);
                        subjectsChip.setEnabled(false);
                    }
                    Contact contact = new Contact(id, name, email, phone);
                    subjects.add(contact);
                }
                subjectAdapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getConnections(final Task task) {
        NetworkManager.customJsonObjectGetRequest(
                NewTaskActivity.this, Constants.GET_CONNECTIONS_PICKLIST_URL, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processConnections(response, task);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ERRROR=", message);
                    }
                }, false);
    }

    private void processConnections(JSONObject data, Task task) {
        try {
            connections.clear();
            String id = "";
            String name = "", first_name = "", last_name = "";
            String email = "", phone = "";
            if (Integer.parseInt(data.getString("result")) == 1) {
                JSONArray users = data.getJSONArray("users");
                for (int i = 0; i < users.length(); i++) {
                    JSONObject obj = users.getJSONObject(i);
                    email = obj.has("email") ? obj.getString("email") : "";
                    phone = obj.has("phone") ? obj.getString("phone") : "";
                    id = obj.getString("id");
                    first_name = obj.has("first_name") ? obj.getString("first_name") : "";
                    last_name = obj.has("last_name") ? obj.getString("last_name") : "";
                    name = first_name + " " + last_name;
                    if (name != null) {
                        name = Util.capitalize(name);
                    }
                    if (task != null) {
                        if (task.getUserId().equals(id)) {
                            connectionsChip.setText(name);
                        }
                    }
                    Contact contact = new Contact(id, name, email, phone);
                    connections.add(contact);
                }
                connectionsAdapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getCommunities(final Task task) {
        JSONObject postData = new JSONObject();
        try {
            postData.put("is_active", true);
            postData.put("page_size", 5000);
            postData.put("exclude_manual_data_source", false);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        NetworkManager.customJsonObjectRequest(
                NewTaskActivity.this, Constants.GET_COMMUNITIES_PICKLIST_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processCommunities(response, task);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ERRROR=", message);
                    }
                }, false);
    }

    private void processCommunities(JSONObject response, Task task) {
        try {
            subjects.clear();
            String id = "";
            String name = "";
            String email = "", phone = "";
            if (Integer.parseInt(response.getString("result")) == 1) {
                JSONArray lead_contacts = response.getJSONArray("properties");
                for (int i = 0; i < lead_contacts.length(); i++) {
                    JSONObject obj = lead_contacts.getJSONObject(i);
                    JSONArray emailArray = obj.getJSONArray("email_list");
                    for (int j = 0; j < emailArray.length(); j++) {
                        JSONObject emailObj = emailArray.getJSONObject(j);
                        email = emailObj.getString("email");
                    }
                    JSONArray phoneArray = obj.getJSONArray("phone_list");
                    for (int j = 0; j < phoneArray.length(); j++) {
                        JSONObject phoneObj = phoneArray.getJSONObject(j);
                        phone = phoneObj.getString("phone");
                    }
                    id = obj.getString("id");
                    name = obj.getString("name");
                    if (name != null) {
                        name = Util.capitalize(name);
                    }
                    if (task != null) {
                        if (task.getSubjectId().equals(id)) {
                            toolbar.setSubtitle(getResources().getString(R.string.for_) + " " + name);
                            subjectsChip.setText(name);
                        }
                    }
                    Contact contact = new Contact(id, name, email, phone);
                    subjects.add(contact);
                }
                subjectAdapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getAccounts(final Task task) {
        NetworkManager.customJsonObjectRequest(
                NewTaskActivity.this, Constants.GET_ACCOUNTS_PICKLIST_URL, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processAccounts(response, task);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ERRROR=", message);
                    }
                }, false);
    }

    private void processAccounts(JSONObject response, Task task) {
        try {
            subjects.clear();
            String id = "";
            String name = "";
            String email = "";
            String phone = "";
            if (Integer.parseInt(response.getString("result")) == 1) {
                JSONObject result_Data = response.getJSONObject("data");
                JSONArray leads = result_Data.getJSONArray("accounts");
                for (int i = 0; i < leads.length(); i++) {
                    JSONObject obj = leads.getJSONObject(i);
                    JSONArray emailArray = obj.getJSONArray("email_list");
                    for (int j = 0; j < emailArray.length(); j++) {
                        JSONObject emailObj = emailArray.getJSONObject(j);
                        email = emailObj.getString("email");
                    }

                    JSONArray phoneArray = obj.getJSONArray("phone_list");
                    for (int j = 0; j < phoneArray.length(); j++) {
                        JSONObject phoneObj = phoneArray.getJSONObject(j);
                        phone = phoneObj.getString("phone");
                    }
                    id = obj.getString("id");
                    name = obj.getString("name");
                    if (name != null) {
                        name = Util.capitalize(name);
                    }
                    if (task != null) {
                        if (task.getSubjectId().equals(id)) {
                            toolbar.setSubtitle(getResources().getString(R.string.for_) + " " + name);
                            subjectsChip.setText(name);
                        }
                    }
                    Contact contact = new Contact(id, name, email, phone);
                    subjects.add(contact);
                }
                subjectAdapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getOpportunityById(String oppor_id) {
        JSONObject postData = new JSONObject();
        try {
            postData.put("action", "get");
            postData.put("opportunity_id", oppor_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        NetworkManager.customJsonObjectRequest(
                NewTaskActivity.this, Constants.OPPORTUNITY_MANAGE_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processOpportunities(response, null);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("OPPOR. PROCESS ERROR", message);
                    }
                }, true);
    }

    private void getOpportunities(final Task task) {
        JSONObject postData = new JSONObject();
        NetworkManager.customJsonObjectRequest(
                NewTaskActivity.this, Constants.GET_OPPORTUNITIES_PICKLIST_URL, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processOpportunities(response, task);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ERRROR=", message);
                    }
                }, false);
    }

    private void processOpportunities(JSONObject response, Task task) {
        try {
            subjects.clear();
            String id = "";
            String name = "";
            String email = "", phone = "";
            if (Integer.parseInt(response.getString("result")) == 1) {
                JSONObject result_Data = response.getJSONObject("data");
                JSONArray leads = result_Data.getJSONArray("opportunities");
                for (int i = 0; i < leads.length(); i++) {
                    JSONObject obj = leads.getJSONObject(i);
//                    JSONArray emailArray = obj.getJSONArray("email_list");
//                    List<Email> emails = new ArrayList<>();
//                    for (int j = 0; j < emailArray.length(); j++) {
//                        JSONObject emailObj = emailArray.getJSONObject(j);
//                        email = emailObj.getString("email");
//                    }
                    id = obj.getString("id");
                    name = obj.getString("name");
                    if (name != null) {
                        name = Util.capitalize(name);
                    }
                    if (task != null) {
                        if (task.getSubjectId().equals(id)) {
                            subjectsChip.setText(name);
                        }
                    }
                    if (OPPORTUNITY_ID != null && OPPORTUNITY_ID.equals(id)) {
                        toolbar.setSubtitle(getResources().getString(R.string.for_) + " " + name);
                        subjectsChip.setText(name);
                        subjectsChip.setEnabled(false);
                    }
                    Contact contact = new Contact(id, name, email, phone);
                    subjects.add(contact);
                }
                subjectAdapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateDueDate() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        dueDate.setText(sdf.format(myCalendar.getTime()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_task_view, menu);
        MenuItem attachment = menu.findItem(R.id.action_attachment);
        MenuItem delete = menu.findItem(R.id.action_delete);
        if (TASK_ID == null || TASK_ID.isEmpty()) {
            attachment.setVisible(false);
            delete.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_save) {
            if (!validate()) {
                Util.onMessage(getApplicationContext(), getResources().getString(R.string.save_failed));
            } else {
                createTask();
            }
        }
        if (id == R.id.action_delete) {
            onDelete();
        }
        if (id == R.id.action_attachment) {
            Intent intent = new Intent(getApplicationContext(), AttachmentActivity.class);
            intent.putExtra("ENTITY_ID", TASK_ID);
            intent.putExtra("ENTITY_TYPE", "task");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean validate() {
        boolean valid = true;
        String task_name_value = task_name.getText().toString();
        if (task_name_value != null && task_name_value.isEmpty()) {
            task_name.setError(getResources().getString(R.string.name_empty_err));
            valid = false;
        } else {
            task_name.setError(null);
        }

//        if (USER_ID == null) {
//            connectionsChip.setError("Please assign task to someone");
//            valid = false;
//        } else {
//            connectionsChip.setError(null);
//        }
        if (dueDate == null && dueDate.getText().toString().isEmpty()) {
            dueDate.setError(getResources().getString(R.string.due_date_is_mandatory));
            valid = false;
        } else {
            dueDate.setError(null);
        }
        return valid;
    }

    private JSONObject requestData() {
        JSONObject payload = new JSONObject();
        try {
            if (USER_ID == null || USER_ID.isEmpty()) {
                USER_ID = SharedPreferenceData.getInstance(getApplicationContext()).getLoggedInUserID();
            }

            payload.put("name", task_name.getText());
            if (!dueDate.getText().toString().isEmpty()) {
                payload.put("due_date", dueDate.getText().toString());
            }

            if (SUBJECT_ID != null) {
                payload.put("subject", SUBJECT_ID);
            }
            payload.put("subject_type", subjectTypeSpinner.getSelectedItem());
            int priority = 0;
            if (prioritySpinner.getSelectedItem() != null) {
                if (prioritySpinner.getSelectedItem().equals(getResources().getString(R.string.high))) {
                    priority = 1;
                } else if (prioritySpinner.getSelectedItem().equals(getResources().getString(R.string.medium))) {
                    priority = 2;
                } else if (prioritySpinner.getSelectedItem().equals(getResources().getString(R.string.low))) {
                    priority = 3;
                }
                if (priority > 0) {
                    payload.put("priority", priority);
                }
            }
            payload.put("project_name", task_project_name.getText().toString());
            payload.put("user_id", USER_ID);
            payload.put("status", statusSpinner.getSelectedItem().toString().toLowerCase());
            payload.put("action", actionSpinner.getSelectedItem().toString().toLowerCase());
            payload.put("action_data", action_text.getText().toString());
            payload.put("description", task_desc.getText().toString());
            payload.put("comments", comments.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return payload;
    }

    private void createTask() {
//        progressDialog = new ProgressDialog(NewTaskActivity.this,
//                R.style.AppTheme_Dark_Dialog);
//        progressDialog.setCanceledOnTouchOutside(false);
//        progressDialog.setIndeterminate(true);
//        progressDialog.setMessage(PROCESSING_MESSAGE);
//        progressDialog.show();
        showProgress();
        JSONObject postData = new JSONObject();
        try {
            if (TASK_ID != null) {
                postData.put("action", "update");
                postData.put("user_task_id", TASK_ID);
            } else {
                postData.put("action", "add");
            }
            postData.put("payload", requestData());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        LogUtils.Print("postData", postData.toString());
        NetworkManager.customJsonObjectRequest(
                NewTaskActivity.this, Constants.TASK_LIST_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideProgress();
//                        progressDialog.dismiss();
                        processResult(response);
                    }

                    @Override
                    public void onError(String message) {
                        hideProgress();
//                        progressDialog.dismiss();
                        LogUtils.Print("EVENT ERROR", message);
                    }
                }, true);
    }

    private void processResult(JSONObject response) {
        try {
            if (Integer.parseInt(response.getString("result")) == 1) {
                if (TASK_ID != null && !TASK_ID.equals("")) {
                    Util.onMessage(getApplicationContext(), getResources().getString(R.string.task_update_success));
                } else {
                    Util.onMessage(getApplicationContext(), getResources().getString(R.string.task_created_successfully));
                }
                Intent intent = new Intent(this, TaskActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            } else {
                Util.onMessage(getApplicationContext(), getResources().getString(R.string.task_update_fail));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getTasksById(String task_id) {
        disableFields();
        JSONObject postData = new JSONObject();
        try {
            postData.put("action", "get");
            postData.put("user_task_id", task_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        NetworkManager.customJsonObjectRequest(
                NewTaskActivity.this, Constants.TASK_LIST_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        enableFields();
                        processData(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ERRROR=", message);
                    }
                }, true);
    }

    private void processData(JSONObject data) {
        try {
            Task task = null;
            if (Integer.parseInt(data.getString("result")) == 1) {
                JSONObject result_Data = data.getJSONObject("data");
                JSONArray tasks = result_Data.getJSONArray("tasks");
                for (int i = 0; i < tasks.length(); i++) {
                    JSONObject obj = tasks.getJSONObject(i);
                    task = new Task();
                    if (obj.has("user_task_id")) {
                        task.setTaskId(obj.getString("user_task_id"));
                    }
                    if (obj.has("name")) {
                        task.setName(obj.getString("name"));
                    }
                    if (obj.has("status")) {
                        task.setStatus(obj.getString("status"));
                    }
                    if (obj.has("subject")) {
                        task.setSubjectId(obj.getString("subject"));
                    }
                    if (obj.has("subject_info")) {
                        JSONObject subjectInfo = obj.getJSONObject("subject_info");
                        task.setSubjectName(subjectInfo.getString("name"));
                    }
                    if (obj.has("subject_type")) {
                        task.setType(obj.getString("subject_type"));
                    }
                    if (obj.has("due_date")) {
                        task.setDueDate(obj.getLong("due_date"));
                    }
                    if (obj.has("reminder_days")) {
                        task.setReminderDays(obj.getString("reminder_days"));
                    }
                    if (obj.has("user_id")) {
                        task.setUserId(obj.getString("user_id"));
                    }
                    if (obj.has("cal_html_link")) {
                        task.setLink(obj.getString("cal_html_link"));
                    }
                    if (obj.has("action")) {
                        task.setAction(obj.getString("action"));
                    }
                    if (obj.has("action_data")) {
                        task.setAction_data(obj.getString("action_data"));
                    }
                    if (obj.has("description")) {
                        task.setDescription(obj.getString("description"));
                    }
                    if (obj.has("priority")) {
                        task.setPriority(obj.getInt("priority"));
                    }
                    if (obj.has("project_name")) {
                        task.setProjectName(obj.getString("project_name"));
                    }
                }
                if (task != null) {
                    updateView(task);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void updateView(Task task) {
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String due_date = simpleDateFormat.format(new Date(Long.parseLong(task.getDueDate().toString()) * 1000));

        dueDate.setText(due_date);
        task_name.setText(task.getName());
        subjectTypeSpinner.setSelection(subjectTypeAdapter.getPosition(task.getType()));
        statusSpinner.setSelection(statusAdapter.getPosition(task.getStatus()));
        actionSpinner.setSelection(actionAdapter.getPosition(Util.capitalize(task.getAction().toLowerCase())));
        action_text.setText(task.getAction_data());
        Spanned result = Html.fromHtml(task.getDescription(), HtmlCompat.FROM_HTML_MODE_LEGACY);
        task_desc.setText(result);
        task_project_name.setText(task.getProjectName());
        if (task.getPriority() == 1) {
            prioritySpinner.setSelection(0);
        } else if (task.getPriority() == 2) {
            prioritySpinner.setSelection(1);
        } else if (task.getPriority() == 3) {
            prioritySpinner.setSelection(2);
        }

        if (getResources().getString(R.string.lead).equalsIgnoreCase(task.getType())) {
            getLeads(task);
        } else if (getResources().getString(R.string.contact).equalsIgnoreCase(task.getType())) {
            getContacts(task);
        } else if (getResources().getString(R.string.communities).equalsIgnoreCase(task.getType())) {
            getCommunities(task);
        } else if (getResources().getString(R.string.account).equalsIgnoreCase(task.getType())) {
            getAccounts(task);
        } else if (getResources().getString(R.string.opportunity).equalsIgnoreCase(task.getType())) {
            getOpportunities(task);
        }
        getConnections(task);
        SUBJECT_ID = task.getSubjectId();
        USER_ID = task.getUserId();
    }

    private void onDelete() {
        new AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.delete))
                .setMessage(getResources().getString(R.string.delete_confirmation_msg))
                .setIcon(android.R.drawable.ic_menu_delete)
                .setPositiveButton(getResources().getString(R.string.yes), (dialog, whichButton) -> deleteTask(TASK_ID))
                .setNegativeButton(getResources().getString(R.string.no), null).show();
    }

    private void deleteTask(String task_id) {
        JSONObject postData = new JSONObject();
        try {
            postData.put("action", "delete");
            postData.put("user_task_id", task_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        NetworkManager.customJsonObjectRequest(
                NewTaskActivity.this, Constants.TASK_LIST_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processDeleteResult(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ERRROR=", message);
                    }
                }, true);
    }

    private void processDeleteResult(JSONObject response) {
        try {
            if (Integer.parseInt(response.getString("result")) == 1) {
                Util.onMessage(getApplicationContext(), getResources().getString(R.string.task_delete_success));
                Intent intent = new Intent(this, TaskActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            } else {
                Util.onMessage(getApplicationContext(), getResources().getString(R.string.task_delete_fail));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void disableFields() {
        dueDate.setEnabled(false);
        task_name.setEnabled(false);
        subjectTypeSpinner.setEnabled(false);
        statusSpinner.setEnabled(false);
        subjectsChip.setEnabled(false);
        connectionsChip.setEnabled(false);
        actionSpinner.setEnabled(false);
        action_text.setEnabled(false);
    }

    private void enableFields() {
        dueDate.setEnabled(true);
        task_name.setEnabled(true);
        subjectsChip.setEnabled(true);
        subjectTypeSpinner.setEnabled(true);
        actionSpinner.setEnabled(true);
        action_text.setEnabled(true);
        connectionsChip.setEnabled(true);
        statusSpinner.setEnabled(true);
    }
}