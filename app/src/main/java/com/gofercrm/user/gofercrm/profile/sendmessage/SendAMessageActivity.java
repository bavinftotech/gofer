package com.gofercrm.user.gofercrm.profile.sendmessage;

import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.databinding.DialogSendMessageBinding;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.util.ValidationUtil;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;

import org.json.JSONException;
import org.json.JSONObject;

public class SendAMessageActivity extends BaseActivity {
    private static final String TAG = "SendAMessageActivity";
    private DialogSendMessageBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.dialog_send_message);
        binding.setActivity(this);
    }

    public void onViewClick(View view) {
        if (view == binding.btnSendMessage) {
            if (isValidInput()) {
                submitContactUs();
            }
        }
    }

    private void submitContactUs() {
        String param = "?to_id=" + getIntent().getStringExtra("_ID") +
                "&text=" + binding.etMessage.getText().toString().trim() +
                "&send_email=true";
        String url = Constants.SEND_CONNECTION_MESSAGE + param;
        LogUtils.Print("url", "" + url);
        showProgress();
        NetworkManager.customJsonObjectRequest(
                getApplicationContext(), url, null,
                new VolleyResponseListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        hideProgress();
                        LogUtils.Print(TAG, "response --> " + response);
                        try {
                            if (Integer.parseInt(response.getString("result")) == 1) {
                                onBackPressed();
                            } else {
                                Util.onMessage(SendAMessageActivity.this, getResources().getString(R.string.server_err));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String message) {
                        hideProgress();
                        LogUtils.Print("onError=", message);
                    }
                }, true);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Util.hideSoftKeyboard(binding.btnSendMessage);
    }

    /**
     * CHECK VALIDATION
     *
     * @return
     */
    private boolean isValidInput() {
        if (ValidationUtil.checkEmptyView(binding.etMessage, binding.tlMessage,
                getResources().getString(R.string.enter_message_empty))) {
            binding.etMessage.requestFocus();
            return false;
        } else {
            return true;
        }
    }
}