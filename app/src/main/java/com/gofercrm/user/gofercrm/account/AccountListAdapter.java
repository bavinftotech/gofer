package com.gofercrm.user.gofercrm.account;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.telephony.PhoneNumberUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.entity.Email;
import com.gofercrm.user.gofercrm.entity.Phone;
import com.gofercrm.user.gofercrm.util.Util;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.List;

public class AccountListAdapter extends RecyclerView.Adapter<AccountListAdapter.MyViewHolder> {

    private List<AccountList> accountLists;
    private Context ctx;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, email, phone, status, accountInitial;

        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.name);
            email = view.findViewById(R.id.email);
            phone = view.findViewById(R.id.phone);
            status = view.findViewById(R.id.status);
            accountInitial = view.findViewById(R.id.leadInitial);
        }
    }

    public AccountListAdapter(List<AccountList> accountList, Context ctx) {
        setHasStableIds(true);
        this.accountLists = accountList;
        this.ctx = ctx;
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lead_list_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        AccountList account = accountLists.get(position);
        holder.title.setText(!account.getName().isEmpty() ? Util.capitalize(account.getTitle()) : "");
        holder.accountInitial.setText(Util.initialChar(account.getTitle()));
//        holder.status.setText(!account.getStatus().isEmpty() ? Util.capitalize(account.getStatus()) : "");
//        if (account .getStatus().equals("open")) {
//            holder.status.setTextColor(ContextCompat.getColor(ctx, R.color.open));
//        } else if (account.getStatus().equals("attempted")) {
//            holder.status.setTextColor(ContextCompat.getColor(ctx, R.color.attempted));
//        } else if (account.getStatus().equals("contacted")) {
//            holder.status.setTextColor(ContextCompat.getColor(ctx, R.color.contacted));
//        } else if (account.getStatus().equals("meeting-setup")) {
//            holder.status.setTextColor(ContextCompat.getColor(ctx, R.color.meetingsetup));
//        } else if (account.getStatus().equals("disqualified")) {
//            holder.status.setTextColor(ContextCompat.getColor(ctx, R.color.disqualified));
//        }
//        else if (account .getStatus().equals("assigned")) {
//            holder.status.setTextColor(ContextCompat.getColor(ctx, R.color.assigned));
//        }

        if (account.getEmails().size() > 0) {
            Email email = account.getEmails().get(0);
            holder.email.setText(email.getEmail());
        } else {
            holder.email.setText("");
        }

        if (account.getPhones().size() > 0) {
            Phone phone = account.getPhones().get(0);
            holder.phone.setText(PhoneNumberUtils.formatNumber(phone.getPhone(), "US"));
        } else {
            holder.phone.setText("");
        }

        holder.itemView.setOnClickListener(view -> {
            Intent intent = new Intent(ctx, AccountViewActivity.class);
            intent.putExtra("ACCOUNT_ID", (Serializable) accountLists.get(position).getId());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            ctx.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return accountLists.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }
}