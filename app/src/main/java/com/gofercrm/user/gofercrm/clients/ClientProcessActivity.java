package com.gofercrm.user.gofercrm.clients;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;

import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ClientProcessActivity extends BaseActivity {
    //    ProgressDialog progressDialog;
    String CLIENT_ID = "";
    EditText displayName;
    EditText first_name;
    EditText last_name;
    EditText email;
    Spinner emailType;
    EditText phone, comments;
    Spinner phoneType;
    EditText address;
    Spinner addressType;
    Spinner status, lead_owner;
    Spinner fileas;
    Spinner source;
    Spinner relationship;
    List<String> phoneTypes = new ArrayList<>();
    List<String> emailTypes = new ArrayList<>();
    List<String> addressTypes = new ArrayList<>();
    List<String> statuses = new ArrayList<>();
    List<String> fileaslist = new ArrayList<>();
    List<String> sources = new ArrayList<>();
    List<String> relationships = new ArrayList<>();

    ArrayAdapter<String> emailAdapter;
    ArrayAdapter<String> phoneAdapter;
    ArrayAdapter<String> addressAdapter;
    ArrayAdapter<String> statusAdapter;
    ArrayAdapter<String> fileasAdapter;
    ArrayAdapter<String> sourceAdapter;
    ArrayAdapter<String> relationshipAdapter;
    String PROCESSING_MESSAGE;
    String SUCCESS_MESSAGE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lead_process);
        Toolbar toolbar = findViewById(R.id.toolbar);

        PROCESSING_MESSAGE = getResources().getString(R.string.saving_client);
        SUCCESS_MESSAGE = getResources().getString(R.string.save_client_success);
        displayName = findViewById(R.id.input_lead_displayName);
        first_name = findViewById(R.id.input_lead_firstName);
        last_name = findViewById(R.id.input_lead_lastName);
        email = findViewById(R.id.input_lead_email);
        comments = findViewById(R.id.input_lead_comments);
        emailType = findViewById(R.id.lead_email_type);
        phone = findViewById(R.id.input_lead_phone);
        phoneType = findViewById(R.id.lead_phone_type);
        address = findViewById(R.id.input_lead_address);
        addressType = findViewById(R.id.lead_address_type);
        status = findViewById(R.id.input_lead_status);
        fileas = findViewById(R.id.input_lead_fileas);
        source = findViewById(R.id.input_lead_source);
        relationship = findViewById(R.id.input_lead_relationship);
        lead_owner = findViewById(R.id.input_lead_owner);
        lead_owner.setVisibility(View.GONE);

        phoneTypes = new ArrayList<>();
        Collections.addAll(phoneTypes, getResources().getStringArray(R.array.arrPhoneType));
        phoneAdapter = new ArrayAdapter<>(
                this, R.layout.spinner_selected, phoneTypes);
        phoneAdapter.setDropDownViewResource(R.layout.spinner_drop_dwon);
        phoneType.setAdapter(phoneAdapter);

        emailTypes = new ArrayList<>();
        emailTypes.add(getResources().getString(R.string.personal));
        emailTypes.add(getResources().getString(R.string.work));
        emailTypes.add(getResources().getString(R.string.other));
        emailAdapter = new ArrayAdapter<>(
                this, R.layout.spinner_selected, emailTypes);
        emailAdapter.setDropDownViewResource(R.layout.spinner_drop_dwon);
        emailType.setAdapter(emailAdapter);

        addressTypes = new ArrayList<>();
        addressTypes.add(getResources().getString(R.string.home));
        addressTypes.add(getResources().getString(R.string.work));
        addressTypes.add(getResources().getString(R.string.other));
        addressAdapter = new ArrayAdapter<>(
                this, R.layout.spinner_selected, addressTypes);
        addressAdapter.setDropDownViewResource(R.layout.spinner_drop_dwon);
        addressType.setAdapter(addressAdapter);

        statuses = new ArrayList<>();
        statuses.addAll(Arrays.asList(getResources().getStringArray(R.array.arrClientStatus)));
        statusAdapter = new ArrayAdapter<>(
                this, R.layout.spinner_selected, statuses);
        statusAdapter.setDropDownViewResource(R.layout.spinner_drop_dwon);
        status.setAdapter(statusAdapter);

        fileaslist = new ArrayList<>();
        fileaslist.add(getResources().getString(R.string.immediate_followup));
        fileaslist.add(getResources().getString(R.string.followup_next_month));
        fileaslist.add(getResources().getString(R.string.followup_lease_expiry));
        fileaslist.add(getResources().getString(R.string.unresponsive));

        fileasAdapter = new ArrayAdapter<>(
                this, R.layout.spinner_selected, fileaslist);
        fileasAdapter.setDropDownViewResource(R.layout.spinner_drop_dwon);
        fileas.setAdapter(fileasAdapter);

        sources = new ArrayList<>();
        sources.addAll(Arrays.asList(getResources().getStringArray(R.array.arrSource)));
        sourceAdapter = new ArrayAdapter<>(
                this, R.layout.spinner_selected, sources);
        sourceAdapter.setDropDownViewResource(R.layout.spinner_drop_dwon);
        source.setAdapter(sourceAdapter);

        relationships = new ArrayList<>();
        relationships.addAll(Arrays.asList(getResources().getStringArray(R.array.arrRelationships)));
        relationshipAdapter = new ArrayAdapter<>(
                this, R.layout.spinner_selected, relationships);
        relationshipAdapter.setDropDownViewResource(R.layout.spinner_drop_dwon);
        relationship.setAdapter(relationshipAdapter);

        Intent intent = getIntent();
        CLIENT_ID = intent.getStringExtra("CLIENT_ID");
        if (CLIENT_ID != null) {
            toolbar.setTitle(getResources().getString(R.string.edit_client));
            PROCESSING_MESSAGE = getResources().getString(R.string.updating_client);
            SUCCESS_MESSAGE = getResources().getString(R.string.client_updated_success);
            getLeadById(CLIENT_ID);
        } else {
            toolbar.setTitle(getResources().getString(R.string.create_client));
        }
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_lead_process, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_save) {
            if (!validate()) {
                onMessage(getResources().getString(R.string.save_failed));
            } else {
                saveClient();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private JSONObject getData() {
        JSONObject payload = new JSONObject();
        try {
            payload.put("name", displayName.getText().toString());
            payload.put("first_name", first_name.getText().toString());
            payload.put("last_name", last_name.getText().toString());

            JSONArray emailArray = new JSONArray();
            JSONObject emailObject = new JSONObject();
            emailObject.put("email", email.getText().toString());
            emailObject.put("type", emailType.getSelectedItem().toString());
            emailArray.put(emailObject);
            payload.put("email_list", emailArray);

            JSONArray phoneArray = new JSONArray();
            JSONObject phoneObject = new JSONObject();
            phoneObject.put("phone", phone.getText().toString());
            phoneObject.put("type", phoneType.getSelectedItem().toString());
            phoneArray.put(phoneObject);
            payload.put("phone_list", phoneArray);

            JSONArray addressArray = new JSONArray();
            JSONObject addressObject = new JSONObject();
            addressObject.put("address", address.getText().toString());
            addressObject.put("type", addressType.getSelectedItem().toString());
            addressArray.put(addressObject);
            payload.put("address_list", addressArray);

            payload.put("status", status.getSelectedItem().toString());
//            payload.put("file_as", fileas.getSelectedItem().toString());
            payload.put("source", source.getSelectedItem().toString());
            payload.put("relationship", relationship.getSelectedItem().toString());
            payload.put("comments", comments.getText());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return payload;
    }

    public void onMessage(String text) {
        Toast.makeText(getBaseContext(), text, Toast.LENGTH_LONG).show();
    }

    private void saveClient() {
//        progressDialog = new ProgressDialog(ClientProcessActivity.this,
//                R.style.AppTheme_Dark_Dialog);
//        progressDialog.setCanceledOnTouchOutside(false);
//        progressDialog.setIndeterminate(true);
//        progressDialog.setMessage(PROCESSING_MESSAGE);
//        progressDialog.show();
        showProgress();
        JSONObject postData = new JSONObject();
        try {
            if (CLIENT_ID != null) {
                postData.put("action", "update");
                postData.put("contact_id", CLIENT_ID);
            } else {
                postData.put("action", "add");
            }
            postData.put("payload", getData());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        NetworkManager.customJsonObjectRequest(
                ClientProcessActivity.this, Constants.MANAGE_CONTACT_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideProgress();
//                        progressDialog.dismiss();
                        processResult(response);
                    }

                    @Override
                    public void onError(String message) {
                        hideProgress();
//                        progressDialog.dismiss();

                        LogUtils.Print("CLIENT ERROR", message);
                    }
                }, true);
    }

    private void processResult(JSONObject response) {
        try {
            if (Integer.parseInt(response.getString("result")) == 1) {
                onMessage(SUCCESS_MESSAGE);
            } else {
                onMessage(getResources().getString(R.string.client_save_failed));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getLeadById(String client_id) {
        JSONObject postData = new JSONObject();
        try {
            postData.put("action", "get");
            postData.put("contact_id", client_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        NetworkManager.customJsonObjectRequest(
                ClientProcessActivity.this, Constants.MANAGE_CONTACT_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processData(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("CLIENT PROCESS ERROR", message);
                    }
                }, true);
    }

    private void processData(JSONObject data) {
        try {
            if (Integer.parseInt(data.getString("result")) == 1) {
                JSONObject result_Data = data.getJSONObject("data");
                JSONArray leads = result_Data.getJSONArray("contacts");
                for (int i = 0; i < leads.length(); i++) {

                    JSONObject obj = leads.getJSONObject(i);
                    JSONArray emailArray = obj.getJSONArray("email_list");
                    for (int j = 0; j < emailArray.length(); j++) {

                        JSONObject emailObj = emailArray.getJSONObject(j);
                        email.setText(emailObj.getString("email"));
                        emailType.setSelection(emailAdapter.getPosition(emailObj.getString("type")));
                    }
                    JSONArray phoneArray = obj.getJSONArray("phone_list");
                    for (int j = 0; j < phoneArray.length(); j++) {
                        JSONObject phoneObj = phoneArray.getJSONObject(j);
                        phone.setText(phoneObj.getString("phone"));
                        phoneType.setSelection(phoneAdapter.getPosition(phoneObj.getString("type")));
                    }
                    JSONArray addressArray = obj.getJSONArray("address_list");
                    for (int j = 0; j < addressArray.length(); j++) {
                        JSONObject addressObj = addressArray.getJSONObject(j);
                        address.setText(addressObj.getString("address"));
                        addressType.setSelection(addressAdapter.getPosition(addressObj.getString("type")));
                    }
//                    JSONObject lOObj = obj.getJSONObject("lead_owner");
//                    User user = new User(lOObj.getString("id"),
//                            lOObj.getString("name"));

                    displayName.setText(obj.get("name").toString());
                    first_name.setText(obj.get("first_name").toString());
                    last_name.setText(obj.get("last_name").toString());
                    source.setSelection(sourceAdapter.getPosition(obj.getString("source")));
                    status.setSelection(statusAdapter.getPosition(obj.getString("status")));
                    relationship.setSelection(relationshipAdapter.getPosition(obj.getString("relationship")));
                    fileas.setSelection(fileasAdapter.getPosition(obj.getString("file_as")));
                    comments.setVisibility(View.GONE);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public boolean validate() {
        boolean valid = true;
        String leadEmail = email.getText().toString();
        String leadName = displayName.getText().toString();
        String leadFirstName = first_name.getText().toString();
        String leadLastName = last_name.getText().toString();
        String leadPhone = phone.getText().toString();

        if (leadEmail.isEmpty() && leadPhone.isEmpty()) {
            onMessage(getResources().getString(R.string.email_phone_empty_err));
            valid = false;
        }

        if (!leadEmail.isEmpty() && !android.util.Patterns.EMAIL_ADDRESS.matcher(leadEmail).matches()) {
            email.setError(getResources().getString(R.string.email_invalid));
            valid = false;
        } else {
            email.setError(null);
        }

        if (leadName.isEmpty()) {
            displayName.setError(getResources().getString(R.string.name_empty_err));
            valid = false;
        } else {
            displayName.setError(null);
        }
        if (leadFirstName.isEmpty()) {
            first_name.setError(getResources().getString(R.string.first_name_empty_err));
            valid = false;
        } else {
            first_name.setError(null);
        }
        if (leadLastName.isEmpty()) {
            last_name.setError(getResources().getString(R.string.last_name_empty_err));
            valid = false;
        } else {
            last_name.setError(null);
        }
        return valid;
    }
}