package com.gofercrm.user.gofercrm.comments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Group;

import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.entity.Comment;
import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.util.FileUtil;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.ScalingUtilities;
import com.gofercrm.user.gofercrm.util.Util;
import com.jaiselrahman.filepicker.activity.FilePickerActivity;
import com.jaiselrahman.filepicker.config.Configurations;
import com.jaiselrahman.filepicker.model.MediaFile;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class CommentAttachmentActivity extends BaseActivity {

    private static final String TAG = "CommentAttachmentActivity";
    private static final int FILE_REQUEST_CODE = 228;
    private static final int READ_REQUEST_CODE = 229;
    private String token = "";

    private String strType = "", strId = "";

    private ProgressBar pbCircular;
    private Group groupProgress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment_attachment);
        getDataFromIntent();
        findViewById();
        openFilePickerDialog();
    }

    private void getDataFromIntent() {
        if (getIntent() != null) {
            strType = getIntent().getStringExtra("attachment_entity_type");
            strId = getIntent().getStringExtra("attachment_entity_id");
        }
    }

    private void findViewById() {
        SharedPreferences sharedPref = getApplicationContext()
                .getSharedPreferences("LOGIN", MODE_PRIVATE);
        token = sharedPref.getString("USER_TOKEN", "");

        ProgressBar progressBar = findViewById(R.id.progressBar);
        pbCircular = findViewById(R.id.pbCircular);
        groupProgress = findViewById(R.id.groupProgress);
        ConstraintLayout cl = findViewById(R.id.cl);
    }

    private void openFilePickerDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.add_a_file));
        builder.setItems(new CharSequence[]{
                        getResources().getString(R.string.camera_video),
                        getResources().getString(R.string.file)},
                (dialog, which) -> {
                    switch (which) {
                        case 0: {
                            //Camera & Video
                            dialog.cancel();
                            Intent intent = new Intent(this, FilePickerActivity.class);
                            intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
                                    .setCheckPermission(true)
                                    .setShowImages(true)
                                    .setShowVideos(true)
                                    .setShowAudios(true)
                                    .setShowFiles(true)
                                    .enableImageCapture(true)
                                    .enableVideoCapture(true)
                                    .setSingleChoiceMode(true)
                                    .build());
                            startActivityForResult(intent, FILE_REQUEST_CODE);
                        }
                        break;
                        case 1: {
                            //File
                            dialog.cancel();
                            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                            intent.addCategory(Intent.CATEGORY_OPENABLE);
                            intent.setType("*/*");
                            startActivityForResult(intent, READ_REQUEST_CODE);
                        }
                        break;
                    }
                });
        Dialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
//        dialog.setOnDismissListener(dialogInterface -> onBackPressed());
        dialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LogUtils.Print(TAG, "requestCode ->" + requestCode);
        LogUtils.Print(TAG, "resultCode ->" + resultCode);
        if (requestCode == FILE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                ArrayList<MediaFile> files;
                if (data != null) {
                    if (data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES) != null) {
                        files = data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES);
                        if (files == null || files.size() == 0) {
                            dismissCurrentScreen();
                            return;
                        }
                        Uri uri = files.get(0).getUri();
                        String mimeType = FileUtil.getMimeType(getApplicationContext(), uri);
                        String fileName = FileUtil.getFileName(getApplicationContext(), uri);
                        uploadAfterCompressImage(uri, fileName, mimeType);
                    } else {
                        dismissCurrentScreen();
                    }
                } else {
                    dismissCurrentScreen();
                }
            } else {
                dismissCurrentScreen();
            }
        } else if (requestCode == READ_REQUEST_CODE) {
            Uri uri = null;
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    uri = data.getData();
                    String mimeType = FileUtil.getMimeType(getApplicationContext(), uri);
                    String fileName = FileUtil.getFileName(getApplicationContext(), uri);
                    uploadAfterCompressImage(uri, fileName, mimeType);
                }
            }
        }
    }

    private void uploadAfterCompressImage(Uri uri, final String file_name, String file_type) {
        if (Util.isImageGreaterThan1MB(this, uri, file_name)) {
            LogUtils.Print(TAG, "path --> " + uri);
            compressImage(getRealPathFromURI(uri), uri, file_name, file_type);
        } else {
            uploadFile(uri, file_name, file_type);
        }
    }

    private String getRealPathFromURI(Uri uri) {
        String yourRealPath = "";
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            yourRealPath = cursor.getString(columnIndex);
        } else {
            yourRealPath = uri.toString();
        }
        cursor.close();
        return yourRealPath;
    }

    private int DESIREDWIDTH = 500;
    private int DESIREDHEIGHT = 500;

    private void compressImage(String path, Uri uri, String file_name, String file_type) {
        String strMyImagePath;
        Bitmap scaledBitmap;
        try {
            Bitmap unscaledBitmap = ScalingUtilities.decodeFile(path, DESIREDWIDTH, DESIREDHEIGHT, ScalingUtilities.ScalingLogic.FIT);
            if (!(unscaledBitmap.getWidth() <= DESIREDWIDTH && unscaledBitmap.getHeight() <= DESIREDHEIGHT)) {
                scaledBitmap = ScalingUtilities.createScaledBitmap(unscaledBitmap, DESIREDWIDTH, DESIREDHEIGHT, ScalingUtilities.ScalingLogic.FIT);
            } else {
                unscaledBitmap.recycle();
                uploadFile(uri, file_name, file_type);
                return;
            }

            String extr = getExternalFilesDir(null).getAbsolutePath();
            File mFolder = new File(extr + "/TMMFOLDER");
            if (!mFolder.exists()) {
                mFolder.mkdirs();
            }
            File f = new File(mFolder.getAbsolutePath(), "tmp.png");
            strMyImagePath = f.getAbsolutePath();
            FileOutputStream fos;
            try {
                fos = new FileOutputStream(f);
                scaledBitmap.compress(Bitmap.CompressFormat.PNG, 75, fos);
                fos.flush();
                fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            scaledBitmap.recycle();
        } catch (Throwable e) {
            e.getMessage();
            uploadFile(uri, file_name, file_type);
            return;
        }
        if (strMyImagePath == null) {
            uploadFile(uri, file_name, file_type);
            return;
        }
        LogUtils.Print(TAG, "path --> " + path);
        uploadFile(Uri.fromFile(new File(strMyImagePath)), file_name, file_type);
    }

    private void uploadFile(Uri uri, String file_name, String file_type) {
        OkHttpClient httpClient = new OkHttpClient();
        RequestBody requestBody;
        try {
            File file = FileUtil.from(CommentAttachmentActivity.this, uri);
            requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("filename", file_name)
                    .addFormDataPart("attachment_entity_type", strType)
                    .addFormDataPart("attachment_entity_id", strId)
                    .addFormDataPart("attachment", file_name, RequestBody.create(MediaType.parse(file_type), file)).build();
        } catch (Exception e) {
            e.printStackTrace();
            Util.onMessage(this, getResources().getString(R.string.file_invalid_err));
            dismissCurrentScreen();
            return;
        }
        Request request = new Request.Builder()
                .url(Constants.UPLOAD_ATTACHMENT_URL)
                .addHeader("Authorization", "Bearer " + token)
                .post(requestBody)
                .build();
        showProgress();
        httpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                hideProgress();
                e.printStackTrace();
                Util.onMessage(CommentAttachmentActivity.this,
                        getResources().getString(R.string.server_err));
                dismissCurrentScreen();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                hideProgress();
                ResponseBody responseBody = response.body();
                if (!response.isSuccessful()) {
                    throw new IOException("Error response " + response);
                }
                processResult(responseBody.string());
            }
        });
    }

    private void processResult(String strData) {
        try {
            JSONObject response = new JSONObject(strData);
            if (Integer.parseInt(response.getString("result")) == 1) {
                runOnUiThread(() -> Util.makeToast(getResources().getString(R.string.attachment_update_success)));
                Comment comment = new Comment(response.getString("comment"), response.getString("date"),
                        response.has("url") ? response.getString("url") : "",
                        response.has("content_type") ? response.getString("content_type") : "");
                Intent intent = new Intent();
                intent.putExtra("DATA", comment);
                setResult(RESULT_OK, intent);
                finish();
            } else {
                runOnUiThread(() -> Util.makeToast(getResources().getString(R.string.attachment_update_failed)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void dismissCurrentScreen() {
        pbCircular.setVisibility(View.GONE);
        groupProgress.setVisibility(View.GONE);
        setResult(RESULT_CANCELED);
        finish();
    }
}