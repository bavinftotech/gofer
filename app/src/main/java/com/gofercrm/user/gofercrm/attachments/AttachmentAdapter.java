package com.gofercrm.user.gofercrm.attachments;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.PopupMenu;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.ProgressDialog;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class AttachmentAdapter extends RecyclerView.Adapter<AttachmentAdapter.MyViewHolder> {

    private AttachmentActivity activity;
    private List<Attachment> attachmentList;

    public AttachmentAdapter(AttachmentActivity activity, List<Attachment> attachmentList) {
        this.activity = activity;
        this.attachmentList = attachmentList;
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.attachment_card, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Attachment attachment = attachmentList.get(position);
        holder.title.setText(attachment.getName());
        holder.desc.setText(attachment.getType());
        holder.size.setText(attachment.getSize());

        if (attachment.getType().contains("image")) {
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.file)
                    .error(R.drawable.file);


            Glide.with(activity).load(attachment.getURL()).apply(options).into(holder.thumbnail);

        } else {
            Glide.with(activity).load(attachment.getThumbnail()).into(holder.thumbnail);
            changeImageColor(attachment.getType(), holder.thumbnail);
//            holder.thumbnail.requestLayout();
//            holder.thumbnail.getLayoutParams().height = 300;
////            holder.thumbnail.getLayoutParams().width = 300;
        }
        holder.overflow.setOnClickListener(view -> showPopupMenu(position, holder.overflow, attachment));
    }

    /**
     * Showing popup menu when tapping on 3 dots
     */
    private void showPopupMenu(int position, View view, Attachment attachment) {
        // inflate menu
        PopupMenu popup = new PopupMenu(activity, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.attachment_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new MyMenuItemClickListener(position, attachment));
        popup.show();
    }

    @Override
    public int getItemCount() {
        return attachmentList.size();
    }

    private void changeImageColor(String type, ImageView image) {
        if (type == null) {
            return;
        }

        if (type.contains("pdf")) {
            image.setColorFilter(ContextCompat.getColor(activity, R.color.pdf));
        } else if (type.contains("msword")) {
            image.setColorFilter(ContextCompat.getColor(activity, R.color.msword));
        } else if (type.contains("audio")) {
            image.setColorFilter(ContextCompat.getColor(activity, R.color.colorPrimary));
        } else if (type.contains("video")) {
            image.setColorFilter(ContextCompat.getColor(activity, R.color.colorAccent));
        } else if (type.contains("text")) {
            image.setColorFilter(ContextCompat.getColor(activity, R.color.yellow));
        } else if (type.contains("csv")) {
            image.setColorFilter(ContextCompat.getColor(activity, R.color.csv));
        } else if (type.contains("xml")) {
            image.setColorFilter(ContextCompat.getColor(activity, R.color.xml));
        } else if (type.contains("excel")) {
            image.setColorFilter(ContextCompat.getColor(activity, R.color.excel));
        } else {
            image.setColorFilter(ContextCompat.getColor(activity, R.color.yellow));
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, desc, size;
        public ImageView thumbnail, overflow;

        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.title);
            desc = view.findViewById(R.id.count);
            size = view.findViewById(R.id.size);
            thumbnail = view.findViewById(R.id.thumbnail);
            overflow = view.findViewById(R.id.overflow);
        }
    }

    /**
     * Click listener for popup menu items
     */
    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {
        private static final int REQUEST_EXTERNAL_STORAGE = 1;
        Attachment attachment;
        int position;
        private String[] PERMISSIONS_STORAGE = {
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        };

        public MyMenuItemClickListener(int position, Attachment attachment) {
            this.attachment = attachment;
            this.position = position;
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.action_download:
                    int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    if (permission != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(
                                activity,
                                PERMISSIONS_STORAGE,
                                REQUEST_EXTERNAL_STORAGE
                        );
                    } else {
                        new DownloadFile(attachment.getName()).execute(attachment.getURL());
                    }
                    return true;
                case R.id.action_delete:
                    deleteAttachment(position, attachment.getId(), attachment.getEntity_type(), attachment.getEntity_id());
                    return true;
                case R.id.action_share: {
                    shareTextUrl(attachment.getName(), attachment.getURL());
                    return true;
                }
                case R.id.action_copy_url: {
                    Util.setClipboard(activity, attachment.getURL());
                }
                default:
            }
            return false;
        }

        private void shareTextUrl(String name, String text) {
            Intent share = new Intent(android.content.Intent.ACTION_SEND);
            share.setType("text/plain");
            share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
            share.putExtra(Intent.EXTRA_SUBJECT, activity.getResources().getString(R.string.share_url));
            share.putExtra(Intent.EXTRA_TEXT, text);
            share.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activity.startActivity(Intent.createChooser(share, activity.getResources().getString(R.string.share_link_)));
        }

        public void deleteAttachment(int position, String attachment_id, String entity_type, String entity_id) {
            JSONObject postData = new JSONObject();
            JSONArray ids = new JSONArray();
            try {
                ids.put(attachment_id);
                postData.put("action", "delete");
                postData.put("entity_type", entity_type);
                postData.put("entity_id", entity_id);
                postData.put("attachment_ids_list", ids);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            NetworkManager.customJsonObjectRequest(
                    activity, Constants.ATTACHMENT_LIST_URL, postData,
                    new VolleyResponseListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            LogUtils.Print("response=> ", response.toString());
                            processResult(position, response);
                        }

                        @Override
                        public void onError(String message) {
                            LogUtils.Print("ERRROR=", message);
                        }
                    }, true);
        }

        private void processResult(int position, JSONObject response) {
            try {
                if (Integer.parseInt(response.getString("att_deleted")) == 1) {
                    if (attachmentList.size() > position) {
                        activity.needToUpdate = true;
                        attachmentList.remove(position);
                        notifyItemRemoved(position);
                    }
                    Toast.makeText(activity,
                            activity.getResources().getString(R.string.file_got_deleted), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(activity,
                            activity.getResources().getString(R.string.error_deleting_file), Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private class DownloadFile extends AsyncTask<String, String, String> {
        //        private ProgressDialog progressDialog;
        private String fileName;
        private String folder;
        private boolean isDownloaded;

        DownloadFile(String fileName) {
            this.fileName = fileName;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            this.progressDialog = new ProgressDialog(activity);
//            progressDialog.setCanceledOnTouchOutside(false);
//            this.progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//            this.progressDialog.setCancelable(false);
//            this.progressDialog.show();
            ProgressDialog.getInstance().show(activity);
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection connection = url.openConnection();
                connection.connect();
                int lengthOfFile = connection.getContentLength();
                InputStream input = new BufferedInputStream(url.openStream(), 8192);
                String timestamp = new SimpleDateFormat("yyyy-MM-dd-HH-mm").format(new Date());
                fileName = timestamp + "_" + fileName;

                //Default Download directory
                folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();

                OutputStream output = new FileOutputStream(folder + "/" + fileName);

                byte[] data = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    publishProgress("" + (int) ((total * 100) / lengthOfFile));
                    output.write(data, 0, count);
                }
                output.flush();
                output.close();
                input.close();
                return activity.getResources().getString(R.string.downloaded_at_) + " " + folder + "/" + fileName;
            } catch (Exception e) {
                LogUtils.Print("Error: ", e.getMessage());
            }
            return activity.getResources().getString(R.string.something_went_wrong);
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
//            progressDialog.setProgress(Integer.parseInt(progress[0]));
            ProgressDialog.getInstance().dismiss();
        }

        @Override
        protected void onPostExecute(String message) {
//            this.progressDialog.dismiss();
            ProgressDialog.getInstance().dismiss();
            Toast.makeText(activity,
                    message, Toast.LENGTH_LONG).show();
        }
    }
}