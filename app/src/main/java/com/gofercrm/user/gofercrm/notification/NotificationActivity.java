package com.gofercrm.user.gofercrm.notification;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.chat.video.single.VideoChatViewActivity;
import com.gofercrm.user.gofercrm.databinding.ActivityNotificationBinding;
import com.gofercrm.user.gofercrm.entity.Comment;
import com.gofercrm.user.gofercrm.implementor.DialogButtonClickListener;
import com.gofercrm.user.gofercrm.implementor.RecyclerViewClickListener;
import com.gofercrm.user.gofercrm.notification.adapter.NotificationAdapter;
import com.gofercrm.user.gofercrm.notification.model.DataNotification;
import com.gofercrm.user.gofercrm.task.Task;
import com.gofercrm.user.gofercrm.task.view.TaskViewActivity;
import com.gofercrm.user.gofercrm.util.DialogUtils;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.SharedPreferenceData;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class NotificationActivity extends BaseActivity implements RecyclerViewClickListener {

    private static final String TAG = "NotificationActivity";

    private ActivityNotificationBinding binding;
    private final List<DataNotification> list = new ArrayList<>();
    private NotificationAdapter adapter;
    private SharedPreferences sharedPref;
    private boolean needToRefreshCount = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_notification);
        binding.setActivity(this);
        setUpHeaderView();
        getNotificationList();
    }

    private void setUpHeaderView() {
        sharedPref = getSharedPreferences("LOGIN", MODE_PRIVATE);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.notification));
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        adapter = new NotificationAdapter(list, this);
        adapter.setOnRecyclerViewItemClickListener(this);
        binding.rv.setAdapter(adapter);
    }

    private void getNotificationList() {
        showProgressBar(true);
        NetworkManager.customJsonObjectRequest(
                this, Constants.NOTIFICATION_API, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showProgressBar(false);
                        LogUtils.Print(TAG, "onResponse --> " + response);
                        try {
                            if (list.size() > 0)
                                list.clear();
                            if (response.has("notification_payload")) {
                                JSONObject jsonObject = response.getJSONObject("notification_payload");
                                if (jsonObject.has("video_call_logs")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("video_call_logs");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject object = jsonArray.getJSONObject(i);
                                        DataNotification dataNotification = new DataNotification();
                                        dataNotification.setType(DataNotification.notificationVideoCall);
                                        if (object.has("from_userid")) {
                                            String strUserId = object.getString("from_userid");
                                            if (!strUserId.equals(sharedPref.getString("USER_ID", ""))) {
                                                dataNotification.setUserId(object.getString("from_userid"));
                                                if (object.has("from_username")) {
                                                    dataNotification.setName(object.getString("from_username"));
                                                }
                                                if (object.has("to_username")) {
                                                    dataNotification.setToName(object.getString("to_username"));
                                                }
                                                if (object.has("ts")) {
                                                    dataNotification.setTime(object.getLong("ts"));
                                                }
                                                if (object.has("from_user_avatar")) {
                                                    dataNotification.setImage(object.getString("from_user_avatar"));
                                                }
                                                list.add(dataNotification);
                                            }
                                        }
                                    }
                                }

                                if (jsonObject.has("outstanding_connection_req_list")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("outstanding_connection_req_list");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject object = jsonArray.getJSONObject(i);
                                        DataNotification dataNotification = new DataNotification();
                                        dataNotification.setType(DataNotification.notificationConnectionRequest);
                                        if (object.has("id")) {
                                            dataNotification.setUserId(object.getString("id"));
                                        }
                                        if (object.has("friend")) {
                                            dataNotification.setFriend(object.getInt("friend"));
                                        }
                                        if (object.has("name")) {
                                            dataNotification.setName(object.getString("name"));
                                        }
                                        if (object.has("ts")) {
                                            dataNotification.setTime(object.getLong("ts"));
                                        }
                                        if (object.has("avatar_url")) {
                                            dataNotification.setImage(object.getString("avatar_url"));
                                        }
                                        list.add(dataNotification);
                                    }
                                }

                                if (jsonObject.has("past_due_task_dict")) {
                                    JSONObject jsonObjectPast = jsonObject.getJSONObject("past_due_task_dict");
                                    if (jsonObjectPast.has("tasks")) {
                                        JSONArray jsonArray = jsonObjectPast.getJSONArray("tasks");
                                        if (jsonArray.length() > 0) {
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                JSONObject object = jsonArray.getJSONObject(i);
                                                DataNotification dataNotification = new DataNotification();
                                                dataNotification.setType(DataNotification.notificationPastTask);
                                                dataNotification.setJsonObject(object);
                                                if (object.has("name")) {
                                                    dataNotification.setName(object.getString("name"));
                                                }
                                                list.add(dataNotification);
                                            }
                                        }
                                    }
                                }

                                if (jsonObject.has("upcoming_task_dict")) {
                                    JSONObject jsonObjectUpcoming = jsonObject.getJSONObject("upcoming_task_dict");
                                    if (jsonObjectUpcoming.has("tasks")) {
                                        JSONArray jsonArray = jsonObjectUpcoming.getJSONArray("tasks");
                                        if (jsonArray.length() > 0) {
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                JSONObject object = jsonArray.getJSONObject(i);
                                                DataNotification dataNotification = new DataNotification();
                                                dataNotification.setType(DataNotification.notificationUpcomingTask);
                                                dataNotification.setJsonObject(object);
                                                if (object.has("name")) {
                                                    dataNotification.setName(object.getString("name"));
                                                }
                                                list.add(dataNotification);
                                            }
                                        }
                                    }
                                }
                            }
                            if (list.size() == 0) {
                                binding.tvNoRecord.setVisibility(View.VISIBLE);
                            } else {
                                binding.tvNoRecord.setVisibility(View.GONE);
                            }
                            adapter.notifyDataSetChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String message) {
                        showProgressBar(false);
                        LogUtils.Print(TAG, "onError --> " + message);
                        Util.makeToast(message);
                    }
                }, false);
    }

    private void showProgressBar(boolean flag) {
        if (flag) {
            if (!binding.sl.isRefreshing())
                showProgress();
        } else {
            hideProgress();
            binding.sl.setRefreshing(false);
        }
    }

    /**
     * SET-UP PULL TO REFRESH
     */
    public void refreshList() {
        binding.sl.setRefreshing(true);
        if (list.size() > 0) {
            list.clear();
            adapter.notifyDataSetChanged();
        }
        getNotificationList();
    }

    @Override
    public void onItemClick(int flag, int position, View view) {
        if (flag == 0) {
            Intent intent = new Intent(this, VideoChatViewActivity.class);
            intent.putExtra("_NAME", list.get(position).getName());
            intent.putExtra("_USER_ID", SharedPreferenceData.getInstance(this).getLoggedInUserID());
            intent.putExtra("_LOGIN_USER_NAME", list.get(position).getToName());
            intent.putExtra("_OPO_USER_ID", list.get(position).getUserId());
            intent.putExtra("_CHANNEL_NAME", SharedPreferenceData.getInstance(this).getLoggedInUserID() + list.get(position).getUserId());
            intent.putExtra("_FROM", 1);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else if (flag == 1) {
            openConfirmationDialog(getResources().getString(R.string.allow_request_confirmation), 1, position);
        } else if (flag == 2) {
            switch (list.get(position).getFriend()) {
                case 1://Existing Friend
                    openConfirmationDialog(getResources().getString(R.string.disconnect_request_confirmation)
                            + " " + list.get(position).getName(), 0, position);
                    break;
                case 2://Waiting Response on your connection request
                    openConfirmationDialog(getResources().getString(R.string.withdraw_request_confirmation), 0, position);
                    break;
                case 3://Accept or Reject Incoming Request
                    openConfirmationDialog(getResources().getString(R.string.reject_request_confirmation), 0, position);
                    break;
            }
        } else if (flag == 3) {
            if (list.get(position).getType() == DataNotification.notificationPastTask ||
                    list.get(position).getType() == DataNotification.notificationUpcomingTask) {
                navigateToTaskDetailsScreen(list.get(position));
            }
        }
    }

    private void navigateToTaskDetailsScreen(DataNotification dataNotification) {
        JSONObject obj = dataNotification.getJsonObject();
        try {
            List<Comment> comments = new ArrayList();
            JSONArray commentsArray = new JSONArray();
            if (obj.has("comments")) {
                Object comments_obj;
                comments_obj = obj.get("comments");
                if (comments_obj instanceof JSONArray) {
                    commentsArray = (JSONArray) comments_obj;
                }
                for (int j = 0; j < commentsArray.length(); j++) {
                    JSONObject commentObj = commentsArray.getJSONObject(j);
                    comments.add(new Comment(commentObj.getString("comment"), commentObj.getString("date"),
                            commentObj.has("url") ? commentObj.getString("url") : "",
                            commentObj.has("content_type") ? commentObj.getString("content_type") : ""));
                }
            }

            Task task = new Task();
            task.setComments(comments);
            if (obj.has("user_task_id")) {
                task.setTaskId(obj.getString("user_task_id"));
            }
            if (obj.has("name")) {
                task.setName(obj.getString("name"));
                LogUtils.Print(TAG, "NAME =========>  " + obj.getString("name"));
            }
            if (obj.has("status")) {
                task.setStatus(obj.getString("status"));
            }
            if (obj.has("subject")) {
                task.setSubjectId(obj.getString("subject"));
            }
            if (obj.has("subject_info")) {
                JSONObject subjectInfo = obj.getJSONObject("subject_info");
                task.setSubjectName(subjectInfo.getString("name"));
            }
            if (obj.has("subject_type")) {
                task.setType(obj.getString("subject_type"));
            }
            if (obj.has("due_date")) {
                task.setDueDate(obj.getLong("due_date"));
            }
            if (obj.has("reminder_days")) {
                task.setReminderDays(obj.getString("reminder_days"));
            }
            if (obj.has("user_id")) {
                task.setUserId(obj.getString("user_id"));
            }
            if (obj.has("cal_html_link")) {
                task.setLink(obj.getString("cal_html_link"));
            }
            if (obj.has("action")) {
                task.setAction(obj.getString("action"));
            }
            if (obj.has("action_data")) {
                task.setAction_data(obj.getString("action_data"));
            }
            if (obj.has("description")) {
                task.setDescription(obj.getString("description"));
            }
            if (obj.has("priority")) {
                task.setPriority(obj.getInt("priority"));
            }

            TaskViewActivity.task = task;
            Intent intent = new Intent(this, TaskViewActivity.class);
            startActivity(intent);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void openConfirmationDialog(String strMsg, int type, int position) {
        DialogUtils.showConfirmationDialog(this,
                new DialogButtonClickListener() {
                    @Override
                    public void onPositiveButtonClick() {
                        removeAddUserFromList(type, position);
                    }

                    @Override
                    public void onNegativeButtonClick() {

                    }
                }, strMsg);
    }

    /**
     * @param type     1 add, 2 - remove
     * @param position
     */
    private void removeAddUserFromList(int type, int position) {
        NetworkManager.customJsonObjectRequest(this,
                ((type == 1) ? Constants.ADD_USER : Constants.REMOVE_USER) +
                        "?userid=" + list.get(position).getUserId(), null,
                new VolleyResponseListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtils.Print("RESPONSE", response.toString());
                        try {
                            if (response.has("result") && Integer.parseInt(response.getString("result")) == 1) {
                                needToRefreshCount = true;
                                list.remove(position);
                                adapter.notifyItemRemoved(position);
                                if (list.size() == 0) {
                                    binding.tvNoRecord.setVisibility(View.VISIBLE);
                                } else {
                                    binding.tvNoRecord.setVisibility(View.GONE);
                                }
                            } else {
                                Util.onMessage(NotificationActivity.this, getResources().getString(R.string.server_err));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Util.onMessage(NotificationActivity.this, getResources().getString(R.string.server_err));
                        }
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("UPDATE PROFILE ERROR", message);
                    }
                }, false);
    }

    @Override
    public void onBackPressed() {
        if (needToRefreshCount) {
            setResult(RESULT_OK);
            finish();
            return;
        }
        super.onBackPressed();
    }
}