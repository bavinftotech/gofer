package com.gofercrm.user.gofercrm;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;

import com.gofercrm.user.gofercrm.chat.video.group.openvcall.model.AGEventHandler;
import com.gofercrm.user.gofercrm.chat.video.group.openvcall.model.CurrentUserSettings;
import com.gofercrm.user.gofercrm.chat.video.group.openvcall.model.EngineConfig;
import com.gofercrm.user.gofercrm.chat.video.group.openvcall.model.MyEngineEventHandler;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.SharedPreferenceData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import io.agora.rtc.Constants;
import io.agora.rtc.RtcEngine;

public class ApplicationContext extends Application {

    public JSONObject getPersonal_dict() {
        String strDate = preferences.getString("personal_dict");
        try {
            return new JSONObject(strDate);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setPersonal_dict(JSONObject personal_dict) {
        preferences.putString("personal_dict", personal_dict.toString());
    }

    /**
     * VIDEO CALL
     */
    private CurrentUserSettings mVideoSettings = new CurrentUserSettings();

    private RtcEngine mRtcEngine;
    private EngineConfig mConfig;
    private MyEngineEventHandler mEventHandler;

    public RtcEngine rtcEngine() {
        return mRtcEngine;
    }

    public EngineConfig config() {
        return mConfig;
    }

    public CurrentUserSettings userSettings() {
        return mVideoSettings;
    }

    public void addEventHandler(AGEventHandler handler) {
        mEventHandler.addEventHandler(handler);
    }

    public void remoteEventHandler(AGEventHandler handler) {
        mEventHandler.removeEventHandler(handler);
    }

    public static Context ctx;

    public static String key_lang = "language";
    public static String language_english = "en";
    public static String language_spanish = "es";
    public static String language_french = "fr";
    public static String language_turkish = "tr";
    public static String language_russian = "ru";
    public static String language_armenian = "hy";

    public static SharedPreferenceData preferences;

    @Override
    public void onCreate() {
        super.onCreate();
        ctx = this;
        preferences = SharedPreferenceData.getInstance(ctx);
        createRtcEngine();
        initializeLanguage();
    }

    private void createRtcEngine() {
        Context context = getApplicationContext();
        String appId = context.getString(R.string.agora_app_id);
        if (TextUtils.isEmpty(appId)) {
            throw new RuntimeException("NEED TO use your App ID, get your own ID at https://dashboard.agora.io/");
        }

        mEventHandler = new MyEngineEventHandler();
        try {
            mRtcEngine = RtcEngine.create(context, appId, mEventHandler);
        } catch (Exception e) {
            LogUtils.Print("TAG", "createRtcEngine: " + Log.getStackTraceString(e));
            throw new RuntimeException("NEED TO check rtc sdk init fatal error\n" + Log.getStackTraceString(e));
        }

        mRtcEngine.setChannelProfile(Constants.CHANNEL_PROFILE_COMMUNICATION);
        mRtcEngine.enableVideo();
        mRtcEngine.enableAudioVolumeIndication(200, 3, false);

        mConfig = new EngineConfig();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        initializeLanguage();
    }

    /**
     * SET APP LANGUAGE FIRST TIME (SET DEFAULT ENGLISH)
     */
    public void initializeLanguage() {
        LogUtils.Print("Default lan ", Locale.getDefault().getLanguage());
        if (!preferences.getString(key_lang).equals("")) {
            updateResources(ctx, preferences.getString(key_lang));
        } else {
            if (Locale.getDefault().getLanguage().equalsIgnoreCase(language_spanish)) {
                updateResources(ctx, language_spanish);
            } else if (Locale.getDefault().getLanguage().equalsIgnoreCase(language_french)) {
                updateResources(ctx, language_french);
            } else if (Locale.getDefault().getLanguage().equalsIgnoreCase(language_turkish)) {
                updateResources(ctx, language_turkish);
            } else if (Locale.getDefault().getLanguage().equalsIgnoreCase(language_russian)) {
                updateResources(ctx, language_russian);
            } else if (Locale.getDefault().getLanguage().equalsIgnoreCase(language_armenian)) {
                updateResources(ctx, language_armenian);
            } else {
                updateResources(ctx, language_english);
            }
        }
    }

    /**
     * SET LANGUAGE, UPDATE RESOURCES, CONFIGURATIONS
     *
     * @param language
     * @return
     */
    public static Context updateResources(Context mContext, String language) {
        String newLanguage;
        if (language == null || language.isEmpty())
            newLanguage = language_english;
        else
            newLanguage = language;
        preferences.putString(key_lang, newLanguage);
//        Locale locale = new Locale(newLanguage);
//        Locale.setDefault(locale);
//        Resources res = mContext.getResources();
//        Configuration config = new Configuration(res.getConfiguration());
//        config.setLocale(locale);
//        mContext = mContext.createConfigurationContext(config);
        ctx = mContext;
        return mContext;
    }
}
