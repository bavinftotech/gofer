package com.gofercrm.user.gofercrm.clients;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ContentProviderOperation;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.appointments.NewEventActivity;
import com.gofercrm.user.gofercrm.appointments.ViewAppointmentActivity;
import com.gofercrm.user.gofercrm.attachments.AttachmentActivity;
import com.gofercrm.user.gofercrm.entity.Address;
import com.gofercrm.user.gofercrm.entity.Comment;
import com.gofercrm.user.gofercrm.entity.Email;
import com.gofercrm.user.gofercrm.entity.MatchPreference;
import com.gofercrm.user.gofercrm.entity.Phone;
import com.gofercrm.user.gofercrm.lead.LeadOwnerBSFragment;
import com.gofercrm.user.gofercrm.lead.upload.UploadActivity;
import com.gofercrm.user.gofercrm.opportunityclientlead.list.OpportunityLeadClientActivity;
import com.gofercrm.user.gofercrm.task.NewTaskActivity;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ClientViewActivity extends BaseActivity {

    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;
    ArrayList<ClientView> clientPage = new ArrayList<>();
    ImageButton call_btn;
    ClientList client_obj = new ClientList();
    EditText commentText;
    List comments = new ArrayList<Comment>();
    String CLIENT_ID;
    ImageButton upgrade_to_contact_btn, assign_to_me_btn;
    TextView upgrade_to_contact_text, assign_to_me_text;
    JSONObject result_obj = new JSONObject();
    Switch lead_campaign;
    private ClientViewPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    String LOGIN_USER_ID = "";
    SharedPreferences sharedPref;
    private JSONArray arrAppointment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lead_view);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedPref = getSharedPreferences("LOGIN", MODE_PRIVATE);
        LOGIN_USER_ID = sharedPref.getString("USER_ID", "");

        upgrade_to_contact_btn = findViewById(R.id.upgrade_to_contact_btn);
        upgrade_to_contact_btn.setVisibility(View.GONE);

        upgrade_to_contact_text = findViewById(R.id.upgrade_to_contact_text);
        upgrade_to_contact_text.setVisibility(View.GONE);

        assign_to_me_btn = findViewById(R.id.assign_to_me_btn);
        assign_to_me_btn.setVisibility(View.GONE);

        assign_to_me_text = findViewById(R.id.assign_to_me_text);
        assign_to_me_text.setVisibility(View.GONE);

        lead_campaign = findViewById(R.id.lead_campaign);
        Intent intent = getIntent();
        CLIENT_ID = intent.getStringExtra("CLIENT_ID");
        if (CLIENT_ID != null) {
            getClientById(CLIENT_ID);
        }
    }

    private MenuItem menuCampaign;
    private MenuItem menuCall;
    private MenuItem menuSMS;
    private MenuItem menuEmail;
    private MenuItem menuAssignToMe;
    private MenuItem actionViewAppointment;

    @SuppressLint("RestrictedApi")
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (menu instanceof MenuBuilder) {
            ((MenuBuilder) menu).setOptionalIconsVisible(true);
        }
        getMenuInflater().inflate(R.menu.menu_client_view, menu);
        menuCampaign = menu.findItem(R.id.menuCampaign).setIcon(ContextCompat.getDrawable(this,
                client_obj.isCampaign() ? R.drawable.ic_baseline_toggle_on_24 : R.drawable.ic_baseline_toggle_off_24));
        menuAssignToMe = menu.findItem(R.id.menuAssignToMe);
        menuCall = menu.findItem(R.id.menuCall);
        menuSMS = menu.findItem(R.id.menuSMS);
        menuEmail = menu.findItem(R.id.menuEmail);
        menuEmail = menu.findItem(R.id.menuEmail);
        actionViewAppointment = menu.findItem(R.id.action_view_appointment);
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menuCampaign) {
            menuCampaign.setChecked(!menuCampaign.isChecked());
            menuCampaign.setIcon(ContextCompat.getDrawable(this,
                    menuCampaign.isChecked() ? R.drawable.ic_baseline_toggle_on_24 : R.drawable.ic_baseline_toggle_off_24));
//            invalidateOptionsMenu();
            putItOnCampaign(CLIENT_ID, menuCampaign.isChecked());
        } else if (id == R.id.menuCall) {
            if (client_obj.getPhones().size() > 0) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + client_obj.getPhones().get(0).getPhone()));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } else {
                if (item.getActionView() != null)
                    Snackbar.make(item.getActionView(), getResources().getString(R.string.no_phone_is_associated_contact), Snackbar.LENGTH_LONG)
                            .setAction(getResources().getString(R.string.action), null).show();
            }
        } else if (id == R.id.menuAssignToMe) {
            if (!client_obj.getId().equals(LOGIN_USER_ID)) {
                assignToMe(CLIENT_ID);
            }
        } else if (id == R.id.menuSMS) {
            if (client_obj.getPhones().size() > 0 && client_obj.getPhones().get(0).getPhone() != null) {
                try {
                    Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                    smsIntent.setType("vnd.android-dir/mms-sms");
                    smsIntent.putExtra("address", "" + client_obj.getPhones().get(0).getPhone());
                    smsIntent.putExtra("sms_body", "");
                    startActivity(smsIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                    Snackbar.make(item.getActionView(), getResources().getString(R.string.no_phone_is_associated_lead), Snackbar.LENGTH_LONG)
                            .setAction(getResources().getString(R.string.action), null).show();
                }
            } else {
                if (item.getActionView() != null)
                    Snackbar.make(item.getActionView(), getResources().getString(R.string.no_phone_is_associated_contact), Snackbar.LENGTH_LONG)
                            .setAction(getResources().getString(R.string.action), null).show();
            }
        } else if (id == R.id.menuEmail) {
            if (client_obj.getEmails().size() > 0 && client_obj.getEmails().get(0).getEmail() != null) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL, new String[]{client_obj.getEmails().get(0).getEmail()});
                i.putExtra(Intent.EXTRA_SUBJECT, "");
                i.putExtra(Intent.EXTRA_TEXT, "");
                try {
                    startActivity(Intent.createChooser(i, getResources().getString(R.string.send_mail_)));
                } catch (android.content.ActivityNotFoundException ex) {
                    Snackbar.make(item.getActionView(), getResources().getString(R.string.no_phone_is_associated_contact), Snackbar.LENGTH_LONG)
                            .setAction(getResources().getString(R.string.action), null).show();
                }
            } else {
                if (item.getActionView() != null)
                    Snackbar.make(item.getActionView(), getResources().getString(R.string.no_phone_is_associated_contact), Snackbar.LENGTH_LONG)
                            .setAction(getResources().getString(R.string.action), null).show();
            }
        } else if (id == R.id.action_upload) {
            Intent intent = new Intent(getApplicationContext(), UploadActivity.class);
            intent.putExtra("FROM", UploadActivity.fromClient);
            intent.putExtra("CLIENT_ID", CLIENT_ID);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else if (id == R.id.menuManageOpportunity) {
            Intent intent = new Intent(getApplicationContext(), OpportunityLeadClientActivity.class);
            ClientList clientList = new ClientList();
            clientList.setId(CLIENT_ID);
            clientList.setName(client_obj != null && client_obj.getName() != null ?
                    client_obj.getName() : "");
            intent.putExtra("DATA", clientList);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else if (id == R.id.action_new_meeting) {
            Intent intent = new Intent(getApplicationContext(), NewEventActivity.class);
            intent.putExtra("CLIENT_ID", CLIENT_ID);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else if (id == R.id.action_task) {
            Intent intent = new Intent(getApplicationContext(), NewTaskActivity.class);
            intent.putExtra("CLIENT_ID", CLIENT_ID);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else if (id == R.id.action_attachment) {
            Intent intent = new Intent(getApplicationContext(), AttachmentActivity.class);
            intent.putExtra("ENTITY_ID", CLIENT_ID);
            intent.putExtra("ENTITY_TYPE", "contact");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else if (id == R.id.action_import && result_obj != null) {

            new AlertDialog.Builder(this)
                    .setTitle(getResources().getString(R.string.import_))
                    .setMessage(getResources().getString(R.string.contact_add_address_book_confirmation))
                    .setIcon(R.drawable.account_outline)
                    .setPositiveButton(getResources().getString(R.string.yes), (dialog, whichButton) -> {
                        int hasWriteContactsPermission = checkSelfPermission(Manifest.permission.READ_CONTACTS);
                        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{Manifest.permission.WRITE_CONTACTS},
                                    REQUEST_CODE_ASK_PERMISSIONS);
                        } else {
                            importContact();
                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.no), null).show();
        } else if (id == R.id.menuChangeOwner) {
            LeadOwnerBSFragment leadOwnerBSFragment = new LeadOwnerBSFragment("CLIENT", client_obj.getId());
            leadOwnerBSFragment.setCurrentObj(leadOwnerBSFragment);
            leadOwnerBSFragment.show(getSupportFragmentManager(), leadOwnerBSFragment.getTag());
        } else if (id == R.id.action_view_appointment) {
            Intent intent = new Intent(this, ViewAppointmentActivity.class);
            intent.putExtra("DATA", arrAppointment.toString());
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    private void assignToMe(String clientId) {
        JSONObject postData = new JSONObject();
        JSONArray array = new JSONArray();
        array.put(clientId);
        try {
            postData.put("lead_ids_list", array);
            postData.put("self_assign", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        NetworkManager.customJsonObjectRequest(
                this, Constants.SELF_ASSIGN_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processAssignToMeResult(response, getResources().getString(R.string.client_assigned_to_you));
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("Client PROCESS ERROR", message);
                    }
                }, true);
    }

    private void processAssignToMeResult(JSONObject response, String message) {
        try {
            if (Integer.parseInt(response.getString("result")) == 1) {
                Util.onMessage(getApplicationContext(), message);
            } else {
                Util.onMessage(getApplicationContext(), getResources().getString(R.string.changes_failed));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NotNull String[] permissions,
                                           @NotNull int[] grantResults) {
        if (requestCode == REQUEST_CODE_ASK_PERMISSIONS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                importContact();
            } else {
                Util.onMessage(getApplicationContext(), getResources().getString(R.string.contact_access_denied));
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void importContact() {
        String email = "", address = "", phone = "", firstName = "", lastName = "", displayName = "";
        JSONArray emailArray = null;
        try {
            emailArray = result_obj.getJSONArray("email_list");

            for (int j = 0; j < emailArray.length(); j++) {
                JSONObject emailObj = emailArray.getJSONObject(j);
                email = emailObj.getString("email");
                if (email != null && !email.isEmpty()) {
                    break;
                }
            }
            JSONArray phoneArray = result_obj.getJSONArray("phone_list");
            for (int j = 0; j < phoneArray.length(); j++) {
                JSONObject phoneObj = phoneArray.getJSONObject(j);
                phone = phoneObj.getString("phone");
                if (phone != null && !phone.isEmpty()) {
                    break;
                }
            }
            JSONArray addressArray = result_obj.getJSONArray("address_list");
            for (int j = 0; j < addressArray.length(); j++) {
                JSONObject addressObj = addressArray.getJSONObject(j);
                address = addressObj.getString("address");
                if (address != null && !address.isEmpty()) {
                    break;
                }
            }
            if (result_obj.has("name")) {
                displayName = result_obj.getString("name");
            }
            if (result_obj.has("first_name")) {
                firstName = result_obj.getString("first_name");
            }
            if (result_obj.has("last_name")) {
                lastName = result_obj.getString("last_name");
            }
            writeContact(displayName, firstName, lastName, email, phone, address);
            Util.onMessage(getApplicationContext(), displayName + " " + getResources().getString(R.string.imported_successfully));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<ClientView> getData() {
        return clientPage;
    }

    public List<Comment> getComments() {
        comments = client_obj.getComments();
        return comments;
    }

    public void refreshData() {
        clientPage.clear();
        getClientById(CLIENT_ID);
    }

    private void getClientById(String client_id) {
        JSONObject postData = new JSONObject();
        try {
            postData.put("action", "get");
            postData.put("contact_id", client_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        NetworkManager.customJsonObjectRequest(
                ClientViewActivity.this, Constants.MANAGE_CONTACT_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processData(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("CLIENT PROCESS ERROR", message);
                    }
                }, true);
    }

    private void processResult(JSONObject response, String message) {
        try {
            if (Integer.parseInt(response.getString("result")) == 1) {
                Util.onMessage(getApplicationContext(), message);
            } else {
                Util.onMessage(getApplicationContext(), getResources().getString(R.string.changes_failed));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void putItOnCampaign(String contact_id, final boolean addToCampaign) {
        JSONObject postData = new JSONObject();
        try {
            postData.put("action", "update");
            postData.put("contact_id", contact_id);
            JSONObject payload = new JSONObject();
            payload.put("marketing_campaign_enabled", addToCampaign);
            postData.put("payload", payload);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        NetworkManager.customJsonObjectRequest(
                ClientViewActivity.this, Constants.MANAGE_CONTACT_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processResult(response, addToCampaign ? getResources().getString(R.string.campaign_add_success)
                                : getResources().getString(R.string.campaign_remove));
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("CLIENT PROCESS ERROR", message);
                    }
                }, true);
    }

    private void processData(JSONObject data) {
        try {
            if (Integer.parseInt(data.getString("result")) == 1) {

                JSONObject result_Data = data.getJSONObject("data");
                JSONArray leads = result_Data.getJSONArray("contacts");
                for (int i = 0; i < leads.length(); i++) {

                    result_obj = leads.getJSONObject(i);
                    JSONArray emailArray = result_obj.getJSONArray("email_list");
                    List<Email> emails = new ArrayList<>();
                    for (int j = 0; j < emailArray.length(); j++) {
                        JSONObject emailObj = emailArray.getJSONObject(j);
                        emails.add(new Email(emailObj.getString("email"), emailObj.getString("type")));
                    }
                    JSONArray phoneArray = result_obj.getJSONArray("phone_list");
                    List<Phone> phones = new ArrayList<>();
                    for (int j = 0; j < phoneArray.length(); j++) {
                        JSONObject phoneObj = phoneArray.getJSONObject(j);
                        phones.add(new Phone(phoneObj.getString("phone"), phoneObj.getString("type")));
                    }
                    JSONArray addressArray = result_obj.getJSONArray("address_list");
                    List<Address> addresses = new ArrayList();
                    for (int j = 0; j < addressArray.length(); j++) {
                        JSONObject addressObj = addressArray.getJSONObject(j);
                        addresses.add(new Address(addressObj.getString("address"), addressObj.getString("type")));
                    }
                    if (result_obj.has("appointment_list")) {
                        arrAppointment = result_obj.getJSONArray("appointment_list");
                    }

//                    JSONArray commentsArray = obj.getJSONArray("comments");
//                    List<Comment> comments = new ArrayList();
//
//                    for (int j = 0; j < commentsArray.length(); j++) {
//                        JSONObject commentObj = commentsArray.getJSONObject(j);
//                        comments.add(new Comment(commentObj.getString("comment"), commentObj.getString("date")));
//                    }

                    List<Comment> comments_data = new ArrayList();
                    if (result_obj.has("comments")) {
                        Object comment_instance = result_obj.get("comments");
                        if (comment_instance instanceof JSONArray) {
                            JSONArray commentsArray = result_obj.getJSONArray("comments");

                            for (int j = 0; j < commentsArray.length(); j++) {
                                JSONObject commentObj = commentsArray.getJSONObject(j);
                                comments_data.add(new Comment(commentObj.getString("comment"), commentObj.getString("date"),
                                        commentObj.has("url") ? commentObj.getString("url") : "",
                                        commentObj.has("content_type") ? commentObj.getString("content_type") : ""));
                            }
                        }
                    }

//                    JSONObject lOObj = obj.getJSONObject("lead_owner");
//                    LeadOwner leadOwner = new LeadOwner(lOObj.getString("id"),
//                            lOObj.getString("name"));


                    JSONObject mpObj = result_obj.getJSONObject("match_preferences_dict");
                    MatchPreference mp = new MatchPreference();
                    if (mpObj.has("lease_length")) {
                        mp.setLease_length(Integer.parseInt(mpObj.getString("lease_length")));
                    }
                    if (mpObj.has("bedrooms")) {
                        mp.setBeds(mpObj.getInt("bedrooms"));
                    }
                    if (mpObj.has("bedrooms")) {
                        mp.setBeds(mpObj.getInt("bedrooms"));
                    }
                    if (mpObj.has("bathrooms")) {
                        mp.setBathrooms(mpObj.getInt("bathrooms"));
                    }
                    if (mpObj.has("budget")) {
                        mp.setBudget(mpObj.getDouble("budget"));
                    }

                    client_obj = new ClientList(result_obj.getString("id"),
                            result_obj.getString("name"),
                            result_obj.getString("status"),
                            result_obj.getString("file_as"),
                            result_obj.getString("source"),
                            comments_data,
                            addresses,
                            emails,
                            phones,
                            result_obj.getString("name"),
                            mp,
                            result_obj.getBoolean("marketing_campaign_enabled"),
                            result_obj.getLong("created_ts"),
                            result_obj.getLong("status_update_date")
                    );
                    renderView();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void renderView() {
        actionViewAppointment.setVisible(arrAppointment != null && arrAppointment.length() > 0);

        ClientView cv;
        cv = new ClientView(getResources().getString(R.string.name), client_obj.getTitle(), "", R.drawable.ic_account, 0);
        clientPage.add(cv);
        for (int i = 0; i < client_obj.getEmails().size(); i++) {
            Email email = client_obj.getEmails().get(i);
            String et = getResources().getString(R.string.email);
            if (!email.getType().isEmpty()) {
                et = et + "(" + email.getType() + ")";
            }
            cv = new ClientView(et, email.getEmail(), "EMAIL", R.drawable.ic_email, 0);
            clientPage.add(cv);
        }

        for (int i = 0; i < client_obj.getPhones().size(); i++) {
            Phone phone = client_obj.getPhones().get(i);
            String pt = getResources().getString(R.string.phone);
            if (!phone.getType().isEmpty()) {
                pt = pt + "(" + phone.getType() + ")";
            }
            cv = new ClientView(pt, phone.getPhone(), "PHONE", R.drawable.ic_call, R.drawable.ic_message);
            clientPage.add(cv);
        }

        for (int i = 0; i < client_obj.getAddresses().size(); i++) {
            Address address = client_obj.getAddresses().get(i);
            String at = getResources().getString(R.string.address);
            if (!address.getType().isEmpty()) {
                at = at + "(" + address.getType() + ")";
            }
            cv = new ClientView(at, address.getAddress(), "ADDRESS", R.drawable.ic_location, R.drawable.ic_directions);
            clientPage.add(cv);
        }

//        cv = new AccountView("Lead Owner", client_obj.getLeadOwner().getName(), "", R.drawable.ic_account_circle, 0);
//        clientPage.add(cv);

        cv = new ClientView(getResources().getString(R.string.source), client_obj.getLeadSource(), "", 0, 0);
        clientPage.add(cv);
        cv = new ClientView(getResources().getString(R.string.file_as), client_obj.getFileAs(), "", 0, 0);
        clientPage.add(cv);

        cv = new ClientView(getResources().getString(R.string.created_on), Util.formatDate(client_obj.getCreated_on()), "", 0, 0);
        clientPage.add(cv);

        cv = new ClientView(getResources().getString(R.string.updated_on), Util.formatDate(client_obj.getUpdated_on()), "", 0, 0);
        clientPage.add(cv);

        cv = new ClientView(getResources().getString(R.string.lease_length), client_obj.getMatchPreference().getLease_length() != null ? client_obj.getMatchPreference().getLease_length() + "" : "NA", "", 0, 0);
        clientPage.add(cv);
        cv = new ClientView(getResources().getString(R.string.budget), client_obj.getMatchPreference().getBudget() != null ? +client_obj.getMatchPreference().getBudget() + "" : "NA", "", 0, 0);
        clientPage.add(cv);
        cv = new ClientView(getResources().getString(R.string.bedrooms), client_obj.getMatchPreference().getBeds() != null ? client_obj.getMatchPreference().getBeds() + "" : "NA", "", 0, 0);
        clientPage.add(cv);

        cv = new ClientView(getResources().getString(R.string.bathrooms), client_obj.getMatchPreference().getBathrooms() != null ? client_obj.getMatchPreference().getBathrooms() + "" : "NA", "", 0, 0);
        clientPage.add(cv);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(client_obj.getTitle());
        mSectionsPagerAdapter = new ClientViewPagerAdapter(getSupportFragmentManager());
        mViewPager = findViewById(R.id.leadView_viewPager_id);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        lead_campaign.setChecked(client_obj.isCampaign());
        if (menuCampaign != null) {
            menuCampaign.setIcon(ContextCompat.getDrawable(this,
                    client_obj.isCampaign() ? R.drawable.ic_baseline_toggle_on_24 : R.drawable.ic_baseline_toggle_off_24));
            menuCampaign.setChecked(client_obj.isCampaign());
        }

        final FloatingActionButton fab = findViewById(R.id.lead_view_edit_id);
        fab.setOnClickListener(view -> {
            Intent leadIntent = new Intent(getApplicationContext(), ClientProcessActivity.class);
            leadIntent.putExtra("CLIENT_ID", client_obj.getId());
            leadIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(leadIntent);
        });

        TabLayout tabLayout = findViewById(R.id.leadView_tabs_id);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout) {
            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        fab.show();
                        break;
                    case 1:
                        fab.hide();
                        break;
                }
            }
        });
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        call_btn = findViewById(R.id.lead_appbar_Call);
        call_btn.setOnClickListener(view -> {
            if (client_obj.getPhones().size() > 0) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + client_obj.getPhones().get(0).getPhone()));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } else {
                Snackbar.make(view, getResources().getString(R.string.no_phone_is_associated_contact), Snackbar.LENGTH_LONG)
                        .setAction(getResources().getString(R.string.action), null).show();
            }
        });
        if (menuCall != null && client_obj.getPhones() != null)
            menuCall.setVisible(client_obj.getPhones().size() > 0);
        if (menuSMS != null && client_obj.getPhones() != null)
            menuSMS.setVisible(client_obj.getPhones().size() > 0);
        if (menuEmail != null && client_obj.getEmails() != null)
            menuEmail.setVisible(client_obj.getEmails().size() > 0);

        lead_campaign.setOnCheckedChangeListener((buttonView, isChecked) -> putItOnCampaign(CLIENT_ID, isChecked));

    }

    private void writeContact(String displayName, String givenName, String familyName, String email, String phone, String address) {
        ArrayList contentProviderOperations = new ArrayList();
        //insert raw contact using RawContacts.CONTENT_URI
        contentProviderOperations.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null).withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null).build());
        //insert contact display name using Data.CONTENT_URI

        contentProviderOperations.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0).withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, displayName).build());

        contentProviderOperations.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0).withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME, givenName).build());

        contentProviderOperations.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0).withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME, familyName).build());


        //insert mobile number using Data.CONTENT_URI
        contentProviderOperations.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0).withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, phone).withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE).build());

        contentProviderOperations.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0).withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Email.ADDRESS, email).withValue(ContactsContract.CommonDataKinds.Email.TYPE, ContactsContract.CommonDataKinds.Email.TYPE_OTHER).build());

        contentProviderOperations.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0).withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredPostal.STREET, address).withValue(ContactsContract.CommonDataKinds.StructuredPostal.TYPE, ContactsContract.CommonDataKinds.StructuredPostal.STREET).build());
        try {
            getApplicationContext().getContentResolver().
                    applyBatch(ContactsContract.AUTHORITY, contentProviderOperations);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        }
    }
}