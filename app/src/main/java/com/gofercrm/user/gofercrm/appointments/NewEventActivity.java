package com.gofercrm.user.gofercrm.appointments;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.util.Linkify;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;

import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.chat.ui.main.model.Member;
import com.gofercrm.user.gofercrm.entity.Email;
import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.SharedPreferenceData;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.tokenautocomplete.TokenCompleteTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;

public class NewEventActivity extends BaseActivity implements View.OnClickListener,
        TokenCompleteTextView.TokenListener<Contact> {
    private static final String TAG = "NewEventActivity";
    Calendar date;
    Calendar startDateTime;
    Calendar endDateTime;

    TextView startTime;
    TextView endTime;

    EditText title;
    EditText privateComment, description;
    EditText location;
    Switch eventNotification;
    Switch upsert_opportunity;
    Spinner timeZone;
//    ProgressDialog progressDialog;

    JSONObject SELECTED_EVENT;

    List<Contact> emailToLists = new ArrayList<>();

    ContactsCompletionView contactsChip;
    List<Contact> contacts = new ArrayList<>();
    ArrayAdapter<Contact> contactAdapter;

    ContactsCompletionView leadsChip;
    List<Contact> leads = new ArrayList<>();
    ArrayAdapter<Contact> leadsAdapter;

    ContactsCompletionView connectionssChip;
    List<Contact> connections = new ArrayList<>();
    ArrayAdapter<Contact> connectionsAdapter;

    HashSet<String> selected_emails = new HashSet<>();

    String LEAD_ID, CLIENT_ID;

    String medium_pattern = "E, MM d, yyyy hh:mm a";

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(medium_pattern, java.util.Locale.getDefault()); //Locale.US

    String PROCESSING_MESSAGE;
    String SUCCESS_MESSAGE;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_event);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        PROCESSING_MESSAGE = getResources().getString(R.string.creating_appointment_);
        SUCCESS_MESSAGE = getResources().getString(R.string.appointment_created_success);
        LinearLayout start_time_event = findViewById(R.id.event_start_time_layout_id);
        start_time_event.setOnClickListener(this);

        LinearLayout end_time_event = findViewById(R.id.event_end_time_layout_id);
        end_time_event.setOnClickListener(this);

        startTime = findViewById(R.id.input_event_start_time);
        endTime = findViewById(R.id.input_event_end_time);

        String[] timezones = TimeZone.getAvailableIDs();
        timeZone = findViewById(R.id.input_event_timezone);

        List<String> timeZoneList = new ArrayList<>(Arrays.asList(timezones));

        ArrayAdapter<String> timeZoneAdapter =
                new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, timeZoneList);
        timeZone.setAdapter(timeZoneAdapter);
        for (int i = 0; i < timeZoneAdapter.getCount(); i++) {
            if (timeZoneAdapter.getItem(i).equals(TimeZone.getDefault().getID())) {
                timeZone.setSelection(i);
            }
        }

        title = findViewById(R.id.input_event_title);
        privateComment = findViewById(R.id.input_event_private_comment);
        description = findViewById(R.id.input_event_description);
        startTime = findViewById(R.id.input_event_start_time);
        endTime = findViewById(R.id.input_event_end_time);
        location = findViewById(R.id.input_event_location);
        eventNotification = findViewById(R.id.input_event_notify);
        upsert_opportunity = findViewById(R.id.input_event_upsert_opportunity);


        if (getIntent().getSerializableExtra("ROOM_MEMBER") != null) {

            if (getIntent().getSerializableExtra("ROOM_NAME") != null) {
                title.setText(getIntent().getSerializableExtra("ROOM_NAME") + " " +
                        getResources().getString(R.string.meeting));
            } else {
                title.setText(getResources().getString(R.string.room_meeting));
            }
        }
        startTime.setText(Util.getNextHourCurrentDate(simpleDateFormat));
        endTime.setText(Util.get30MinAfter1HourDate(simpleDateFormat));

        LinearLayout llContacts = findViewById(R.id.llContacts);
        LinearLayout llLeads = findViewById(R.id.llLeads);
//        llLeads.setVisibility(SharedPreferenceData.getInstance(this).isFullAccessAccount() ?
//                View.VISIBLE : View.GONE);
//        llContacts.setVisibility(SharedPreferenceData.getInstance(this).isFullAccessAccount() ?
//                View.VISIBLE : View.GONE);
        llContacts.setVisibility(View.GONE);
        llLeads.setVisibility(View.GONE);

        contactAdapter = new ContactAdapter(this, R.layout.contact_ac_layout, contacts);
        contactsChip = findViewById(R.id.contacts_id);
        contactsChip.setTokenListener(this);
        contactsChip.setTokenClickStyle(TokenCompleteTextView.TokenClickStyle.Select);
        contactsChip.setAdapter(contactAdapter);


        leadsAdapter = new ContactAdapter(this, R.layout.contact_ac_layout, leads);
        leadsChip = findViewById(R.id.leads_id);
        leadsChip.setTokenListener(this);
        leadsChip.setTokenClickStyle(TokenCompleteTextView.TokenClickStyle.Select);
        leadsChip.setAdapter(leadsAdapter);

        connectionsAdapter = new ContactAdapter(this, R.layout.contact_ac_layout, connections);
        connectionssChip = findViewById(R.id.connections_id);
        connectionssChip.setTokenListener(this);
        connectionssChip.setTokenClickStyle(TokenCompleteTextView.TokenClickStyle.Select);
        connectionssChip.setAdapter(connectionsAdapter);

        Intent intent = getIntent();
        LEAD_ID = intent.getStringExtra("LEAD_ID");
        CLIENT_ID = intent.getStringExtra("CLIENT_ID");

        getContacts();
        getLeads();
        getConnections();
        processEdit();
        processRoomData();
    }

    public void showDateTimePicker(final boolean starDate) {
        final Calendar currentDate = Calendar.getInstance();
        date = Calendar.getInstance();
        new DatePickerDialog(NewEventActivity.this, (view, year, monthOfYear, dayOfMonth) -> {
            date.set(year, monthOfYear, dayOfMonth);
            new TimePickerDialog(NewEventActivity.this, (view1, hourOfDay, minute) -> {
                date.set(Calendar.HOUR_OF_DAY, hourOfDay);
                date.set(Calendar.MINUTE, minute);
                if (starDate) {
                    startDateTime = date;
                    String formatted_date = simpleDateFormat.format(date.getTime());
                    startTime.setText(formatted_date);
                } else {
                    endDateTime = date;
                    String formatted_date = simpleDateFormat.format(date.getTime());
                    endTime.setText(formatted_date);
                }

            }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), false).show();
        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show();
    }

    private void processEdit() {
        Intent intent = getIntent();
        String event_extra = intent.getStringExtra("EVENT_OBJ");
        if (event_extra != null) {
            try {
                SELECTED_EVENT = new JSONObject(event_extra);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (SELECTED_EVENT != null) {
            if (getSupportActionBar() != null)
                getSupportActionBar().setTitle(getResources().getString(R.string.edit_appointment));
            PROCESSING_MESSAGE = getResources().getString(R.string.updating_appointment_);
            SUCCESS_MESSAGE = getResources().getString(R.string.appointment_updated_success);
            renderEvent(SELECTED_EVENT);
        }
    }

    private void processRoomData() {
        Intent intent = getIntent();
        List<Member> members = (ArrayList<Member>) intent.getSerializableExtra("ROOM_MEMBER");
        if (members != null) {
            String email = "";
            for (Member m : members) {
                email = m.getEmail();
                if (email != null && !email.isEmpty() && !email.equals("null")) {
                    Contact contact = new Contact(m.get_Id(), m.getmName(), email, "");
                    connectionssChip.addObjectAsync(contact);
                    selected_emails.add(email);
//                    String txt = connectionssChip.getText().toString();
//                    if (txt.isEmpty()) {
//                        txt = m.getmName();
//                    } else {
//                        txt = txt + "," + m.getmName();
//                    }
//                    connectionssChip.setText(txt);
//                    selected_emails.add(email);
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.event_start_time_layout_id) {
            showDateTimePicker(true);
        }
        if (v.getId() == R.id.event_end_time_layout_id) {
            showDateTimePicker(false);
        }
    }

    private void renderEvent(JSONObject event) {
        String to_list = "";
        JSONArray attendees = new JSONArray();
        try {
            if (event.has("summary")) {
                title.setText(event.getString("summary"));
            }
            if (event.has("private_comment")) {
                privateComment.setText(event.getString("private_comment"));
                Linkify.addLinks(privateComment, Linkify.ALL);

            }
            if (event.has("description")) {
                description.setText(event.getString("description"));
                Linkify.addLinks(description, Linkify.ALL);

            }
            if (event.has("attendees")) {
                attendees = event.getJSONArray("attendees");

                String email = "";
                for (int i = 0; i < attendees.length(); i++) {
                    JSONObject att_obj = attendees.getJSONObject(i);
                    email = att_obj.getString("email");
                    String txt = connectionssChip.getText().toString();
                    if (txt.isEmpty()) {
                        txt = email;
                    } else {
                        txt = txt + "," + email;
                    }
                    connectionssChip.setText(txt);
                    selected_emails.add(email);

                }
            }
            if (event.has("start")) {
                startTime.setText(Util.zFormatStringToDate(event.getString("start")));
            }
            if (event.has("end")) {
                endTime.setText(Util.zFormatStringToDate(event.getString("end")));
            }
            if (event.has("location")) {
                location.setText(event.getString("location"));
                Linkify.addLinks(location, Linkify.ALL);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Contact findContactByEmail(String email) {
        Contact c = null;
        for (Contact p : contacts) {
            if (p.getEmail().equals(email)) {
                c = p;
                break;
            }
        }
        return c;
    }

    private Contact findLeadsByEmail(String email) {
        Contact c = null;
        for (Contact p : leads) {
            if (p.getEmail().equals(email)) {
                c = p;
                break;
            }
        }
        return c;
    }

    private Contact findConnectionsByEmail(String email) {
        Contact c = null;
        for (Contact p : connections) {
            if (p.getEmail().equals(email)) {
                c = p;
                break;
            }
        }
        return c;
    }

    private void getContacts() {
        NetworkManager.customJsonObjectRequest(
                NewEventActivity.this, Constants.GET_CONTACTS_PICKLIST_URL, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processContacts(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ERRROR=", message);
                    }
                }, false);
    }

    private void processContacts(JSONObject data) {
        LogUtils.Print(TAG, "Data --> " + data.toString());
        try {
            contacts.clear();
            String id = "";
            String name = "";
            String email = "";
            if (Integer.parseInt(data.getString("result")) == 1) {
                JSONObject result_Data = data.getJSONObject("data");
                JSONArray leads = result_Data.getJSONArray("contacts");
                for (int i = 0; i < leads.length(); i++) {
                    JSONObject obj = leads.getJSONObject(i);
                    JSONArray emailArray = obj.getJSONArray("email_list");
                    List<Email> emails = new ArrayList<>();
                    for (int j = 0; j < emailArray.length(); j++) {
                        JSONObject emailObj = emailArray.getJSONObject(j);
                        email = emailObj.has("email") ? emailObj.getString("email") : "";
                    }
                    id = obj.getString("id");
                    name = obj.getString("name");
                    if (name != null) {
                        name = Util.capitalize(name);
                    }
                    Contact contact = new Contact(id, name, email, "");
                    contacts.add(contact);
                    if (CLIENT_ID != null && CLIENT_ID.equals(id)) {
                        toolbar.setSubtitle(getResources().getString(R.string.with) + " " + name);
//                        contactsChip.setEnabled(false);
                        contactsChip.setText(name);
                        emailToLists.add(contact);
                    }
                }
                contactAdapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getLeads() {
        NetworkManager.customJsonObjectRequest(
                NewEventActivity.this, Constants.GET_LEADS_PICKLIST_URL, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processLeads(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ERRROR=", message);
                    }
                }, false);
    }

    private void processLeads(JSONObject data) {
        try {
            leads.clear();
            String id = "";
            String name = "";
            String email = "", phone = "";
            if (Integer.parseInt(data.getString("result")) == 1) {
                JSONObject result_Data = data.getJSONObject("data");
                JSONArray lead_contacts = result_Data.getJSONArray("lead_contacts");
                for (int i = 0; i < lead_contacts.length(); i++) {
                    JSONObject obj = lead_contacts.getJSONObject(i);
                    JSONArray emailArray = obj.getJSONArray("email_list");
                    for (int j = 0; j < emailArray.length(); j++) {
                        JSONObject emailObj = emailArray.getJSONObject(j);
                        email = emailObj.has("email") ? emailObj.getString("email") : "";
                    }

                    JSONArray phoneArray = obj.getJSONArray("phone_list");
                    for (int j = 0; j < phoneArray.length(); j++) {
                        JSONObject phoneObj = phoneArray.getJSONObject(j);
                        phone = phoneObj.has("phone") ? phoneObj.getString("phone") : "";
                    }
                    id = obj.getString("id");
                    name = obj.getString("name");
                    if (name != null) {
                        name = Util.capitalize(name);
                    }

                    Contact contact = new Contact(id, name, email, phone);
                    leads.add(contact);
                    if (LEAD_ID != null && LEAD_ID.equals(id)) {
                        toolbar.setSubtitle(getResources().getString(R.string.with) + " " + name);
//                        leadsChip.setEnabled(false);
                        leadsChip.setText(name);
                        emailToLists.add(contact);
                    }
                }
                leadsAdapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getConnections() {
        NetworkManager.customJsonObjectGetRequest(
                NewEventActivity.this, Constants.GET_CONNECTIONS_PICKLIST_URL, null,
                new VolleyResponseListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        processConnections(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ERRROR=", message);
                    }

                }, false);
    }

    private void processConnections(JSONObject data) {
        try {
            connections.clear();
            String id = "";
            String name = "";
            String email = "";
            if (Integer.parseInt(data.getString("result")) == 1) {
                JSONArray users = data.getJSONArray("users");
                for (int i = 0; i < users.length(); i++) {
                    JSONObject obj = users.getJSONObject(i);
                    email = obj.has("email") ? obj.getString("email") : "";
                    id = obj.getString("id");
                    name = obj.getString("name");
                    if (name != null) {
                        name = Util.capitalize(name);
                    }
                    Contact contact = new Contact(id, name, email, "");
                    connections.add(contact);
                }
                connectionsAdapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTokenAdded(Contact token) {
        emailToLists.add(token);
    }

    @Override
    public void onTokenRemoved(Contact token) {
        emailToLists.remove(token);
    }

    @Override
    public void onTokenIgnored(Contact token) {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_event, menu);
        MenuItem delete_menu = menu.findItem(R.id.action_event_delete);
        if (SELECTED_EVENT == null) {
            delete_menu.setVisible(false);
        } else {
            delete_menu.setVisible(true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_event_save) {
            if (!validate()) {
                Util.onMessage(getApplicationContext(), getResources().getString(R.string.missing_mandatory_fields));
            } else {
                if (providerIsGoogle()) {
                    getAccessToken(1);
                } else {
                    createEvent();
                }
            }
        }

        if (id == R.id.action_event_delete) {
            new AlertDialog.Builder(this)
                    .setTitle(getResources().getString(R.string.delete_event))
                    .setMessage(getResources().getString(R.string.delete_confirmation_msg_))
                    .setIcon(android.R.drawable.ic_menu_delete)
                    .setPositiveButton(getResources().getString(R.string.delete), (dialogInterface, i) -> {
                        if (providerIsGoogle()) {
                            getAccessToken(0);
                        } else {
                            deleteEvent();
                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.cancel), null).show();
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean validate() {
        boolean valid = true;
        String title_value = title.getText().toString().trim();
        if (title_value.isEmpty()) {
            title.setError(getResources().getString(R.string.title_empty_err));
            valid = false;
        } else {
            title.setError(null);
        }
        if (emailToLists.size() <= 0 && selected_emails.size() <= 0) {
            connectionssChip.setError(getResources().getString(R.string.empty_lead_contacts_connection_err));
            valid = false;
        } else {
            connectionssChip.setError(null);
        }
        return valid;
    }

    private JSONObject deleteRequest() {
        JSONObject payload = new JSONObject();
        if (SELECTED_EVENT != null) {
            try {
                payload.put("event_id", SELECTED_EVENT.getString("event_id"));
                payload.put("calendar_id", SELECTED_EVENT.getString("calendarId"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return payload;
    }

    private JSONObject requestData() {
        JSONObject payload = new JSONObject();
        try {
            if (SELECTED_EVENT != null) {
                payload.put("event_id", SELECTED_EVENT.getString("event_id"));
                payload.put("calendar_id", SELECTED_EVENT.getString("calendarId"));
            }
            payload.put("event_name", title.getText().toString());
            payload.put("start_date", startTime.getText().toString());
            payload.put("end_date", endTime.getText().toString());
            payload.put("location", location.getText().toString());
            payload.put("time_zone", timeZone.getSelectedItem().toString());

            JSONArray phoneArray = new JSONArray();
            //To avoid duplicates
            for (int i = 0; i < emailToLists.size(); i++) {
                Contact con = emailToLists.get(i);
                selected_emails.add(con.getEmail());

            }
            for (String s : selected_emails) {
                phoneArray.put(s);
            }
            payload.put("to_list", phoneArray);
            payload.put("private_comment", privateComment.getText());
            payload.put("description", description.getText());
            payload.put("send_update_notification", eventNotification.isChecked());
            payload.put("upsert_opportunity", upsert_opportunity.isChecked());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        LogUtils.Print(TAG, "payload --> " + payload);
        return payload;
    }

    private boolean providerIsGoogle() {
        return SharedPreferenceData.getInstance(this).getProvider().equalsIgnoreCase("google");
    }

    /**
     * 1 = CREATE, 0 = DELETE
     *
     * @param from
     */
    private void getAccessToken(int from) {
        showProgress();
        String params = "?provider_uid=" + SharedPreferenceData.getInstance(this).getProviderId() +
                "&mobile_client_id=" + Constants.getClientId() +
                "&mobile_client_type=android";
        LogUtils.Print(TAG, "getAccessToken --> " + params);
        NetworkManager.customJsonObjectRequest(
                NewEventActivity.this, Constants.GOOGLE_ACCESS_TOKEN + params, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideProgress();
                        if (from == 1) {
                            createEvent();
                        } else {
                            deleteEvent();
                        }
                    }

                    @Override
                    public void onError(String message) {
                        hideProgress();
                        if (from == 1) {
                            createEvent();
                        } else {
                            deleteEvent();
                        }
                    }
                }, true);
    }

    private void createEvent() {
        String req_url = Constants.CREATE_EVENT_URL;
//        progressDialog = new ProgressDialog(NewEventActivity.this,
//                R.style.AppTheme_Dark_Dialog);
//        progressDialog.setCanceledOnTouchOutside(false);
//        progressDialog.setIndeterminate(true);
//        progressDialog.setMessage(PROCESSING_MESSAGE);
//        progressDialog.show();
        showProgress();
        JSONObject postData = new JSONObject();
        try {
            postData.put("event_payload", requestData());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (SELECTED_EVENT != null) {
            req_url = Constants.UPDATE_EVENT_URL;
        }

        NetworkManager.customJsonObjectRequest(
                NewEventActivity.this, req_url, postData,
                new VolleyResponseListener() {

                    @Override
                    public void onResponse(JSONObject response) {
//                        progressDialog.dismiss();
                        hideProgress();
                        processResult(response);
                    }

                    @Override
                    public void onError(String message) {
//                        progressDialog.dismiss();
                        hideProgress();
                        LogUtils.Print("EVENT ERROR", message);
                    }
                }, true);
    }

    private void processResult(JSONObject response) {
        try {
            if (Integer.parseInt(response.getString("result")) == 1) {
                Util.onMessage(getApplicationContext(), getResources().getString(R.string.calendar_updated));
                Intent intent = new Intent(this, AppointmentsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            } else {
                Util.onMessage(getApplicationContext(), getResources().getString(R.string.calendar_updated_failed));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void deleteEvent() {
//        progressDialog = new ProgressDialog(NewEventActivity.this,
//                R.style.AppTheme_Dark_Dialog);
//        progressDialog.setCanceledOnTouchOutside(false);
//        progressDialog.setIndeterminate(true);
//        progressDialog.setMessage("Deleting event...");
//        progressDialog.show();
        showProgress();
        JSONObject postData = new JSONObject();
        try {
            postData.put("event_payload", deleteRequest());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        NetworkManager.customJsonObjectRequest(
                NewEventActivity.this, Constants.DELETE_EVENT_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideProgress();
//                        progressDialog.dismiss();
                        processResult(response);
                    }

                    @Override
                    public void onError(String message) {
                        hideProgress();
//                        progressDialog.dismiss();
                        LogUtils.Print("EVENT ERROR", message);
                    }
                }, true);
    }
}