package com.gofercrm.user.gofercrm.lead;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import androidx.core.widget.NestedScrollView;

import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.clients.ClientViewActivity;
import com.gofercrm.user.gofercrm.entity.User;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class LeadOwnerBSFragment extends BottomSheetDialogFragment implements View.OnClickListener {

    BottomSheetBehavior sheetBehavior;
    NestedScrollView layoutBottomSheet;
    String LEAD_ID;
    private Spinner lead_owner;
    private Button bs_lead_owner_cancel, bs_lead_owner_change;
    private ArrayAdapter<User> leadOwnerAdapter;
    private LeadOwnerBSFragment currentObj;
    private ArrayList<User> owners = new ArrayList<>();
    private Activity activity;

    private String from, id;

    public LeadOwnerBSFragment(String from, String id) {
        this.from = from;
        this.id = id;
    }

    public LeadOwnerBSFragment getCurrentObj() {
        return currentObj;
    }

    public void setCurrentObj(LeadOwnerBSFragment currentObj) {
        this.currentObj = currentObj;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        activity = getActivity();
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.leadowner_bottom_sheet, container, false);

        bs_lead_owner_cancel = v.findViewById(R.id.account_contact_cancel);
        bs_lead_owner_cancel.setOnClickListener(this);
        bs_lead_owner_change = v.findViewById(R.id.account_contact_save);
        bs_lead_owner_change.setOnClickListener(this);
        lead_owner = v.findViewById(R.id.bs_lead_owner);
        leadOwnerAdapter = new ArrayAdapter<User>(getContext(), android.R.layout.simple_spinner_item, owners);
        leadOwnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        lead_owner.setAdapter(leadOwnerAdapter);
        getLeadOwners();
        return v;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.account_contact_cancel) {
            if (currentObj != null) {
                currentObj.dismiss();
            }
        } else if (view.getId() == R.id.account_contact_save) {
            changeOwner();
        }
    }

    private void changeOwner() {
        JSONObject postData = new JSONObject();
        User user = (User) lead_owner.getSelectedItem();
        String to_id = user.getId();
        try {
            JSONArray lead_ids = new JSONArray();
            lead_ids.put(id);
            if (from.equals("LEAD")) {
                postData.put("lead_ids_list", lead_ids);
            } else {
                postData.put("contact_ids_list", lead_ids);
            }
            postData.put("to_userid", to_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String strAPI;
        if (from.equals("LEAD")) {
            strAPI = Constants.CHANGE_LEAD_OWNER_URL;
        } else {
            strAPI = Constants.GET_CONTACT_USER_URL;
        }
        NetworkManager.customJsonObjectRequest(
                getContext(), strAPI, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processResult(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("LEAD OWNER CHANGE ERROR", message);
                    }
                }, true);
    }

    private void processResult(JSONObject response) {
        try {
            if (Integer.parseInt(response.getString("result")) == 1) {
                Util.onMessageTop(getContext(), getResources().getString(R.string.lead_owner_changed));
                currentObj.dismiss();
                if (from.equals("LEAD")) {
                    ((LeadViewActivity) activity).refreshData();
                } else if (from.equals("CLIENT")) {
                    ((ClientViewActivity) activity).refreshData();
                }
            } else {
                Util.onMessageTop(getContext(), getResources().getString(R.string.lead_owner_changed_failed));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getLeadOwners() {
        NetworkManager.customJsonObjectRequest(
                getContext(), Constants.GET_TENANT_USER_URL, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processOwners(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ERRROR=", message);
                    }
                }, true);
    }

    private void processOwners(JSONObject data) {
        LogUtils.Print("OWNER=", data.toString());
        try {
            owners.clear();
            String id = "";
            String name = "";
            if (Integer.parseInt(data.getString("result")) == 1) {
                JSONArray users = data.getJSONArray("users");
                for (int i = 0; i < users.length(); i++) {
                    JSONObject obj = users.getJSONObject(i);
                    id = obj.getString("id");
                    name = obj.getString("name");
                    if (name != null) {
                        name = Util.capitalize(name);
                    }
                    User user = new User(id, name);
                    owners.add(user);
                }
                leadOwnerAdapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}