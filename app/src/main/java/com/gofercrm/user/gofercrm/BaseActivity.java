package com.gofercrm.user.gofercrm;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;

import androidx.appcompat.app.AppCompatActivity;

import com.gofercrm.user.gofercrm.util.ProgressDialog;
import com.gofercrm.user.gofercrm.util.SharedPreferenceData;

abstract public class BaseActivity extends AppCompatActivity {

    public void showProgress() {
        ProgressDialog.getInstance().show(this);
    }

    public void hideProgress() {
        ProgressDialog.getInstance().dismiss();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        SharedPreferenceData preferences = SharedPreferenceData.getInstance(newBase);
        super.attachBaseContext(ApplicationContext.updateResources(newBase,
                preferences.getString(ApplicationContext.key_lang)));
    }

    @Override
    public void applyOverrideConfiguration(Configuration overrideConfiguration) {
        if (overrideConfiguration != null) {
            int uiMode = overrideConfiguration.uiMode;
            overrideConfiguration.setTo(getBaseContext().getResources().getConfiguration());
            overrideConfiguration.uiMode = uiMode;
        }
        super.applyOverrideConfiguration(overrideConfiguration);
    }

    public void start(Class<? extends Activity> activity) {
        startActivity(new Intent(this, activity));
    }

    public void startWithClearStack(Class<? extends Activity> activity) {
        startActivity(new Intent(this, activity).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
        this.finish();
    }
}