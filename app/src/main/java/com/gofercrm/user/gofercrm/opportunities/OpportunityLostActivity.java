package com.gofercrm.user.gofercrm.opportunities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class OpportunityLostActivity extends BaseActivity {

    final Calendar myCalendar = Calendar.getInstance();
    EditText input_oppor_lost_close_date, input_oppor_followup_date,
            input_oppor_lost_ls, input_oppor_lost_comments;
    String selected_obj;
    JSONObject opportunity_obj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opportunity_lost);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.lost));
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.lost);
        fab.setOnClickListener(view -> {
            if (!validate()) {
                Util.onMessage(getApplicationContext(), getResources().getString(R.string.fields_value_in_correct));
            } else {
                saveOpportunity();
            }
        });
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        input_oppor_lost_close_date = findViewById(R.id.input_oppor_lost_close_date);
        input_oppor_followup_date = findViewById(R.id.input_oppor_followup_date);
        input_oppor_lost_ls = findViewById(R.id.input_oppor_lost_ls);
        input_oppor_lost_comments = findViewById(R.id.input_oppor_lost_comments);

        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        input_oppor_lost_close_date.setText(sdf.format(myCalendar.getTime()));
        input_oppor_followup_date.setText(sdf.format(myCalendar.getTime()));

        final DatePickerDialog.OnDateSetListener close_date = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateCloseDate();
        };

        input_oppor_lost_close_date.setOnClickListener(v -> new DatePickerDialog(OpportunityLostActivity.this, close_date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show());

        final DatePickerDialog.OnDateSetListener renewal_date = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateRenewalDate();
        };
        input_oppor_followup_date.setOnClickListener(v -> new DatePickerDialog(OpportunityLostActivity.this, renewal_date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show());

        Intent intent = getIntent();
        selected_obj = intent.getStringExtra("OPPORTUNITY_TO_LOST");
        if (selected_obj != null) {
            try {
                opportunity_obj = new JSONObject(selected_obj);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void updateCloseDate() {
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        input_oppor_lost_close_date.setText(sdf.format(myCalendar.getTime()));
    }

    private void updateRenewalDate() {
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        input_oppor_followup_date.setText(sdf.format(myCalendar.getTime()));
    }

    private void saveOpportunity() {
        JSONObject postData = new JSONObject();
        try {
            String id = opportunity_obj.getString("opportunity_id");
            if (id == null) {
                Util.onMessage(getApplicationContext(), getResources().getString(R.string.no_appo_id_found));
                return;
            }
            Util.onMessage(getApplicationContext(), getResources().getString(R.string.updating_opportunity));
            postData.put("action", "update");
            postData.put("opportunity_id", id);
            postData.put("payload", requestData());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        LogUtils.Print("POST_DATA=", postData.toString());
        NetworkManager.customJsonObjectRequest(
                OpportunityLostActivity.this, Constants.OPPORTUNITY_MANAGE_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processResult(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("Opportunity Lost Error", message);
                    }
                }, true);
    }

    public boolean validate() {
        boolean valid = true;
        if (input_oppor_lost_close_date.getText().toString().isEmpty()) {
            input_oppor_lost_close_date.setError(getResources().getString(R.string.close_date_is_mandatory));
        } else {
            input_oppor_lost_close_date.setError(null);
        }
        if (input_oppor_lost_ls.getText().toString().isEmpty()) {
            input_oppor_lost_ls.setError(getResources().getString(R.string.loss_reason_is_mandatory));
        } else {
            input_oppor_lost_ls.setError(null);
        }
        return valid;
    }

    private JSONObject requestData() {
        JSONObject payload = new JSONObject();
        try {
            payload.put("close_date", input_oppor_lost_close_date.getText().toString() + "T00:00:00.000Z");
            payload.put("followup_date", input_oppor_followup_date.getText().toString() + "T00:00:00.000Z");
            payload.put("stage", "closed_lost");

            JSONObject dict = new JSONObject();
            dict.put("loss_reasons", input_oppor_lost_ls.getText().toString());
            dict.put("comments", input_oppor_lost_comments.getText().toString());
            payload.put("close_dict", dict);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return payload;
    }

    private void processResult(JSONObject response) {
        try {
            if (Integer.parseInt(response.getString("result")) == 1) {
                Util.onMessage(getApplicationContext(), getResources().getString(R.string.update_success));
                Intent intent = new Intent(getApplicationContext(), OpportunityActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } else {
                JSONObject error_obj = response.getJSONObject("error");
                if (error_obj.has("reason")) {
                    Util.onMessage(getApplicationContext(), error_obj.getString("reason"));
                } else {
                    Util.onMessage(getApplicationContext(), getResources().getString(R.string.oppo_update_failed));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}