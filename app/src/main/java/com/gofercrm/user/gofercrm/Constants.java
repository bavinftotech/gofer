package com.gofercrm.user.gofercrm;

import com.android.volley.BuildConfig;

public class Constants {

    public static final String EMAIL = "";
    public static final String PASSWORD = "";
    public static final String ACCESS_TOKEN = "token";
    public static final String URL = "https://goferapi.ambivo.com/";
    public static final String Login_URL = URL + "user/login";
    public static final String REGISTRATION_API = URL + "user/register";
    public static final String LEAD_LIST_URL = URL + "user/lead/manage_contacts";
    public static final String GET_EVENTS_URL = URL + "user/calendar/events";
    public static final String UPGRADE_TO_CONTACT_URL = URL + "user/lead/convert_to_contact";
    public static final String SELF_ASSIGN_URL = URL + "user/lead/lead_owner";
    public static final String CHANGE_LEAD_OWNER_URL = URL + "user/lead/lead_owner";
    public static final String UPDATE_PROFILE = URL + "user/update_profile";

    public static final String CALENDAR_LIST_URL = URL + "user/calendar/list";
    public static final String CALENDAR_SET_DEFAULT_URL = URL + "user/calendar/make_default";
    public static final String CALENDAR_REFRESH_ALL_URL = URL + "user/calendar/RefreshCalendars";

    public static final String OPPORTUNITY_MANAGE_URL = URL + "user/opportunities/manage";

    public static final String GET_CONTACTS_PICKLIST_URL = URL + "user/contact/picklist";
    public static final String GET_LEADS_PICKLIST_URL = URL + "user/lead/picklist";
    public static final String GET_CONNECTIONS_PICKLIST_URL = URL + "friends/fetch";
    public static final String GET_ACCOUNTS_PICKLIST_URL = URL + "user/customer/accounts/picklist";
    public static final String GET_OPPORTUNITIES_PICKLIST_URL = URL + "user/opportunities/picklist";
    public static final String GET_COMMUNITIES_PICKLIST_URL = URL + "properties/properties/picklist";
    public static final String GET_TENANT_USER_URL = URL + "user/get_tenant_users";
    public static final String GET_CONTACT_USER_URL = URL + "user/contact/contact_owner";

    public static final String CREATE_EVENT_URL = URL + "user/create_calendar_event";
    public static final String UPDATE_EVENT_URL = URL + "user/update_calendar_event";
    public static final String DELETE_EVENT_URL = URL + "user/delete_calendar_event";

    public static final String SEARCH_LEAD_URL = URL + "user/lead/search_contacts";
    public static final String SEARCH_TASK_URL = URL + "user/tasks/search";
    public static final String SEARCH_OPPORTUNITY_URL = URL + "user/opportunities/search";

    public static final String TASK_LIST_URL = URL + "user/tasks/manage";

    public static final String MANAGE_CONTACT_URL = URL + "user/contact/manage_contacts";
    public static final String SEARCH_CONTACT_URL = URL + "user/contact/search";

    public static final String MANAGE_ACCOUNT_URL = URL + "user/customer/manage_accounts";
    public static final String SEARCH_ACCOUNT_URL = URL + "user/customer/search_accounts";
    public static final String GET_ACCOUNT_CONTACTS_URL = URL + "user/customer/accounts/get_contacts";

    public static final String USR_BY_TOKEN_URL = URL + "user/get_from_token";
    public static final String TOKEN_REQUEST_URL = URL + "admin/gapps/signup_with_token";
    public static final String GOOGLE_TOKENS = "https://www.googleapis.com/oauth2/v4/token";

    public static final String ATTACHMENT_LIST_URL = URL + "user/attachment/manage";
    public static final String UPLOAD_ATTACHMENT_URL = URL + "messages/attachment_send";

    public static final String GET_PRODUCT_LIST_URL = URL + "product/picklist";
    public static final String GET_OPPORTUNITY_STAGE_LIST_URL = URL + "user/opportunity/stage/values";

    public static final String FORGOT_PASSWORD_URL = URL + "user/reset_password";

    public static final String FRIENDS_FETCH_URL = URL + "friends/fetch";

    public static final String CLIENTS_USERS_URL = URL + "user/get_tenant_client_users";

    public static final String GET_ROOM_MESSAGES = URL + "messages/fetch_room_messages";
    public static final String SEND_ROOM_MESSAGE = URL + "messages/send_room_message";
    public static final String EDIT_ROOM_MESSAGE = URL + "messages/edit_room_msg";
    public static final String DELETE_ROOM_MESSAGE = URL + "messages/delete_room_msg";
    public static final String MUTE_ROOM_NOTIFICATION = URL + "room/member/notifications";

    public static final String GET_CONNECTION_MESSAGES = URL + "messages/fetch";
    public static final String SEND_CONNECTION_MESSAGE = URL + "messages/send";
    public static final String EDIT_CONNECTION_MESSAGE = URL + "messages/edit";
    public static final String DELETE_CONNECTION_MESSAGE = URL + "messages/delete";

    public static final String MARK_MESSAGE_AS_READ_URL = URL + "messages/mark";

    public static final String MARK_ROOM_MESSAGE_AS_READ_URL = URL + "messages/mark_room_msg";

    public static final String GET_ROOM_INFO = URL + "room/info/";
    public static final String MANAGE_ROOM = URL + "room";
    public static final String ADD_ROOM_MEMBERS = URL + "room/member";

    public static final String SEARCH_ROOM_MESSAGES = URL + "room/messages/search";
    public static final String SEARCH_CONNECTION_MESSAGES = URL + "messages/search";

    public static final String GET_USER_DATA = URL + "user/get_public_profile";
    public static final String SET_USER_CONVERSATION_APP_STATUS = URL + "discovery/app_status";
    public static final String LOGOUT_USER = URL + "user/logout";

    public static final String FILE_UPLOAD = URL + "user/file_upload";
    public static final String FILE_SHARE = URL + "user/file_share";
    public static final String VIDEO_ELIGIBILITY = URL + "user/videocall/eligibility";
    public static final String SOCKET_NOTIFICATION = URL + "user/socketnotification/push";
    public static final String ADD_USER = URL + "friends/add";
    public static final String REMOVE_USER = URL + "friends/remove";
    //    public static final String SEARCH_USER = URL + "friends/search";
    public static final String SEARCH_USER = URL + "public/discovery/get_neighbors";
    public static final String MY_DATA = URL + "user/my_data";
    public static final String CHANGE_AVATAR = URL + "user/avatar";

    public static final String OPPORTUNITY_FIND_ONE = URL + "user/opportunities/find_one";
    public static final String PROPERTY_LIST = URL + "user/opportunities/product_ref_picklist";
    public static final String MARKETING_SOURCE_LIST = URL + "user/marketing_sources";
    public static final String manageTenantsComments = URL + "user/tenant_comments";
    public static final String googleSignupTenantCB = URL + "admin/gapps/signupTenantCB";
    public static final String microsoftSignupTenantCB = URL + "admin/microsoft/signupTenantCB";
    public static final String GOOGLE_ACCESS_TOKEN = URL + "user/GoogleAccessToken/get";
    public static final String CONTACT_US = URL + "admin/contact_us";
    public static final String UPDATE_LEAD_STATUS = URL + "user/lead/status";
    public static final String MESSAGES_DELETE = URL + "messages/delete";
    public static final String DELETE_ACCOUNT = URL + "user/close_account";
    public static final String SET_PUBLIC_PROFILE = URL + "user/set_public_profile";
    public static final String RESOLVE_USER = URL + "admin/kamailio/resolve_user";
    public static final String RESEND_VERIFY_CODE_API = URL + "user/resend_verify_code";
    public static final String API_GET_STREAMING = URL + "user/music";
    public static final String SEND_SMS_API = URL + "phone/user/send_sms";
    public static final String SEND_FAX_API = URL + "messages/send_fax";
    public static final String NOTIFICATION_API = URL + "user/daily_notifications";

    //server id
    public static final String clientId3 = "887708581481-3n710msj5he0lta6naliu97pl33s50od.apps.googleusercontent.com";
    //android client id
    //old debug
//    public static final String clientId1 = "887708581481-31lgqh2o2lglulkcdhn5kufsj55vmft6.apps.googleusercontent.com";
    //new debug
    public static final String cIdDebug = "887708581481-4uvlb877pcdkpoaoujrc9ecdmg6iidor.apps.googleusercontent.com";
    //new release
    public static final String cIdRelease = "887708581481-enl55qkilclj6svadqmq3434l0o89233.apps.googleusercontent.com";

    public static String getClientId() {
        if (BuildConfig.DEBUG) {
            return cIdDebug;
        } else {
            return cIdRelease;
        }
    }

    public static String getMicrosoftClientId() {
        if (BuildConfig.DEBUG) {
            return "98a3d88d-8e4e-4e8f-8ef4-f2d4b03d72b2";
        } else {
            return "98a3d88d-8e4e-4e8f-8ef4-f2d4b03d72b2";
        }
    }
}