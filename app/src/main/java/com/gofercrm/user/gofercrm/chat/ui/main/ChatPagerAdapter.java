package com.gofercrm.user.gofercrm.chat.ui.main;

import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.util.SharedPreferenceData;

import org.jetbrains.annotations.NotNull;

public class ChatPagerAdapter extends FragmentPagerAdapter {

    private String[] tabTitles;

    private final Context mContext;
    public ConnectionFragment fragment;
    public static Intent intent;

    public ChatPagerAdapter(Context context, FragmentManager fm, Intent intent1) {
        super(fm);
        mContext = context;
        intent = intent1;
        tabTitles = new String[]{
                mContext.getResources().getString(R.string.connections),
                mContext.getResources().getString(R.string.rooms),
                mContext.getResources().getString(R.string.clients)};
    }

    @NotNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                //return RoomFragment.newInstance(0);
                fragment = ConnectionFragment.newInstance(0);
                return fragment;
            case 1:
                return RoomFragment.newInstance(1);
            //fragment =  ConnectionFragment.newInstance(1);
            //return fragment;
            case 2:
                return ClientFragment.newInstance(2);
            default:
                return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

    @Override
    public int getCount() {
        return SharedPreferenceData.getInstance(mContext).isLeadContactAccessAvailable() ? 3 : 2;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}