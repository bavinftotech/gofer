package com.gofercrm.user.gofercrm.chat.video.single;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.chat.video.group.openvcall.ui.CallActivity;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;

import org.json.JSONException;
import org.json.JSONObject;

public class InComingCallActivity extends BaseActivity {

    private static final String TAG = InComingCallActivity.class.getSimpleName();
    public static int fromGroup = 2, fromPersonal = 1;

    private String _NAME = "";
    private String _IMAGE = "";
    private int _CALL_TYPE = 0;
    private String _OPO_USER_ID;
    private String _LOGIN_USER_NAME;
    private String _CHANNEL_NAME;

    public static InComingCallActivity activity;
    private MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incoming_call);
        activity = this;
        getDataFromIntent();
        findViewById();
        playRawNotificationSound();
    }

    private void playRawNotificationSound() {
        mp = MediaPlayer.create(this, R.raw.phone_ring);
        mp.setLooping(true);
        mp.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mp != null) {
            mp.stop();
            mp.release();
            mp = null;
        }
    }

    private void findViewById() {
        TextView tvName = findViewById(R.id.tvName);
        tvName.setText(_NAME);

        ImageView ivUser = findViewById(R.id.ivUser);
        if (_IMAGE != null && !_IMAGE.equals(""))
            Glide.with(this).load(_IMAGE).into(ivUser);
    }

    private void getDataFromIntent() {
        Intent intent = getIntent();
        if (getIntent() != null) {
            _NAME = intent.getStringExtra("_NAME"); //For IncomingActivity _NAME represent the other caller
            _CALL_TYPE = intent.getIntExtra("_CALL_TYPE", 0);
            _IMAGE = intent.getStringExtra("_IMAGE");
            _OPO_USER_ID = intent.getStringExtra("_OPO_USER_ID");
            _LOGIN_USER_NAME = intent.getStringExtra("_LOGIN_USER_NAME");
            _CHANNEL_NAME = intent.getStringExtra("_CHANNEL_NAME");
        }
    }

    private void notifyOpoUserForVideoHangup() {
        //Some logic here for personal (1) or group (2) type call
        String url = Constants.SOCKET_NOTIFICATION;
        JSONObject postData = new JSONObject();
        try {
            postData.put("to_userid", _OPO_USER_ID);
            postData.put("send_as_websocket", true);
            postData.put("message_notification_type", "video_hangup");

            JSONObject subData = new JSONObject();
            subData.put("call_type", "personal");
            subData.put("name", _NAME); //For IncomingActivity _NAME represent the other caller
            subData.put("channel_name", _CHANNEL_NAME);
            postData.put("message_notification_payload", subData);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        LogUtils.Print(TAG, "postData -> " + postData.toString());
        NetworkManager.customJsonObjectRequest(
                getApplicationContext(), url, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtils.Print(TAG, "response -> " + response.toString());
                    }

                    @Override
                    public void onError(String message) {
                        Util.onMessage(InComingCallActivity.this, message);
                        LogUtils.Print(TAG, "message -> " + message);
                    }
                }, true);
    }

    public void onCallDecline(View view) {
        notifyOpoUserForVideoHangup();
        finish();
    }

    public void onCallAccept(View view) {
        if (_CALL_TYPE == fromPersonal) {
            Intent intent = new Intent(this, VideoChatViewActivity.class);
            intent.putExtras(getIntent());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, CallActivity.class);
            intent.putExtras(getIntent());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        finish();
    }

    public static void destroyActivity() {
        activity.runOnUiThread(() -> Util.makeToast("Call ended."));
        activity.finish();
    }
}