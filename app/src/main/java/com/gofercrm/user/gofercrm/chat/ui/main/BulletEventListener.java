package com.gofercrm.user.gofercrm.chat.ui.main;

public interface BulletEventListener {
    void onBulletEventReceived(int type, String text);
}