package com.gofercrm.user.gofercrm.chat.ui.main.model;

import com.gofercrm.user.gofercrm.chat.ui.main.model.Member;

import java.io.Serializable;
import java.util.List;

public class Room implements Serializable {

    private String _Id;
    private String name;
    private String desc;
    private Long updatedOn;
    private String phone;
    private boolean selfRemove;
    private boolean selfAdd;

    private List<Member> members;

    public String get_Id() {
        return _Id;
    }

    public void set_Id(String _Id) {
        this._Id = _Id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Long getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Long updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isSelfRemove() {
        return selfRemove;
    }

    public void setSelfRemove(boolean selfRemove) {
        this.selfRemove = selfRemove;
    }

    public boolean isSelfAdd() {
        return selfAdd;
    }

    public void setSelfAdd(boolean selfAdd) {
        this.selfAdd = selfAdd;
    }

    public List<Member> getMembers() {
        return members;
    }

    public void setMembers(List<Member> members) {
        this.members = members;
    }
}