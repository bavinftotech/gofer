package com.gofercrm.user.gofercrm.chat.video.group.openvcall.model;

public class User {
    public User(int uid, String name) {
        this.uid = uid;
        this.name = name;
    }

    public final int uid;
    public final String name;
}
