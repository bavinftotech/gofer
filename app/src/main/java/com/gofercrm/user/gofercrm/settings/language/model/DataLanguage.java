package com.gofercrm.user.gofercrm.settings.language.model;

public class DataLanguage {
    private String language;
    private String val;
    private boolean select;

    public DataLanguage(String language, String val, boolean select) {
        this.language = language;
        this.val = val;
        this.select = select;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }
}
