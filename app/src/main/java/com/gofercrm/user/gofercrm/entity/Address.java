package com.gofercrm.user.gofercrm.entity;

import java.io.Serializable;

public class Address implements Serializable {

    private String address, type;

    public Address(String address, String type) {
        this.address = address;
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
