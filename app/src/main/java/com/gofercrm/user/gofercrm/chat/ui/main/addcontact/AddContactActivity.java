package com.gofercrm.user.gofercrm.chat.ui.main.addcontact;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.chat.ui.main.addcontact.model.DataContact;
import com.gofercrm.user.gofercrm.chat.ui.main.model.ChatData;
import com.gofercrm.user.gofercrm.implementor.DialogButtonClickListener;
import com.gofercrm.user.gofercrm.profile.ProfileActivity;
import com.gofercrm.user.gofercrm.util.DialogUtils;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AddContactActivity extends BaseActivity implements AddContactAdapter.ViewHolder.ClickListener {

    private static final String TAG = "AddContactActivity";

    private EditText etSearch;
    private TextView tvInfo;
    private RecyclerView rv;
    private ProgressBar pb;

    //adapter
    private AddContactAdapter adp;
    private List<DataContact> data = new ArrayList<>();
    private SharedPreferences sharedPref;
    private boolean somethingUpdate = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);
        findViewById();
        main();
        setUpAdapterView();
    }

    private void findViewById() {
        tvInfo = findViewById(R.id.tvInfo);
        etSearch = findViewById(R.id.etSearch);
        rv = findViewById(R.id.rv);
        pb = findViewById(R.id.pb);
        sharedPref = getSharedPreferences("LOGIN", MODE_PRIVATE);
    }

    private void setUpAdapterView() {
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this));
        adp = new AddContactAdapter(this, data, this);
        rv.setAdapter(adp);
    }

    private void main() {
        etSearch.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                if (etSearch.getText().toString().trim().isEmpty()) {
                    Util.onMessage(AddContactActivity.this,
                            getResources().getString(R.string.search_keyword_empty_err));
                } else {
                    Util.hideSoftKeyboard(v);
                    getContacts();
                }
                return true;
            }
            return false;
        });
    }

    /**
     * RECYCLER VIEW ITEM CLICK
     *
     * @param position
     */
    @Override
    public void onItemClicked(int position, int type) {
        switch (type) {
            case 1:
                //add
                addUserInFriendList(position);
                break;
            case 0:
                //reject, withdraw, remove
                switch (data.get(position).getFriend()) {
                    case 0://Existing Friend
                        addUserInFriendList(position);
                        break;
                    case 1://Existing Friend
                        openConfirmationDialog(getResources().getString(R.string.disconnect_request_confirmation)
                                + " " + data.get(position).getName(), position, data.get(position).getFriend());
                        break;
                    case 2://Waiting Response on your connection request
                        openConfirmationDialog(getResources().getString(R.string.withdraw_request_confirmation), position, data.get(position).getFriend());
                        break;
                    case 3://Accept or Reject Incoming Request
                        openConfirmationDialog(getResources().getString(R.string.reject_request_confirmation), position, data.get(position).getFriend());
                        break;
                }
                break;
            case 2:
                goToProfile(position);
                break;
        }
    }

    private void goToProfile(int position) {
        Intent intent = new Intent(this, ProfileActivity.class);
        intent.putExtra("USER_ID", data.get(position).getId());
        startActivity(intent);
    }

    private void openConfirmationDialog(String strMsg, int position, int type) {
        DialogUtils.showConfirmationDialog(this,
                new DialogButtonClickListener() {
                    @Override
                    public void onPositiveButtonClick() {
                        removeAddUserFromList(position, type);
                    }

                    @Override
                    public void onNegativeButtonClick() {

                    }
                }, strMsg);
    }

    /**
     * @param position
     * @param type
     */
    private void removeAddUserFromList(int position, int type) {
        NetworkManager.customJsonObjectRequest(this,
                Constants.REMOVE_USER + "?userid=" + data.get(position).getId(), null,
                new VolleyResponseListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtils.Print("RESPONSE", response.toString());
                        try {
                            if (response.has("result") && Integer.parseInt(response.getString("result")) == 1) {
                                somethingUpdate = true;
                                data.get(position).setFriend(0);
                                adp.notifyDataSetChanged();
                            } else {
                                Util.onMessage(AddContactActivity.this, getResources().getString(R.string.server_err));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Util.onMessage(AddContactActivity.this, getResources().getString(R.string.server_err));
                        }
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("UPDATE PROFILE ERROR", message);
                    }
                }, false);
    }

    /**
     * CLOSE BUTTON CLICK
     *
     * @param view
     */
    public void onCloseButtonClick(View view) {
        Util.hideSoftKeyboard(etSearch);
        onBackPressed();
    }

    private void getContacts() {
        pb.setVisibility(View.VISIBLE);
        String params = "?token=" + sharedPref.getString("USER_TOKEN", "") + "&radius=6370&search_text=" + etSearch.getText().toString().trim() + "&page_size=10";
        NetworkManager.customJsonObjectGetRequest(
                this, Constants.SEARCH_USER + params, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        pb.setVisibility(View.GONE);
                        LogUtils.Print(TAG, "response --> " + response);
                        processData(response);
                    }

                    @Override
                    public void onError(String message) {
                        pb.setVisibility(View.GONE);
                        tvInfo.setText(getResources().getString(R.string.msg_add_connection_error));
                        tvInfo.setVisibility(View.VISIBLE);
                        rv.setVisibility(View.GONE);
                        Util.onMessage(AddContactActivity.this, message);
                    }
                }, false);
    }

    private void processData(JSONObject response) {
        try {
            if (Integer.parseInt(response.getString("result")) == 1) {
                data.clear();
                JSONArray users = response.getJSONArray("users");
                JSONObject result_obj = new JSONObject();
                for (int i = 0; i < users.length(); i++) {
                    result_obj = users.getJSONObject(i);
                    DataContact dataContact = new DataContact();
                    if (result_obj.has("id")) {
                        dataContact.setId(result_obj.getString("id"));
                    }
                    if (result_obj.has("name")) {
                        String name = result_obj.getString("name");
                        dataContact.setName(name);
                    }
                    if (result_obj.has("email")) {
                        String email = result_obj.getString("email");
                        dataContact.setEmail(email);
                    }
                    if (result_obj.has("phone")) {
                        String phone = result_obj.getString("phone");
                        dataContact.setPhone(phone);
                    }
                    if (result_obj.has("distance")) {
                        double distance = result_obj.getDouble("distance");
                        dataContact.setDistance(distance);
                    }
                    if (result_obj.has("avatar_url")) {
                        String name = result_obj.getString("avatar_url");
                        dataContact.setAvatarUrl(name);
                    }
                    if (result_obj.has("friend")) {
                        int friend = result_obj.getInt("friend");
                        dataContact.setFriend(friend);
                    }
                    data.add(dataContact);
                }
            }
            if (data.size() > 0) {
                tvInfo.setVisibility(View.GONE);
                rv.setVisibility(View.VISIBLE);
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    tvInfo.setText(Html.fromHtml(getResources().getString(R.string.msg_add_connection_error_)
                            .replace("XXX", etSearch.getText().toString().trim()), Html.FROM_HTML_MODE_COMPACT));
                } else {
                    tvInfo.setText(Html.fromHtml(getResources().getString(R.string.msg_add_connection_error_)
                            .replace("XXX", etSearch.getText().toString().trim())));
                }
                tvInfo.setVisibility(View.VISIBLE);
                rv.setVisibility(View.GONE);
            }
            adp.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param position
     */
    private void addUserInFriendList(int position) {
        pb.setVisibility(View.VISIBLE);
        NetworkManager.customJsonObjectRequest(this,
                Constants.ADD_USER + "?userid=" + data.get(position).getId(), null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        pb.setVisibility(View.GONE);
                        LogUtils.Print("RESPONSE", response.toString());
                        try {
                            if (response.has("result") && Integer.parseInt(response.getString("result")) == 1) {
                                somethingUpdate = true;
                                if (data.get(position).getFriend() == 3) {
                                    data.get(position).setFriend(1);
                                } else {
                                    data.get(position).setFriend(2);
                                }
                                adp.notifyDataSetChanged();
                            } else {
                                Util.onMessage(AddContactActivity.this, getResources().getString(R.string.server_err));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Util.onMessage(AddContactActivity.this, getResources().getString(R.string.server_err));
                        }
                    }

                    @Override
                    public void onError(String message) {
                        pb.setVisibility(View.GONE);
                        LogUtils.Print("UPDATE PROFILE ERROR", message);
                    }
                }, false);
    }

    @Override
    public void onBackPressed() {
        if (somethingUpdate) {
            setResult(RESULT_OK);
            finish();
        } else {
            super.onBackPressed();
        }
    }
}