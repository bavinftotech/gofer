package com.gofercrm.user.gofercrm.websocket;

import android.content.Context;

import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.SharedPreferenceData;
import com.gofercrm.user.gofercrm.websocket.listener.WsStatusListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.WebSocket;
import okio.ByteString;

public class WSSingleton {

    private static WebSocket mInstance;
    private static WsManager wsManager;
    private static Context mcontext;
    private static WsStatusListener wsStatusListener = new WsStatusListener() {
        @Override
        public void onOpen(Response response) {
            String loggedInUserID = SharedPreferenceData.getInstance(mcontext).getLoggedInUserID();
            String value = SharedPreferenceData.getInstance(mcontext).getClientConnectionID();
            JSONObject intro_data = new JSONObject();
            try {
                intro_data.put("type", "intro");
                intro_data.put("sender_id", loggedInUserID);
                intro_data.put("connection_id", value);
                WsManager ws = WSSingleton.getInstanceWM(mcontext);
                if (ws.isWsConnected()) {
                    ws.sendMessage(intro_data.toString());
                }
                SharedObserver.setSocketState(true);
                SharedObserver.setSocketObserver("{'type':'local_socket_state', 'value':'true'}");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onMessage(String text) {
            SharedObserver.setSocketObserver(text);
            //LogUtils.Print("WSSingleton onMessage:", text);
        }

        @Override
        public void onMessage(ByteString bytes) {
        }

        @Override
        public void onReconnect() {
            SharedObserver.setSocketState(true);
            SharedObserver.setSocketObserver("{'type':'local_socket_state', 'value':'true'}");
        }

        @Override
        public void onClosing(int code, String reason) {
        }

        @Override
        public void onClosed(int code, String reason) {
            SharedObserver.setSocketState(false);
            SharedObserver.setSocketObserver("{'type':'local_socket_state', 'value':'false'}");
        }

        @Override
        public void onFailure(Throwable t, Response response) {
            LogUtils.Print("FAILURE", "Socket Failed");
            SharedObserver.setSocketState(false);
            SharedObserver.setSocketObserver("{'type':'local_socket_state', 'value':'false'}");
        }
    };

    public static synchronized WsManager getInstanceWM(Context context) {
        mcontext = context;
        if (wsManager == null) {
            wsManager = new WsManager.Builder(context)
                    .client(
                            new OkHttpClient().newBuilder()
                                    .pingInterval(15, TimeUnit.SECONDS)
                                    .retryOnConnectionFailure(true)
                                    .build())
                    .needReconnect(true)
                    .wsUrl("wss://goferapi.ambivo.com/chat")
                    .build();
            wsManager.setWsStatusListener(wsStatusListener);
        }
        return wsManager;
    }
}