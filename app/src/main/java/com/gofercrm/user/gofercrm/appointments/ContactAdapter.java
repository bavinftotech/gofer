package com.gofercrm.user.gofercrm.appointments;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;

import com.gofercrm.user.gofercrm.R;
import com.tokenautocomplete.FilteredArrayAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class ContactAdapter extends FilteredArrayAdapter<Contact> {
    @LayoutRes
    private int layoutId;

    public ContactAdapter(Context context, @LayoutRes int layoutId, List<Contact> contact) {
        super(context, layoutId, contact);
        this.layoutId = layoutId;
    }

    @NotNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater l = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = l.inflate(layoutId, parent, false);
        }
        Contact p = getItem(position);
        ((TextView) convertView.findViewById(R.id.name)).setText(p.getName());
        ((TextView) convertView.findViewById(R.id.email)).setText(p.getEmail());
        return convertView;
    }

    @Override
    protected boolean keepObject(Contact person, String mask) {
        mask = mask.toLowerCase();
        return person.getName().toLowerCase().startsWith(mask) || person.getEmail().toLowerCase().startsWith(mask);
    }
}