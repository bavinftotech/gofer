package com.gofercrm.user.gofercrm.passwordmanagement;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.login.LoginActivity;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;

import org.json.JSONException;
import org.json.JSONObject;

public class ResetPasswordActivity extends BaseActivity {

    private EditText emailId;
    private TextView submit;
    private ProgressBar _RP_ProgressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        submit = findViewById(R.id.forgot_button);
        TextView back = findViewById(R.id.backToLoginBtn);
        _RP_ProgressBar = findViewById(R.id.rp_progressBar);
        emailId = findViewById(R.id.registered_emailid);
        submit.setOnClickListener(view -> submitButtonTask());
        back.setOnClickListener(view -> {
            Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
            loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(loginIntent);
            fileList();
        });
    }

    private void submitButtonTask() {
        String emailId_value = emailId.getText().toString();
        if (!emailId_value.isEmpty() && Util.isValidEmail(emailId_value)) {
            submit.setVisibility(View.GONE);
            _RP_ProgressBar.setVisibility(View.VISIBLE);
            sendResetPasswordInstruction(emailId_value);
            emailId.setError(null);
        } else {
            emailId.setError(getResources().getString(R.string.email_invalid));
            Util.onMessage(getApplicationContext(), getResources().getString(R.string.email_invalid));
        }
    }

    private void sendResetPasswordInstruction(String email) {
        String url = Constants.FORGOT_PASSWORD_URL + "?email=" + email;
        NetworkManager.customJsonObjectRequest(
                ResetPasswordActivity.this, url, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processResult(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("Reset Password Error", message);
                        submit.setVisibility(View.VISIBLE);
                        _RP_ProgressBar.setVisibility(View.GONE);
                    }
                }, false);
    }

    private void processResult(JSONObject response) {
        try {
            if (Integer.parseInt(response.getString("result")) == 1) {
                Util.onMessage(getApplicationContext(), getResources().getString(R.string.reset_email_sent_success));
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } else {
                submit.setVisibility(View.VISIBLE);
                _RP_ProgressBar.setVisibility(View.GONE);
                Util.onMessage(getApplicationContext(), getResources().getString(R.string.reset_email_sent_fail));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}