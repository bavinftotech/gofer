package com.gofercrm.user.gofercrm.comments;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.entity.Comment;
import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.util.LogUtils;

public class WebViewActivity extends BaseActivity {

    private static final String TAG = "WebViewActivity";

    private Comment comment;
    private WebView wView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview_attachments);
        findViewById();
        getDataFromIntent();
        setUpHeaderView();
    }

    private void findViewById() {
        wView = findViewById(R.id.web_view);
    }

    private void getDataFromIntent() {
        if (getIntent() != null && getIntent().hasExtra("DATA")) {
            comment = (Comment) getIntent().getSerializableExtra("DATA");
        }
    }

    private void setUpHeaderView() {
        wView.setWebViewClient(new MyBrowser());
        wView.getSettings().setLoadsImagesAutomatically(true);
        wView.getSettings().setJavaScriptEnabled(true);
        wView.getSettings().setLoadWithOverviewMode(true);
        wView.getSettings().setUseWideViewPort(true);
        wView.getSettings().setAllowFileAccess(true);

        String strType = comment.getContent_type();
        String strURL = comment.getContent_type();
        if (strType.startsWith("txt")) {
            strURL = "" + comment.getUrl();
        } else if (strType.contains("pdf")) {
            strURL = "http://drive.google.com/viewerng/viewer?embedded=true&url=" + comment.getUrl();
        } else if (strType.equalsIgnoreCase("doc") ||
                strType.equalsIgnoreCase("docx") ||
                strType.equalsIgnoreCase("docs") ||
                strType.equalsIgnoreCase("xls") ||
                strType.equalsIgnoreCase("xlsx") ||
                strType.equalsIgnoreCase("csv") ||
                strType.equalsIgnoreCase("ppt") ||
                strType.equalsIgnoreCase("pptx") ||
                strType.equalsIgnoreCase("pps") ||
                strType.contains("sheet") ||
                strType.contains("ms-excel") ||
                strType.contains("msword") ||
                strType.contains("document") ||
                strType.contains("powerpoint")) {
            strURL = "http://docs.google.com/gview?embedded=true&url=" + comment.getUrl();
        }
        LogUtils.Print(TAG, "strURL -> " + strURL);
        wView.loadUrl(strURL);
    }

    private class MyBrowser extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            showProgress();
        }

        @SuppressWarnings("deprecation")
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            final Uri uri = Uri.parse(url);
            view.loadUrl(url);
            return true;
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            return shouldOverrideUrlLoading(view, request.getUrl().toString());
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            hideProgress();
        }
    }

    @Override
    public void onPause() {
        wView.onPause();
        wView.pauseTimers();
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        wView.resumeTimers();
        wView.onResume();
    }

    @Override
    protected void onDestroy() {
        wView.destroy();
        wView = null;
        super.onDestroy();
    }
}