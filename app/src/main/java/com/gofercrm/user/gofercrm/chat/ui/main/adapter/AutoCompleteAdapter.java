package com.gofercrm.user.gofercrm.chat.ui.main.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.chat.ui.main.model.AllLanguages;

import java.util.ArrayList;

public class AutoCompleteAdapter extends ArrayAdapter<AllLanguages> {
    private ArrayList<AllLanguages> items;
    private ArrayList<AllLanguages> itemsAll;
    private ArrayList<AllLanguages> suggestions;
    private int viewResourceId;
    private int spaceLR,spaceTB;

    public AutoCompleteAdapter(Context context, int viewResourceId, ArrayList<AllLanguages> items) {
        super(context, viewResourceId, items);
        this.items = items;
        spaceTB = (int) context.getResources().getDimension(R.dimen.space5);
        spaceLR = (int) context.getResources().getDimension(R.dimen.space15);
        this.itemsAll = (ArrayList<AllLanguages>) items.clone();
        this.suggestions = new ArrayList<>();
        this.viewResourceId = viewResourceId;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(viewResourceId, null);
            v.setPadding(spaceLR, spaceTB, spaceLR, spaceTB);
        }
        AllLanguages customer = items.get(position);
        if (customer != null) {
            TextView customerNameLabel = v.findViewById(android.R.id.text1);
            if (customerNameLabel != null) {
                customerNameLabel.setText(customer.getEnglish());
                if (customer.isSelect()) {
                    customerNameLabel.setTypeface(null, Typeface.BOLD);
                } else {
                    customerNameLabel.setTypeface(null, Typeface.NORMAL);
                }
            }
        }
        return v;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            String str = ((AllLanguages) (resultValue)).getEnglish();
            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (AllLanguages customer : itemsAll) {
                    if (customer.getEnglish().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                        suggestions.add(customer);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<AllLanguages> filteredList = (ArrayList<AllLanguages>) results.values;
            if (results != null && results.count > 0) {
                clear();
                for (AllLanguages c : filteredList) {
                    add(c);
                }
                notifyDataSetChanged();
            }
        }
    };

}