package com.gofercrm.user.gofercrm.account;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.R;

public class AccountGeneralFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";

    public AccountGeneralFragment() {
    }

    public static AccountGeneralFragment newInstance(int sectionNumber) {
        AccountGeneralFragment fragment = new AccountGeneralFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rv = inflater.inflate(R.layout.general_tab_lead_view, container, false);
        AccountViewActivity activity = (AccountViewActivity) getActivity();
        RecyclerView recyclerView = rv.findViewById(R.id.lead_view_recycle);

        AccountGeneralAdapter aGAdapter = new AccountGeneralAdapter(activity.getData(), getContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(aGAdapter);
        return rv;
    }
}