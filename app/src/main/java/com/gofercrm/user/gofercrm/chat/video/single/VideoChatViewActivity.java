package com.gofercrm.user.gofercrm.chat.video.single;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.fcm.FireMessagingService;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.gofercrm.user.gofercrm.websocket.WsManager;

import org.json.JSONException;
import org.json.JSONObject;

import io.agora.rtc.IRtcEngineEventHandler;
import io.agora.rtc.RtcEngine;
import io.agora.rtc.video.VideoCanvas;
import io.agora.rtc.video.VideoEncoderConfiguration;

public class VideoChatViewActivity extends BaseActivity {
    private static final String TAG = VideoChatViewActivity.class.getSimpleName();

    private static final int PERMISSION_REQ_ID = 22;

    // Permission WRITE_EXTERNAL_STORAGE is not mandatory
    // for Agora RTC SDK, just in case if you wanna save
    // logs to external sdcard.
    private static final String[] REQUESTED_PERMISSIONS = {
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private RtcEngine mRtcEngine;
    private boolean mCallEnd;
    private boolean mMuted;

    private FrameLayout mLocalContainer;
    private RelativeLayout mRemoteContainer;
    private SurfaceView mLocalView;
    private SurfaceView mRemoteView;

    private boolean mVideoEnabled = true;
    private ImageView btn_video;
    private ImageView mCallBtn;
    private ImageView mMuteBtn;
    private ImageView mSwitchCameraBtn;

    Handler handler = new Handler(Looper.getMainLooper());
    int iterationCount = 0;
    private Runnable runnableCode = new Runnable() {
        @Override
        public void run() {
            if (iterationCount > 20) {
                handler.removeCallbacks(this);
                onCallClicked(mCallBtn);
                return;
            }
            iterationCount++;
            // Do something here on the main thread
            LogUtils.Print("Handlers", "Called on main thread");
            handler.postDelayed(this, 1000);
        }
    };

    /**
     * Event handler registered into RTC engine for RTC callbacks.
     * Note that UI operations needs to be in UI thread because RTC
     * engine deals with the events in a separate thread.
     */
    private final IRtcEngineEventHandler mRtcEventHandler = new IRtcEngineEventHandler() {
        @Override
        public void onJoinChannelSuccess(String channel, final int uid, int elapsed) {
            runOnUiThread(() -> {
                // Start the initial runnable task by posting through the handler
                runnableCode.run();
                LogUtils.Print(TAG, "onJoinChannelSuccess");
            });
        }

        @Override
        public void onRemoteVideoStateChanged(final int uid, int state, int reason, int elapsed) {
            super.onRemoteVideoStateChanged(uid, state, reason, elapsed);
            runOnUiThread(() -> setupRemoteVideo(uid));
        }

        @Override
        public void onUserJoined(int uid, int elapsed) {
            super.onUserJoined(uid, elapsed);
            LogUtils.Print(TAG, "onUserJoined");
            handler.removeCallbacks(runnableCode);
        }

        /*@Override
        public void onFirstRemoteVideoDecoded(final int uid, int width, int height, int elapsed) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    setupRemoteVideo(uid);
                }
            });
        }*/

        @Override
        public void onUserOffline(final int uid, int reason) {
            runOnUiThread(() -> onRemoteUserLeft());
        }
    };

    private void setupRemoteVideo(int uid) {
        // Only one remote video view is available for this
        // tutorial. Here we check if there exists a surface
        // view tagged as this uid.
        int count = mRemoteContainer.getChildCount();
        View view = null;
        for (int i = 0; i < count; i++) {
            View v = mRemoteContainer.getChildAt(i);
            if (v.getTag() instanceof Integer && ((int) v.getTag()) == uid) {
                view = v;
            }
        }

        if (view != null) {
            return;
        }

        mRemoteView = RtcEngine.CreateRendererView(getBaseContext());
        mRemoteContainer.addView(mRemoteView);
        mRtcEngine.setupRemoteVideo(new VideoCanvas(mRemoteView, VideoCanvas.RENDER_MODE_HIDDEN, uid));
        mRemoteView.setTag(uid);
    }

    private void onRemoteUserLeft() {
        removeRemoteVideo();
    }

    private void removeRemoteVideo() {
        if (mRemoteView != null) {
            mRemoteContainer.removeView(mRemoteView);
        }
        mRemoteView = null;
        finish();
    }

    private String _NAME = "";
    private String _LOGIN_USER_NAME = "";
    private String _USER_ID = "";
    private String _OPO_USER_ID = "";
    private String _CHANNEL_NAME = "";
    private int _FROM = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_chat_view);
        getDataFromIntent();
        initUI();

        // Ask for permissions at runtime.
        // This is just an example set of permissions. Other permissions
        // may be needed, and please refer to our online documents.
        if (checkSelfPermission(REQUESTED_PERMISSIONS[0], PERMISSION_REQ_ID) &&
                checkSelfPermission(REQUESTED_PERMISSIONS[1], PERMISSION_REQ_ID) &&
                checkSelfPermission(REQUESTED_PERMISSIONS[2], PERMISSION_REQ_ID)) {
            initEngineAndJoinChannel();
        }
        WsManager.updateActivity(this);
        FireMessagingService.updateActivity(this);
    }

    private void getDataFromIntent() {
        Intent intent = getIntent();
        if (getIntent() != null) {
            _NAME = intent.getStringExtra("_NAME");
            _USER_ID = intent.getStringExtra("_USER_ID");
            _OPO_USER_ID = intent.getStringExtra("_OPO_USER_ID");
            _CHANNEL_NAME = intent.getStringExtra("_CHANNEL_NAME");
            _FROM = intent.getIntExtra("_FROM", 0);
            _LOGIN_USER_NAME = intent.getStringExtra("_LOGIN_USER_NAME");
        }
    }

    private void initUI() {
        mLocalContainer = findViewById(R.id.local_video_view_container);
        mRemoteContainer = findViewById(R.id.remote_video_view_container);

        btn_video = findViewById(R.id.btn_video);
        mCallBtn = findViewById(R.id.btn_call);
        mMuteBtn = findViewById(R.id.btn_mute);
        mSwitchCameraBtn = findViewById(R.id.btn_switch_camera);
    }

    private boolean checkSelfPermission(String permission, int requestCode) {
        if (ContextCompat.checkSelfPermission(this, permission) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, REQUESTED_PERMISSIONS, requestCode);
            return false;
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQ_ID) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED ||
                    grantResults[1] != PackageManager.PERMISSION_GRANTED ||
                    grantResults[2] != PackageManager.PERMISSION_GRANTED) {
                showLongToast(getResources().getString(R.string.need_permissions) + " " +
                        Manifest.permission.RECORD_AUDIO + "/" + Manifest.permission.CAMERA + "/" +
                        Manifest.permission.WRITE_EXTERNAL_STORAGE);
                finish();
                return;
            }

            // Here we continue only if all permissions are granted.
            // The permissions can also be granted in the system settings manually.
            initEngineAndJoinChannel();
        }
    }

    private void showLongToast(final String msg) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void initEngineAndJoinChannel() {
        // This is our usual steps for joining
        // a channel and starting a call.
        if (_FROM == 1)
            notifyOpoUserForVideoCall();
        initializeEngine();
        setupVideoConfig();
        setupLocalVideo();
        joinChannel();
    }

    private void notifyOpoUserForVideoCall() {
        String url = Constants.SOCKET_NOTIFICATION;
        JSONObject postData = new JSONObject();
        try {
            postData.put("to_userid", _OPO_USER_ID);
            postData.put("message_notification_type", "video_voip");

            JSONObject subData = new JSONObject();
            subData.put("call_type", "personal");
            subData.put("name", _LOGIN_USER_NAME);
            subData.put("channel_name", _CHANNEL_NAME);
            postData.put("message_notification_payload", subData);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        LogUtils.Print(TAG, "postData -> " + postData.toString());
        NetworkManager.customJsonObjectRequest(
                getApplicationContext(), url, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtils.Print(TAG, "response -> " + response.toString());
                    }

                    @Override
                    public void onError(String message) {
                        Util.onMessage(VideoChatViewActivity.this, message);
                        LogUtils.Print(TAG, "message -> " + message);
                    }
                }, true);
    }

    private void notifyOpoUserForVideoHangup() {
        String url = Constants.SOCKET_NOTIFICATION;
        JSONObject postData = new JSONObject();
        try {
            postData.put("to_userid", _OPO_USER_ID);
            postData.put("send_as_websocket", true);
            postData.put("message_notification_type", "video_hangup");

            JSONObject subData = new JSONObject();
            subData.put("call_type", "personal");
            subData.put("name", _LOGIN_USER_NAME);
            subData.put("channel_name", _CHANNEL_NAME);
            postData.put("message_notification_payload", subData);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        LogUtils.Print(TAG, "postData -> " + postData.toString());
        NetworkManager.customJsonObjectRequest(
                getApplicationContext(), url, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtils.Print(TAG, "response -> " + response.toString());
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print(TAG, "message -> " + message);
                    }
                }, true);
    }

    private void initializeEngine() {
        try {
            mRtcEngine = RtcEngine.create(getBaseContext(),
                    getString(R.string.agora_app_id),
                    mRtcEventHandler);
        } catch (Exception e) {
            LogUtils.Print(TAG, Log.getStackTraceString(e));
            throw new RuntimeException("NEED TO check rtc sdk init fatal error\n" + Log.getStackTraceString(e));
        }
    }

    private void setupVideoConfig() {
        // In simple use cases, we only need to enable video capturing
        // and rendering once at the initialization step.
        // Note: audio recording and playing is enabled by default.
        mRtcEngine.enableVideo();

        // Please go to this page for detailed explanation
        // https://docs.agora.io/en/Video/API%20Reference/java/classio_1_1agora_1_1rtc_1_1_rtc_engine.html#af5f4de754e2c1f493096641c5c5c1d8f
        mRtcEngine.setVideoEncoderConfiguration(new VideoEncoderConfiguration(
                VideoEncoderConfiguration.VD_640x360,
                VideoEncoderConfiguration.FRAME_RATE.FRAME_RATE_FPS_15,
                VideoEncoderConfiguration.STANDARD_BITRATE,
                VideoEncoderConfiguration.ORIENTATION_MODE.ORIENTATION_MODE_FIXED_PORTRAIT));
    }

    private void setupLocalVideo() {
        // This is used to set a local preview.
        // The steps setting local and remote view are very similar.
        // But note that if the local user do not have a uid or do
        // not care what the uid is, he can set his uid as ZERO.
        // Our server will assign one and return the uid via the event
        // handler callback function (onJoinChannelSuccess) after
        // joining the channel successfully.
        mLocalView = RtcEngine.CreateRendererView(getBaseContext());
        mLocalView.setZOrderMediaOverlay(true);
        mLocalContainer.addView(mLocalView);
        mRtcEngine.setupLocalVideo(new VideoCanvas(mLocalView, VideoCanvas.RENDER_MODE_HIDDEN, 0));
    }

    private void joinChannel() {
        // 1. Users can only see each other after they join the
        // same channel successfully using the same app id.
        // 2. One token is only valid for the channel name that
        // you use to generate this token.
        String token = getString(R.string.agora_access_token);
        if (TextUtils.isEmpty(token) || TextUtils.equals(token, "#YOUR ACCESS TOKEN#")) {
            token = null; // default, no token
        }
        mRtcEngine.joinChannel(token, _CHANNEL_NAME, "", 0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (handler != null && runnableCode != null)
            handler.removeCallbacks(runnableCode);
        if (!mCallEnd) {
            leaveChannel();
        }
        RtcEngine.destroy();
    }

    private void leaveChannel() {
        notifyOpoUserForVideoHangup();
        mRtcEngine.leaveChannel();
    }

    public void onLocalAudioMuteClicked(View view) {
        mMuted = !mMuted;
        mRtcEngine.muteLocalAudioStream(mMuted);
        int res = mMuted ? R.drawable.btn_mute : R.drawable.btn_unmute;
        mMuteBtn.setImageResource(res);
    }

    public void onSwitchCameraClicked(View view) {
        mRtcEngine.switchCamera();
    }

    public void onCallClicked(View view) {
        if (mCallEnd) {
            startCall();
            mCallEnd = false;
            mCallBtn.setImageResource(R.drawable.btn_endcall);
        } else {
            endCall();
            mCallEnd = true;
//            mCallBtn.setImageResource(R.drawable.btn_startcall);
        }
        showButtons(!mCallEnd);
    }

    public void onLocalVideoStateClicked(View view) {
        mVideoEnabled = !mVideoEnabled;
        mRtcEngine.enableLocalVideo(mVideoEnabled);
        int res = mVideoEnabled ? R.drawable.ic_baseline_videocam_24 :
                R.drawable.ic_baseline_videocam_off_24;
        btn_video.setImageResource(res);
    }

    private void startCall() {
        setupLocalVideo();
        joinChannel();
    }

    private void endCall() {
        removeLocalVideo();
        removeRemoteVideo();
        leaveChannel();
    }

    private void removeLocalVideo() {
        if (mLocalView != null) {
            mLocalContainer.removeView(mLocalView);
        }
        mLocalView = null;
    }

    private void showButtons(boolean show) {
        int visibility = show ? View.VISIBLE : View.GONE;
        mMuteBtn.setVisibility(visibility);
        mSwitchCameraBtn.setVisibility(visibility);
        btn_video.setVisibility(visibility);
    }
}