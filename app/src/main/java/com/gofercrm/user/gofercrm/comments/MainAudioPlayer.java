package com.gofercrm.user.gofercrm.comments;

import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.jean.jcplayer.JcPlayerManagerListener;
import com.example.jean.jcplayer.general.JcStatus;
import com.example.jean.jcplayer.general.errors.OnInvalidPathListener;
import com.example.jean.jcplayer.model.JcAudio;
import com.example.jean.jcplayer.view.JcPlayerView;
import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.R;

import java.util.ArrayList;

public class MainAudioPlayer extends BaseActivity
        implements OnInvalidPathListener, JcPlayerManagerListener {

    private JcPlayerView player;
    private String strURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_player);
        getDataFromIntent();
        setUpAudioPlayer();
    }

    private void getDataFromIntent() {
        strURL = getIntent().getStringExtra("DATA");
    }

    private void setUpAudioPlayer() {
        player = findViewById(R.id.jcplayer);

        ArrayList<JcAudio> jcAudios = new ArrayList<>();
        jcAudios.add(JcAudio.createFromURL("AUDIO", strURL));

        player.initPlaylist(jcAudios, this);
        player.playAudio(player.getMyPlaylist().get(0));
    }

    @Override
    protected void onStop() {
        super.onStop();
        player.createNotification();
    }

    @Override
    public void onPause() {
        super.onPause();
        player.createNotification();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        player.kill();
    }

    @Override
    public void onPathError(JcAudio jcAudio) {
        Toast.makeText(this, jcAudio.getPath() + " "+getResources().getString(R.string.with_problems), Toast.LENGTH_LONG).show();
//        player.removeAudio(jcAudio);
//        player.next();
    }


    @Override
    public void onPreparedAudio(JcStatus status) {
        player.onPlaying(status);
    }

    @Override
    public void onCompletedAudio() {
        finish();
    }

    @Override
    public void onPaused(JcStatus status) {

    }

    @Override
    public void onContinueAudio(JcStatus status) {

    }

    @Override
    public void onPlaying(JcStatus status) {

    }

    @Override
    public void onTimeChanged(@NonNull JcStatus status) {
//        updateProgress(status);
    }

    @Override
    public void onJcpError(@NonNull Throwable throwable) {
        Toast.makeText(this, throwable.getMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStopped(JcStatus status) {

    }
}