package com.gofercrm.user.gofercrm.task.view.tab.general;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.telephony.PhoneNumberUtils;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.lead.LeadOwnerBSFragment;
import com.gofercrm.user.gofercrm.task.DataTaskView;
import com.gofercrm.user.gofercrm.util.Util;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class TaskGeneralAdapter extends RecyclerView.Adapter<TaskGeneralAdapter.MyViewHolder> {

    private LeadOwnerBSFragment leadOwnerBSFragment;
    private List<DataTaskView> leadPageElements;
    private Context ctx;

    public TaskGeneralAdapter(List<DataTaskView> leadViewList, Context ctx) {
        setHasStableIds(true);
        this.leadPageElements = leadViewList;
        this.ctx = ctx;
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lead_view_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final DataTaskView lead = leadPageElements.get(position);
        holder.label.setText(lead.getLabel());
        if (lead.getType().equals("PHONE") && !lead.getValue().isEmpty()) {
            holder.value.setText(PhoneNumberUtils.formatNumber(lead.getValue(), "US"));
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                holder.value.setText(Html.fromHtml(lead.getValue(), Html.FROM_HTML_MODE_COMPACT));
            } else {
                holder.value.setText(Html.fromHtml(lead.getValue()));
            }
        }
        if (lead.getLeftDrawable() != 0) {
            holder.leftImage.setImageResource(lead.getLeftDrawable());
        }
        if (lead.getRightDrawable() != 0) {
            holder.rightImage.setImageResource(lead.getRightDrawable());
        }
        holder.itemView.setOnClickListener(view -> {
            if (lead.getType().equals("PHONE") && !lead.getValue().isEmpty()) {
                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.setData(Uri.parse("sms:" + lead.getValue()));
                sendIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ctx.startActivity(sendIntent);
            } else if (lead.getType().equals("EMAIL") && !lead.getValue().isEmpty()) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                Uri data = Uri.parse("mailto:" + lead.getValue());
                intent.setData(data);
                ctx.startActivity(intent);
            } else if (lead.getType().equals("ADDRESS") && !lead.getValue().isEmpty()) {
                Uri gmmIntentUri = Uri.parse("google.navigation:q=" + lead.getValue());
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mapIntent.setPackage("com.google.android.apps.maps");
                ctx.startActivity(mapIntent);
//            } else if (lead.getLabel().equals("Lead Owner") && !lead.getValue().isEmpty()) {
//                FragmentManager fm = ((AppCompatActivity) ctx).getSupportFragmentManager();
//                leadOwnerBSFragment = new LeadOwnerBSFragment("LEAD", lead.getId());
//                leadOwnerBSFragment.setCurrentObj(leadOwnerBSFragment);
//                leadOwnerBSFragment.show(fm, leadOwnerBSFragment.getTag());
            }
        });

        holder.itemView.setOnLongClickListener(view -> {
            if (lead.getType().equals("PHONE") && !lead.getValue().isEmpty()) {
                Util.copyDataOnClipboard(ctx, lead.getValue());
                Util.onMessage(ctx, ctx.getResources().getString(R.string.phone_number_copied));
            } else if (lead.getType().equals("EMAIL") && !lead.getValue().isEmpty()) {
                Util.copyDataOnClipboard(ctx, lead.getValue());
                Util.onMessage(ctx, ctx.getResources().getString(R.string.email_copied));
            }
            return true;
        });
    }

    @Override
    public int getItemCount() {
        return leadPageElements.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView label, value;
        public ImageView leftImage, rightImage;

        public MyViewHolder(View view) {
            super(view);
            label = view.findViewById(R.id.leadView_label_id);
            value = view.findViewById(R.id.lead_view_data_id);
            leftImage = view.findViewById(R.id.lead_img_btn_left);
            rightImage = view.findViewById(R.id.lead_img_btn_right);
        }
    }
}