package com.gofercrm.user.gofercrm.opportunities;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.util.Util;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.List;

public class OpportunityListAdapter extends RecyclerView.Adapter<OpportunityListAdapter.OpportunityListViewHolder> {

    private List<Opportunity> opportunities;
    private Context ctx;

    public OpportunityListAdapter(List<Opportunity> opportunities, Context ctx) {
        setHasStableIds(true);
        this.opportunities = opportunities;
        this.ctx = ctx;
    }

    @NotNull
    @Override
    public OpportunityListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.content_opportunity, parent, false);
        return new OpportunityListViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(OpportunityListViewHolder holder, final int position) {
        final Opportunity _oppor = opportunities.get(position);
        String name = Util.capitalize(_oppor.getName());
        holder.name.setText(name);
        holder.status.setText(_oppor.getStage());
        if (_oppor.getStage().equals("closed_won")) {
            holder.won.setImageDrawable(ctx.getResources().getDrawable(R.drawable.thumb_up_outline));
            holder.won.setColorFilter(ContextCompat.getColor(ctx, R.color.green));
        } else if (_oppor.getStage().equals("closed_lost")) {
            holder.won.setImageDrawable(ctx.getResources().getDrawable(R.drawable.thumb_down_outline));
            holder.won.setColorFilter(ContextCompat.getColor(ctx, R.color.rejected));
        } else {
            holder.won.setImageDrawable(ctx.getResources().getDrawable(R.drawable.checkbox_blank_circle_outline));
            holder.won.setColorFilter(ContextCompat.getColor(ctx, R.color.colorPrimaryDark));
        }
        holder.itemView.setOnClickListener(view -> {
            Intent intent = new Intent(ctx, OpportunityViewActivity.class);
            intent.putExtra("OPPOR_ID", (Serializable) _oppor.getId());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            ctx.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return opportunities.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public class OpportunityListViewHolder extends RecyclerView.ViewHolder {
        public TextView name, status;
        public ImageView won, lost, edit;

        public OpportunityListViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.opportunity_title_id);
            status = view.findViewById(R.id.opportunity_status_id);
            won = view.findViewById(R.id.opportunity_btn_won);
        }
    }
}