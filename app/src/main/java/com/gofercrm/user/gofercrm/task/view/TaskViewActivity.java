package com.gofercrm.user.gofercrm.task.view;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.implementor.DialogButtonClickListener;
import com.gofercrm.user.gofercrm.task.DataTaskView;
import com.gofercrm.user.gofercrm.task.NewTaskActivity;
import com.gofercrm.user.gofercrm.task.Task;
import com.gofercrm.user.gofercrm.util.DialogUtils;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class TaskViewActivity extends BaseActivity {

    ArrayList<DataTaskView> dataTaskList = new ArrayList<>();
    SharedPreferences sharedPref;
    String LOGIN_USER_ID = "";
    private FloatingActionButton fbEditTask;

    public static Task task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_view);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        sharedPref = getSharedPreferences("LOGIN", MODE_PRIVATE);
        LOGIN_USER_ID = sharedPref.getString("USER_ID", "");
//        getDataFromIntent();
        renderView();
    }

//    private void getDataFromIntent() {
//        if (getIntent() != null && getIntent().hasExtra("DATA")) {
//            task = (Task) getIntent().getSerializableExtra("DATA");
//        }
//    }

    public ArrayList<DataTaskView> getData() {
        return dataTaskList;
    }

    private MenuItem menuStatus;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_task_details, menu);
        menuStatus = menu.findItem(R.id.action_complete);
        setMenuStatus();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_complete) {
            if (!task.getStatus().equals("completed")) {
                DialogUtils.showConfirmationDialog(this, new DialogButtonClickListener() {
                    @Override
                    public void onPositiveButtonClick() {
                        createTask();
                    }

                    @Override
                    public void onNegativeButtonClick() {

                    }
                }, getResources().getString(R.string.complete_task_confirmation));
            }
        } else if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void createTask() {
        showProgress();
        JSONObject postData = new JSONObject();
        try {
            postData.put("action", "update");
            postData.put("user_task_id", task.getTaskId());
            JSONObject payload = new JSONObject();
            try {
                payload.put("status", "completed");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            postData.put("payload", payload);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        NetworkManager.customJsonObjectRequest(
                this, Constants.TASK_LIST_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideProgress();
                        setResult(RESULT_OK);
                        finish();
                    }

                    @Override
                    public void onError(String message) {
                        hideProgress();
                        Util.onMessage(TaskViewActivity.this, message);
                    }
                }, true);
    }

    private void setMenuStatus() {
        if (task != null && task.getStatus() != null && task.getStatus().equals("completed")) {
            if (menuStatus != null)
                menuStatus.setIcon(R.drawable.ic_check_box_true_white);
        } else {
            if (menuStatus != null)
                menuStatus.setIcon(R.drawable.ic_check_box_false_white);
        }
    }

    private void renderView() {
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(task.getName());
        TaskViewPagerAdapter taskViewPagerAdapter = new TaskViewPagerAdapter(getSupportFragmentManager(), task);
        ViewPager mViewPager = findViewById(R.id.leadView_viewPager_id);
        mViewPager.setAdapter(taskViewPagerAdapter);

        fbEditTask = findViewById(R.id.fbEditTask);
        fbEditTask.setOnClickListener(view -> {
            Intent intent = new Intent(this, NewTaskActivity.class);
            intent.putExtra("TASK_ID", task.getTaskId());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        });

        TabLayout tabLayout = findViewById(R.id.leadView_tabs_id);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout) {
            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        fbEditTask.show();
                        break;
                    case 1:
                        fbEditTask.hide();
                        break;
                }
            }
        });
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        setMenuStatus();
        DataTaskView data;
        data = new DataTaskView(getResources().getString(R.string.task_name), task.getName(), "", 0, 0);
        dataTaskList.add(data);

        data = new DataTaskView(getResources().getString(R.string.description), task.getDescription(), "", 0, 0);
        dataTaskList.add(data);

        String pattern = "MMM dd, yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String due_date = simpleDateFormat.format(new Date(Long.parseLong(task.getDueDate().toString()) * 1000));
        data = new DataTaskView(getResources().getString(R.string.due_date), due_date, "", 0, 0);
        dataTaskList.add(data);

        data = new DataTaskView(getResources().getString(R.string.assign_to), task.getName(), "", 0, 0);
        dataTaskList.add(data);

        data = new DataTaskView(getResources().getString(R.string.priority_),
                task.getPriority() == 1 ? getResources().getString(R.string.high) :
                        task.getPriority() == 2 ? getResources().getString(R.string.medium) :
                                getResources().getString(R.string.low),
                "", 0, 0);
        dataTaskList.add(data);

        data = new DataTaskView(getResources().getString(R.string.status_cap), task.getStatus(), "", 0, 0);
        dataTaskList.add(data);
    }
}