package com.gofercrm.user.gofercrm.streaming.ui.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class DataSteaming implements Parcelable {

	@SerializedName("country")
	private String country;

	@SerializedName("static_location_coords")
	private List<Double> staticLocationCoords;

	@SerializedName("distance")
	private int distance;

	@SerializedName("city")
	private String city;

	@SerializedName("logo_url")
	private String logoUrl;

	@SerializedName("channel")
	private String channel;

	@SerializedName("id")
	private String id;

	@SerializedName("url")
	private String url;

	@SerializedName("ts")
	private int ts;

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public List<Double> getStaticLocationCoords() {
		return staticLocationCoords;
	}

	public void setStaticLocationCoords(List<Double> staticLocationCoords) {
		this.staticLocationCoords = staticLocationCoords;
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getTs() {
		return ts;
	}

	public void setTs(int ts) {
		this.ts = ts;
	}


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.country);
		dest.writeList(this.staticLocationCoords);
		dest.writeInt(this.distance);
		dest.writeString(this.city);
		dest.writeString(this.logoUrl);
		dest.writeString(this.channel);
		dest.writeString(this.id);
		dest.writeString(this.url);
		dest.writeInt(this.ts);
	}

	public DataSteaming() {
	}

	protected DataSteaming(Parcel in) {
		this.country = in.readString();
		this.staticLocationCoords = new ArrayList<Double>();
		in.readList(this.staticLocationCoords, Double.class.getClassLoader());
		this.distance = in.readInt();
		this.city = in.readString();
		this.logoUrl = in.readString();
		this.channel = in.readString();
		this.id = in.readString();
		this.url = in.readString();
		this.ts = in.readInt();
	}

	public static final Parcelable.Creator<DataSteaming> CREATOR = new Parcelable.Creator<DataSteaming>() {
		@Override
		public DataSteaming createFromParcel(Parcel source) {
			return new DataSteaming(source);
		}

		@Override
		public DataSteaming[] newArray(int size) {
			return new DataSteaming[size];
		}
	};
}