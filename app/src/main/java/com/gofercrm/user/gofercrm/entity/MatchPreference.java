package com.gofercrm.user.gofercrm.entity;

import java.io.Serializable;

public class MatchPreference implements Serializable {

    private Integer lease_length;
    private Double budget;
    private Integer beds;
    private Integer bathrooms;

    public MatchPreference() {
    }

    public MatchPreference(Integer lease_length, Double budget, Integer beds, Integer bathrooms) {
        this.lease_length = lease_length;
        this.budget = budget;
        this.beds = beds;
        this.bathrooms = bathrooms;
    }

    public Integer getLease_length() {
        return lease_length;
    }

    public void setLease_length(Integer lease_length) {
        this.lease_length = lease_length;
    }

    public Double getBudget() {
        return budget;
    }

    public void setBudget(Double budget) {
        this.budget = budget;
    }

    public Integer getBeds() {
        return beds;
    }

    public void setBeds(Integer beds) {
        this.beds = beds;
    }

    public Integer getBathrooms() {
        return bathrooms;
    }

    public void setBathrooms(Integer bathrooms) {
        this.bathrooms = bathrooms;
    }
}