package com.gofercrm.user.gofercrm.entity;

import java.io.Serializable;

public class Comment implements Serializable {

    private String comment;
    private String date;
    private String url;
    private String content_type;

    public Comment(String comment, String date, String url, String content_type) {
        this.comment = comment;
        this.date = date;
        this.url = url;
        this.content_type = content_type;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getContent_type() {
        return content_type;
    }

    public void setContent_type(String content_type) {
        this.content_type = content_type;
    }
}