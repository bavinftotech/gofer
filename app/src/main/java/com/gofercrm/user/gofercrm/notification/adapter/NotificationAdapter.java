package com.gofercrm.user.gofercrm.notification.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.databinding.RowNotificationBinding;
import com.gofercrm.user.gofercrm.implementor.RecyclerViewClickListener;
import com.gofercrm.user.gofercrm.notification.model.DataNotification;
import com.gofercrm.user.gofercrm.util.TimeAgo_;

import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    private final List<DataNotification> list;
    private final Activity activity;

    public NotificationAdapter(List<DataNotification> list, Activity activity) {
        this.list = list;
        this.activity = activity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RowNotificationBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.row_notification, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.setData(list.get(position));
        holder.binding.executePendingBindings();
        if (list.get(position).getType() == DataNotification.notificationVideoCall ||
                list.get(position).getType() == DataNotification.notificationConnectionRequest) {
            holder.binding.tvTime.setVisibility(View.VISIBLE);
            holder.binding.tvTime.setText(TimeAgo_.getTimeAgo(activity, list.get(position).getTime()));
        } else {
            holder.binding.tvTime.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final RowNotificationBinding binding;

        public ViewHolder(RowNotificationBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            itemView.setOnClickListener(this);
            binding.ivVideoCall.setOnClickListener(this);
            binding.ivAccept.setOnClickListener(this);
            binding.ivRemove.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view == itemView) {
                if (getAdapterPosition() != RecyclerView.NO_POSITION) {
                    if (listener != null) {
                        listener.onItemClick(3, getAdapterPosition(), view);
                    }
                }
            } else if (view == binding.ivVideoCall) {
                if (getAdapterPosition() != RecyclerView.NO_POSITION) {
                    if (listener != null) {
                        listener.onItemClick(0, getAdapterPosition(), view);
                    }
                }
            } else if (view == binding.ivAccept) {
                if (getAdapterPosition() != RecyclerView.NO_POSITION) {
                    if (listener != null) {
                        listener.onItemClick(1, getAdapterPosition(), view);
                    }
                }
            } else if (view == binding.ivRemove) {
                if (getAdapterPosition() != RecyclerView.NO_POSITION) {
                    if (listener != null) {
                        listener.onItemClick(2, getAdapterPosition(), view);
                    }
                }
            }
        }
    }

    private RecyclerViewClickListener listener;

    public void setOnRecyclerViewItemClickListener(RecyclerViewClickListener clickListener) {
        listener = clickListener;
    }
}