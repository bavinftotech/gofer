package com.gofercrm.user.gofercrm.util;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public abstract class EndlessScrollRecyclerViewListener extends RecyclerView.OnScrollListener {

    @Override
    public void onScrolled(@NonNull RecyclerView mRecyclerView, int dx, int dy) {
        super.onScrolled(mRecyclerView, dx, dy);
        LinearLayoutManager mLayoutManager = (LinearLayoutManager) mRecyclerView
                .getLayoutManager();

        assert mLayoutManager != null;
        int visibleItemCount = mLayoutManager.getChildCount();
        int totalItemCount = mLayoutManager.getItemCount();
        int pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition();
        onScroll(pastVisibleItems, visibleItemCount, totalItemCount);
    }

    private void onScroll(int pastVisibleItems, int visibleItemCount, int totalItemCount) {
        if (pastVisibleItems + visibleItemCount >= totalItemCount) {
            onLoadMore();
        }
    }

    public abstract void onLoadMore();
}