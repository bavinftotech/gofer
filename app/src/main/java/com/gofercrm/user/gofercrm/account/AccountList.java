package com.gofercrm.user.gofercrm.account;

import com.gofercrm.user.gofercrm.entity.Address;
import com.gofercrm.user.gofercrm.entity.Comment;
import com.gofercrm.user.gofercrm.entity.Email;
import com.gofercrm.user.gofercrm.entity.Phone;

import java.io.Serializable;
import java.util.List;

public class AccountList implements Serializable {
    private String id, title, status, fileAs, leadSource, name;
    private List<Comment> comments;
    private List<Address> addresses;
    private List<Email> emails;
    private List<Phone> phones;

    public AccountList() {
    }

    public AccountList(String id, String title, String status, String fileAs, String leadSource, List<Comment> comments, List<Address> addresses, List<Email> emails, List<Phone> phones, String name) {
        this.id = id;
        this.title = title;
        this.status = status;
        this.fileAs = fileAs;
        this.leadSource = leadSource;
        this.comments = comments;
        this.addresses = addresses;
        this.emails = emails;
        this.phones = phones;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFileAs() {
        return fileAs;
    }

    public void setFileAs(String fileAs) {
        this.fileAs = fileAs;
    }

    public String getLeadSource() {
        return leadSource;
    }

    public void setLeadSource(String leadSource) {
        this.leadSource = leadSource;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    public List<Email> getEmails() {
        return emails;
    }

    public void setEmails(List<Email> emails) {
        this.emails = emails;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}