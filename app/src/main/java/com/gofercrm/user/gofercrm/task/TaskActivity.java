package com.gofercrm.user.gofercrm.task;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.entity.Comment;
import com.gofercrm.user.gofercrm.implementor.DialogButtonClickListener;
import com.gofercrm.user.gofercrm.implementor.RecyclerViewClickListener;
import com.gofercrm.user.gofercrm.task.view.tab.comments.TaskAdapter;
import com.gofercrm.user.gofercrm.util.DialogUtils;
import com.gofercrm.user.gofercrm.util.EndlessScrollRecyclerViewListener;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TaskActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener, RecyclerViewClickListener {
    private static final String TAG = "TaskActivity";
    private static final int REQUEST_CODE = 101;
    public List<Task> taskList = new ArrayList<>();
    RecyclerView recyclerView;
    String last_id = null;
    ProgressBar paginationProgressBar;
    ProgressBar progressBar;
    boolean isMoreDataAvailable = true;
    boolean isScrolling = false;
    boolean isOnSearch = false;
    TextView no_task_records;
    SwipeRefreshLayout mSwipeRefreshLayout;
    private TaskAdapter taskAdapter;
    private String strFilter = "";
    private String strUID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(getResources().getString(R.string.tasks));
        }
        no_task_records = findViewById(R.id.no_lead_records);
        recyclerView = findViewById(R.id.task_recycle);
        progressBar = findViewById(R.id.task_list_progressBar);
        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        taskAdapter = new TaskAdapter(taskList, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(taskAdapter);
        taskAdapter.setOnRecyclerViewItemClickListener(this);

        mSwipeRefreshLayout = findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        paginationProgressBar = findViewById(R.id.task_list_pagination_progress);

        getTasks();

        FloatingActionButton fab = findViewById(R.id.new_task_fab);
        fab.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), NewTaskActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        });

        recyclerView.addOnScrollListener(new EndlessScrollRecyclerViewListener() {
            @Override
            public void onLoadMore() {
                if (isMoreDataAvailable && !isOnSearch && !isScrolling) {
                    isScrolling = true;
                    getTasks();
                }
            }
        });

//        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
//                super.onScrollStateChanged(recyclerView, newState);
//            }
//
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//                if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
//                    final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
//                            .getLayoutManager();
//                    int totalItemCount = linearLayoutManager.getItemCount();
//                    int lastVisibleItem = linearLayoutManager
//                            .findLastVisibleItemPosition();
//                    if (isMoreDataAvailable && !isOnSearch && !isScrolling && totalItemCount <= (lastVisibleItem + 5)) {
//                        isScrolling = true;
//                        getTasks();
//                    }
//                }
//            }
//        });
    }

    private void getTasks() {
        JSONObject postData = new JSONObject();
        try {
            postData.put("action", "get");
            postData.put("page_size", "10");
            if (last_id != null) {
                postData.put("last_id", last_id);
                paginationProgressBar.setVisibility(View.VISIBLE);
            }

            JSONObject jsonFilter = new JSONObject();
            if (!strFilter.equals("")) {
                if (strFilter.equals("uncompleted")) {
                    JSONObject jsonStatus = new JSONObject();
                    jsonStatus.put("$ne", "completed");
                    jsonFilter.put("status", jsonStatus);
                } else {
                    jsonFilter.put("status", strFilter);
                }
            }
            jsonFilter.put("user_id", strUID);
            postData.put("match_filter_dict", jsonFilter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        LogUtils.Print(TAG, "postData --> " + postData);
        NetworkManager.customJsonObjectRequest(
                TaskActivity.this, Constants.TASK_LIST_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        LogUtils.Print(TAG, "response --> " + response.toString());
                        processData(response, false);
                    }

                    @Override
                    public void onError(String message) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        LogUtils.Print("ERRROR=", message);
                    }
                }, true);
    }

    private void processData(JSONObject data, boolean isSearch) {
        try {
            Task task;
            JSONObject result_Data = new JSONObject();
            if (Integer.parseInt(data.getString("result")) == 1) {
                if (isSearch) {
                    result_Data = data.getJSONObject("response");
                } else {
                    result_Data = data.getJSONObject("data");
                }

                last_id = result_Data.getString("last_id");
                JSONArray tasks = result_Data.getJSONArray("tasks");
                for (int i = 0; i < tasks.length(); i++) {

                    JSONObject obj = tasks.getJSONObject(i);
                    List<Comment> comments = new ArrayList();
                    JSONArray commentsArray = new JSONArray();
                    if (obj.has("comments")) {
                        Object comments_obj = obj.get("comments");
                        if (comments_obj instanceof JSONArray) {
                            commentsArray = (JSONArray) comments_obj;
                        }
                        for (int j = 0; j < commentsArray.length(); j++) {
                            JSONObject commentObj = commentsArray.getJSONObject(j);
                            comments.add(new Comment(commentObj.getString("comment"), commentObj.getString("date"),
                                    commentObj.has("url") ? commentObj.getString("url") : "",
                                    commentObj.has("content_type") ? commentObj.getString("content_type") : ""));
                        }
                    }

                    task = new Task();
                    task.setComments(comments);
                    if (obj.has("user_task_id")) {
                        task.setTaskId(obj.getString("user_task_id"));
                    }
                    if (obj.has("name")) {
                        task.setName(obj.getString("name"));
                        LogUtils.Print(TAG, "NAME =========>  " + obj.getString("name"));
                    }
                    if (obj.has("status")) {
                        task.setStatus(obj.getString("status"));
                    }
                    if (obj.has("subject")) {
                        task.setSubjectId(obj.getString("subject"));
                    }
                    if (obj.has("subject_info")) {
                        JSONObject subjectInfo = obj.getJSONObject("subject_info");
                        task.setSubjectName(subjectInfo.getString("name"));
                    }
                    if (obj.has("subject_type")) {
                        task.setType(obj.getString("subject_type"));
                    }
                    if (obj.has("due_date")) {
                        task.setDueDate(obj.getLong("due_date"));
                    }
                    if (obj.has("reminder_days")) {
                        task.setReminderDays(obj.getString("reminder_days"));
                    }
                    if (obj.has("user_id")) {
                        task.setUserId(obj.getString("user_id"));
                    }
                    if (obj.has("cal_html_link")) {
                        task.setLink(obj.getString("cal_html_link"));
                    }
                    if (obj.has("action")) {
                        task.setAction(obj.getString("action"));
                    }
                    if (obj.has("action_data")) {
                        task.setAction_data(obj.getString("action_data"));
                    }
                    if (obj.has("description")) {
                        task.setDescription(obj.getString("description"));
                    }
                    if (obj.has("priority")) {
                        task.setPriority(obj.getInt("priority"));
                    }
                    taskList.add(task);
                }
                isMoreDataAvailable = tasks.length() > 0;
                onLoaded();
            } else {
                JSONObject error = data.getJSONObject("error");
                if (error.has("message")) {
                    Util.onMessage(getApplicationContext(), error.getString("message"));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void onLoaded() {
        if (taskList.size() > 0) {
            progressBar.setVisibility(View.GONE);
            paginationProgressBar.setVisibility(View.GONE);
            isScrolling = false;
            recyclerView.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
        taskAdapter.notifyDataSetChanged();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menuAll) {
            applyFilter("");
        } else if (id == R.id.menuCompleted) {
            applyFilter("completed");
        } else if (id == R.id.menuUncompleted) {
            applyFilter("uncompleted");
        } else if (id == R.id.menuAssigned) {
            applyFilter("assigned");
        } else if (id == R.id.menuInProgress) {
            applyFilter("in-progress");
        } else if (id == R.id.menuRejected) {
            applyFilter("rejected");
        } else if (id == R.id.menuReviewSubmit) {
            applyFilter("review-submit");
        } else if (id == R.id.menuReviewPass) {
            applyFilter("review-pass");
        } else if (id == R.id.menuFilter) {
            Intent intent = new Intent(this, TaskUserFilterActivity.class);
            intent.putExtra("DATA", strUID);
            startActivityForResult(intent, REQUEST_CODE);
        }
        return super.onOptionsItemSelected(item);
    }

    private void applyFilter(String strFilterType) {
        last_id = null;
        taskList.clear();
        taskAdapter.notifyDataSetChanged();
        mSwipeRefreshLayout.setRefreshing(true);
        paginationProgressBar.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        strFilter = strFilterType;
        getTasks();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_client_all, menu);
        SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView search = (SearchView) menu.findItem(R.id.lead_search).getActionView();
        search.setSearchableInfo(manager.getSearchableInfo(getComponentName()));
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                isOnSearch = true;
                no_task_records.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                search.setIconified(true);
                taskList.clear();
                taskAdapter.notifyDataSetChanged();
                last_id = null;
                searchTasks(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        search.setOnCloseListener(() -> {
            isOnSearch = false;
            last_id = null;
            taskList.clear();
            taskAdapter.notifyDataSetChanged();
            progressBar.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            getTasks();
            return false;
        });
        return true;
    }

    private void searchTasks(String query) {
        String url = Constants.SEARCH_TASK_URL + "?all_words=false&search_all=true&search_text=" + query;
        NetworkManager.customJsonObjectRequest(
                TaskActivity.this, url, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processData(response, true);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("TASK SEARCH ERROR=", message);
                    }
                }, false);
    }

    @Override
    public void onRefresh() {
        last_id = null;
        taskList.clear();
        taskAdapter.notifyDataSetChanged();
        mSwipeRefreshLayout.setRefreshing(true);
        paginationProgressBar.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        getTasks();
    }

    @Override
    public void onItemClick(int flag, int position, View view) {
        DialogUtils.showConfirmationDialog(this, new DialogButtonClickListener() {
            @Override
            public void onPositiveButtonClick() {
                createTask(position);
            }

            @Override
            public void onNegativeButtonClick() {

            }
        }, getResources().getString(R.string.complete_task_confirmation));
    }

    private void createTask(int position) {
        showProgress();
        JSONObject postData = new JSONObject();
        try {
            postData.put("action", "update");
            postData.put("user_task_id", taskList.get(position).getTaskId());
            JSONObject payload = new JSONObject();
            try {
                payload.put("status", "completed");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            postData.put("payload", payload);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        NetworkManager.customJsonObjectRequest(
                TaskActivity.this, Constants.TASK_LIST_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideProgress();
                        if (taskList.size() > position) {
                            taskList.get(position).setStatus("completed");
                            taskAdapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onError(String message) {
                        hideProgress();
                        Util.onMessage(TaskActivity.this, message);
                    }
                }, true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null && data.hasExtra("DATA")) {
                    last_id = null;
                    taskList.clear();
                    taskAdapter.notifyDataSetChanged();
                    mSwipeRefreshLayout.setRefreshing(true);
                    paginationProgressBar.setVisibility(View.GONE);
                    progressBar.setVisibility(View.GONE);
                    strUID = data.getStringExtra("DATA");
                    getTasks();
                }
            }
        } else if (requestCode == 222) {
            if (resultCode == Activity.RESULT_OK) {
                last_id = null;
                taskList.clear();
                taskAdapter.notifyDataSetChanged();
                mSwipeRefreshLayout.setRefreshing(true);
                paginationProgressBar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                getTasks();
            }
        }
    }
}