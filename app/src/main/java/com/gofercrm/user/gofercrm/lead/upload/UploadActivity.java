package com.gofercrm.user.gofercrm.lead.upload;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Group;

import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.util.DialogUtils;
import com.gofercrm.user.gofercrm.util.FileUtil;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.jaiselrahman.filepicker.activity.FilePickerActivity;
import com.jaiselrahman.filepicker.config.Configurations;
import com.jaiselrahman.filepicker.model.MediaFile;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import io.github.lizhangqu.coreprogress.ProgressHelper;
import io.github.lizhangqu.coreprogress.ProgressUIListener;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class UploadActivity extends BaseActivity {

    private static final String TAG = "UploadActivity";
    private static final int FILE_REQUEST_CODE = 228;
    private String strMessage = "";
    private String token = "";

    private int from = 0;
    private String strId = "0";
    public static int fromLead = 0, fromClient = 1;

    private ProgressBar progressBar;
    private ProgressBar pbCircular;
    private Group groupProgress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);
        getDataFromIntent();
        findViewById();
        openInputTextPopup();
    }

    private void getDataFromIntent() {
        if (getIntent() != null) {
            from = getIntent().getIntExtra("FROM", 0);
            if (from == fromLead)
                strId = getIntent().getStringExtra("LEAD_ID");
            else if (from == fromClient)
                strId = getIntent().getStringExtra("CLIENT_ID");
        }
    }

    private void findViewById() {
        SharedPreferences sharedPref = getApplicationContext()
                .getSharedPreferences("LOGIN", MODE_PRIVATE);
        token = sharedPref.getString("USER_TOKEN", "");

        progressBar = findViewById(R.id.progressBar);
        pbCircular = findViewById(R.id.pbCircular);
        groupProgress = findViewById(R.id.groupProgress);
    }

    private void openInputTextPopup() {
        DialogUtils.showUserInputDialog(this,
                getResources().getString(R.string.enter_message),
                new DialogUserInputListener() {
                    @Override
                    public void onDialogUserInput(String strText) {
                        strMessage = strText;
                        openFilePickerDialog();
                    }

                    @Override
                    public void onDialogUserCancelInput() {
                        dismissCurrentScreen();
                    }
                });
    }

    private void openFilePickerDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.add_a_file));
        builder.setItems(new CharSequence[]{
                        getResources().getString(R.string.phone_library),
                        getResources().getString(R.string.video),
                        getResources().getString(R.string.file)},
                (dialog, which) -> {
                    switch (which) {
                        case 0:
                            //phone_library
                            openImageFilePicker();
                            dialog.cancel();
                            break;
                        case 1:
                            //video
                            openVideoFilePicker();
                            dialog.cancel();
                            break;
                        case 2:
                            //file
                            openFilePicker();
                            dialog.cancel();
                            break;
                    }
                });
        builder.create().show();
    }

    private void openImageFilePicker() {
        Intent intent = new Intent(this, FilePickerActivity.class);
        intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
                .setCheckPermission(true)
                .setShowImages(true)
                .setShowVideos(false)
                .setSingleChoiceMode(true)
                .enableImageCapture(false)
                .build());
        startActivityForResult(intent, FILE_REQUEST_CODE);
    }

    private void openVideoFilePicker() {
        Intent intent = new Intent(this, FilePickerActivity.class);
        intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
                .setCheckPermission(true)
                .setShowVideos(true)
                .setShowImages(false)
                .setSingleChoiceMode(true)
                .enableImageCapture(false)
                .setSkipZeroSizeFiles(true)
                .build());
        startActivityForResult(intent, FILE_REQUEST_CODE);
    }

    private void openFilePicker() {
        Intent intent = new Intent(this, FilePickerActivity.class);
        intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
                .setCheckPermission(true)
                .setShowFiles(true)
                .setShowAudios(true)
                .setSuffixes("pdf")
                .setSingleChoiceMode(true)
                .enableImageCapture(false)
                .setSkipZeroSizeFiles(true)
                .build());
        startActivityForResult(intent, FILE_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LogUtils.Print(TAG, "requestCode ->" + requestCode);
        LogUtils.Print(TAG, "resultCode ->" + resultCode);
        if (requestCode == FILE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                ArrayList<MediaFile> files;
                if (data != null) {
                    if (data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES) != null) {
                        files = data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES);
                        if (files == null || files.size() == 0) return;
                        Uri uri = files.get(0).getUri();
                        String mimeType = FileUtil.getMimeType(getApplicationContext(), uri);
                        String fileName = FileUtil.getFileName(getApplicationContext(), uri);
                        uploadFile(uri, fileName, mimeType);
                    }
                }
            } else {
                dismissCurrentScreen();
            }
        }
    }

    private void uploadFile(Uri uri, String file_name, String file_type) {
        //client
        OkHttpClient okHttpClient = new OkHttpClient();
        //request builder
        Request.Builder builder = new Request.Builder();
        builder.url(Constants.FILE_UPLOAD);
        builder.addHeader("Authorization", "Bearer " + token);

        //your original request body
        MultipartBody body = null;
        try {
            File file = FileUtil.from(this, uri);
            MultipartBody.Builder bodyBuilder = new MultipartBody.Builder();
            bodyBuilder.setType(MultipartBody.FORM);
            bodyBuilder.addFormDataPart("storefile_file_purpose", "store_file");
            bodyBuilder.addFormDataPart("storefile_description", file_name);
            bodyBuilder.addFormDataPart("storefile", file_name, RequestBody.create(MediaType.parse(file_type), file));
            body = bodyBuilder.build();
        } catch (Exception e) {
            e.printStackTrace();
            Util.onMessage(UploadActivity.this, getResources().getString(R.string.server_err));
            dismissCurrentScreen();
        }

        //wrap your original request body with progress
        RequestBody requestBody = ProgressHelper.withProgress(body, new ProgressUIListener() {

            //if you don't need this method, don't override this methd. It isn't an abstract method, just an empty method.
            @Override
            public void onUIProgressStart(long totalBytes) {
                super.onUIProgressStart(totalBytes);
                LogUtils.Print("TAG", "onUIProgressStart:" + totalBytes);
                groupProgress.setVisibility(View.VISIBLE);
                pbCircular.setVisibility(View.VISIBLE);
                progressBar.setProgress(0);
            }

            @Override
            public void onUIProgressChanged(long numBytes, long totalBytes, float percent, float speed) {
                progressBar.setProgress((int) (percent * 100));
                LogUtils.Print("TAG", "percent:" + percent);
            }

            //if you don't need this method, don't override this methd. It isn't an abstract method, just an empty method.
            @Override
            public void onUIProgressFinish() {
                super.onUIProgressFinish();
                LogUtils.Print("TAG", "onUIProgressFinish:");
                groupProgress.setVisibility(View.GONE);
            }
        });

        //post the wrapped request body
        builder.post(requestBody);
        //call
        Call call = okHttpClient.newCall(builder.build());
        //enqueue
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                groupProgress.setVisibility(View.GONE);
                LogUtils.Print("TAG", "=============onFailure===============");
                e.printStackTrace();
                Toast.makeText(UploadActivity.this, getResources().getString(R.string.server_err), Toast.LENGTH_SHORT).show();
                dismissCurrentScreen();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                groupProgress.setVisibility(View.GONE);
                ResponseBody responseBody = response.body();
                if (!response.isSuccessful()) {
                    Toast.makeText(UploadActivity.this, getResources().getString(R.string.server_err), Toast.LENGTH_SHORT).show();
                    dismissCurrentScreen();
                    throw new IOException("Error response " + response);
                }
                String jsonData = responseBody.string();
                try {
                    JSONObject jsonObject = new JSONObject(jsonData);
                    LogUtils.Print(TAG, "response jsonObject --> " + jsonObject);
                    if (Integer.parseInt(jsonObject.getString("result")) == 1) {
                        if (jsonObject.has("file_id")) {
                            shareFileAPIRequest(jsonObject.getString("file_id"));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    LogUtils.Print(TAG,"Exception -->"+ e.getMessage());
                    Toast.makeText(UploadActivity.this, getResources().getString(R.string.server_err), Toast.LENGTH_SHORT).show();
                    dismissCurrentScreen();
                }
            }
        });

//        showProgress();
//        OkHttpClient httpClient = new OkHttpClient();
//        RequestBody requestBody = null;
//        try {
//            File file = FileUtil.from(this, uri);
//            requestBody = new MultipartBody.Builder()
//                    .setType(MultipartBody.FORM)
//                    .addFormDataPart("storefile_file_purpose", "store_file")
//                    .addFormDataPart("storefile_description", file_name)
//                    .addFormDataPart("storefile", file_name, RequestBody.create(MediaType.parse(file_type),
//                            file)).build();
//            LogUtils.Print(TAG, "requestBody " + requestBody.toString());
//        } catch (Exception e) {
//            e.printStackTrace();
//            Util.onMessage(UploadActivity.this, getResources().getString(R.string.server_err));
//            dismissCurrentScreen();
//        }
//        Request request = new Request.Builder()
//                .url(Constants.FILE_UPLOAD)
//                .addHeader("Authorization", "Bearer " + token)
//                .post(requestBody)
//                .build();
//
//        httpClient.newCall(request).enqueue(new Callback() {
//            @Override
//            public void onFailure(Call call, IOException e) {
//                e.printStackTrace();
//                dismissCurrentScreen();
//                Util.onMessage(UploadActivity.this, "error in getting response using async okhttp call");
//            }
//
//
//            @Override
//            public void onResponse(Call call, Response response) throws IOException {
//                ResponseBody responseBody = response.body();
//                if (!response.isSuccessful()) {
//                    Util.onMessage(UploadActivity.this, getResources().getString(R.string.server_err));
//                    dismissCurrentScreen();
//                    throw new IOException("Error response " + response);
//                }
//                String jsonData = responseBody.string();
//                try {
//                    JSONObject jsonObject = new JSONObject(jsonData);
//                    LogUtils.Print(TAG, "response jsonObject --> " + jsonObject);
//                    if (Integer.parseInt(jsonObject.getString("result")) == 1) {
//                        if (jsonObject.has("file_id")) {
//                            shareFileAPIRequest(jsonObject.getString("file_id"));
//                        }
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    Util.onMessage(UploadActivity.this, getResources().getString(R.string.server_err));
//                    dismissCurrentScreen();
//                }
//            }
//        });
    }

    private void shareFileAPIRequest(String fileId) {
        JSONObject postData = new JSONObject();
        try {
            postData.put("file_id", fileId);
            if (from == fromLead) {
                postData.put("lead_id", strId);
            } else if (from == fromClient) {
                postData.put("client_id", strId);
            }
            postData.put("message_text", strMessage);
            LogUtils.Print(TAG, "postData --> " + postData);
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(UploadActivity.this, getResources().getString(R.string.server_err), Toast.LENGTH_SHORT).show();
            dismissCurrentScreen();
        }

        NetworkManager.customJsonObjectRequest(
                this, Constants.FILE_SHARE, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtils.Print(TAG, "response --> " + response);
                        Util.onMessage(UploadActivity.this, getResources().getString(R.string.upload_successfully));
                        dismissCurrentScreen();
                    }

                    @Override
                    public void onError(String message) {
                        Util.onMessage(UploadActivity.this, message);
                        dismissCurrentScreen();
                    }
                }, true);
    }

    private void dismissCurrentScreen() {
        pbCircular.setVisibility(View.GONE);
        groupProgress.setVisibility(View.GONE);
        setResult(RESULT_CANCELED);
        finish();
    }
}