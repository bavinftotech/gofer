package com.gofercrm.user.gofercrm.clients;

public class ClientView {

    private String label;
    private String value;
    private String type;
    private int leftDrawable;
    private int rightDrawable;

    public ClientView(String label, String value, String type, int leftDrawable, int rightDrawable) {
        this.label = label;
        this.value = value;
        this.type = type;
        this.leftDrawable = leftDrawable;
        this.rightDrawable = rightDrawable;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getLeftDrawable() {
        return leftDrawable;
    }

    public void setLeftDrawable(int leftDrawable) {
        this.leftDrawable = leftDrawable;
    }

    public int getRightDrawable() {
        return rightDrawable;
    }

    public void setRightDrawable(int rightDrawable) {
        this.rightDrawable = rightDrawable;
    }
}