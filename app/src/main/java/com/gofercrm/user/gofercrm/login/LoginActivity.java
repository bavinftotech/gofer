package com.gofercrm.user.gofercrm.login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gofercrm.user.gofercrm.ApplicationContext;
import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.NavigationActivity;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.databinding.ActivityLoginBinding;
import com.gofercrm.user.gofercrm.passwordmanagement.ResetPasswordActivity;
import com.gofercrm.user.gofercrm.tenantcomment.ToastActivity;
import com.gofercrm.user.gofercrm.util.DeviceInfo;
import com.gofercrm.user.gofercrm.util.DialogUtils;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.SharedPreferenceData;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleySingleton;
import com.gofercrm.user.gofercrm.websocket.WSListener;
import com.gofercrm.user.gofercrm.websocket.WSSingleton;
import com.gofercrm.user.gofercrm.websocket.WsManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.microsoft.identity.client.AuthenticationCallback;
import com.microsoft.identity.client.IAuthenticationResult;
import com.microsoft.identity.client.PublicClientApplication;
import com.microsoft.identity.client.exception.MsalClientException;
import com.microsoft.identity.client.exception.MsalException;
import com.microsoft.identity.client.exception.MsalServiceException;
import com.microsoft.identity.client.exception.MsalUiRequiredException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends BaseActivity implements WSListener {
    public static final int REQUEST_GOOGLE_LOGIN = 610;
    private static final String TAG = "LoginActivity";

    private SharedPreferences sharedPref;
    private ActivityLoginBinding binding;
    private boolean isRegistrationVisible = false;

    //MSFT Login
    final static String[] SCOPES = {"https://graph.microsoft.com/calendars.readwrite", "https://graph.microsoft.com/mail.send", "openid","email", "profile", "offline_access"};
    final static String MSGRAPH_URL = "https://graph.microsoft.com/v1.0/me";
    private PublicClientApplication sampleApp;
    private IAuthenticationResult authResult;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        binding.setActivity(this);
        setUpHeaderView();
        showRegistration(false);
        sharedPref = getSharedPreferences("LOGIN", MODE_PRIVATE);
        deleteCache();
    }

    private void setUpHeaderView() {
        binding.tlEmail.setEndIconOnClickListener(view -> {
            //call resolve_user api
            if (binding.etEmail.getText().toString().trim().isEmpty()) {
                Util.showSnackBar(getResources().getString(R.string.email_empty_err), LoginActivity.this);
            } else if (!Util.isValidEmail(binding.etEmail.getText().toString().trim())) {
                Util.showSnackBar(getResources().getString(R.string.email_invalid), LoginActivity.this);
            } else {
                checkUserAvailability();
            }
        });
        binding.etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (binding.btnLogin.getVisibility() == View.VISIBLE) {
                    showRegistration(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        //MSFT Login
        /* Configure your sample app and save state for this activity */
        sampleApp = new PublicClientApplication(
                this.getApplicationContext(),
                R.raw.auth_config_single_account);
        /* Attempt to get a user and acquireTokenSilent
         * If this fails we do an interactive request
         */
//        sampleApp.getAccounts(accounts -> {
//            if (!accounts.isEmpty()) {
//                /* This sample doesn't support multi-account scenarios, use the first account */
//                sampleApp.acquireTokenSilentAsync(SCOPES, accounts.get(0), getAuthSilentCallback());
//            } else {
//                /* No accounts or >1 account */
//            }
//        });
    }

    private void checkUserAvailability() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", binding.etEmail.getText().toString().trim());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        LogUtils.Print(TAG, "jsonObject -> " + jsonObject);
        showProgress();
        NetworkManager.customJsonObjectRequest(
                this, Constants.RESOLVE_USER, jsonObject,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideProgress();
                        LogUtils.Print(TAG, "onResponse -> " + response);
                        try {
                            if (Integer.parseInt(response.getString("result")) == 1) {
                                if (response.getString("userid").equals("")) {
                                    //new user
                                    Intent intent = new Intent(LoginActivity.this, ToastActivity.class);
                                    intent.putExtra("data", getResources().getString(R.string.registration_info));
                                    intent.putExtra("from", ToastActivity.forLoginInfo);
                                    startActivity(intent);
                                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

                                    Util.hideSoftKeyboard(binding.etEmail);
                                    showRegistration(true);
                                } else {
                                    //old user
                                    showLogin(true);
                                }
                            } else if (Integer.parseInt(response.getString("result")) == 2) {
                                Util.showSnackBar(getResources().getString(R.string.invalid_email_not_usable), LoginActivity.this);
                            } else {
                                Util.showSnackBar(getResources().getString(R.string.server_err), LoginActivity.this);
                            }
                        } catch (Exception e) {
                            Util.showSnackBar(getResources().getString(R.string.server_err), LoginActivity.this);
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String message) {
                        hideProgress();
                        LogUtils.Print(TAG, "message -> " + message);
                        Util.showSnackBar(message, LoginActivity.this);
                    }
                }, true, "gnzInOENLnWBeKMVWIcSNJLkdlDqflSz");
    }

    private void showLogin(boolean flag) {
        if (flag) {
            binding.etPassword.requestFocus();
            binding.tlPassword.setVisibility(View.VISIBLE);
            binding.btnLogin.setVisibility(View.VISIBLE);
        } else {
            binding.etEmail.requestFocus();
            binding.etEmail.setText("");
            binding.tlPassword.setVisibility(View.GONE);
            binding.etPassword.setText("");
            binding.btnLogin.setVisibility(View.GONE);
        }
    }

    private void showRegistration(boolean flag) {
        isRegistrationVisible = flag;
        if (flag) {
            binding.etFirstName.requestFocus();
            binding.tlFirstName.setVisibility(View.VISIBLE);
            binding.tlLastName.setVisibility(View.VISIBLE);
            binding.tlPassword.setVisibility(View.VISIBLE);
            binding.btnLogin.setVisibility(View.VISIBLE);
        } else {
            binding.etEmail.requestFocus();
            binding.tlFirstName.setVisibility(View.GONE);
            binding.etFirstName.setText("");
            binding.tlLastName.setVisibility(View.GONE);
            binding.etLastName.setText("");
            binding.tlPassword.setVisibility(View.GONE);
            binding.etPassword.setText("");
            binding.btnLogin.setVisibility(View.GONE);
        }
    }

    /**
     * VIEW CLICK EVENT
     *
     * @param view
     */
    public void onViewClicked(View view) {
        if (view == binding.ivGoogle) {
            signIn();
        } else if (view == binding.ivContactUs) {
            startActivity(new Intent(this, ContactUsActivity.class));
        } else if (view == binding.ivInfo) {
            Intent intent = new Intent(this, ToastActivity.class);
            intent.putExtra("data", getResources().getString(R.string.login_info));
            intent.putExtra("from", ToastActivity.forLoginInfo);
            startActivity(intent);
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        } else if (view == binding.tvForgotPassword) {
            Intent intent = new Intent(getApplicationContext(), ResetPasswordActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else if (view == binding.ivMSFT) {
            onCallGraphClicked();
        } else if (view == binding.btnLogin) {
            if (isRegistrationVisible && binding.etFirstName.getText().toString().trim().isEmpty()) {
                binding.etFirstName.requestFocus();
                Util.showSnackBar(getResources().getString(R.string.first_name_empty_err), LoginActivity.this);
            } else if (isRegistrationVisible && binding.etLastName.getText().toString().trim().isEmpty()) {
                binding.etLastName.requestFocus();
                Util.showSnackBar(getResources().getString(R.string.last_name_empty_err), LoginActivity.this);
            } else if (binding.etEmail.getText().toString().trim().isEmpty()) {
                binding.etEmail.requestFocus();
                Util.showSnackBar(getResources().getString(R.string.email_empty_err), LoginActivity.this);
            } else if (!Util.isValidEmail(binding.etEmail.getText().toString().trim())) {
                binding.etEmail.requestFocus();
                Util.showSnackBar(getResources().getString(R.string.email_invalid), LoginActivity.this);
            } else if (isRegistrationVisible && !Util.isValidPassword(binding.etPassword.getText().toString())) {
                binding.etPassword.requestFocus();
                Intent intent = new Intent(this, ToastActivity.class);
                intent.putExtra("data", getResources().getString(R.string.password_invalid_err));
                intent.putExtra("from", ToastActivity.forLoginInfo);
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            } else if (!isRegistrationVisible && binding.etPassword.getText().toString().isEmpty()) {
                binding.etPassword.requestFocus();
                Util.showSnackBar(getResources().getString(R.string.password_invalid_err), LoginActivity.this);
            } else {
                performLoginOperation();
//                if (isRegistrationVisible) {
//                    //call registration Api
//                    performLoginOperation();
//                } else {
//                    //call login api
//                    performLoginOperation();
//                }
            }
        }
    }

    private void performLoginOperation() {
        showProgress();
        String URL = "";
        if (isRegistrationVisible) {
            URL += Constants.REGISTRATION_API;
        } else {
            URL += Constants.Login_URL;
        }
        StringRequest request = new StringRequest(Request.Method.POST, URL,
                response -> {
                    processResponse(response);
                    LogUtils.Print("Login response", response);
                    hideProgress();
                }, e -> {
            LogUtils.Print("Login Output", e.toString());
            hideProgress();
            Util.showSnackBar(getResources().getString(R.string.error_in_login), LoginActivity.this);
            e.printStackTrace();
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("email", binding.etEmail.getText().toString().trim());
                params.put("password", binding.etPassword.getText().toString());
                params.put("os_version", DeviceInfo.getAndroidVersion());
                params.put("os", "android");
                params.put("device_id", DeviceInfo.getDeviceID(getApplicationContext()));
                params.put("device_meta", DeviceInfo.getDeviceName());
                params.put("is_mobile", "true");

                if (isRegistrationVisible) {
                    params.put("name", binding.etFirstName.getText().toString().trim() + " " +
                            binding.etLastName.getText().toString().trim());
                    params.put("apns_device_token", Util.GetDeviceID(LoginActivity.this));
                    params.put("device_type", "android");
                }
                return params;
            }

        };
        request.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 500000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 500000;
            }

            @Override
            public void retry(VolleyError error) {

            }
        });
        RequestQueue queue = VolleySingleton.getInstance(getApplicationContext()).getRequestQueue();
        queue.add(request);
    }

    private void processResponse(String response) {
        if (isRegistrationVisible) {
            //handle registration response
//            {"error": {"message": "already registered", "reason": "email is not eligible for new accounts"}, "result": 0}
//            {"error": {"message": "tenant is not confirmed"}, "result": 0}
            try {
                JSONObject object = new JSONObject(response);
                if (Integer.parseInt(object.getString("result")) == 1) {
//                    JSONObject userObject = object.getJSONObject("user");
//                    saveDataAndNavigateToNextScreen(userObject);
                } else if (Integer.parseInt(object.getString("result")) == 0) {
                    if (object.has("error")) {
                        JSONObject jsonError = object.getJSONObject("error");
                        if (jsonError.has("reason")) {
                            Util.showSnackBar(jsonError.getString("reason"), LoginActivity.this);
                        } else if (jsonError.has("message")) {
                            if (jsonError.getString("message").equals("tenant is not confirmed")) {
                                openResendCodePopup();
                            } else {
                                Util.showSnackBar(jsonError.getString("message"), LoginActivity.this);
                            }
                        } else {
                            Util.showSnackBar(getResources().getString(R.string.server_err), LoginActivity.this);
                        }
                    } else {
                        Util.showSnackBar(getResources().getString(R.string.server_err), LoginActivity.this);
                    }
                } else {
                    Util.showSnackBar(getResources().getString(R.string.server_err), LoginActivity.this);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            //handle login response
            try {
                JSONObject object = new JSONObject(response);
                if (Integer.parseInt(object.getString("result")) == 1) {
                    JSONObject userObject = object.getJSONObject("user");
                    saveDataAndNavigateToNextScreen(userObject);
                } else {
                    Util.showSnackBar(getResources().getString(R.string.invalid_credentials), LoginActivity.this);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void openResendCodePopup() {
        //call resend code API
        DialogUtils.showConfirmationDialog(this, this::reSendCodeAPIRequest,
                getResources().getString(R.string.re_send_email_msg),
                getResources().getString(R.string.resend_code),
                getResources().getString(R.string.cancel));
    }

    private void reSendCodeAPIRequest() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", binding.etEmail.getText().toString().trim());
            jsonObject.put("is_alt_email", "false");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        LogUtils.Print(TAG, "jsonObject -> " + jsonObject);
        showProgress();
        NetworkManager.customJsonObjectRequest(
                this, Constants.RESEND_VERIFY_CODE_API, jsonObject,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideProgress();
                        LogUtils.Print(TAG, "onResponse -> " + response);
//                        {"result":2,"msg":"user account not found"}
                    }

                    @Override
                    public void onError(String message) {
                        hideProgress();
                        LogUtils.Print(TAG, "message -> " + message);
                    }
                }, true, "gnzInOENLnWBeKMVWIcSNJLkdlDqflSz");
    }

    private void saveDataAndNavigateToNextScreen(JSONObject userObject) {
        try {
//            JSONObject userObject = object.getJSONObject("user");
            //int accountLevel = userObject.getInt("account_level");
            String userName = userObject.getString("name");
            String userFullName = userName;
            boolean is_tenant_admin = userObject.getBoolean("is_tenant_admin");
            String email = userObject.getString("email");
            String imageURL = "";
            String tenant_id = "";
            String tenantName = "";
            String tenantImageURL = "";
            String client_connection_id = "";
            boolean softphone_enabled = false;
            String sip_phone = "";
            String sip_fax_phone = "";
            String virtual_phone = "";

            if (userObject.has("softphone_enabled")) {
                softphone_enabled = userObject.getBoolean("softphone_enabled");
            }
            if (userObject.has("virtual_phone")) {
                virtual_phone = userObject.getString("virtual_phone");
            }
            if (userObject.has("sip_creds_dict")) {
                JSONObject obj = userObject.getJSONObject("sip_creds_dict");
                if (obj.has("sip_phone")) {
                    sip_phone = obj.getString("sip_phone");
                }
                if (obj.has("sip_fax_phone")) {
                    sip_fax_phone = obj.getString("sip_fax_phone");
                }
            }
            if (userObject.has("tenant_id")) {
                tenant_id = userObject.getString("tenant_id");
            }
            if (userObject.has("avatar_url")) {
                imageURL = userObject.getString("avatar_url");
                LogUtils.Print(TAG, "imageURL --> " + imageURL);
            }
            if (userObject.has("client_connection_id")) {
                client_connection_id = userObject.getString("client_connection_id");
            }
            if (userObject.has("formatted_name")) {
                userFullName = userObject.getString("formatted_name");
            }
            if (userObject.has("tenant_dict")) {
                JSONObject tenantObject = userObject.getJSONObject("tenant_dict");
                tenantName = tenantObject.getString("company_name");
                if (tenantName.length() == 0) {
                    tenantName = userFullName;
                }
                tenantImageURL = tenantObject.getString("avatar_url");
                if (tenantImageURL.length() == 0) {
                    tenantImageURL = imageURL;
                }

                if (tenantObject.has("personalization_dict")) {
                    JSONObject personalization_dict = tenantObject.getJSONObject("personalization_dict");
                    sharedPref.edit().putString("PERSONAL_DICT", personalization_dict.toString()).apply();
                    if (personalization_dict.has("display_labels")) {
                        JSONObject pd = personalization_dict.getJSONObject("display_labels");
                        ((ApplicationContext) this.getApplication()).setPersonal_dict(pd);
                        Map<String, String> personal_dict = Util.toMap(pd);
                    }
                }

                if (tenantObject.has("telco_enabled")) {
                    sharedPref.edit().putBoolean("telco_enabled", tenantObject.getBoolean("telco_enabled")).apply();
                }
            }
            if (userObject.has("presence_status_dict")) {
                JSONObject presenceStatusDict = userObject.getJSONObject("presence_status_dict");
                if (presenceStatusDict.has("presence")) {
                    sharedPref.edit().putString("presence", presenceStatusDict.getString("presence")).apply();
                }
            }

            if (userObject.has("calendars")) {
                JSONArray calendars = userObject.getJSONArray("calendars");
                if (calendars.length() > 0) {
                    JSONObject jsonObject = (JSONObject) calendars.get(0);
                    if (jsonObject.has("provider")) {
                        sharedPref.edit().putString("PROVIDER", jsonObject.getString("provider")).apply();
                    }
                    if (jsonObject.has("provider_uid")) {
                        sharedPref.edit().putString("PROVIDERID", jsonObject.getString("provider_uid")).apply();
                    }
                }

            }

            if (userObject.has("status"))
                sharedPref.edit().putString("STATUS",
                        userObject.getString("status")).apply();
            if (userObject.has("preferred_language")) {
                String lang = userObject.getString("preferred_language");
                if (!sharedPref.getString(ApplicationContext.key_lang, "").equals(lang)) {
                    ApplicationContext.updateResources(this, lang);
                }
            }
            if (userObject.has("timeZone"))
                sharedPref.edit().putString("_TIMEZONE", userObject.getString("timeZone")).apply();

            if (userObject.has("public_profile")) {
                JSONObject jsonObject = userObject.getJSONObject("public_profile");
                if (jsonObject.has("cell_visibility"))
                    sharedPref.edit().putBoolean("cell_visibility", jsonObject.getBoolean("cell_visibility")).apply();
                if (jsonObject.has("email_visibility"))
                    sharedPref.edit().putBoolean("email_visibility", jsonObject.getBoolean("email_visibility")).apply();
                if (jsonObject.has("display_on_website"))
                    sharedPref.edit().putBoolean("display_on_website", jsonObject.getBoolean("display_on_website")).apply();
                if (jsonObject.has("allow_contact_public_profile"))
                    sharedPref.edit().putBoolean("allow_contact_public_profile", jsonObject.getBoolean("allow_contact_public_profile")).apply();
                if (jsonObject.has("last_active_visibility"))
                    sharedPref.edit().putBoolean("last_active_visibility", jsonObject.getBoolean("last_active_visibility")).apply();
            }

//            Util.makeToast(userObject.getJSONObject("module_access").toString());
            if (userObject.has("module_access")) {
                JSONObject objModuleAccess = userObject.getJSONObject("module_access");
                if (objModuleAccess.has("all") && objModuleAccess.getString("all").equals("*")) {
                    sharedPref.edit().putBoolean("ACCESS_LEAD", true).apply();
                    sharedPref.edit().putBoolean("ACCESS_CONTACT", true).apply();
                    sharedPref.edit().putBoolean("ACCESS_TASK", true).apply();
                    sharedPref.edit().putBoolean("ACCESS_ACCOUNT", true).apply();
                    sharedPref.edit().putBoolean("ACCESS_OPPORTUNITY", true).apply();
                } else {
                    sharedPref.edit().putBoolean("ACCESS_LEAD", setModuleAccess(objModuleAccess, "lead")).apply();
                    sharedPref.edit().putBoolean("ACCESS_CONTACT", setModuleAccess(objModuleAccess, "contact")).apply();
                    sharedPref.edit().putBoolean("ACCESS_TASK", setModuleAccess(objModuleAccess, "task")).apply();
                    sharedPref.edit().putBoolean("ACCESS_ACCOUNT", setModuleAccess(objModuleAccess, "account")).apply();
                    sharedPref.edit().putBoolean("ACCESS_OPPORTUNITY", setModuleAccess(objModuleAccess, "opportunity")).apply();
                }
            }
            sharedPref.edit().putString("TENANT_NAME", tenantName).apply();
            sharedPref.edit().putString("CLIENT_CONNECTION_ID", client_connection_id).apply();
            sharedPref.edit().putString("tenant_id", tenant_id).apply();
            sharedPref.edit().putString("TENANT_IMAGE_URL", tenantImageURL).apply();
            //sharedPref.edit().putInt("ACCOUNT_LEVEL", accountLevel).apply();
            sharedPref.edit().putString("USER_NAME", userName).apply();
            sharedPref.edit().putString("USER_FORMATTED_NAME", userFullName).apply();
            sharedPref.edit().putString("USER_EMAIL", email).apply();
            sharedPref.edit().putBoolean("IS_TENANT_ADMIN", is_tenant_admin).apply();
            sharedPref.edit().putString("USER_ID", userObject.getString("id")).apply();
            sharedPref.edit().putBoolean("IS_ADMIN", userObject.getBoolean("is_tenant_admin")).apply();
            sharedPref.edit().putString("USER_TOKEN", userObject.getString("token")).apply();
            sharedPref.edit().putString("USER_IMAGEURL", imageURL).apply();
            sharedPref.edit().putBoolean("softphone_enabled", softphone_enabled).apply();
            sharedPref.edit().putString("sip_phone", sip_phone).apply();
            sharedPref.edit().putString("sip_fax_phone", sip_fax_phone).apply();
            sharedPref.edit().putString("virtual_phone", virtual_phone).apply();
            WsManager ws = WSSingleton.getInstanceWM(getApplicationContext());
            ws.startConnect();
            getFCMToken();
            Intent intent = new Intent(getApplicationContext(), NavigationActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        // disable going back to the MainActivity
        moveTaskToBack(true);
    }

    private boolean setModuleAccess(JSONObject jsonObject, String moduleName) {
        try {
            if (jsonObject.has(moduleName)) {
                StringBuilder strArr = new StringBuilder();
                JSONArray arr = jsonObject.getJSONArray(moduleName);
                if (arr.length() > 0) {
                    for (int i = 0; i < arr.length(); i++) {
                        strArr.append(arr.getString(i));
                    }
                }
                return strArr.toString().equals("CRUD");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    private void getFCMToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        LogUtils.Print(TAG, "getInstanceId failed" + task.getException());
                        return;
                    }
                    // Get new Instance ID token
                    String token = task.getResult().getToken();
                    LogUtils.Print(TAG, "token --> " + token);
                    SharedPreferenceData.getInstance(LoginActivity.this).setFCMToken(token);

                    updateProfile(token);
                    String msg = getString(R.string.msg_token_fmt, token);
                    LogUtils.Print(TAG, msg);
                });
    }

    private void updateProfile(String fcm_token) {
        JSONObject postData = new JSONObject();
        String deviceId = DeviceInfo.getDeviceID(getApplicationContext());
        String req = "{\"" + deviceId + "\":\"" + fcm_token + "\"}";
        try {
            postData.put("push_notification_dict", req);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        LogUtils.Print(TAG, "postData --> " + postData);
        NetworkManager.customJsonObjectRequest(
                LoginActivity.this, Constants.UPDATE_PROFILE + "?push_notification_dict=" + req + "&connection_id =" + SharedPreferenceData.getInstance(getApplicationContext()).getClientConnectionID() + "&os=android" + "&os_version=" + DeviceInfo.getAndroidVersion() + "&device_meta=" + DeviceInfo.getDeviceName(), null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtils.Print("RESPONSE", response.toString());
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("UPDATE PROFILE ERROR", message);
                    }
                }, false);
    }

    @Override
    public void onResult(String message) {

    }

    private void signIn() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(new Scope("https://www.googleapis.com/auth/calendar.readonly"))
                .requestScopes(new Scope("https://www.googleapis.com/auth/calendar.events"))
                .requestServerAuthCode(Constants.clientId3)
                .requestEmail()
                .build();
        GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, REQUEST_GOOGLE_LOGIN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_GOOGLE_LOGIN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(@NonNull Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            if (account != null) {
                String authCode = account.getServerAuthCode();
                LogUtils.Print(TAG, "authCode" + authCode);
                signUpTenantCB(authCode, "google", "");
            } else {
                Util.onMessage(this, getResources().getString(R.string.server_err));
            }
        } catch (ApiException e) {
            LogUtils.Print(TAG, "signInResult:failed code=" + e.getStatusCode());
            Util.onMessage(this, getResources().getString(R.string.server_err));
        }
    }

    private void signUpTenantCB(String authCode, String provider, String queryString) {

        String URL = (provider.equals("google") ? Constants.googleSignupTenantCB : Constants.microsoftSignupTenantCB) +
                "?code=" + authCode +
                "&mobile_client_type=android" +
                "&mobile_client_id=" + (provider.equals("google") ? Constants.getClientId(): Constants.getMicrosoftClientId())  +
                "&os=android" +
                "&os_version=" + DeviceInfo.getAndroidVersion() +
                "&device_meta=" + DeviceInfo.getDeviceName();
        if (queryString !=null && queryString.length() > 0) {
            URL +=queryString;
        }
        LogUtils.Print(TAG, "URL --> " + URL);
        showProgress();
        NetworkManager.customJsonObjectRequest(this, URL, null, new VolleyResponseListener() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                hideProgress();
                LogUtils.Print(TAG, jsonObject.toString());
                try {
                    if (Integer.parseInt(jsonObject.getString("result")) == 1) {
                        saveDataAndNavigateToNextScreen(jsonObject);
                    } else {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                if (jsonObject.has("msg")) {
                                    try {
                                        Util.onMessage(LoginActivity.this, jsonObject.getString("msg"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    Util.onMessage(LoginActivity.this, getResources().getString(R.string.server_err));
                                }
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String message) {
                hideProgress();
                LogUtils.Print(TAG, message);
            }
        }, false);
    }

    private void deleteCache() {
        try {
            File dir = getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            if (children != null) {
                for (String child : children) {
                    boolean success = deleteDir(new File(dir, child));
                    if (!success) {
                        return false;
                    }
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    //MSFT Login
    /* Use MSAL to acquireToken for the end-user
     * Callback will call Graph api w/ access token & update UI
     */
    private void onCallGraphClicked() {
        sampleApp.acquireToken(this, SCOPES, getAuthInteractiveCallback());
    }

    /* Use Volley to make an HTTP request to the /me endpoint from MS Graph using an access token */
    private void callGraphAPI() {
        LogUtils.Print(TAG, "Starting volley request to graph");

        /* Make sure we have a token to send to graph */
        String accessToken = authResult.getAccessToken();
        String idToken = authResult.getIdToken();
        String[] scope = authResult.getScope();


        RequestQueue queue = Volley.newRequestQueue(this);
        JSONObject parameters = new JSONObject();

        try {
            parameters.put("key", "value");
        } catch (Exception e) {
            LogUtils.Print(TAG, "Failed to put parameters: " + e.toString());
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, MSGRAPH_URL,
                parameters, response -> {
            /* Successfully called graph, process data and send to UI */
            LogUtils.Print(TAG, "Response: " + response.toString());
            try {
                JSONObject jsonObject = new JSONObject(response.toString());
                if (jsonObject.has("id")) {
                    signUpTenantCB("", "microsoft", "&p_access_token="+accessToken+"&p_id_token="+idToken + "&p_scope="+ Arrays.toString(scope));
                } else {
                    Util.makeToast(getResources().getString(R.string.server_err));
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Util.makeToast(getResources().getString(R.string.server_err));
            }
        }, error -> {
            LogUtils.Print(TAG, "Error: " + error.toString());
            Util.makeToast(error.toString());
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + authResult.getAccessToken());
                return headers;
            }
        };

        LogUtils.Print(TAG, "Adding HTTP GET to Queue, Request: " + request.toString());

        request.setRetryPolicy(new DefaultRetryPolicy(
                3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }

    /* Callback used in for silent acquireToken calls.
     * Looks if tokens are in the cache (refreshes if necessary and if we don't forceRefresh)
     * else errors that we need to do an interactive request.
     */
    private AuthenticationCallback getAuthSilentCallback() {
        return new AuthenticationCallback() {

            @Override
            public void onSuccess(IAuthenticationResult authenticationResult) {
                /* Successfully got a token, call graph now */
                LogUtils.Print(TAG, "Successfully authenticated");
                /* Store the authResult */
                authResult = authenticationResult;
                /* call graph */
                callGraphAPI();
                /* update the UI to post call graph state */
            }

            @Override
            public void onError(MsalException exception) {
                /* Failed to acquireToken */
                LogUtils.Print(TAG, "Authentication failed: " + exception.toString());
                if (exception instanceof MsalClientException) {
                    /* Exception inside MSAL, more info inside MsalError.java */
                    Util.makeToast(exception.getMessage());
                } else if (exception instanceof MsalServiceException) {
                    /* Exception when communicating with the STS, likely config issue */
                    Util.makeToast(exception.getMessage());
                } else if (exception instanceof MsalUiRequiredException) {
                    /* Tokens expired or no session, retry with interactive */
                    Util.makeToast(exception.getMessage());
                }
            }

            @Override
            public void onCancel() {
                /* User cancelled the authentication */
                LogUtils.Print(TAG, "User cancelled login.");
                Util.makeToast(getResources().getString(R.string.login_failed));
            }
        };
    }

    /* Callback used for interactive request.  If succeeds we use the access
     * token to call the Microsoft Graph. Does not check cache
     */
    private AuthenticationCallback getAuthInteractiveCallback() {
        return new AuthenticationCallback() {
            @Override
            public void onSuccess(IAuthenticationResult authenticationResult) {
                /* Successfully got a token, call graph now */
                LogUtils.Print(TAG, "Successfully authenticated");
                LogUtils.Print(TAG, "ID Token: " + authenticationResult.getIdToken());
                /* Store the auth result */
                authResult = authenticationResult;
                /* call graph */
                callGraphAPI();
                /* update the UI to post call graph state */
            }

            @Override
            public void onError(MsalException exception) {
                /* Failed to acquireToken */
                LogUtils.Print(TAG, "Authentication failed: " + exception.toString());
                if (exception instanceof MsalClientException) {
                    Util.makeToast(exception.getMessage());
                    /* Exception inside MSAL, more info inside MsalError.java */
                } else if (exception instanceof MsalServiceException) {
                    /* Exception when communicating with the STS, likely config issue */
                    Util.makeToast(exception.getMessage());
                }
            }

            @Override
            public void onCancel() {
                /* User canceled the authentication */
                LogUtils.Print(TAG, "User cancelled login.");
                Util.makeToast(getResources().getString(R.string.login_failed));
            }
        };
    }
}