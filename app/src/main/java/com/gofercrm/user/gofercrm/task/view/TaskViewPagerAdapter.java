package com.gofercrm.user.gofercrm.task.view;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.gofercrm.user.gofercrm.task.Task;
import com.gofercrm.user.gofercrm.task.view.tab.comments.TaskCommentsFragment;
import com.gofercrm.user.gofercrm.task.view.tab.general.TaskGeneralFragment;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

public class TaskViewPagerAdapter extends FragmentPagerAdapter {
    private Task task;

    public TaskViewPagerAdapter(FragmentManager fm, Task task) {
        super(fm);
        this.task = task;
    }

    @NotNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new TaskGeneralFragment();
            case 1:
                TaskCommentsFragment fragment = new TaskCommentsFragment();
                Bundle bundle = new Bundle();
                bundle.putString("TASK_NAME", task.getName());
                bundle.putString("TASK_ID", task.getTaskId());
                bundle.putSerializable("COMMENTS", (Serializable) task.getComments());
                fragment.setArguments(bundle);
                return fragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}