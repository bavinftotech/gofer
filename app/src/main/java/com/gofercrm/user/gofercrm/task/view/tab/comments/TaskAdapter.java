package com.gofercrm.user.gofercrm.task.view.tab.comments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.implementor.RecyclerViewClickListener;
import com.gofercrm.user.gofercrm.task.NewTaskActivity;
import com.gofercrm.user.gofercrm.task.Task;
import com.gofercrm.user.gofercrm.task.view.TaskViewActivity;
import com.gofercrm.user.gofercrm.util.Util;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.TaskViewHolder> {

    private List<Task> tasks;
    private Activity activity;

    public TaskAdapter(List<Task> tasks, Activity activity) {
//        setHasStableIds(true);
        this.tasks = tasks;
        this.activity = activity;
    }

    public static long getDateDiff(SimpleDateFormat format, String oldDate, String newDate) {
        try {
            return TimeUnit.DAYS.convert(format.parse(newDate).getTime() - format.parse(oldDate).getTime(), TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @NotNull
    @Override
    public TaskAdapter.TaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.content_task, parent, false);
        return new TaskAdapter.TaskViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TaskAdapter.TaskViewHolder holder, final int position) {
        final Task task = tasks.get(position);
        holder.type.setText(task.getType());
        holder.name.setText(Util.capitalize(task.getName() != null ? task.getName() : ""));
        holder.status.setText(task.getStatus());
        Drawable drawable = ContextCompat.getDrawable(activity, task.getStatus().equals("completed") ?
                R.drawable.ic_check_box_true : R.drawable.ic_check_box_false);
        holder.tvStatus.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
        holder.tvStatus.setClickable(!task.getStatus().equals("completed"));
        String pattern = "E MMM dd,yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String due_date = simpleDateFormat.format(new Date(Long.parseLong(task.getDueDate().toString()) * 1000));
        holder.due_date.setText(activity.getResources().getString(R.string.due_on_) + " " + due_date);

        String current_date = simpleDateFormat.format(new Date());

        switch (task.getStatus()) {
            case "assigned":
                holder.borderView.setBackgroundColor(ContextCompat.getColor(activity, R.color.assigned));
                holder.status.setTextColor(ContextCompat.getColor(activity, R.color.assigned));
                break;
            case "completed":
                holder.borderView.setBackgroundColor(ContextCompat.getColor(activity, R.color.completed));
                holder.status.setTextColor(ContextCompat.getColor(activity, R.color.completed));
                break;
            case "in-progress":
                holder.borderView.setBackgroundColor(ContextCompat.getColor(activity, R.color.progress));
                holder.status.setTextColor(ContextCompat.getColor(activity, R.color.progress));
                break;
            case "rejected":
            default:
                holder.borderView.setBackgroundColor(ContextCompat.getColor(activity, R.color.rejected));
                holder.status.setTextColor(ContextCompat.getColor(activity, R.color.rejected));
                break;
        }

        int dateDifference = (int) getDateDiff(new SimpleDateFormat("E MMM dd,yyyy"), due_date, current_date);
        if (dateDifference < 0) {
            holder.task_remaining_days.setText(String.valueOf(dateDifference * -1));
            holder.task_remaining_days.setBackground(ContextCompat.getDrawable(activity, R.drawable.circlebackgroundgreen));
            holder.difference_text.setText(activity.getResources().getString(R.string.days_remaining));
            holder.task_remaining_days.setVisibility(View.VISIBLE);
            holder.difference_text.setVisibility(View.VISIBLE);
        } else if (dateDifference == 0) {
            holder.task_remaining_days.setText(String.valueOf(dateDifference));
            holder.difference_text.setText(activity.getResources().getString(R.string.due_now));
            holder.task_remaining_days.setBackground(ContextCompat.getDrawable(activity, R.drawable.circlebackgroundyellow));
            holder.task_remaining_days.setVisibility(View.VISIBLE);
            holder.difference_text.setVisibility(View.VISIBLE);
        } else if (dateDifference == 1) {
            holder.task_remaining_days.setText(String.valueOf(dateDifference));
            holder.difference_text.setText(activity.getResources().getString(R.string.day_over_due));
            holder.task_remaining_days.setBackground(ContextCompat.getDrawable(activity, R.drawable.circlebackgroundorange));
            holder.borderView.setBackgroundColor(ContextCompat.getColor(activity, R.color.rejected));
            holder.task_remaining_days.setVisibility(View.VISIBLE);
            holder.difference_text.setVisibility(View.VISIBLE);
        } else {
            holder.task_remaining_days.setVisibility(View.GONE);
            holder.difference_text.setVisibility(View.GONE);
            holder.task_remaining_days.setText(String.valueOf(dateDifference));
            holder.difference_text.setText(activity.getResources().getString(R.string.day_over_due));
            holder.task_remaining_days.setBackground(ContextCompat.getDrawable(activity, R.drawable.circlebackgroundorange));
            holder.borderView.setBackgroundColor(ContextCompat.getColor(activity, R.color.rejected));
        }
        holder.subject.setText(task.getSubjectName());

        switch (task.getAction()) {
            case "call":
                holder.task_action_btn.setVisibility(View.VISIBLE);
                holder.task_action_btn.setImageResource(R.drawable.phone);
                break;
            case "postcard":
                holder.task_action_btn.setVisibility(View.VISIBLE);
                holder.task_action_btn.setImageResource(R.drawable.ic_location);
                break;
            case "text":
                holder.task_action_btn.setVisibility(View.VISIBLE);
                holder.task_action_btn.setImageResource(R.drawable.ic_message);
                break;
            case "email":
                holder.task_action_btn.setVisibility(View.VISIBLE);
                holder.task_action_btn.setImageResource(R.drawable.ic_email);
                break;
            case "general":
            case "":
            default:
                holder.task_action_btn.setVisibility(View.INVISIBLE);
                break;
        }

        holder.task_action_btn.setOnClickListener(view -> {
            if (task.getAction().equals("call") && task.getAction_data() != null && !task.getAction_data().isEmpty()) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + task.getAction_data()));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(intent);
            } else if (task.getAction().equals("postcard") && task.getAction_data() != null && !task.getAction_data().isEmpty()) {
                Uri gmmIntentUri = Uri.parse("google.navigation:q=" + task.getAction_data());
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                mapIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(mapIntent);
            } else if (task.getAction().equals("text") && task.getAction_data() != null && !task.getAction_data().isEmpty()) {
                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.setData(Uri.parse("sms:" + task.getAction_data()));
                sendIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(sendIntent);
            } else if (task.getAction().equals("email") && task.getAction_data() != null && !task.getAction_data().isEmpty()) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                Uri data = Uri.parse("mailto:" + task.getAction_data());
                intent.setData(data);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(intent);
            }
        });

        holder.task_comment_btn.setOnClickListener(v -> {
//            Intent intent = new Intent(activity, TaskCommentsActivity.class);
//            intent.putExtra("TASK_NAME", tasks.get(position).getName());
//            intent.putExtra("TASK_ID", tasks.get(position).getTaskId());
//            intent.putExtra("COMMENTS", (Serializable) tasks.get(position).getComments());
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            activity.startActivity(intent);
        });
        holder.task_edit_btn.setOnClickListener(v -> {
            Intent intent = new Intent(activity, NewTaskActivity.class);
            intent.putExtra("TASK_ID", tasks.get(position).getTaskId());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activity.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return tasks.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public class TaskViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView type, status, name, subject, due_date, task_remaining_days, difference_text;
        public View borderView;
        public ImageView task_comment_btn;
        public ImageView task_edit_btn;
        public ImageView task_action_btn;
        public TextView tvStatus;
        public CardView cardView;

        public TaskViewHolder(View view) {
            super(view);
            cardView = view.findViewById(R.id.cardView);
            tvStatus = view.findViewById(R.id.tvStatus);
            type = view.findViewById(R.id.task_type_id);
            status = view.findViewById(R.id.task_status_id);
            name = view.findViewById(R.id.task_name_id);
            subject = view.findViewById(R.id.task_subject_id);
            due_date = view.findViewById(R.id.task_duedate_id);
            borderView = view.findViewById(R.id.taskborder_id);
            task_remaining_days = view.findViewById(R.id.task_remaining_days);
            difference_text = view.findViewById(R.id.difference_text);
            task_comment_btn = view.findViewById(R.id.task_comments_id);
            task_action_btn = view.findViewById(R.id.task_action_btn_id);
            task_edit_btn = view.findViewById(R.id.task_edit_id);

            tvStatus.setOnClickListener(this);
            cardView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v == cardView) {
                if (getAdapterPosition() != RecyclerView.NO_POSITION) {
                    TaskViewActivity.task = tasks.get(getAdapterPosition());
                    Intent intent = new Intent(activity, TaskViewActivity.class);
//                    intent.putExtra("DATA", tasks.get(getAdapterPosition()));
                    activity.startActivityForResult(intent, 222);
                }
            } else {
                if (!tasks.get(getAdapterPosition()).getStatus().equals("completed")) {
                    if (listener != null) {
                        listener.onItemClick(1, getAdapterPosition(), v);
                    }
                }
            }
        }
    }

    private RecyclerViewClickListener listener;

    public void setOnRecyclerViewItemClickListener(RecyclerViewClickListener clickListener) {
        listener = clickListener;
    }
}