package com.gofercrm.user.gofercrm.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;

import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.implementor.DialogButtonClickListener;
import com.gofercrm.user.gofercrm.lead.upload.DialogUserInputListener;

public class DialogUtils {

    public static void showUserInputDialog(final Context context, String strTitle,
                                           final DialogUserInputListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getResources().getString(R.string.app_name));
        builder.setIcon(R.mipmap.ic_launcher_round);
        builder.setMessage(strTitle);
        builder.setCancelable(false);

        View viewInflated = LayoutInflater.from(context)
                .inflate(R.layout.dialog_user_input, null, false);

        final EditText editText = viewInflated.findViewById(R.id.et);
        builder.setView(viewInflated);

        builder.setPositiveButton(android.R.string.ok, (dialog, which) -> {
            if (!editText.getText().toString().trim().isEmpty()) {
                Util.hideSoftKeyboard(editText);
                dialog.dismiss();
                if (listener != null) {
                    listener.onDialogUserInput(editText.getText().toString().trim());
                }
            } else {
                Util.onMessage(context, context.getResources().getString(R.string.enter_message_empty));
                if (listener != null) {
                    listener.onDialogUserCancelInput();
                }
            }
        });
        builder.setNegativeButton(android.R.string.cancel, (dialog, which) -> {
            Util.hideSoftKeyboard(editText);
            dialog.cancel();
            if (listener != null) {
                listener.onDialogUserCancelInput();
            }
        });
        builder.show();
    }

    public static void showConfirmationDialog(Context context, final DialogButtonClickListener listener,
                                              String strMsg) {
        showConfirmationDialog(context, listener, strMsg,
                context.getResources().getString(android.R.string.ok),
                context.getResources().getString(android.R.string.cancel));
    }

    public static void showConfirmationDialog(Context context, final DialogButtonClickListener listener,
                                              String strMsg, String strPositive, String strNegative) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getResources().getString(R.string.app_name));
        builder.setIcon(R.mipmap.ic_launcher_round);
        builder.setMessage(strMsg);
        builder.setPositiveButton(
                strPositive,
                (dialog, which) -> {
                    dialog.dismiss();
                    if (listener != null)
                        listener.onPositiveButtonClick();
                });
        builder.setNegativeButton(
                strNegative,
                (dialog, which) -> {
                    dialog.dismiss();
                    if (listener != null)
                        listener.onNegativeButtonClick();
                });
        AlertDialog alert = builder.create();
        alert.setCanceledOnTouchOutside(false);
        alert.setCancelable(false);
        alert.show();
    }

    public static void showDisqualifyDialog(final Context context, String strTitle, String strHint,
                                            String strPositive, String strNegative,
                                            final DialogUserInputListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(strTitle);
        builder.setCancelable(false);

        View viewInflated = LayoutInflater.from(context)
                .inflate(R.layout.dialog_user_input, null, false);

        final EditText editText = viewInflated.findViewById(R.id.et);
        editText.setHint(strHint);
        builder.setView(viewInflated);

        builder.setPositiveButton(strPositive, (dialog, which) -> {
            if (!editText.getText().toString().trim().isEmpty()) {
                Util.hideSoftKeyboard(editText);
                dialog.dismiss();
                if (listener != null) {
                    listener.onDialogUserInput(editText.getText().toString().trim());
                }
            } else {
                Util.onMessage(context, context.getResources().getString(R.string.reason_empty_err));
                if (listener != null) {
                    listener.onDialogUserCancelInput();
                }
            }
        });
        builder.setNegativeButton(strNegative, (dialog, which) -> {
            Util.hideSoftKeyboard(editText);
            dialog.cancel();
            if (listener != null) {
                listener.onDialogUserCancelInput();
            }
        });
        builder.show();
    }

    public static void showInformationDialog(Context context, final DialogButtonClickListener listener,
                                             String strMsg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(
                context);
        builder.setTitle(context.getResources().getString(R.string.app_name));
        builder.setMessage(strMsg);
        builder.setPositiveButton(
                context.getResources().getString(R.string.ok),
                (dialog, which) -> {
                    dialog.dismiss();
                    if (listener != null)
                        listener.onPositiveButtonClick();
                });
        AlertDialog alert = builder.create();
        alert.setCanceledOnTouchOutside(false);
        alert.setCancelable(false);
        alert.show();
    }
}