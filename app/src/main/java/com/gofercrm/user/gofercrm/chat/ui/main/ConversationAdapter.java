package com.gofercrm.user.gofercrm.chat.ui.main;

import android.Manifest;
import android.animation.Animator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Environment;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.widget.PopupMenu;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.github.chrisbanes.photoview.PhotoView;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.chat.ui.main.model.ChatData;
import com.gofercrm.user.gofercrm.comments.MainAudioPlayer;
import com.gofercrm.user.gofercrm.comments.PlayVideoActivity;
import com.gofercrm.user.gofercrm.profile.ProfileActivity;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.ProgressDialog;
import com.gofercrm.user.gofercrm.util.Util;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ConversationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int DATE = 0, YOU = 1, ME = 2;
    boolean isImageFitToScreen;
    private final List<ChatData> items;
    private final Context mContext;
    private String search_text;
    private String toolbar_name;
    private Animator mCurrentAnimatorEffect;
    private int mShortAnimationDurationEffect;
    private final int fromType;

    public ConversationAdapter(Context context, List<ChatData> items, int fromType) {
        setHasStableIds(true);
        this.mContext = context;
        this.items = items;
        this.fromType = fromType;
    }

    @Override
    public int getItemCount() {
        return this.items.size();
    }

    @Override
    public int getItemViewType(int position) {
        //More to come
        switch (items.get(position).getType()) {
            case "0":
                return DATE;
            case "1":
                return YOU;
            case "2":
                return ME;
        }
        return -1;
    }

    public void setSearch_text(String search_text) {
        this.search_text = search_text;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        switch (viewType) {
            case DATE:
                View v1 = inflater.inflate(R.layout.layout_holder_date, viewGroup, false);
                viewHolder = new HolderDate(v1);
                break;
            case YOU:
                View v2 = inflater.inflate(R.layout.layout_holder_you, viewGroup, false);
                viewHolder = new HolderYou(v2);
                break;
            default:
                View v = inflater.inflate(R.layout.layout_holder_me, viewGroup, false);
                viewHolder = new HolderMe(v);
                break;
        }
        return viewHolder;
    }

    public void addItem(List<ChatData> item) {
        items.addAll(item);
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        switch (viewHolder.getItemViewType()) {
            case DATE:
                HolderDate vh1 = (HolderDate) viewHolder;
                configureDateHeader(vh1, position);
                break;
            case YOU:
                HolderYou vh2 = (HolderYou) viewHolder;
                configureYou(vh2, position);
                break;
            default:
                HolderMe vh = (HolderMe) viewHolder;
                configureMe(vh, position);
                break;
        }
    }

    private void configureMe(final HolderMe vh1, int position) {
        final ChatData chatData = items.get(position);
        vh1.getTime().setText(items.get(position).getTime());
        vh1.getIvSMS().setVisibility(chatData.getUser_message_type() != null &&
                chatData.getUser_message_type().equals("sms") ?
                View.VISIBLE : View.GONE);
        if (chatData.getRead() == 1 && chatData.getConnection_type() != 2) {
            vh1.getImage_read().setVisibility(View.VISIBLE);
            vh1.getImage_read().setImageResource(R.drawable.check);
        } else if (chatData.getRead() == 0 && chatData.getConnection_type() != 2) {
            vh1.getImage_read().setVisibility(View.VISIBLE);
            vh1.getImage_read().setImageResource(R.drawable.check_all);
        } else {
            vh1.getImage_read().setVisibility(View.GONE);
        }
        if (chatData.getEdit() == 0) {
            vh1.getEdit().setVisibility(View.GONE);
        } else {
            vh1.getEdit().setVisibility(View.VISIBLE);
        }

        if (chatData.getText() != null && !chatData.getText().isEmpty()) {
            vh1.getChatText().setVisibility(View.VISIBLE);
            vh1.getChatText().setText(chatData.getText());
            Linkify.addLinks(vh1.getChatText(), Linkify.ALL);
            vh1.getChatText().setOnClickListener(view -> showChatMenu(vh1.getChatText(), chatData, position));
            vh1.getChatText().setOnLongClickListener(view -> {
                Util.copyDataOnClipboard(mContext, chatData.getText());
                Util.onMessage(mContext, mContext.getResources().getString(R.string.message_copied));
                return true;
            });

        } else {
            vh1.getChatText().setVisibility(View.GONE);
        }
        if (chatData.getQuotedText() != null && !chatData.getQuotedText().isEmpty()) {
            vh1.getQuotedText().setVisibility(View.VISIBLE);
            vh1.getQuotedText().setText(chatData.getQuotedText());
            Linkify.addLinks(vh1.getQuotedText(), Linkify.ALL);
        } else {
            vh1.getQuotedText().setVisibility(View.GONE);
        }
        vh1.getIvPlay().setVisibility(View.GONE);
        if (chatData.getContent_type() != null && chatData.getAtt_url() != null
                && !chatData.getContent_type().isEmpty() && !chatData.getAtt_url().isEmpty()) {
            vh1.getCardView().setVisibility(View.VISIBLE);
            vh1.getTitle().setText(chatData.getAtt_name());
            vh1.getSize().setText(chatData.getAtt_file_size());

            if (chatData.getContent_type().contains("image")) {
                RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.file_image)
                        .error(R.drawable.file_image);
                Glide.with(mContext).load(chatData.getAtt_url()).apply(options).into(vh1.getThumbnail());
                vh1.getThumbnail().setOnClickListener(v -> loadPhoto(chatData.getAtt_url()));
            } else if (chatData.getContent_type().contains("video")) {
                RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.file_video)
                        .error(R.drawable.file_video);
                Glide.with(mContext).load(chatData.getAtt_url()).apply(options).into(vh1.getThumbnail());
                vh1.getThumbnail().setOnClickListener(v -> loadVideo(chatData.getAtt_url()));
                vh1.getIvPlay().setVisibility(View.VISIBLE);
            } else if (chatData.getContent_type().contains("audio")) {
                vh1.getThumbnail().setImageResource(R.drawable.audiobook);
                vh1.getThumbnail().setOnClickListener(v -> loadAudio(chatData.getAtt_url()));
                vh1.getIvPlay().setVisibility(View.VISIBLE);
            } else {
                Glide.with(mContext).load(chatData.getThumbnail()).into(vh1.getThumbnail());
                changeImageColor(chatData.getContent_type(), vh1.getThumbnail());
            }
            vh1.getOverflow().setOnClickListener(view -> showPopupMenu(vh1.getOverflow(), chatData, position));

        } else {
            vh1.getCardView().setVisibility(View.GONE);
        }
        if (chatData.getConnection_type() == 2) {
            vh1.getThreadBtn().setVisibility(View.VISIBLE);
            vh1.getThreadBtn().setText(chatData.getThread_count() <= 0 ?
                    mContext.getResources().getString(R.string.start_a_thread) : chatData.getThread_count() +
                    " " + mContext.getResources().getString(R.string.replies));
            vh1.getThreadBtn().setOnClickListener(v -> {
                Intent intent = new Intent(mContext, ThreadActivity.class);
                intent.putExtra("MSG_OBJ", chatData);
                intent.putExtra("_NAME", (Serializable) toolbar_name);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            });
        } else {
            vh1.getThreadBtn().setVisibility(View.GONE);
        }
        if (search_text != null) {
            vh1.getChatText().setText(highLightText(search_text, chatData));
        }
    }

    private void configureYou(final HolderYou vh1, int position) {
        final ChatData chatData = items.get(position);
        vh1.getTime().setText(chatData.getTime());
        vh1.getIvSMS().setVisibility(chatData.getUser_message_type() != null &&
                chatData.getUser_message_type().equals("sms") ?
                View.VISIBLE : View.GONE);
        if (chatData.getEdit() == 0) {
            vh1.getEdit().setVisibility(View.GONE);
        } else {
            vh1.getEdit().setVisibility(View.VISIBLE);
        }
        if (chatData.getUser() != null) {
            if (chatData.getUser().getImage() != null && !chatData.getUser().getImage().isEmpty()
                    && !chatData.getUser().getImage().equals("null")) {
                vh1.getYouInitial().setVisibility(View.GONE);
                vh1.getUserImage().setVisibility(View.VISIBLE);
                RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .circleCropTransform()
                        .placeholder(R.drawable.user_icon)
                        .error(R.drawable.user_icon);
                Glide.with(mContext).load(chatData.getUser().getImage()).apply(options).into(vh1.getUserImage());
                vh1.getUserImage().setOnClickListener(v -> goToProfile(chatData));
            } else {
                vh1.getUserImage().setVisibility(View.GONE);
                vh1.getYouInitial().setVisibility(View.VISIBLE);
//                LogUtils.Print("CRASH",""+chatData.getUser().getFirst_name());
//                LogUtils.Print("CRASH",""+chatData.getUser().getLast_name());
                vh1.getYouInitial().setText(
                        (chatData.getUser().getFirst_name() != null &&
                                !chatData.getUser().getFirst_name().equals("") ?
                                chatData.getUser().getFirst_name().charAt(0) : "") + "" +
                                (chatData.getUser().getLast_name() != null &&
                                        !chatData.getUser().getLast_name().equals("") ?
                                        chatData.getUser().getLast_name().charAt(0) : ""));
                vh1.getYouInitial().setOnClickListener(v -> goToProfile(chatData));
            }
        }
        if (chatData.getText() != null && !chatData.getText().isEmpty()) {
            vh1.getChatText().setVisibility(View.VISIBLE);
            vh1.getChatText().setText(chatData.getText());
            Linkify.addLinks(vh1.getChatText(), Linkify.ALL);
            vh1.getChatText().setOnClickListener(view -> showChatMenu(vh1.getChatText(), chatData, position));
            vh1.getChatText().setOnLongClickListener(view -> {
                Util.copyDataOnClipboard(mContext, chatData.getText());
                Util.onMessage(mContext, mContext.getResources().getString(R.string.message_copied));
                return true;
            });
        } else {
            vh1.getChatText().setVisibility(View.GONE);
        }
        if (chatData.getQuotedText() != null && !chatData.getQuotedText().isEmpty()) {
            vh1.getQuotedText().setVisibility(View.VISIBLE);
            vh1.getQuotedText().setText(chatData.getQuotedText());
            Linkify.addLinks(vh1.getQuotedText(), Linkify.ALL);
        } else {
            vh1.getQuotedText().setVisibility(View.GONE);
        }

        vh1.getIvPlay().setVisibility(View.GONE);
        if (chatData.getContent_type() != null && chatData.getAtt_url() != null
                && !chatData.getContent_type().isEmpty() && !chatData.getAtt_url().isEmpty()) {
            vh1.getCardView().setVisibility(View.VISIBLE);
            vh1.getTitle().setText(chatData.getAtt_name());
            vh1.getSize().setText(chatData.getAtt_file_size());

            if (chatData.getContent_type().contains("image")) {
                RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.file_image)
                        .error(R.drawable.file_image);
                Glide.with(mContext).load(chatData.getAtt_url()).apply(options).into(vh1.getThumbnail());
                vh1.getThumbnail().setOnClickListener(v -> {
                    loadPhoto(chatData.getAtt_url());
                });
            } else if (chatData.getContent_type().contains("video")) {
                RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.file_video)
                        .error(R.drawable.file_video);
                Glide.with(mContext).load(chatData.getAtt_url()).apply(options).into(vh1.getThumbnail());
                vh1.getThumbnail().setOnClickListener(v -> loadVideo(chatData.getAtt_url()));
                vh1.getIvPlay().setVisibility(View.VISIBLE);
            } else if (chatData.getContent_type().contains("audio")) {
                vh1.getThumbnail().setImageResource(R.drawable.audiobook);
                vh1.getThumbnail().setOnClickListener(v -> loadAudio(chatData.getAtt_url()));
                vh1.getIvPlay().setVisibility(View.VISIBLE);
            } else {
                Glide.with(mContext).load(chatData.getThumbnail()).into(vh1.getThumbnail());
                changeImageColor(chatData.getContent_type(), vh1.getThumbnail());
            }
            vh1.getOverflow().setOnClickListener(view -> showPopupMenu(vh1.getOverflow(), chatData, position));

        } else {
            vh1.getCardView().setVisibility(View.GONE);
        }
        if (chatData.getConnection_type() == 2) {
            vh1.getThreadBtn().setVisibility(View.VISIBLE);
            vh1.getThreadBtn().setText(chatData.getThread_count() <= 0 ?
                    mContext.getResources().getString(R.string.start_a_thread) + "" : chatData.getThread_count() +
                    " " + mContext.getResources().getString(R.string.start_a_thread));
            vh1.getThreadBtn().setOnClickListener(v -> {
                Intent intent = new Intent(mContext, ThreadActivity.class);
                intent.putExtra("MSG_OBJ", chatData);
                intent.putExtra("_NAME", (Serializable) toolbar_name);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            });
        } else {
            vh1.getThreadBtn().setVisibility(View.GONE);
        }
        if (search_text != null) {
            vh1.getChatText().setText(highLightText(search_text, chatData));
        }
    }

    private void goToProfile(ChatData data) {
        Intent intent = new Intent(mContext, ProfileActivity.class);
        intent.putExtra("USER_ID", data.getUser().getId());
        intent.putExtra("NAME", data.getUser().getFirst_name() + "" + data.getUser().getLast_name());
        intent.putExtra("IMAGE", data.getUser().getImage());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

    private SpannableString highLightText(String query, ChatData data) {
        SpannableString spannableStringSearch = null;
        if ((query != null) && (!query.isEmpty())) {
            spannableStringSearch = new SpannableString(data.getText());
            Pattern pattern = Pattern.compile(query, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(data.getText());
            while (matcher.find()) {
                spannableStringSearch.setSpan(new ForegroundColorSpan(
                                ContextCompat.getColor(mContext, R.color.attempted)),
                        matcher.start(), matcher.end(),
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }
        return spannableStringSearch;
    }

    private void configureDateHeader(HolderDate vh1, int position) {
        vh1.getDate().setText(items.get(position).getText());
    }

    private void changeImageColor(String type, ImageView image) {
        if (type == null) {
            return;
        }
        if (type.contains("pdf")) {
            image.setColorFilter(ContextCompat.getColor(mContext, R.color.pdf));
        } else if (type.contains("msword")) {
            image.setColorFilter(ContextCompat.getColor(mContext, R.color.msword));
        } else if (type.contains("audio")) {
            image.setColorFilter(ContextCompat.getColor(mContext, R.color.colorPrimary));
        } else if (type.contains("video")) {
            image.setColorFilter(ContextCompat.getColor(mContext, R.color.colorAccent));
        } else if (type.contains("text")) {
            image.setColorFilter(ContextCompat.getColor(mContext, R.color.yellow));
        } else if (type.contains("csv")) {
            image.setColorFilter(ContextCompat.getColor(mContext, R.color.csv));
        } else if (type.contains("xml")) {
            image.setColorFilter(ContextCompat.getColor(mContext, R.color.xml));
        } else if (type.contains("excel")) {
            image.setColorFilter(ContextCompat.getColor(mContext, R.color.excel));
        } else {
            image.setColorFilter(ContextCompat.getColor(mContext, R.color.yellow));
        }
    }

    private void showPopupMenu(View view, ChatData attachment, int position) {
        // inflate menu
        PopupMenu popup = new PopupMenu(mContext, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.attachment_message_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new MyMenuItemClickListener(attachment, position));
        popup.show();
    }

    public void setToolbar_name(String toolbar_name) {
        this.toolbar_name = toolbar_name;
    }

    private void loadAudio(String audioURL) {
        Intent intent = new Intent(mContext, MainAudioPlayer.class);
        intent.putExtra("DATA", audioURL);
        mContext.startActivity(intent);
    }

    private void loadVideo(String videoURL) {
        Intent intent = new Intent(mContext, PlayVideoActivity.class);
        intent.putExtra("DATA", videoURL);
        mContext.startActivity(intent);
    }

    private void loadPhoto(String image_src) {
        AlertDialog.Builder imageDialog = new AlertDialog.Builder(mContext);
        LayoutInflater imageInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View image_layout = imageInflater.inflate(R.layout.custom_full_image, null, false);
        PhotoView image = image_layout.findViewById(R.id.fullimage);
        Glide.with(mContext).load(image_src).into(image);
        imageDialog.setView(image_layout);
        imageDialog.setPositiveButton(mContext.getResources().getString(R.string.close), (dialog, which) -> dialog.dismiss());
        imageDialog.create();
        imageDialog.show();
    }

    private void showChatMenu(View view, ChatData data, int position) {
        PopupMenu popup = new PopupMenu(mContext, view);
        Menu popupMenu = popup.getMenu();
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_chat_conversation, popupMenu);
        //type = 2 = own message
        //fromType = 2 = Room type
        if (data.getType().equals("2") && fromType != 2) {
            popupMenu.findItem(R.id.action_message_edit).setVisible(true);
            popupMenu.findItem(R.id.action_message_delete).setVisible(true);
        } else {
            popupMenu.findItem(R.id.action_message_edit).setVisible(false);
            popupMenu.findItem(R.id.action_message_delete).setVisible(false);
        }

        popup.setOnMenuItemClickListener(new ConversationAdapter.MyMenuItemClickListener(data, position));
        try {
            Field[] fields = popup.getClass().getDeclaredFields();
            for (Field field : fields) {
                if ("mPopup".equals(field.getName())) {
                    field.setAccessible(true);
                    Object menuPopupHelper = field.get(popup);
                    Class<?> classPopupHelper = Class.forName(menuPopupHelper
                            .getClass().getName());
                    Method setForceIcons = classPopupHelper.getMethod(
                            "setForceShowIcon", boolean.class);
                    setForceIcons.invoke(menuPopupHelper, true);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        popup.show();
    }

    private class DownloadFile extends AsyncTask<String, String, String> {

        //        private ProgressDialog progressDialog;
        private String fileName;
        private String folder;
        private boolean isDownloaded;

        DownloadFile(String fileName) {
            this.fileName = fileName;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            this.progressDialog = new ProgressDialog(mContext);
//            progressDialog.setCanceledOnTouchOutside(false);
//            this.progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//            this.progressDialog.setCancelable(false);
//            this.progressDialog.show();
            ProgressDialog.getInstance().show(mContext);
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection connection = url.openConnection();
                connection.connect();
                int lengthOfFile = connection.getContentLength();
                InputStream input = new BufferedInputStream(url.openStream(), 8192);
                String timestamp = new SimpleDateFormat("yyyy-MM-dd-HH-mm").format(new Date());
                fileName = timestamp + "_" + fileName;

                //Default Download directory
                folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();

                OutputStream output = new FileOutputStream(folder + "/" + fileName);

                byte[] data = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    publishProgress("" + (int) ((total * 100) / lengthOfFile));
                    output.write(data, 0, count);
                }
                output.flush();
                output.close();
                input.close();
                return mContext.getResources().getString(R.string.downloaded_at_) + " " + folder + "/" + fileName;

            } catch (Exception e) {
                e.printStackTrace();
                LogUtils.Print("Error: ", e.getMessage());
            }
            return mContext.getResources().getString(R.string.something_went_wrong);
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            ProgressDialog.getInstance().dismiss();
//            progressDialog.setProgress(Integer.parseInt(progress[0]));
        }

        @Override
        protected void onPostExecute(String message) {
//            this.progressDialog.dismiss();
            ProgressDialog.getInstance().dismiss();
            Toast.makeText(mContext,
                    message, Toast.LENGTH_LONG).show();
        }
    }

    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {
        private static final int REQUEST_EXTERNAL_STORAGE = 1;
        ChatData attachment;
        int position;
        private final String[] PERMISSIONS_STORAGE = {
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        };

        public MyMenuItemClickListener(ChatData attachment, int position) {
            this.attachment = attachment;
            this.position = position;
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            Activity activity = (Activity) mContext;
            switch (menuItem.getItemId()) {
                case R.id.action_download:
                    int permission = ActivityCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    if (permission != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(
                                activity,
                                PERMISSIONS_STORAGE,
                                REQUEST_EXTERNAL_STORAGE
                        );
                    } else {
                        new DownloadFile(attachment.getAtt_name()).execute(attachment.getAtt_url());
                    }

                    return true;
                case R.id.action_share: {
                    shareTextUrl(attachment.getAtt_name(), attachment.getAtt_url());
                    return true;
                }
                case R.id.action_copy_url: {
                    Util.setClipboard(mContext, attachment.getAtt_url());
                }
                case R.id.action_message_quote: {
                    if (mContext instanceof Conversation) {
                        ((Conversation) mContext).setQuoteText(attachment.getText());
                    }
                    return true;
                }
                case R.id.action_message_edit: {
                    if (mContext instanceof Conversation) {
                        Conversation con_activity = ((Conversation) mContext);
                        con_activity.setMsgText(attachment.getText(), attachment.getId());
                        String text = con_activity.getMsgText();
                        if (attachment.getRoom_id() != null && !attachment.getRoom_id().isEmpty() && text != null && !text.isEmpty()) {
                            attachment.setText(text);
                            attachment.setEdit(1);
                        }
                    }
                    return true;
                }
                case R.id.action_message_delete: {
                    if (mContext instanceof Conversation) {
                        Conversation con_activity = ((Conversation) mContext);
                        new androidx.appcompat.app.AlertDialog.Builder(mContext)
                                .setTitle(mContext.getResources().getString(R.string.delete))
                                .setMessage(mContext.getResources().getString(R.string.delete_message_confirmation))
                                .setIcon(android.R.drawable.ic_menu_delete)
                                .setPositiveButton(mContext.getResources().getString(R.string.delete), (dialog, whichButton) -> {
                                    if (attachment.getRoom_id() != null && !attachment.getRoom_id().equals("")) {

                                        con_activity.deleteRoomMessage(attachment.getId());
                                    } else {
                                        con_activity.deleteConnectionMessage(attachment.getId());
                                    }
                                    items.remove(position);
                                    notifyItemRemoved(position);
                                    notifyItemRangeChanged(position, items.size());

                                })
                                .setNegativeButton(mContext.getResources().getString(R.string.no), null).show();
                    }
                }
                default:
            }
            return false;
        }

        private void shareTextUrl(String name, String text) {
            Intent share = new Intent(android.content.Intent.ACTION_SEND);
            share.setType("text/plain");
            share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
            share.putExtra(Intent.EXTRA_SUBJECT, mContext.getResources().getString(R.string.share_url));
            share.putExtra(Intent.EXTRA_TEXT, text);
            share.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(Intent.createChooser(share, mContext.getResources().getString(R.string.share_link_)));
        }
    }
}