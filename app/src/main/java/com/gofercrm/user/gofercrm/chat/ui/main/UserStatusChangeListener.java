package com.gofercrm.user.gofercrm.chat.ui.main;

public interface UserStatusChangeListener {
    void onUserStatusChanges(int type, String strId);

    default void onUpdateHappen() {
    }
}