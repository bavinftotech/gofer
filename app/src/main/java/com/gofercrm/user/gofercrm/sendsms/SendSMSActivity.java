package com.gofercrm.user.gofercrm.sendsms;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.databinding.ActivitySendSmsBinding;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SendSMSActivity extends BaseActivity {

    private ActivitySendSmsBinding binding;
    private final int REQUEST_CODE_ASK_PERMISSIONS = 123;
    private final int PICK_CONTACT = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_send_sms);
        binding.setActivity(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.send_sms));
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());
        setUpHeaderView();
    }

    private void setUpHeaderView() {
        binding.etComment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                binding.tvCount.setText(charSequence.toString().length() + " / 600");
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    public void onContactClicked() {
        int hasWriteContactsPermission = checkSelfPermission(Manifest.permission.READ_CONTACTS);
        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_CONTACTS},
                    REQUEST_CODE_ASK_PERMISSIONS);
        } else {
            Intent i = new Intent(Intent.ACTION_PICK);
            i.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
            i.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(i, PICK_CONTACT);
        }
    }

    public void onSubmitClicked() {
        if (binding.etPhoneNumber.getText().toString().trim().isEmpty()) {
            binding.etPhoneNumber.requestFocus();
            Util.showSnackBar(getResources().getString(R.string.phone_empty_err), SendSMSActivity.this);
        } else if (binding.etComment.getText().toString().trim().isEmpty()) {
            binding.etComment.requestFocus();
            Util.showSnackBar(getResources().getString(R.string.comment_empty_err), SendSMSActivity.this);
        } else {
            sendSMSAPIRequest();
        }
    }

    private void sendSMSAPIRequest() {
        JSONObject postData = new JSONObject();
        try {
            JSONObject jsonObject = new JSONObject();
            postData.put("sms_payload", jsonObject);
            jsonObject.put("dst", binding.etPhoneNumber.getText().toString().trim());
            jsonObject.put("text", binding.etComment.getText().toString().trim());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        LogUtils.Print("postData--> ", "" + postData);
        NetworkManager.customJsonObjectRequest(
                SendSMSActivity.this, Constants.SEND_SMS_API, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtils.Print("onResponse ", "" + response);
                        try {
                            if (Integer.parseInt(response.getString("result")) == 1) {
                                Util.onMessage(SendSMSActivity.this, getResources().getString(R.string.send_sms_success));
                                binding.etComment.setText("");
                                binding.etPhoneNumber.setText("");
                                Util.hideSoftKeyboard(binding.etComment);
                                Util.hideSoftKeyboard(binding.etPhoneNumber);
                            } else {
                                Util.onMessage(SendSMSActivity.this, getResources().getString(R.string.send_sms_failure));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Util.showSnackBar(getResources().getString(R.string.server_err), SendSMSActivity.this);
                        }
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("onError=", message);
                        Util.showSnackBar(message, SendSMSActivity.this);
                    }
                }, true);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_CONTACT) {
            if (resultCode == Activity.RESULT_OK) {
                Uri contactData = data.getData();
                Cursor c = getContentResolver().query(contactData, null, null, null, null);
                if (c.moveToFirst()) {
                    String phoneNumber = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    binding.etPhoneNumber.setText(phoneNumber);
                }
                c.close();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_ASK_PERMISSIONS) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent i = new Intent(Intent.ACTION_PICK);
                i.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                i.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivityForResult(i, PICK_CONTACT);
            } else {
                Util.onMessage(getApplicationContext(), getResources().getString(R.string.contact_access_denied));
            }
        }
    }
}