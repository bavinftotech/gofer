package com.gofercrm.user.gofercrm.lead;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.telephony.PhoneNumberUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.chat.ui.main.Conversation;
import com.gofercrm.user.gofercrm.entity.Email;
import com.gofercrm.user.gofercrm.entity.Phone;
import com.gofercrm.user.gofercrm.util.Util;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.List;

public class LeadListAdapter extends RecyclerView.Adapter<LeadListAdapter.MyViewHolder> {

    private List<LeadList> leadLists;
    private Activity activity;

    public LeadListAdapter(List<LeadList> leadsList, Activity activity) {
        setHasStableIds(true);
        this.leadLists = leadsList;
        this.activity = activity;
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lead_list_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        LeadList lead = leadLists.get(position);
        holder.title.setText(!lead.getTitle().isEmpty() ? Util.capitalize(lead.getTitle()) : "");
        holder.leadInitial.setText(Util.initialChar(lead.getTitle()));
        holder.status.setText(!lead.getStatus().isEmpty() ? Util.capitalize(lead.getStatus()) : "");
        switch (lead.getStatus()) {
            case "open":
                holder.status.setTextColor(ContextCompat.getColor(activity, R.color.open));
                break;
            case "attempted":
                holder.status.setTextColor(ContextCompat.getColor(activity, R.color.attempted));
                break;
            case "contacted":
                holder.status.setTextColor(ContextCompat.getColor(activity, R.color.contacted));
                break;
            case "meeting-setup":
                holder.status.setTextColor(ContextCompat.getColor(activity, R.color.meetingsetup));
                break;
            case "disqualified":
                holder.status.setTextColor(ContextCompat.getColor(activity, R.color.disqualified));
                break;
            case "assigned":
                holder.status.setTextColor(ContextCompat.getColor(activity, R.color.assigned));
                break;
        }
        if (lead.getFileAs().equals("mobile_app")) {
            holder.mobile_lead_id.setVisibility(View.VISIBLE);
            holder.mobile_lead_id.setOnClickListener(v -> {
                Intent intent = new Intent(activity, Conversation.class);
                intent.putExtra("_ID", lead.getId());
                intent.putExtra("_TYPE", (Serializable) 0);
                intent.putExtra("_NAME", lead.getTitle());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(intent);
            });
        } else {
            holder.mobile_lead_id.setVisibility(View.GONE);
        }
        if (lead.getEmails().size() > 0) {
            Email email = lead.getEmails().get(0);
            holder.email.setText(email.getEmail());
        } else {
            holder.email.setText("");
        }

        if (lead.getPhones().size() > 0) {
            Phone phone = lead.getPhones().get(0);
            holder.phone.setText(PhoneNumberUtils.formatNumber(phone.getPhone(), "US"));
        } else {
            holder.phone.setText("");
        }

        holder.itemView.setOnClickListener(view -> {
            Intent intent = new Intent(activity, LeadViewActivity.class);
            intent.putExtra("LEAD_ID", (Serializable) leadLists.get(position).getId());
            activity.startActivityForResult(intent, 222);
        });
    }

    @Override
    public int getItemCount() {
        return leadLists.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, email, phone, status, leadInitial;
        public ImageView mobile_lead_id;

        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.name);
            email = view.findViewById(R.id.email);
            phone = view.findViewById(R.id.phone);
            status = view.findViewById(R.id.status);
            leadInitial = view.findViewById(R.id.leadInitial);
            mobile_lead_id = view.findViewById(R.id.mobile_lead_id);
        }
    }
}