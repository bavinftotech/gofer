package com.gofercrm.user.gofercrm.tenantcomment;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.comments.CommentAttachmentActivity;
import com.gofercrm.user.gofercrm.comments.CommentsAdapter;
import com.gofercrm.user.gofercrm.entity.Comment;
import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.SharedPreferenceData;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.gofercrm.user.gofercrm.websocket.WsManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TenantCommentActivity extends BaseActivity {
    private static final String TAG = "TenantCommentActivity";

    private static final int REQUEST_CODE = 100;

    private RecyclerView recyclerView;
    private Switch switchNotify;
    private EditText commentText;
    private CommentsAdapter commentAdapter;
    private List<Comment> commentsData = new ArrayList<>();
    private ImageButton sendComment;
    private ImageButton btnAttach;
    private LinearLayout commentsControls;
    private SharedPreferences sharedPref;
    private TextView tvTenantMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tenant_comment);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getResources().getString(R.string.bulletin_comment));
        findViewById();
        setUpHeaderView();
        getBulletinBoardComments();
    }

    private void findViewById() {
        recyclerView = findViewById(R.id.comments_recycler);
        commentsControls = findViewById(R.id.commentsControls);
        switchNotify = findViewById(R.id.switchNotify);
        commentText = findViewById(R.id.commentsText);
        btnAttach = findViewById(R.id.btnAttach);
        sendComment = findViewById(R.id.btn_update_comment);
        tvTenantMessage = findViewById(R.id.tvTenantMessage);
    }

    private void setUpHeaderView() {
        sharedPref = getSharedPreferences("LOGIN", MODE_PRIVATE);
        if (!SharedPreferenceData.getInstance(this).isTenantAdmin()) {
            commentsControls.setVisibility(View.GONE);
        } else {
            commentsControls.setVisibility(View.VISIBLE);
        }

        commentAdapter = new CommentsAdapter(commentsData, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(commentAdapter);

        sendComment.setOnClickListener(v -> {
                    String comment = commentText.getText().toString().trim();
                    if (comment.isEmpty()) {
                        Util.makeToast(getResources().getString(R.string.comment_empty));
                        return;
                    }
                    updateComment(comment);

                    Comment objComment = new Comment(
                            SharedPreferenceData.getInstance(this).loginSharedPref.getString("USER_FORMATTED_NAME", "") + ": " + comment,
                            String.valueOf(Util.getCurrentTimeMillis()),
                            "", "");
                    commentsData.add(objComment);
                    commentText.setText("");
                    commentAdapter.notifyDataSetChanged();
                    recyclerView.post(() -> recyclerView.smoothScrollToPosition(commentAdapter.getItemCount() - 1));
                }
        );

        btnAttach.setOnClickListener(view -> {
            Intent intent1 = new Intent(this, CommentAttachmentActivity.class);
            intent1.putExtra("attachment_entity_type", "tenant");
            intent1.putExtra("attachment_entity_id", sharedPref.getString("tenant_id", ""));
            startActivityForResult(intent1, REQUEST_CODE);
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        WsManager.setOnBulletEventListener((int from, String text) -> runOnUiThread(this::getBulletinBoardComments));
    }

    @Override
    protected void onPause() {
        super.onPause();
        WsManager.setOnBulletEventListener(null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        WsManager.setOnBulletEventListener(null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_bulletin_board, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_refresh: {
                getBulletinBoardComments();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void getBulletinBoardComments() {
        showProgress();
        NetworkManager.customJsonObjectGetRequest(
                TenantCommentActivity.this, Constants.manageTenantsComments, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideProgress();
                        processEventsTenantMsg(response);
                    }

                    @Override
                    public void onError(String message) {
                        hideProgress();
                        LogUtils.Print(TAG, "onError --> " + message);
                    }
                }, true);
    }

    private void processEventsTenantMsg(JSONObject response) {
        try {
            List<Comment> comments = new ArrayList();
            if (Integer.parseInt(response.getString("result")) == 1) {
                JSONArray commentsList = response.getJSONArray("comments_list");

                for (int j = 0; j < commentsList.length(); j++) {
                    try {
                        JSONObject commentObj = commentsList.getJSONObject(j);
                        comments.add(new Comment(
                                commentObj.getString("comment"),
                                commentObj.getString("date"),
                                commentObj.has("url") ? commentObj.getString("url") : "",
                                commentObj.has("content_type") ? commentObj.getString("content_type") : ""));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                commentsData.clear();
                commentsData.addAll(comments);
                commentAdapter.notifyDataSetChanged();
                if (commentsData.size() > 2) {
                    recyclerView.smoothScrollToPosition(commentsData.size() - 1);
                }
                if (commentsData.size() == 0) {
                    tvTenantMessage.setVisibility(View.VISIBLE);
                } else {
                    tvTenantMessage.setVisibility(View.GONE);
                }
            } else {
                tvTenantMessage.setVisibility(View.VISIBLE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void updateComment(String comment) {
        if (comment == null || comment.isEmpty()) {
            return;
        }
        JSONObject postData = new JSONObject();
        try {
            postData.put("do_notify", switchNotify.isChecked());

            JSONObject jsonComments = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            JSONObject commentReq = new JSONObject();
            commentReq.put("comment", comment);
            jsonArray.put(commentReq);
            jsonComments.put("comments", jsonArray);

            postData.put("payload", jsonComments);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }
        LogUtils.Print(TAG, "postData --> " + postData);
//        sendComment.setEnabled(false);
        showProgress();
        NetworkManager.customJsonObjectRequest(
                TenantCommentActivity.this, Constants.manageTenantsComments, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        sendComment.setEnabled(true);
                        hideProgress();
//                        commentText.setText("");
//                        processEventsTenantMsg(response);
                        LogUtils.Print(TAG, "onResponse --> " + response);
                    }

                    @Override
                    public void onError(String message) {
//                        sendComment.setEnabled(true);
                        hideProgress();
                        LogUtils.Print(TAG, "onError --> " + message);
                    }
                }, true);
    }

//    private void processResult(JSONObject response) {
//        try {
//            if (Integer.parseInt(response.getString("result")) == 1) {
//                commentText.setText("");
//                manipulateComments(response);
////                Util.onMessage(this, "Comments Updated Successfully");
//            } else {
//                Util.onMessage(this, "Couldn't update the comment");
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private void manipulateComments(JSONObject response) {
//        JSONObject dataObj;
//        JSONArray commentsArray;
//        Comment comment;
//        try {
//            dataObj = response.getJSONObject("data");
//            if (dataObj != null) {
//                commentsArray = dataObj.getJSONArray("comments");
//                if (commentsArray.length() >= 1) {
//                    JSONObject obj = (JSONObject) commentsArray.get(commentsArray.length() - 1);
//                    comment = new Comment(obj.getString("comment"), obj.getString("date"),
//                            obj.has("url") ? obj.getString("url") : "",
//                            obj.has("content_type") ? obj.getString("content_type") : "");
//                    commentsData.add(comment);
//                    tvTenantMessage.setVisibility(View.GONE);
//                }
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null && data.hasExtra("DATA")) {
                    Comment comment = (Comment) data.getSerializableExtra("DATA");
                    commentsData.add(comment);
                    commentAdapter.notifyItemInserted(commentsData.size() - 1);
                    if (commentsData.size() > 2) {
                        new Handler(Looper.getMainLooper()).postDelayed(() -> recyclerView.smoothScrollToPosition(commentsData.size() - 1), 200);
                    }
                    tvTenantMessage.setVisibility(View.GONE);
                }
            }
        }
    }
}