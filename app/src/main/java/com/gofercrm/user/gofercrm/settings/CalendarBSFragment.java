package com.gofercrm.user.gofercrm.settings;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import androidx.core.widget.NestedScrollView;

import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.entity.User;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CalendarBSFragment extends BottomSheetDialogFragment implements View.OnClickListener {

    BottomSheetBehavior sheetBehavior;
    NestedScrollView layoutBottomSheet;
    String LEAD_ID;
    private Spinner lead_owner;
    private Button bs_lead_owner_cancel, bs_lead_owner_change;
    private ArrayAdapter<User> leadOwnerAdapter;
    private CalendarBSFragment currentObj;
    private ArrayList<User> owners = new ArrayList<>();
    private SettingActivity activity;

    public CalendarBSFragment() {
    }

    public CalendarBSFragment getCurrentObj() {
        return currentObj;
    }

    public void setCurrentObj(CalendarBSFragment currentObj) {
        this.currentObj = currentObj;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        activity = (SettingActivity) getActivity();
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.calendar_bottom_sheet, container, false);
        bs_lead_owner_cancel = v.findViewById(R.id.account_contact_cancel);
        bs_lead_owner_cancel.setOnClickListener(this);
        bs_lead_owner_change = v.findViewById(R.id.account_contact_save);
        bs_lead_owner_change.setOnClickListener(this);
        lead_owner = v.findViewById(R.id.bs_lead_owner);
        leadOwnerAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, owners);
        leadOwnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        lead_owner.setAdapter(leadOwnerAdapter);
        getLeadOwners();
        return v;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.account_contact_cancel) {
            if (currentObj != null) {
                currentObj.dismiss();
            }
        } else if (view.getId() == R.id.account_contact_save) {
            changeDefaultCalendar();
        }
    }

    private void changeDefaultCalendar() {
        User user = (User) lead_owner.getSelectedItem();
        String calendar_id = user.getId();
        NetworkManager.customJsonObjectRequest(
                getContext(), Constants.CALENDAR_SET_DEFAULT_URL + "?calendarID=" + calendar_id, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processResult(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("CALENDAR DEFAUlT ERR", message);
                    }
                }, false);
    }

    private void processResult(JSONObject response) {
        try {
            JSONArray calendars = response.getJSONArray("calendars");
            if (calendars.length() > 0) {
                Util.onMessageTop(getContext(), getResources().getString(R.string.calendar_defaulted_success));
                currentObj.dismiss();
                activity.refreshData();
            } else {
                Util.onMessageTop(getContext(), getResources().getString(R.string.calendar_defaulted_fail));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getLeadOwners() {
        NetworkManager.customJsonObjectRequest(
                getContext(), Constants.CALENDAR_LIST_URL, null,
                new VolleyResponseListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        processOwners(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ERRROR=", message);
                    }
                }, true);
    }

    private void processOwners(JSONObject data) {
        try {
            owners.clear();
            String id;
            String name;
            try {
                JSONArray calendars = data.getJSONArray("calendars");
                for (int i = 0; i < calendars.length(); i++) {
                    JSONObject obj = calendars.getJSONObject(i);
                    id = obj.getString("calendarID");
                    name = obj.getString("summary");
                    if (name != null) {
                        name = Util.capitalize(name);
                    }
                    if (obj.getBoolean("is_default")) {
                        name = name + " (Default)";
                    }
                    User user = new User(id, name);
                    owners.add(user);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            leadOwnerAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}