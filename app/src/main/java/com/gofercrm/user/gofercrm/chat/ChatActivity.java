package com.gofercrm.user.gofercrm.chat;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.LauncherActivity;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.chat.ui.main.ChatPagerAdapter;
import com.gofercrm.user.gofercrm.chat.ui.main.addcontact.AddContactActivity;
import com.gofercrm.user.gofercrm.room.ManageRoomActivity;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.SharedPreferenceData;
import com.gofercrm.user.gofercrm.websocket.SharedObserver;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.gofercrm.user.gofercrm.websocket.WSSingleton;
import com.gofercrm.user.gofercrm.websocket.WsManager;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONException;
import org.json.JSONObject;

import io.reactivex.disposables.Disposable;

import static com.jaiselrahman.filepicker.activity.FilePickerActivity.TAG;

public class ChatActivity extends BaseActivity {
    //public boolean isSocketConnected;
    private static final String TAG = "ChatActivity";
    private Switch mSwitchShowSecure;
    private ChatPagerAdapter chatPagerAdapter;
    private Disposable subscribe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(getResources().getString(R.string.chat));
        }
        boolean isFromShare = false;
        if (getIntent() != null && getIntent().getBooleanExtra(LauncherActivity.IS_FROM_SHARE, false)) {
            isFromShare = true;
        }
        chatPagerAdapter = new ChatPagerAdapter(this, getSupportFragmentManager(), isFromShare ? getIntent() : null);
        ViewPager viewPager = findViewById(R.id.chat_pager_id);
        viewPager.setAdapter(chatPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);

        final FloatingActionButton fab = findViewById(R.id.chat_view_room_create);
        fab.setOnClickListener(view -> {
            if (viewPager.getCurrentItem() == 1) {
                Intent leadIntent = new Intent(getApplicationContext(), ManageRoomActivity.class);
                leadIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(leadIntent);
            } else if (viewPager.getCurrentItem() == 0) {
                Intent leadIntent = new Intent(getApplicationContext(), AddContactActivity.class);
                startActivityForResult(leadIntent, 228);
            }
        });
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs) {
            @Override
            public void onPageSelected(int position) {
                if (mSwitchShowSecure != null)
                    mSwitchShowSecure.setVisibility(position != 2 ? View.VISIBLE : View.GONE);
                switch (position) {
                    case 0:
                    case 1:
                        fab.show();
                        break;
                    case 2:
                    default:
                        fab.hide();
                        break;
                }
            }
        });

        subscribe = SharedObserver.getSocketObserver().subscribe(s -> {

            try {
                JSONObject input = new JSONObject(s);
                if (input.has("type") && input.getString("type").equals("local_socket_state")) {
                    Log.d(TAG, "******************ChatActivity.SharedObserver.getSocketObserver(): " + s);
                    if (input.has("value")) {
                        //isSocketConnected = input.getString("value").equals("true");
                        supportInvalidateOptionsMenu();
                    }
                }
            }catch (JSONException e) {
                e.printStackTrace();
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        Log.d(TAG, "******************ChatActivity.onCreateOptionsMenu()***** ");
        getMenuInflater().inflate(R.menu.menu_chat, menu);
        final MenuItem menu_signal = menu.findItem(R.id.soket_connection_id);
        WsManager ws = WSSingleton.getInstanceWM(getApplicationContext());
        if (ws.isWsConnected()) {
            menu_signal.setIcon(R.drawable.wifi_strength_4);
        } else {
            menu_signal.setIcon(R.drawable.wifi_strength_off);

        }

        mSwitchShowSecure = menu.findItem(R.id.show_secure).getActionView().findViewById(R.id.switch_show_protected);
        mSwitchShowSecure.setChecked(SharedPreferenceData.getInstance(ChatActivity.this).isUserActive());
        mSwitchShowSecure.setOnCheckedChangeListener((buttonView, isChecked) -> {
            SharedPreferenceData.getInstance(ChatActivity.this)
                    .setIsUserActive(isChecked ? "Available" : "Busy");
            updateUserStatus(isChecked);
        });
        return true;
    }

    //when local_socket_state observable message is received menu will be Invalidated and this will be invoked
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        Log.d(TAG, "======ChatActivity.onPrepareOptionsMenu()===== ");
        final MenuItem menu_signal = menu.findItem(R.id.soket_connection_id);
        menu_signal.setOnMenuItemClickListener((menuItem) -> {
            menuSignalListener(menu_signal);
            return false;
        });
        WsManager ws = WSSingleton.getInstanceWM(getApplicationContext());
        if (ws.isWsConnected()) {
            menu_signal.setIcon(R.drawable.wifi_strength_4);
        } else {
            menu_signal.setIcon(R.drawable.wifi_strength_off);
            //If we navigate here after local internet connection was broken and then restored
            // the websocket will not reconnect by iteself (unlike if the server had disconnected) so lets try to reconnect
            //be wary of infinite loop socket status change===>SharedObserver.getSocketObserver()==Invalidate
            ws.startConnect();

        }

        return true;
    }

    //manually tap to try and connect websocket
    private void  menuSignalListener(MenuItem menu_signal) {

        WsManager ws = WSSingleton.getInstanceWM(getApplicationContext());
        if (!(ws.isWsConnected())) {
            Log.d(TAG, "trying to Connect Socket: ");
            ws.startConnect();
        } else {
            Log.d(TAG, "No Socket action needed ");
        }

    }

    private void updateUserStatus(boolean flag) {
        NetworkManager.customJsonObjectRequest(
                ChatActivity.this, Constants.UPDATE_PROFILE +
                        "?status=" + (flag ? "Available" : "Busy"), null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtils.Print(TAG, response.toString());
                        Toast.makeText(getApplicationContext(),
                                getResources().getString(R.string.your_status_is_now) + " " +
                                        (flag ? "Available" : "Busy"), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print(TAG, message);
                    }
                }, false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LogUtils.Print(TAG, "requestCode -> " + requestCode);
        LogUtils.Print(TAG, "resultCode -> " + resultCode);
        if (requestCode == 228) {
            if (resultCode == RESULT_OK) {
                if (chatPagerAdapter != null) {
//                    chatPagerAdapter.notifyDataSetChanged();
                    chatPagerAdapter.fragment.getContacts();
                }
            }
        }
    }
}