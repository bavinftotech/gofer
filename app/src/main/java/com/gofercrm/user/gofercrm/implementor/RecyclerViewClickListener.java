package com.gofercrm.user.gofercrm.implementor;

import android.view.View;

public interface RecyclerViewClickListener {
    void onItemClick(int flag, int position, View view);
}
