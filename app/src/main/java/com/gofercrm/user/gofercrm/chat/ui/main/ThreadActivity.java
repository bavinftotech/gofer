package com.gofercrm.user.gofercrm.chat.ui.main;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.icu.util.Calendar;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.chat.ui.main.model.ChatData;
import com.gofercrm.user.gofercrm.chat.ui.main.model.MessageData;
import com.gofercrm.user.gofercrm.entity.User;
import com.gofercrm.user.gofercrm.util.FileUtil;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.google.android.material.snackbar.Snackbar;
import com.jaiselrahman.filepicker.activity.FilePickerActivity;
import com.jaiselrahman.filepicker.config.Configurations;
import com.jaiselrahman.filepicker.model.MediaFile;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class ThreadActivity extends BaseActivity {

    private static final int READ_REQUEST_CODE = 42;
    private static final int FILE_REQUEST_CODE = 228;
    List<ChatData> data = new ArrayList<>();
    ChatData parent_message;
    String _ID;
    String ROOM_ID, ROOM_NAME;
    ImageView threadSenderImage;
    TextView threadSenderName, threadTime, threadText, threadInitial;
    SharedPreferences sharedPref;
    String token;
    private RecyclerView mRecyclerView;
    private ThreadAdapter mAdapter;
    private Button send;
    private ImageButton btn_attachment;
    private EditText text;
    private CoordinatorLayout coordinatorLayout;
    private Switch switchSMS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thread);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mRecyclerView = findViewById(R.id.thread_recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new ThreadAdapter(this, data);
        mRecyclerView.setAdapter(mAdapter);

        sharedPref = getApplicationContext().getSharedPreferences("LOGIN", MODE_PRIVATE);
        token = sharedPref.getString("USER_TOKEN", "");
        coordinatorLayout = findViewById(R.id
                .attachment_coordinator_layout_id);

        Intent intent = getIntent();
        parent_message = (ChatData) intent.getSerializableExtra("MSG_OBJ");
        ROOM_NAME = intent.getStringExtra("_NAME");
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle("Thread");
        _ID = parent_message.getId();
        ROOM_ID = parent_message.getRoom_id();

        switchSMS = findViewById(R.id.switchSMS);
        if (!sharedPref.getString("virtual_phone", "").equals("")) {
            switchSMS.setVisibility(View.VISIBLE);
            switchSMS.setChecked(false);
        }

        threadSenderImage = findViewById(R.id.thread_sender_photo);
        threadSenderName = findViewById(R.id.thread_sender_name);
        threadTime = findViewById(R.id.thread_time);

        threadSenderName.setText(parent_message.getSender_name());
        threadText = findViewById(R.id.thread_text);
        threadInitial = findViewById(R.id.thread_initial);
        threadText.setText(parent_message.getText());
        threadTime.setText(parent_message.getTime());

        if (_ID != null && ROOM_ID != null) {
            getRoomMessage(ROOM_ID, _ID, 1000);
        }
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setImage(parent_message.getUser().getImage(), parent_message.getUser().getFirst_name());
        text = findViewById(R.id.et_message);
        text.setOnClickListener(view -> mRecyclerView.postDelayed(new Runnable() {
            @Override
            public void run() {
                mRecyclerView.smoothScrollToPosition(mRecyclerView.getAdapter().getItemCount() - 1);
            }
        }, 500));

        send = findViewById(R.id.bt_send);
        send.setOnClickListener(view -> {
            if (!text.getText().equals("")) {
                List<ChatData> data = new ArrayList<>();
                ChatData item = new ChatData();
                item.setTime(getResources().getString(R.string.just_now));
                item.setSender_name(getResources().getString(R.string.you));
                item.setType("2");
                item.setText(text.getText().toString());
                data.add(item);
                mAdapter.addItem(data);
                mRecyclerView.smoothScrollToPosition(mRecyclerView.getAdapter().getItemCount() - 1);
                sendMessage(_ID, ROOM_ID, text.getText().toString(), "", null);
                text.setText("");
            }
        });
        btn_attachment = findViewById(R.id.bt_attachment);
        btn_attachment.setOnClickListener(v -> openFilePickerDialog());
    }

    private void setImage(String image, String text) {
        if (image != null && !image.isEmpty() && !image.equals("null")) {
            threadSenderImage.setVisibility(View.VISIBLE);
            threadInitial.setVisibility(View.GONE);
            RequestOptions options = new RequestOptions()
                    .centerCrop();
            Glide.with(getApplicationContext()).load(image).apply(options).into(threadSenderImage);
        } else {
            threadSenderImage.setVisibility(View.GONE);
            threadInitial.setVisibility(View.VISIBLE);
            threadInitial.setText(text.charAt(0) + "");
        }
    }

    private void sendMessage(String parent_message_id, String _ID, String text, String quoted_text, String attachment_id) {
        sendRoomMessage(parent_message_id, _ID, text, quoted_text, attachment_id);
    }

    private void sendRoomMessage(String parent_message_id, String room_id, String text, String quoted_text, String attachment_id) {
        String param = "?room_id=" + room_id + "&text=" + text + "&quoted_text=" + quoted_text + "&parent_message_id=" + parent_message_id;
        if (attachment_id != null) {
            param = param + "&attachment_id=" + attachment_id;
        }
        if (switchSMS.isChecked()) {
            param = param + "&user_message_type=sms";
        }
        String url = Constants.SEND_ROOM_MESSAGE + param;
        NetworkManager.customJsonObjectRequest(
                getApplicationContext(), url, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ERRROR=", message);
                    }
                }, true);
    }

    private void openFilePickerDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.add_a_file));
        builder.setItems(new CharSequence[]{
                        getResources().getString(R.string.camera_video),
                        getResources().getString(R.string.file)},
                (dialog, which) -> {
                    switch (which) {
                        case 0: {
                            //Camera & Video
                            dialog.cancel();
                            Intent intent = new Intent(this, FilePickerActivity.class);
                            intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
                                    .setCheckPermission(true)
                                    .setShowImages(true)
                                    .setShowVideos(true)
                                    .setShowAudios(true)
                                    .setShowFiles(true)
                                    .enableImageCapture(true)
                                    .enableVideoCapture(true)
                                    .setSingleChoiceMode(true)
                                    .build());
                            startActivityForResult(intent, FILE_REQUEST_CODE);
                        }
                        break;
                        case 1: {
                            //File
                            dialog.cancel();
                            performFileSearch();
                        }
                        break;
                    }
                });
        builder.create().show();
        builder.setCancelable(false);
    }

    public void performFileSearch() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        startActivityForResult(intent, READ_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                ArrayList<MediaFile> files;
                if (data != null) {
                    if (data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES) != null) {
                        files = data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES);
                        if (files == null || files.size() == 0) {
                            return;
                        }
                        Uri uri = files.get(0).getUri();
                        String mimeType = FileUtil.getMimeType(getApplicationContext(), uri);
                        String fileName = FileUtil.getFileName(getApplicationContext(), uri);
                        uploadFile(uri, fileName, mimeType);
                    }
                }
            }
        } else if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri uri = null;
            if (data != null) {
                uri = data.getData();
                String mimeType = FileUtil.getMimeType(getApplicationContext(), uri);
                String fileName = FileUtil.getFileName(getApplicationContext(), uri);
                uploadFile(uri, fileName, mimeType);
            }
        }
    }

    private void uploadFile(Uri uri, final String file_name, String file_type) {
        final Snackbar snackbar = Snackbar.make(coordinatorLayout, getResources().getString(R.string.a_file_is_being_uploaded), Snackbar.LENGTH_LONG);
        snackbar.show();

        OkHttpClient httpClient = new OkHttpClient();
        RequestBody requestBody = null;
        try {
            File file = FileUtil.from(getApplicationContext(), uri);
            requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("filename", file_name)
                    .addFormDataPart("attachment", file_name, RequestBody.create(MediaType.parse(file_type),
                            file)).build();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Request request = new Request.Builder()
                .url(Constants.UPLOAD_ATTACHMENT_URL)
                .addHeader("Authorization", "Bearer " + token)
                .post(requestBody)
                .build();
        showProgress();
        httpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                hideProgress();
                e.printStackTrace();
                LogUtils.Print("ERORRORR", "error in getting response using async okhttp call");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                hideProgress();
                ResponseBody responseBody = response.body();
                if (!response.isSuccessful()) {
                    throw new IOException("Error response " + response);
                }
                String jsonData = responseBody.string();
                LogUtils.Print("OUTPUT", jsonData);
                try {
                    new Handler(Looper.getMainLooper()).post(() -> sendAttachment(jsonData, file_name));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void sendAttachment(String data, String file_name) {
        try {
            JSONObject response = new JSONObject(data);
            String attachment_id = "";
            if (Integer.parseInt(response.getString("result")) == 1) {
                List<ChatData> chat_data = new ArrayList<ChatData>();
                ChatData item = new ChatData();
                item.setTime(getResources().getString(R.string.just_now));
                item.setType("2");
                item.setAtt_name(file_name);
                if (response.has("content_type")) {
                    item.setContent_type(response.getString("content_type"));
                }
                if (response.has("att_id")) {
                    attachment_id = response.getString("att_id");
                }
                if (response.has("size")) {
                    item.setAtt_file_size(response.getString("size"));
                }
                if (response.has("link")) {
                    item.setAtt_url(response.getString("link"));
                }
                item.setText(text.getText().toString());
                chat_data.add(item);
                mAdapter.addItem(chat_data);
                mRecyclerView.smoothScrollToPosition(mRecyclerView.getAdapter().getItemCount() - 1);
                sendMessage(_ID, ROOM_ID, text.getText().toString(), "", attachment_id);
                text.setText("");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void onLoaded() {
        mAdapter.notifyDataSetChanged();
        if (data.size() > 0) {
            mRecyclerView.postDelayed(() -> mRecyclerView.smoothScrollToPosition(mRecyclerView.getAdapter().getItemCount() - 1), 500);
        }
    }

    private void resetData() {
        data.clear();
    }

    private void getRoomMessage(String room_id, String parent_message_id, int page_size) {

        String url = Constants.GET_ROOM_MESSAGES + "?room_id=" + room_id + "&parent_message_id=" + parent_message_id + "&page_size=" + page_size;
        NetworkManager.customJsonObjectRequest(
                getApplicationContext(), url, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processData(response);
                    }

                    @Override
                    public void onError(String message) {
                        System.out.println("Error" + message);
                        LogUtils.Print("ERRROR=", message);
                    }
                }, true);
    }

    private void processData(JSONObject response) {
        List<MessageData> msgs_array = new ArrayList<>();
        JSONArray child_messages = new JSONArray();
        JSONArray user_array = new JSONArray();
        JSONObject child_obj;
        try {
            if (Integer.parseInt(response.getString("result")) == 1) {
                resetData();
                JSONArray messages = response.getJSONArray("messages");
                if (messages == null || messages.length() == 0) {
                    return;
                }
                for (int i = 0; i < messages.length(); i++) {
                    child_obj = messages.getJSONObject(i);
                    child_messages = child_obj.getJSONObject("child_messages").getJSONArray("messages");
                    user_array = child_obj.getJSONObject("child_messages").getJSONArray("users");
                }
                JSONObject result_obj = new JSONObject();
                for (int i = 0; i < child_messages.length(); i++) {
                    result_obj = child_messages.getJSONObject(i);

                    MessageData msgs = new MessageData();
                    if (result_obj.has("text")) {
                        msgs.setText(result_obj.getString("text"));
                    }
                    if (result_obj.has("user_message_type")) {
                        msgs.setUser_message_type(result_obj.getString("user_message_type"));
                    }
                    if (result_obj.has("quoted_text")) {
                        msgs.setQuotedText(result_obj.getString("quoted_text"));
                    }
                    if (result_obj.has("content_type")) {
                        msgs.setContent_type(result_obj.getString("content_type"));
                    }
                    if (result_obj.has("att_url")) {
                        msgs.setAtt_url(result_obj.getString("att_url"));
                    }
                    if (result_obj.has("att_filesize")) {
                        msgs.setAtt_file_size(result_obj.getString("att_filesize"));
                    }
                    if (result_obj.has("att_filename")) {
                        msgs.setAtt_name(result_obj.getString("att_filename"));
                    }
                    if (result_obj.has("sender_name")) {
                        msgs.setSender_name(result_obj.getString("sender_name"));
                    }
                    if (result_obj.has("pdf_url")) {
                        msgs.setPdf_url(result_obj.getString("pdf_url"));
                    }
                    if (result_obj.has("date_time")) {

                        msgs.setDataTime(result_obj.getLong("date_time"));
                    }
                    if (result_obj.has("direction")) {
                        msgs.setType(result_obj.getString("direction"));
                    }

                    if (result_obj.has("date_time")) {
                        String pattern = "dd MMMM yyyy";
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
                        String date = simpleDateFormat.format(new Date(result_obj.getLong("date_time") * 1000));
                        msgs.setFormattedDate(date);
                    }

                    if (result_obj.has("user_id")) {
                        JSONObject user = getPosition(user_array, "id", result_obj.getString("user_id"));
                        if (user != null) {
                            User user_obj = new User();
                            user_obj.setId(user.has("id") ? user.getString("id") : "");
                            user_obj.setFirst_name(user.has("first_name") ? user.getString("first_name") : "");
                            user_obj.setLast_name(user.has("last_name") ? user.getString("last_name") : "");
                            user_obj.setImage(user.has("avatar_url") ? user.getString("avatar_url") : "");
                            user_obj.setName(user.has("name") ? user.getString("name") : "");
                            user_obj.setCompany(user.has("company_name") ? user.getString("company_name") : "");
                            user_obj.setLastSeen(user.has("last_seen") ? Util.timeAgo(user.getLong("last_seen")) : "");
                            msgs.setUser(user_obj);
                        }
                    } else if (result_obj.has("sender_id")) {
                        JSONObject user = getPosition(user_array, "id", result_obj.getString("sender_id"));
                        if (user != null) {
                            User user_obj = new User();
                            user_obj.setId(user.has("id") ? user.getString("id") : "");
                            user_obj.setFirst_name(user.has("first_name") ? user.getString("first_name") : "");
                            user_obj.setLast_name(user.has("last_name") ? user.getString("last_name") : "");
                            user_obj.setImage(user.has("avatar_url") ? user.getString("avatar_url") : "");
                            user_obj.setName(user.has("name") ? user.getString("name") : "");
                            user_obj.setCompany(user.has("company_name") ? user.getString("company_name") : "");
                            user_obj.setLastSeen(user.has("last_seen") ? Util.timeAgo(user.getLong("last_seen")) : "");
                            msgs.setUser(user_obj);
                        }
                    }
                    msgs_array.add(msgs);
                }
                Map<Calendar, List<MessageData>> grouped = msgs_array
                        .stream()
                        .collect(Collectors.groupingBy(x -> {
                            Calendar cal = Calendar.getInstance();
                            cal.setTimeInMillis(x.getDataTime() * 1000);
                            cal.set(Calendar.HOUR_OF_DAY, 0);
                            cal.set(Calendar.MINUTE, 0);
                            cal.set(Calendar.SECOND, 0);
                            cal.set(Calendar.MILLISECOND, 0);
                            return cal;
                        }));

                for (Map.Entry<Calendar, List<MessageData>> entry : grouped.entrySet()) {
                    ChatData item = new ChatData();
                    item.setType("0");
                    String pattern = "E dd MMMM yyyy";
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
                    String date = simpleDateFormat.format(entry.getKey().getTime());
                    item.setText(date);
                    item.setTime("");
                    data.add(item);
                    List<MessageData> sortedList = entry.getValue().stream()
                            .sorted(Comparator.comparingLong(MessageData::getDataTime))
                            .collect(Collectors.toList());
                    for (MessageData d : sortedList) {
                        item = new ChatData();
                        item.setText(d.getText());
                        item.setAtt_url(d.getAtt_url());
                        if (d.getAtt_file_size() != null && !d.getAtt_file_size().isEmpty()) {
                            long num = Long.valueOf(d.getAtt_file_size());
                            item.setAtt_file_size(getResources().getString(R.string.size) + " : " + FileUtil.getStringSizeLengthFile(num));
                        } else {
                            item.setAtt_file_size(getResources().getString(R.string.size) + " : 0b");
                        }
                        if (d.getType() != null) {
                            item.setType(d.getType().equals("outgoing") ? "1" : "2");
                        } else {
                            item.setType("1");
                        }
                        item.setAtt_name(d.getAtt_name());
                        item.setSender_name(d.getSender_name());
                        item.setContent_type(d.getContent_type());
                        item.setThumbnail(Util.getThumbNails(d.getContent_type()));
                        item.setThread_count(d.getThread_count());
                        item.setUser(d.getUser());
                        pattern = "hh:mm a";
                        simpleDateFormat = new SimpleDateFormat(pattern);
                        date = simpleDateFormat.format(new Date(d.getDataTime() * 1000));
                        item.setTime(date);
                        data.add(item);
                    }
                }
                onLoaded();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private JSONObject getPosition(JSONArray jsonArray, String attribute, String value) throws JSONException {
        for (int index = 0; index < jsonArray.length(); index++) {
            JSONObject jsonObject = jsonArray.getJSONObject(index);
            if (jsonObject.getString("id").equals(value)) {
                return jsonObject;
            }
        }
        return null;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(), Conversation.class);
        intent.putExtra("_ID", (Serializable) ROOM_ID);
        intent.putExtra("_TYPE", (Serializable) 2);
        intent.putExtra("_NAME", (Serializable) ROOM_NAME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}