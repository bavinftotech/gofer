package com.gofercrm.user.gofercrm.task;

import com.gofercrm.user.gofercrm.entity.Comment;

import java.io.Serializable;
import java.util.List;

public class Task implements Serializable {
    private String type;
    private String name;
    private String status;
    private String taskId;
    private String link;
    private String subjectId;
    private String subjectName;
    private Long dueDate;
    private String reminderDays;
    private String userId;
    private List<Comment> comments;
    private String action;
    private String action_data;
    private String description;
    private int priority;
    private String projectName;

    public Task() {

    }

    public Task(String type, String name, String status, String taskId, String link, String subjectId, String subjectName, Long dueDate, String reminderDays, String userId, List<Comment> comments) {
        this.type = type;
        this.name = name;
        this.status = status;
        this.taskId = taskId;
        this.link = link;
        this.subjectId = subjectId;
        this.dueDate = dueDate;
        this.reminderDays = reminderDays;
        this.userId = userId;
        this.comments = comments;
        this.subjectName = subjectName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public Long getDueDate() {
        return dueDate;
    }

    public void setDueDate(Long dueDate) {
        this.dueDate = dueDate;
    }

    public String getReminderDays() {
        return reminderDays;
    }

    public void setReminderDays(String reminderDays) {
        this.reminderDays = reminderDays;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getAction_data() {
        return action_data;
    }

    public void setAction_data(String action_data) {
        this.action_data = action_data;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
}