package com.gofercrm.user.gofercrm.opportunityclientlead.model;

public class DataMarketingSource {
    private String name;

    public DataMarketingSource(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
