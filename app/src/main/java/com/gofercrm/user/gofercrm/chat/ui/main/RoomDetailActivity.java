package com.gofercrm.user.gofercrm.chat.ui.main;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.appointments.Contact;
import com.gofercrm.user.gofercrm.appointments.ContactAdapter;
import com.gofercrm.user.gofercrm.appointments.ContactsCompletionView;
import com.gofercrm.user.gofercrm.appointments.NewEventActivity;
import com.gofercrm.user.gofercrm.chat.ui.main.model.Member;
import com.gofercrm.user.gofercrm.chat.ui.main.model.Room;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.tokenautocomplete.TokenCompleteTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RoomDetailActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener, TokenCompleteTextView.TokenListener<Contact> {

    SwipeRefreshLayout mSwipeRefreshLayout;
    TextView no_records, room_name, room_purpose, members_text;
    String _ID, _NAME = "Room Detail";
    List<Contact> emailToLists = new ArrayList<>();
    ContactsCompletionView connectionssChip;
    List<Contact> connections = new ArrayList<>();
    ArrayAdapter<Contact> connectionsAdapter;
    ImageButton room_add_someone_btn;
    Room room = null;
    private RecyclerView recyclerView;
    private RoomDetailAdapter rAdapter;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_detail);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(_NAME);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mSwipeRefreshLayout = findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        no_records = findViewById(R.id.no_records);

        room_name = findViewById(R.id.room_name_value);
        room_purpose = findViewById(R.id.room_purpose_value);
        members_text = findViewById(R.id.room_members);
        room_add_someone_btn = findViewById(R.id.room_add_someone_btn);

        connectionsAdapter = new ContactAdapter(this, R.layout.contact_ac_layout, connections);
        connectionssChip = findViewById(R.id.connections_id);
        connectionssChip.setTokenListener(this);
        connectionssChip.setTokenClickStyle(TokenCompleteTextView.TokenClickStyle.Select);
        connectionssChip.setAdapter(connectionsAdapter);
        getConnections();

        recyclerView = findViewById(R.id.room_member_recycle);
        progressBar = findViewById(R.id.room_member_progressBar);
        progressBar.setVisibility(View.VISIBLE);

        rAdapter = new RoomDetailAdapter(getApplicationContext(), null);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(rAdapter);

        Intent intent = getIntent();
        _ID = intent.getStringExtra("_ID");
        _NAME = intent.getStringExtra("_NAME");

        if (_ID != null) {
            getRoomInfo(_ID);

        }
        room_add_someone_btn.setOnClickListener(v -> updateRoom());


    }

    private void onLoaded() {
        rAdapter = new RoomDetailAdapter(getApplicationContext(), room);
        recyclerView.setAdapter(rAdapter);
        rAdapter.notifyDataSetChanged();
        progressBar.setVisibility(View.GONE);

    }

    private void getRoomInfo(String room_id) {
        String url = Constants.GET_ROOM_INFO + "?room_id=" + room_id;
        NetworkManager.customJsonObjectRequest(
                getApplicationContext(), url, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        processData(response);
                    }

                    @Override
                    public void onError(String message) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        System.out.println("Error" + message);
                    }
                }, true);

    }

    private void setRoomInfo(Room room) {
        room_name.setText(room.getName());
        room_purpose.setText(room.getDesc());
        members_text.setText(getResources().getString(R.string.member_list) + " (" + room.getMembers().size() + ")");
    }

    private void processData(JSONObject response) {
        room = new Room();
        try {
            if (Integer.parseInt(response.getString("result")) == 1) {
                JSONObject room_object = response.getJSONObject("room");
                if (room_object.has("room_name")) {
                    room.setName(room_object.getString("room_name"));
                }
                if (room_object.has("description")) {
                    room.setDesc(room_object.getString("description"));
                }
                if (room_object.has("room_id")) {
                    room.set_Id(room_object.getString("room_id"));
                }
                if (room_object.has("room_phone")) {
                    room.setPhone(room_object.getString("room_phone"));
                }
                if (room_object.has("updated_at")) {
                    room.setUpdatedOn(room_object.getLong("updated_at"));
                }
                if (room_object.has("members")) {
                    List<Member> members = new ArrayList<>();
                    JSONObject member_object = new JSONObject();
                    JSONArray room_members_response = room_object.getJSONArray("members");
                    for (int i = 0; i < room_members_response.length(); i++) {

                        member_object = room_members_response.getJSONObject(i);
                        Member member = new Member();

                        if (member_object.has("member_role")) {
                            member.setRole(member_object.getInt("member_role"));
                        }
                        if (member_object.has("mute_notifications")) {
                            member.setMute_notifications(member_object.getBoolean("mute_notifications"));
                        }
                        if (member_object.has("user_id")) {
                            member.set_Id(member_object.getString("user_id"));
                        }
                        if (member_object.has("member_email")) {
                            member.setEmail(member_object.getString("member_email"));
                        }
                        if (member_object.has("avatar_url")) {
                            member.setmImage(member_object.getString("avatar_url"));
                        }
                        if (member_object.has("member_profile")) {
                            String name = "";
                            JSONObject profile = member_object.getJSONObject("member_profile");
                            if (profile.has("first_name")) {
                                name = profile.getString("first_name");
                            }
                            if (profile.has("last_name")) {
                                name = name + " " + profile.getString("last_name");
                            }
                            member.setmName(name);
                            member.set_Id(member_object.getString("user_id"));
                        }
                        members.add(member);
                    }
                    room.setMembers(members);
                }
                setRoomInfo(room);
                onLoaded();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRefresh() {
        room = null;
        mSwipeRefreshLayout.setRefreshing(true);
        progressBar.setVisibility(View.GONE);
        getRoomInfo(_ID);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(), Conversation.class);
        intent.putExtra("_ID", (Serializable) _ID);
        intent.putExtra("_TYPE", (Serializable) 2);
        intent.putExtra("_NAME", _NAME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onTokenAdded(Contact token) {
        emailToLists.add(token);
        if (emailToLists.size() > 0) {
            enableFields();
        } else {
            disableFields();
        }
    }

    @Override
    public void onTokenRemoved(Contact token) {
        emailToLists.remove(token);
        if (emailToLists.size() > 0) {
            enableFields();
        } else {
            disableFields();
        }
    }

    @Override
    public void onTokenIgnored(Contact token) {
    }

    private void getConnections() {
        NetworkManager.customJsonObjectGetRequest(
                RoomDetailActivity.this, Constants.GET_CONNECTIONS_PICKLIST_URL, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processConnections(response);
                    }

                    @Override
                    public void onError(String message) {
                    }
                }, false);
    }

    private void processConnections(JSONObject data) {
        try {
            connections.clear();
            String id;
            String name;
            String email;
            if (Integer.parseInt(data.getString("result")) == 1) {
                JSONArray users = data.getJSONArray("users");
                for (int i = 0; i < users.length(); i++) {
                    JSONObject obj = users.getJSONObject(i);
                    if (obj.has("email")) {
                        email = obj.getString("email");
                        id = obj.getString("id");
                        name = obj.getString("name");
                        if (name != null) {
                            name = Util.capitalize(name);
                        }
                        Contact contact = new Contact(id, name, email, "");
                        connections.add(contact);
                    }
                }
                connectionsAdapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void disableFields() {
        room_add_someone_btn.setVisibility(View.GONE);
    }

    private void enableFields() {
        room_add_someone_btn.setVisibility(View.VISIBLE);
    }

    private void updateRoom() {
        String ids;
        StringBuilder result = new StringBuilder();
        for (Contact contact : emailToLists) {
            result.append(contact.getId());
            result.append(",");
        }
        ids = result.length() > 0 ? result.substring(0, result.length() - 1) : "";
        String req_url = Constants.ADD_ROOM_MEMBERS;
        String param = "?room_id=" + _ID + "&user_ids=" + ids;
        disableFields();
        NetworkManager.customJsonObjectRequest(
                RoomDetailActivity.this, req_url + param, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processResult(response);

                    }

                    @Override
                    public void onError(String message) {
                        enableFields();
                    }
                }, true);

    }

    private void processResult(JSONObject response) {
        try {
            if (Integer.parseInt(response.getString("result")) == 1) {
                Util.onMessage(getApplicationContext(), getResources().getString(R.string.room_updated));
            } else {
                Util.onMessage(getApplicationContext(), getResources().getString(R.string.room_updated_failed));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_room_detail, menu);
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.room_members_appointment && room != null && room.getMembers().size() > 0) {
            Intent intent = new Intent(this, NewEventActivity.class);
            intent.putExtra("ROOM_MEMBER", (Serializable) room.getMembers());
            intent.putExtra("ROOM_NAME", (Serializable) room.getName());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
}