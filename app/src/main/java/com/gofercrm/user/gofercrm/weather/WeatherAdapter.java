package com.gofercrm.user.gofercrm.weather;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.util.Util;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.EventViewHolder> {

    private final JSONArray jsonArray;

    public WeatherAdapter(JSONArray jsonArray) {
        if (jsonArray.length() > 0)
            jsonArray.remove(0);
        this.jsonArray = jsonArray;
    }

    @NotNull
    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_weather, parent, false);
        return new EventViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NotNull WeatherAdapter.EventViewHolder holder, final int position) {
        final JSONObject event;
        try {
            event = jsonArray.getJSONObject(position);
            holder.tvType.setText(event.getString("icon"));
            switch (event.getString("icon")) {
                case "clear-day":
                    holder.ivWIcon.setImageResource(R.drawable.ic_sun_max_fill);
                case "clear-night":
                    holder.ivWIcon.setImageResource(R.drawable.ic_moon_stars_fill);
                case "rain":
                    holder.ivWIcon.setImageResource(R.drawable.ic_cloud_rain_fill);
                case "snow":
                    holder.ivWIcon.setImageResource(R.drawable.ic_snow);
                case "sleet":
                    holder.ivWIcon.setImageResource(R.drawable.ic_cloud_sleet_fill);
                case "wind":
                    holder.ivWIcon.setImageResource(R.drawable.ic_wind);
                case "fog":
                    holder.ivWIcon.setImageResource(R.drawable.ic_cloud_fog_fill);
                case "cloudy":
                    holder.ivWIcon.setImageResource(R.drawable.ic_cloud_fill);
                case "partly-cloudy-day":
                    holder.ivWIcon.setImageResource(R.drawable.ic_cloud_sun_fill);
                case "partly-cloudy-night":
                    holder.ivWIcon.setImageResource(R.drawable.ic_cloud_moon_fill);
                default:
                    holder.ivWIcon.setImageResource(R.drawable.ic_sun_max_fill);
            }
            holder.tvWeather.setText(Math.round(event.getDouble("temp")) + "°");
            holder.tvDay.setText(Util.weatherDate(event.getLong("datetime")));
            holder.tvMin.setText(Math.round(event.getDouble("mint")) + "°");
            holder.tvMax.setText(Math.round(event.getDouble("maxt")) + "°");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    public class EventViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvType, tvWeather, tvDay, tvMin, tvMax;
        private final ImageView ivWIcon;

        public EventViewHolder(View view) {
            super(view);
            tvType = view.findViewById(R.id.tvType);
            tvWeather = view.findViewById(R.id.tvWeather);
            tvDay = view.findViewById(R.id.tvDay);
            tvMin = view.findViewById(R.id.tvMin);
            tvMax = view.findViewById(R.id.tvMax);
            ivWIcon = view.findViewById(R.id.ivWIcon);
        }
    }
}