package com.gofercrm.user.gofercrm.chat.video.group.openvcall.model;

public class CurrentUserSettings {
    public int mEncryptionModeIndex;

    public String mEncryptionKey;

    public String mChannelName;

    public CurrentUserSettings() {
        reset();
    }

    public void reset() {
    }
}
