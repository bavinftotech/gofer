package com.gofercrm.user.gofercrm.opportunities;

import android.os.Parcel;
import android.os.Parcelable;

import com.gofercrm.user.gofercrm.entity.Comment;

import java.io.Serializable;
import java.util.List;

public class Opportunity implements Serializable {

    private String id;
    private String name;
    private String saleValue;
    private String stage;
    private Long closedDate;
    private List<Comment> comments;
    private String contact_id;
    private String product_id;
    private List<String> properties_id;
    private String probability_percent;
    private String competitiveDesc;
    private String businessNeedDesc;
    private String property_id;
    private String contactName;
    private String productName;
    private Long createdOn;
    private String propertyName;
    private String userName;
    private Long openDate;
    private Long renewalDate;
    private String marketingSource;
    private String market_rent_rate;
    private String unit_number;
    private String source_name;
    private String rental_concession;
    private Long lostClosedDate;
    private Long lostFollowUpDate;
    private String loss_reason;
    private String lossComments;

    public Opportunity() {
    }

    public Opportunity(String id, String name, String saleValue, String stage, Long closedDate, List<Comment> comments, String contact_id) {
        this.id = id;
        this.name = name;
        this.saleValue = saleValue;
        this.stage = stage;
        this.closedDate = closedDate;
        this.comments = comments;
        this.contact_id = contact_id;
    }

    public Opportunity(String id, String name, String saleValue, String stage,
                       Long closedDate, List<Comment> comments, String contact_id,
                       String propertyName, Long openDate, Long renewalDate, String userName) {
        this.id = id;
        this.name = name;
        this.saleValue = saleValue;
        this.stage = stage;
        this.closedDate = closedDate;
        this.comments = comments;
        this.contact_id = contact_id;
        this.propertyName = propertyName;
        this.openDate = openDate;
        this.renewalDate = renewalDate;
        this.userName = userName;
    }

    public String getLossComments() {
        return lossComments;
    }

    public void setLossComments(String lossComments) {
        this.lossComments = lossComments;
    }

    public String getLoss_reason() {
        return loss_reason;
    }

    public void setLoss_reason(String loss_reason) {
        this.loss_reason = loss_reason;
    }

    public Long getLostFollowUpDate() {
        return lostFollowUpDate;
    }

    public void setLostFollowUpDate(Long lostFollowUpDate) {
        this.lostFollowUpDate = lostFollowUpDate;
    }

    public Long getLostClosedDate() {
        return lostClosedDate;
    }

    public void setLostClosedDate(Long lostClosedDate) {
        this.lostClosedDate = lostClosedDate;
    }

    public String getMarket_rent_rate() {
        return market_rent_rate;
    }

    public void setMarket_rent_rate(String market_rent_rate) {
        this.market_rent_rate = market_rent_rate;
    }

    public String getUnit_number() {
        return unit_number;
    }

    public void setUnit_number(String unit_number) {
        this.unit_number = unit_number;
    }

    public String getSource_name() {
        return source_name;
    }

    public void setSource_name(String source_name) {
        this.source_name = source_name;
    }

    public String getRental_concession() {
        return rental_concession;
    }

    public void setRental_concession(String rental_concession) {
        this.rental_concession = rental_concession;
    }

    public String getMarketingSource() {
        return marketingSource;
    }

    public void setMarketingSource(String marketingSource) {
        this.marketingSource = marketingSource;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getOpenDate() {
        return openDate;
    }

    public void setOpenDate(Long openDate) {
        this.openDate = openDate;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public Long getRenewalDate() {
        return renewalDate;
    }

    public void setRenewalDate(Long renewalDate) {
        this.renewalDate = renewalDate;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getContact_id() {
        return contact_id;
    }

    public void setContact_id(String contact_id) {
        this.contact_id = contact_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSaleValue() {
        return saleValue;
    }

    public void setSaleValue(String saleValue) {
        this.saleValue = saleValue;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public Long getClosedDate() {
        return closedDate;
    }

    public void setClosedDate(Long closedDate) {
        this.closedDate = closedDate;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public List<String> getProperties_id() {
        return properties_id;
    }

    public void setProperties_id(List<String> properties_id) {
        this.properties_id = properties_id;
    }

    public String getProbability_percent() {
        return probability_percent;
    }

    public void setProbability_percent(String probability_percent) {
        this.probability_percent = probability_percent;
    }

    public String getCompetitiveDesc() {
        return competitiveDesc;
    }

    public void setCompetitiveDesc(String competitiveDesc) {
        this.competitiveDesc = competitiveDesc;
    }

    public String getBusinessNeedDesc() {
        return businessNeedDesc;
    }

    public void setBusinessNeedDesc(String businessNeedDesc) {
        this.businessNeedDesc = businessNeedDesc;
    }

    public String getProperty_id() {
        return property_id;
    }

    public void setProperty_id(String property_id) {
        this.property_id = property_id;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public Long getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Long createdOn) {
        this.createdOn = createdOn;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
}