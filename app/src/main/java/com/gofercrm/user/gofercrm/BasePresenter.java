package com.gofercrm.user.gofercrm;

public interface BasePresenter {
    void start();
}