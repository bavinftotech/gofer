package com.gofercrm.user.gofercrm.opportunities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.appointments.Contact;
import com.gofercrm.user.gofercrm.appointments.ContactAdapter;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class OpportunityWonActivity extends BaseActivity {

    final Calendar myCalendar = Calendar.getInstance();
    EditText input_oppor_won_close_date, input_oppor_renewal_date,
            input_oppor_won_source, input_oppor_won_un, input_oppor_won_mrr,
            input_oppor_won_rc, input_oppor_won_eci, input_oppor_won_ms;
    AutoCompleteTextView propertiesChip;
    List<Contact> properties = new ArrayList<>();
    ArrayAdapter<Contact> propertiesAdapter;

    Contact selected_property;
    String selected_property_id;

    String selected_obj;
    JSONObject opportunity_obj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opportunity_won);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.closed_won));
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.won);
        fab.setOnClickListener(view -> {
            if (!validate()) {
                Util.onMessage(getApplicationContext(), getResources().getString(R.string.fields_value_in_correct));
            } else {
                saveOpportunity();
            }
        });
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        input_oppor_won_close_date = findViewById(R.id.input_oppor_won_close_date);
        input_oppor_renewal_date = findViewById(R.id.input_oppor_renewal_date);
        input_oppor_won_source = findViewById(R.id.input_oppor_won_source);
        input_oppor_won_un = findViewById(R.id.input_oppor_won_un);
        input_oppor_won_mrr = findViewById(R.id.input_oppor_won_mrr);
        input_oppor_won_rc = findViewById(R.id.input_oppor_won_rc);
        input_oppor_won_eci = findViewById(R.id.input_oppor_won_eci);
        input_oppor_won_ms = findViewById(R.id.input_oppor_won_ms);

        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        input_oppor_won_close_date.setText(sdf.format(myCalendar.getTime()));

        input_oppor_renewal_date.setText(sdf.format(myCalendar.getTime()));

        final DatePickerDialog.OnDateSetListener close_date = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateCloseDate();
        };

        input_oppor_won_close_date.setOnClickListener(v -> new DatePickerDialog(OpportunityWonActivity.this, close_date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show());

        final DatePickerDialog.OnDateSetListener renewal_date = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateRenewalDate();
        };
        input_oppor_renewal_date.setOnClickListener(v -> new DatePickerDialog(OpportunityWonActivity.this, renewal_date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show());

        Intent intent = getIntent();
        selected_obj = intent.getStringExtra("OPPORTUNITY_TO_WON");
        if (selected_obj != null) {
            try {
                opportunity_obj = new JSONObject(selected_obj);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (opportunity_obj != null) {
            getCommunities(opportunity_obj);
        } else {
            getCommunities(null);
        }
        propertiesAdapter = new ContactAdapter(this, R.layout.contact_ac_layout, properties);
        propertiesChip = findViewById(R.id.oppor_won_properties_id);
        propertiesChip.setAdapter(propertiesAdapter);
        propertiesChip.setOnItemClickListener((adapterView, view, i, l) -> {
            selected_property = (Contact) adapterView.getItemAtPosition(i);
            selected_property_id = selected_property.getId();
        });
    }

    private void updateCloseDate() {
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        input_oppor_won_close_date.setText(sdf.format(myCalendar.getTime()));
    }

    private void updateRenewalDate() {
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        input_oppor_renewal_date.setText(sdf.format(myCalendar.getTime()));
    }

    private void getCommunities(final JSONObject opportunity) {
        JSONObject postData = new JSONObject();
        try {
            postData.put("is_active", true);
            postData.put("page_size", 5000);
            postData.put("exclude_manual_data_source", false);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        NetworkManager.customJsonObjectRequest(
                OpportunityWonActivity.this, Constants.GET_COMMUNITIES_PICKLIST_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processCommunities(response, opportunity);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ERRROR=", message);
                    }
                }, true);
    }

    private void processCommunities(JSONObject response, JSONObject opportunity) {
        try {
            properties.clear();
            String id = "";
            String name = "";
            String marketing_name = "";

            JSONArray propertyList = response.getJSONArray("properties");
            for (int i = 0; i < propertyList.length(); i++) {
                JSONObject obj = propertyList.getJSONObject(i);
                if (obj.has("id")) {
                    id = obj.getString("id");
                }
                if (obj.has("name")) {
                    name = obj.getString("name");
                }
                if (obj.has("marketing_name")) {
                    name = obj.getString("marketing_name");
                }
                if (name != null && !name.isEmpty()) {
                    name = Util.capitalize(name);
                }
                if (opportunity != null && opportunity.has("property_id")) {
                    if (opportunity.getString("property_id").equals(id)) {
                        propertiesChip.setText(name);
                        selected_property_id = id;
                    }
                }
                Contact contact = new Contact(id, name, marketing_name, "");
                properties.add(contact);
            }
            propertiesAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveOpportunity() {
        JSONObject postData = new JSONObject();
        try {
            String id = opportunity_obj.getString("opportunity_id");
            if (id == null) {
                Util.onMessage(getApplicationContext(), getResources().getString(R.string.no_appo_id_found));
                return;
            }
            Util.onMessage(getApplicationContext(), getResources().getString(R.string.updating_opportunity));
            postData.put("action", "update");
            postData.put("opportunity_id", id);
            postData.put("payload", requestData());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        NetworkManager.customJsonObjectRequest(
                OpportunityWonActivity.this, Constants.OPPORTUNITY_MANAGE_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processResult(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("CLIENT ERROR", message);
                    }
                }, true);
    }

    public boolean validate() {
        boolean valid = true;
        if (selected_property_id == null) {
            propertiesChip.setError(getResources().getString(R.string.property_empty_err));
            valid = false;
        } else {
            propertiesChip.setError(null);
        }
        if (input_oppor_won_close_date.getText().toString().isEmpty()) {
            input_oppor_won_close_date.setError(getResources().getString(R.string.close_date_is_mandatory));
        } else {
            input_oppor_won_close_date.setError(null);
        }
        if (input_oppor_renewal_date.getText().toString().isEmpty()) {
            input_oppor_renewal_date.setError(getResources().getString(R.string.renewal_date_is_mandatory));
        } else {
            input_oppor_renewal_date.setError(null);
        }
        return valid;
    }

    private JSONObject requestData() {
        JSONObject payload = new JSONObject();
        try {
            payload.put("close_date", input_oppor_won_close_date.getText().toString() + "T23:59:59.000Z");
            payload.put("renewal_date", input_oppor_renewal_date.getText().toString() + "T23:59:59.000Z");
            payload.put("stage", "closed_won");

            JSONObject dict = new JSONObject();
            dict.put("unit_number", input_oppor_won_un.getText().toString());
            dict.put("market_rent_rate", input_oppor_won_mrr.getText().toString());
            dict.put("marketing_source", input_oppor_won_ms.getText().toString());
            dict.put("source_name", input_oppor_won_source.getText().toString());
            dict.put("expected_commission_invoice", input_oppor_won_eci.getText().toString());
            dict.put("rental_concession", input_oppor_won_rc.getText().toString());

            payload.put("close_dict", dict);
            payload.put("property_id", selected_property_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return payload;
    }

    private void processResult(JSONObject response) {
        try {
            if (Integer.parseInt(response.getString("result")) == 1) {
                Util.onMessage(getApplicationContext(), getResources().getString(R.string.update_success));
                Intent intent = new Intent(getApplicationContext(), OpportunityActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } else {
                JSONObject error_obj = response.getJSONObject("error");
                if (error_obj.has("reason")) {
                    Util.onMessage(getApplicationContext(), error_obj.getString("reason"));
                } else {
                    Util.onMessage(getApplicationContext(), getResources().getString(R.string.oppo_update_failed));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}