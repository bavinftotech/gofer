package com.gofercrm.user.gofercrm.lead;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.appointments.NewEventActivity;
import com.gofercrm.user.gofercrm.appointments.ViewAppointmentActivity;
import com.gofercrm.user.gofercrm.clients.ClientViewActivity;
import com.gofercrm.user.gofercrm.entity.Address;
import com.gofercrm.user.gofercrm.entity.Comment;
import com.gofercrm.user.gofercrm.entity.Email;
import com.gofercrm.user.gofercrm.entity.LeadOwner;
import com.gofercrm.user.gofercrm.entity.MatchPreference;
import com.gofercrm.user.gofercrm.entity.Phone;
import com.gofercrm.user.gofercrm.lead.upload.UploadActivity;
import com.gofercrm.user.gofercrm.task.NewTaskActivity;
import com.gofercrm.user.gofercrm.util.DialogUtils;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class LeadViewActivity extends BaseActivity {

    ArrayList<LeadView> leadPage = new ArrayList<>();
    ImageButton call_btn;
    ImageButton upgrade_to_contact_btn, assign_to_me_btn;
    TextView upgrade_to_contact_text, assign_to_me_text;
    LeadList lead_obj = new LeadList();
    EditText commentText;
    List comments = new ArrayList<Comment>();
    String LEAD_ID;
    SharedPreferences sharedPref;
    String LOGIN_USER_ID = "";
    boolean IS_ADMIN;
    Switch lead_campaign;
    private LeadViewPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private JSONArray arrAppointment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lead_view);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        sharedPref = getSharedPreferences("LOGIN", MODE_PRIVATE);
        LOGIN_USER_ID = sharedPref.getString("USER_ID", "");
        IS_ADMIN = sharedPref.getBoolean("IS_ADMIN", false);
        upgrade_to_contact_btn = findViewById(R.id.upgrade_to_contact_btn);
        upgrade_to_contact_text = findViewById(R.id.upgrade_to_contact_text);
        assign_to_me_btn = findViewById(R.id.assign_to_me_btn);
        assign_to_me_text = findViewById(R.id.assign_to_me_text);
        lead_campaign = findViewById(R.id.lead_campaign);
        Intent intent = getIntent();
        LEAD_ID = intent.getStringExtra("LEAD_ID");
        if (LEAD_ID != null) {
            getLeadById(LEAD_ID);
        }
    }

    private MenuItem menuCampaign;
    private MenuItem menuAssignToMe;
    private MenuItem menuCall;
    private MenuItem menuSMS;
    private MenuItem menuEmail;
    private MenuItem menuDisqualify;
    private MenuItem actionViewAppointment;

    @SuppressLint("RestrictedApi")
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (menu instanceof MenuBuilder) {
            ((MenuBuilder) menu).setOptionalIconsVisible(true);
        }
        getMenuInflater().inflate(R.menu.menu_lead_view, menu);
        menuCampaign = menu.findItem(R.id.menuCampaign).setIcon(ContextCompat.getDrawable(this,
                lead_obj.isCampaign() ? R.drawable.ic_baseline_toggle_on_24 : R.drawable.ic_baseline_toggle_off_24));
        menuAssignToMe = menu.findItem(R.id.menuAssignToMe);
        menuCall = menu.findItem(R.id.menuCall);
        menuSMS = menu.findItem(R.id.menuSMS);
        menuEmail = menu.findItem(R.id.menuEmail);
        menuDisqualify = menu.findItem(R.id.action_disqualify);
        actionViewAppointment = menu.findItem(R.id.action_view_appointment);
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menuCampaign) {
            menuCampaign.setChecked(!menuCampaign.isChecked());
            menuCampaign.setIcon(ContextCompat.getDrawable(this,
                    menuCampaign.isChecked() ? R.drawable.ic_baseline_toggle_on_24 : R.drawable.ic_baseline_toggle_off_24));
//            invalidateOptionsMenu();
            putItOnCampaign(LEAD_ID, menuCampaign.isChecked());
        } else if (id == R.id.menuUpgrade) {
            if (lead_obj.getContact_id() == null || lead_obj.getContact_id().isEmpty()) {
                convertToContact(LEAD_ID);
                upgrade_to_contact_btn.setImageResource(R.drawable.account_outline);
            } else {
                Intent intent = new Intent(getApplicationContext(), ClientViewActivity.class);
                intent.putExtra("CLIENT_ID", lead_obj.getContact_id());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        } else if (id == R.id.menuAssignToMe) {
            if (!lead_obj.getLeadOwner().getId().equals(LOGIN_USER_ID)) {
                assignToMe(LEAD_ID);
            }
        } else if (id == R.id.menuCall) {
            if (lead_obj.getPhones().size() > 0) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + lead_obj.getPhones().get(0).getPhone()));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } else {
                if (item.getActionView() != null)
                    Snackbar.make(item.getActionView(), getResources().getString(R.string.no_phone_is_associated_lead), Snackbar.LENGTH_LONG)
                            .setAction(getResources().getString(R.string.action), null).show();
            }
        } else if (id == R.id.menuSMS) {
            if (lead_obj.getPhones().size() > 0 && lead_obj.getPhones().get(0).getPhone() != null) {
                try {
                    Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                    smsIntent.setType("vnd.android-dir/mms-sms");
                    smsIntent.putExtra("address", "" + lead_obj.getPhones().get(0).getPhone());
                    smsIntent.putExtra("sms_body", "");
                    startActivity(smsIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                    Snackbar.make(item.getActionView(), getResources().getString(R.string.no_phone_is_associated_lead), Snackbar.LENGTH_LONG)
                            .setAction(getResources().getString(R.string.action), null).show();
                }
            } else {
                if (item.getActionView() != null)
                    Snackbar.make(item.getActionView(), getResources().getString(R.string.no_phone_is_associated_lead), Snackbar.LENGTH_LONG)
                            .setAction(getResources().getString(R.string.action), null).show();
            }
        } else if (id == R.id.menuEmail) {
            if (lead_obj.getEmails().size() > 0 && lead_obj.getEmails().get(0).getEmail() != null) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL, new String[]{lead_obj.getEmails().get(0).getEmail()});
                i.putExtra(Intent.EXTRA_SUBJECT, "");
                i.putExtra(Intent.EXTRA_TEXT, "");
                try {
                    startActivity(Intent.createChooser(i, getResources().getString(R.string.send_mail_)));
                } catch (android.content.ActivityNotFoundException ex) {
                    Snackbar.make(item.getActionView(), getResources().getString(R.string.no_phone_is_associated_lead), Snackbar.LENGTH_LONG)
                            .setAction(getResources().getString(R.string.action), null).show();
                }
            } else {
                if (item.getActionView() != null)
                    Snackbar.make(item.getActionView(), getResources().getString(R.string.no_phone_is_associated_lead), Snackbar.LENGTH_LONG)
                            .setAction(getResources().getString(R.string.action), null).show();
            }
        } else if (id == R.id.action_upload) {
            Intent intent = new Intent(getApplicationContext(), UploadActivity.class);
            intent.putExtra("FROM", UploadActivity.fromLead);
            intent.putExtra("LEAD_ID", LEAD_ID);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else if (id == R.id.action_task) {
            Intent intent = new Intent(getApplicationContext(), NewTaskActivity.class);
            intent.putExtra("LEAD_ID", LEAD_ID);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else if (id == R.id.action_new_meeting) {
            Intent intent = new Intent(getApplicationContext(), NewEventActivity.class);
            intent.putExtra("LEAD_ID", LEAD_ID);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else if (id == R.id.menuChangeOwner) {
            LeadOwnerBSFragment leadOwnerBSFragment = new LeadOwnerBSFragment("LEAD", lead_obj.getId());
            leadOwnerBSFragment.setCurrentObj(leadOwnerBSFragment);
            leadOwnerBSFragment.show(getSupportFragmentManager(), leadOwnerBSFragment.getTag());
        } else if (id == R.id.action_disqualify) {
            DialogUtils.showDisqualifyDialog(this,
                    getResources().getString(R.string.disqualify_lead),
                    getResources().getString(R.string.reason),
                    getResources().getString(R.string.disqualify),
                    getResources().getString(R.string.cancel),
                    this::updateLeadStatus);
        } else if (id == R.id.action_view_appointment) {
            Intent intent = new Intent(this, ViewAppointmentActivity.class);
            intent.putExtra("DATA", arrAppointment.toString());
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateLeadStatus(String reason) {
        JSONObject postData = new JSONObject();
        try {
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(LEAD_ID);
            postData.put("lead_ids_list", jsonArray);
            postData.put("lead_status", "disqualified");
            postData.put("disqualified_reason", reason);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        NetworkManager.customJsonObjectRequest(
                LeadViewActivity.this, Constants.UPDATE_LEAD_STATUS, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processUpdateStatusData(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("LEAD PROCESS ERROR", message);
                    }
                }, true);
    }

    private void processUpdateStatusData(JSONObject data) {
        LogUtils.Print("processUpdateStatusData", data.toString());
        try {
            if (Integer.parseInt(data.getString("result")) == 1) {
                setResult(RESULT_OK);
                finish();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<LeadView> getData() {
        return leadPage;
    }

    public LeadList getLead() {
        return lead_obj;
    }

    public List<Comment> getComments() {
        comments = lead_obj.getComments();
        return comments;
    }

    public String getID() {
        return LEAD_ID;
    }

    public void refreshData() {
        leadPage.clear();
        getLeadById(LEAD_ID);
    }

    private void getLeadById(String lead_id) {
        JSONObject postData = new JSONObject();
        try {
            postData.put("action", "get");
            postData.put("lead_id", lead_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        NetworkManager.customJsonObjectRequest(
                LeadViewActivity.this, Constants.LEAD_LIST_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processData(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("LEAD PROCESS ERROR", message);
                    }
                }, true);
    }

    private void processData(JSONObject data) {
        try {
            LogUtils.Print("processData -> ", "" + data);
            if (Integer.parseInt(data.getString("result")) == 1) {

                JSONObject result_Data = data.getJSONObject("data");
                JSONArray leads = result_Data.getJSONArray("lead_contacts");
                for (int i = 0; i < leads.length(); i++) {
                    JSONObject obj = leads.getJSONObject(i);
                    JSONArray emailArray = obj.getJSONArray("email_list");
                    List<Email> emails = new ArrayList<>();
                    for (int j = 0; j < emailArray.length(); j++) {
                        JSONObject emailObj = emailArray.getJSONObject(j);
                        emails.add(new Email(emailObj.getString("email"), emailObj.getString("type")));
                    }
                    JSONArray phoneArray = obj.getJSONArray("phone_list");
                    List<Phone> phones = new ArrayList<>();
                    for (int j = 0; j < phoneArray.length(); j++) {
                        JSONObject phoneObj = phoneArray.getJSONObject(j);
                        phones.add(new Phone(phoneObj.getString("phone"), phoneObj.getString("type")));
                    }
                    JSONArray addressArray = obj.getJSONArray("address_list");
                    List<Address> addresses = new ArrayList();
                    for (int j = 0; j < addressArray.length(); j++) {
                        JSONObject addressObj = addressArray.getJSONObject(j);
                        addresses.add(new Address(addressObj.getString("address"), addressObj.getString("type")));
                    }
                    if (obj.has("appointment_list")) {
                        arrAppointment = obj.getJSONArray("appointment_list");
                    }
                    List<Comment> comments_data = new ArrayList();
                    if (obj.has("comments")) {
                        Object comment_instance = obj.get("comments");
                        if (comment_instance instanceof JSONArray) {
                            JSONArray commentsArray = obj.getJSONArray("comments");

                            for (int j = 0; j < commentsArray.length(); j++) {
                                JSONObject commentObj = commentsArray.getJSONObject(j);
                                comments_data.add(new Comment(commentObj.getString("comment"), commentObj.getString("date"),
                                        commentObj.has("url") ? commentObj.getString("url") : "",
                                        commentObj.has("content_type") ? commentObj.getString("content_type") : ""));
                            }
                        }
                    }

                    JSONObject lOObj = obj.getJSONObject("lead_owner");
                    LeadOwner leadOwner = new LeadOwner(lOObj.getString("id"),
                            lOObj.getString("name"));
                    JSONObject mpObj = obj.getJSONObject("match_preferences_dict");
                    MatchPreference mp = new MatchPreference();
                    if (mpObj.has("lease_length")) {
                        mp.setLease_length(Integer.parseInt(mpObj.getString("lease_length")));
                    }
                    if (mpObj.has("bedrooms")) {
                        mp.setBeds(mpObj.getInt("bedrooms"));
                    }
                    if (mpObj.has("bedrooms")) {
                        mp.setBeds(mpObj.getInt("bedrooms"));
                    }
                    if (mpObj.has("bathrooms")) {
                        mp.setBathrooms(mpObj.getInt("bathrooms"));
                    }
                    if (mpObj.has("budget")) {
                        mp.setBudget(mpObj.getDouble("budget"));
                    }
                    if (obj.has("lead_status") &&
                            obj.getString("lead_status").equals("disqualified")) {
                        if (menuDisqualify != null)
                            menuDisqualify.setVisible(false);
                    }
                    lead_obj = new LeadList(obj.getString("id"),
                            obj.getString("name"),
                            obj.getString("lead_status"),
                            obj.getString("file_as"),
                            obj.getString("lead_source"),
                            comments_data,
                            addresses,
                            emails,
                            phones,
                            leadOwner,
                            mp,
                            obj.getString("contact_id"),
                            obj.getBoolean("marketing_campaign_enabled"),
                            obj.getLong("created_ts"), obj.getLong("status_update_date"));
                    renderView();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void convertToContact(String lead_id) {
        JSONObject postData = new JSONObject();
        try {
            postData.put("lead_id", lead_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        NetworkManager.customJsonObjectRequest(
                LeadViewActivity.this, Constants.UPGRADE_TO_CONTACT_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processResult(response, getResources().getString(R.string.contact_update_success));
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("LEAD PROCESS ERROR", message);
                    }
                }, true);
    }

    private void putItOnCampaign(String lead_id, final boolean addToCampaign) {
        JSONObject postData = new JSONObject();
        try {
            postData.put("action", "update");
            postData.put("lead_id", lead_id);
            JSONObject payload = new JSONObject();
            payload.put("marketing_campaign_enabled", addToCampaign);
            postData.put("payload", payload);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        NetworkManager.customJsonObjectRequest(
                LeadViewActivity.this, Constants.LEAD_LIST_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processResult(response, addToCampaign ?
                                getResources().getString(R.string.campaign_add_success) :
                                getResources().getString(R.string.campaign_remove));
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("LEAD PROCESS ERROR", message);
                    }
                }, true);
    }

    private void assignToMe(String lead_id) {
        JSONObject postData = new JSONObject();
        JSONArray lead_ids = new JSONArray();
        lead_ids.put(lead_id);
        try {
            postData.put("lead_ids_list", lead_ids);
            postData.put("self_assign", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        NetworkManager.customJsonObjectRequest(
                LeadViewActivity.this, Constants.SELF_ASSIGN_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processResult(response, getResources().getString(R.string.lead_assigned_to_you));
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("LEAD PROCESS ERROR", message);
                    }
                }, true);
    }

    private void processResult(JSONObject response, String message) {
        try {
            if (Integer.parseInt(response.getString("result")) == 1) {
                Util.onMessage(getApplicationContext(), message);
            } else {
                Util.onMessage(getApplicationContext(), getResources().getString(R.string.changes_failed));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void renderView() {
        actionViewAppointment.setVisible(arrAppointment != null && arrAppointment.length() > 0);

        LeadView lv;
        lv = new LeadView(getResources().getString(R.string.name), lead_obj.getTitle(), "", R.drawable.ic_account, 0);
        leadPage.add(lv);
        for (int i = 0; i < lead_obj.getEmails().size(); i++) {
            Email email = lead_obj.getEmails().get(i);
            String et = getResources().getString(R.string.email);
            if (!email.getType().isEmpty()) {
                et = et + "(" + email.getType() + ")";
            }
            lv = new LeadView(et, email.getEmail(), "EMAIL", R.drawable.ic_email, 0);
            leadPage.add(lv);
        }

        for (int i = 0; i < lead_obj.getPhones().size(); i++) {
            Phone phone = lead_obj.getPhones().get(i);
            String pt = getResources().getString(R.string.phone);
            if (!phone.getType().isEmpty()) {
                pt = pt + "(" + phone.getType() + ")";
            }
            lv = new LeadView(pt, phone.getPhone(), "PHONE", R.drawable.ic_call, R.drawable.ic_message);
            leadPage.add(lv);
        }

        for (int i = 0; i < lead_obj.getAddresses().size(); i++) {
            Address address = lead_obj.getAddresses().get(i);
            String at = getResources().getString(R.string.address);
            if (!address.getType().isEmpty()) {
                at = at + "(" + address.getType() + ")";
            }
            lv = new LeadView(at, address.getAddress(), "ADDRESS", R.drawable.ic_location, R.drawable.ic_directions);
            leadPage.add(lv);
        }
        lv = new LeadView(getResources().getString(R.string.lead_owner), lead_obj.getLeadOwner().getName(), "", R.drawable.ic_account_circle, 0);
        leadPage.add(lv);

        lv = new LeadView(getResources().getString(R.string.created_on), Util.formatDate(lead_obj.getCreated_on()), "", 0, 0);
        leadPage.add(lv);
        lv = new LeadView(getResources().getString(R.string.updated_on), Util.formatDate(lead_obj.getUpdated_on()), "", 0, 0);
        leadPage.add(lv);

        lv = new LeadView(getResources().getString(R.string.lead_source), lead_obj.getLeadSource(), "", 0, 0);
        leadPage.add(lv);
        lv = new LeadView(getResources().getString(R.string.file_as), lead_obj.getFileAs(), "", 0, 0);
        leadPage.add(lv);

        lv = new LeadView(getResources().getString(R.string.lease_length), lead_obj.getMatchPreference().getLease_length() != null ? lead_obj.getMatchPreference().getLease_length() + "" : "NA", "", 0, 0);
        leadPage.add(lv);
        lv = new LeadView(getResources().getString(R.string.budget), lead_obj.getMatchPreference().getBudget() != null ? +lead_obj.getMatchPreference().getBudget() + "" : "NA", "", 0, 0);
        leadPage.add(lv);
        lv = new LeadView(getResources().getString(R.string.bedrooms), lead_obj.getMatchPreference().getBeds() != null ? lead_obj.getMatchPreference().getBeds() + "" : "NA", "", 0, 0);
        leadPage.add(lv);

        lv = new LeadView(getResources().getString(R.string.bathrooms), lead_obj.getMatchPreference().getBathrooms() != null ? lead_obj.getMatchPreference().getBathrooms() + "" : "NA", "", 0, 0);
        leadPage.add(lv);

        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(lead_obj.getTitle());
        mSectionsPagerAdapter = new LeadViewPagerAdapter(getSupportFragmentManager());
        mViewPager = findViewById(R.id.leadView_viewPager_id);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        lead_campaign.setChecked(lead_obj.isCampaign());
        if (menuCampaign != null) {
            menuCampaign.setIcon(ContextCompat.getDrawable(this,
                    lead_obj.isCampaign() ? R.drawable.ic_baseline_toggle_on_24 : R.drawable.ic_baseline_toggle_off_24));
            menuCampaign.setChecked(lead_obj.isCampaign());
        }

        final FloatingActionButton fab = findViewById(R.id.lead_view_edit_id);
        fab.setOnClickListener(view -> {
            Intent leadIntent = new Intent(getApplicationContext(), LeadProcessActivity.class);
            leadIntent.putExtra("LEAD_ID", lead_obj.getId());
            leadIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(leadIntent);
        });

        TabLayout tabLayout = findViewById(R.id.leadView_tabs_id);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout) {
            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        fab.show();
                        break;
                    case 1:
                        fab.hide();
                        break;
                }
            }
        });
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        call_btn = findViewById(R.id.lead_appbar_Call);
        call_btn.setOnClickListener(view -> {
            if (lead_obj.getPhones().size() > 0) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + lead_obj.getPhones().get(0).getPhone()));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } else {
                Snackbar.make(view, getResources().getString(R.string.no_phone_is_associated_lead), Snackbar.LENGTH_LONG)
                        .setAction(getResources().getString(R.string.action), null).show();
            }
        });
        if (menuCall != null && lead_obj.getPhones() != null)
            menuCall.setVisible(lead_obj.getPhones().size() > 0);
        if (menuSMS != null && lead_obj.getPhones() != null)
            menuSMS.setVisible(lead_obj.getPhones().size() > 0);
        if (menuEmail != null && lead_obj.getEmails() != null)
            menuEmail.setVisible(lead_obj.getEmails().size() > 0);

        if (lead_obj.getContact_id() != null && !lead_obj.getContact_id().isEmpty()) {
            upgrade_to_contact_btn.setImageResource(R.drawable.account_outline);
            upgrade_to_contact_text.setText(getResources().getString(R.string.contact));

        }
//        if (lead_obj.getLeadOwner().getId().equals(LOGIN_USER_ID) && IS_ADMIN) {
//            assign_to_me_text.setText(getResources().getString(R.string.assigned_to_me));
//            assign_to_me_btn.setEnabled(false);
//            assign_to_me_text.setVisibility(View.GONE);
//            assign_to_me_btn.setVisibility(View.GONE);
//            if (menuAssignToMe != null)
//                menuAssignToMe.setVisible(false);
//        } else if (lead_obj.getLeadOwner().getId().equals(LOGIN_USER_ID) && !IS_ADMIN) {
//            assign_to_me_text.setText(getResources().getString(R.string.assigned_to_me));
//            assign_to_me_text.setVisibility(View.GONE);
//            assign_to_me_btn.setVisibility(View.GONE);
//            if (menuAssignToMe != null)
//                menuAssignToMe.setVisible(false);
//        }
        upgrade_to_contact_btn.setOnClickListener(view -> {
            if (lead_obj.getContact_id() == null || lead_obj.getContact_id().isEmpty()) {
                convertToContact(LEAD_ID);
                upgrade_to_contact_btn.setImageResource(R.drawable.account_outline);
            } else {
                Intent intent = new Intent(getApplicationContext(), ClientViewActivity.class);
                intent.putExtra("CLIENT_ID", lead_obj.getContact_id());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
        assign_to_me_btn.setOnClickListener(view -> {
            if (!lead_obj.getLeadOwner().getId().equals(LOGIN_USER_ID)) {
                assignToMe(LEAD_ID);
            }
        });
        lead_campaign.setOnCheckedChangeListener((buttonView, isChecked) -> putItOnCampaign(LEAD_ID, isChecked));
    }
}