package com.gofercrm.user.gofercrm.settings.deleteaccount;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.login.LoginActivity;
import com.gofercrm.user.gofercrm.util.DialogUtils;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;

import org.json.JSONException;
import org.json.JSONObject;

public class DeleteAccountActivity extends BaseActivity {

    private EditText etMessage;
    private TextView tvCount;
    private Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_account);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.delete_accout));
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());
        findViewById();
        setUpHeaderView();
    }

    private void findViewById() {
        etMessage = findViewById(R.id.etMessage);
        tvCount = findViewById(R.id.tvCount);
        btnSubmit = findViewById(R.id.btnSubmit);
    }

    private void setUpHeaderView() {
        etMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tvCount.setText(charSequence.toString().length() + " / 200");
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        btnSubmit.setOnClickListener(view -> {
            if (etMessage.getText().toString().trim().isEmpty()) {
                Util.showSnackBar(getResources().getString(R.string.reason_empty_err), DeleteAccountActivity.this);
            } else {
                DialogUtils.showConfirmationDialog(DeleteAccountActivity.this,
                        this::deleteAccountAPIRequest,
                        getResources().getString(R.string.delete_account_confirmation));
            }
        });
    }

    private void deleteAccountAPIRequest() {
        JSONObject postData = new JSONObject();
        try {
            postData.put("reason", etMessage.getText().toString().trim());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        LogUtils.Print("postData--> ", "" + postData);
        NetworkManager.customJsonObjectRequest(
                DeleteAccountActivity.this, Constants.DELETE_ACCOUNT, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtils.Print("onResponse ", "" + response);
                        try {
                            if (Integer.parseInt(response.getString("result")) == 1) {
                                startWithClearStack(LoginActivity.class);
                            } else {
                                if (response.has("error")) {
                                    JSONObject error_obj = response.getJSONObject("error");
                                    Util.onMessage(DeleteAccountActivity.this, error_obj.has("reason") ? error_obj.getString("reason") :
                                            getResources().getString(R.string.server_err));
                                } else {
                                    Util.showSnackBar(getResources().getString(R.string.server_err), DeleteAccountActivity.this);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Util.showSnackBar(getResources().getString(R.string.server_err), DeleteAccountActivity.this);
                        }
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("onError=", message);
                        Util.showSnackBar(message, DeleteAccountActivity.this);
                    }
                }, true);
    }
}