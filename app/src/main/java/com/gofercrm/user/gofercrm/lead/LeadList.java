package com.gofercrm.user.gofercrm.lead;

import com.gofercrm.user.gofercrm.entity.Address;
import com.gofercrm.user.gofercrm.entity.Comment;
import com.gofercrm.user.gofercrm.entity.Email;
import com.gofercrm.user.gofercrm.entity.LeadOwner;
import com.gofercrm.user.gofercrm.entity.MatchPreference;
import com.gofercrm.user.gofercrm.entity.Phone;

import java.io.Serializable;
import java.util.List;

public class LeadList implements Serializable {
    private String id, title, status, fileAs, leadSource, contact_id;
    private boolean isCampaign;
    private List<Comment> comments;
    private List<Address> addresses;
    private List<Email> emails;
    private List<Phone> phones;
    private LeadOwner leadOwner;
    private MatchPreference matchPreference;
    private Long created_on;
    private Long updated_on;

    public LeadList() {
    }

    public LeadList(String id, String title, String status, String fileAs, String leadSource, List<Comment> comments,
                    List<Address> addresses, List<Email> emails,
                    List<Phone> phones, LeadOwner leadOwner,
                    MatchPreference matchPreference,
                    String contact_id, boolean isCampaign, Long created_on, Long updated_on) {
        this.id = id;
        this.title = title;
        this.status = status;
        this.fileAs = fileAs;
        this.leadSource = leadSource;
        this.comments = comments;
        this.addresses = addresses;
        this.emails = emails;
        this.phones = phones;
        this.leadOwner = leadOwner;
        this.contact_id = contact_id;
        this.isCampaign = isCampaign;
        this.matchPreference = matchPreference;
        this.created_on = created_on;
        this.updated_on = updated_on;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFileAs() {
        return fileAs;
    }

    public void setFileAs(String fileAs) {
        this.fileAs = fileAs;
    }

    public String getLeadSource() {
        return leadSource;
    }

    public void setLeadSource(String leadSource) {
        this.leadSource = leadSource;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    public List<Email> getEmails() {
        return emails;
    }

    public void setEmails(List<Email> emails) {
        this.emails = emails;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    public LeadOwner getLeadOwner() {
        return leadOwner;
    }

    public void setLeadOwner(LeadOwner leadOwner) {
        this.leadOwner = leadOwner;
    }

    public String getContact_id() {
        return contact_id;
    }

    public void setContact_id(String contact_id) {
        this.contact_id = contact_id;
    }

    public boolean isCampaign() {
        return isCampaign;
    }

    public void setCampaign(boolean campaign) {
        isCampaign = campaign;
    }

    public MatchPreference getMatchPreference() {
        return matchPreference;
    }

    public void setMatchPreference(MatchPreference matchPreference) {
        this.matchPreference = matchPreference;
    }

    public Long getCreated_on() {
        return created_on;
    }

    public void setCreated_on(Long created_on) {
        this.created_on = created_on;
    }

    public Long getUpdated_on() {
        return updated_on;
    }

    public void setUpdated_on(Long updated_on) {
        this.updated_on = updated_on;
    }
}