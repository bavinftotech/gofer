package com.gofercrm.user.gofercrm.settings.language;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.ApplicationContext;
import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.implementor.RecyclerViewClickListener;
import com.gofercrm.user.gofercrm.settings.language.adapter.LanguageAdapter;
import com.gofercrm.user.gofercrm.settings.language.model.DataLanguage;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class LanguageActivity extends BaseActivity implements RecyclerViewClickListener {

    private RecyclerView recyclerView;
    private LanguageAdapter adapter;
    private List<DataLanguage> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.language));
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());
        findViewById();
        setUpAdapterView();
    }

    private void findViewById() {
        recyclerView = findViewById(R.id.rv);
    }

    private void setUpAdapterView() {
        String[] arrLanguage = getResources().getStringArray(R.array.strLanguage);
        String[] arrLanguageVal = getResources().getStringArray(R.array.strLanguageVal);
        for (int i = 0; i < arrLanguage.length; i++) {
            list.add(new DataLanguage(arrLanguage[i], arrLanguageVal[i],
                    ApplicationContext.preferences.getString(ApplicationContext.key_lang).equals(arrLanguageVal[i])));
        }
        adapter = new LanguageAdapter(list, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        adapter.setOnRecyclerViewItemClickListener(this);
    }

    @Override
    public void onItemClick(int flag, int position, View view) {
        if (flag == 0) {
            for (int i = 0; i < list.size(); i++) {
                list.get(i).setSelect(i == position);
            }
            adapter.notifyDataSetChanged();
            updateUserLanguage(list.get(position).getVal());
            ApplicationContext.updateResources(this, list.get(position).getVal());
//            startWithClearStack(NavigationActivity.class);
        }
    }

    private void updateUserLanguage(String language) {
        NetworkManager.customJsonObjectRequest(this, Constants.UPDATE_PROFILE +
                        "?preferred_language=" + language, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtils.Print("updateUserLanguage", response.toString());
                        setResult(RESULT_OK);
                        finish();
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("updateUserLanguage", message);
                    }
                }, false);
    }
}