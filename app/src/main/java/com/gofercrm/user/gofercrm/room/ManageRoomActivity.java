package com.gofercrm.user.gofercrm.room;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import androidx.appcompat.widget.Toolbar;

import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.appointments.Contact;
import com.gofercrm.user.gofercrm.appointments.ContactAdapter;
import com.gofercrm.user.gofercrm.appointments.ContactsCompletionView;
import com.gofercrm.user.gofercrm.chat.ui.main.RoomDetailActivity;
import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.tokenautocomplete.TokenCompleteTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ManageRoomActivity extends BaseActivity implements TokenCompleteTextView.TokenListener<Contact> {

    ContactsCompletionView connectionssChip;
    List<Contact> connections = new ArrayList<>();
    ArrayAdapter<Contact> connectionsAdapter;

    List<Contact> emailToLists = new ArrayList<>();

    EditText name, purpose, phone;
//    ProgressDialog progressDialog;

    String PROCESSING_MESSAGE;
    String SUCCESS_MESSAGE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_room);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.create_room));
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        PROCESSING_MESSAGE = getResources().getString(R.string.room_is_getting_created);
        SUCCESS_MESSAGE = getResources().getString(R.string.room_created_success);
        connectionsAdapter = new ContactAdapter(this, R.layout.contact_ac_layout, connections);
        connectionssChip = findViewById(R.id.connections_id);
        connectionssChip.setTokenListener(this);
        connectionssChip.setTokenClickStyle(TokenCompleteTextView.TokenClickStyle.Select);
        connectionssChip.setAdapter(connectionsAdapter);
        getConnections();
        name = findViewById(R.id.input_room_name);
        purpose = findViewById(R.id.input_room_comments);
        phone = findViewById(R.id.input_room_phone);
    }

    @Override
    public void onTokenAdded(Contact token) {
        emailToLists.add(token);
    }

    @Override
    public void onTokenRemoved(Contact token) {
        emailToLists.remove(token);
    }

    @Override
    public void onTokenIgnored(Contact token) {
    }

    private void getConnections() {
        NetworkManager.customJsonObjectGetRequest(
                ManageRoomActivity.this, Constants.GET_CONNECTIONS_PICKLIST_URL, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processConnections(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("Error", message);
                    }
                }, false);
    }

    private void processConnections(JSONObject data) {
        try {
            connections.clear();
            String id = "";
            String name = "";
            String email = "", phone = "";
            if (Integer.parseInt(data.getString("result")) == 1) {
                JSONArray users = data.getJSONArray("users");
                for (int i = 0; i < users.length(); i++) {
                    JSONObject obj = users.getJSONObject(i);
                    if (obj.has("email")) {
                        email = obj.getString("email");
                        id = obj.getString("id");
                        name = obj.getString("name");
                        if (name != null) {
                            name = Util.capitalize(name);
                        }
                        Contact contact = new Contact(id, name, email, phone);
                        connections.add(contact);
                    }
                }
                connectionsAdapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean validate() {
        boolean valid = true;
        String title_value = name.getText().toString();
        if (title_value != null && title_value.isEmpty()) {
            name.setError(getResources().getString(R.string.name_empty_err));
            valid = false;
        } else {
            name.setError(null);
        }
        if (emailToLists.size() <= 0) {
            connectionssChip.setError(getResources().getString(R.string.ensure_add_connection));
            valid = false;
        } else {
            connectionssChip.setError(null);
        }
        return valid;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_room, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_event_save: {
                if (!validate()) {
                    Util.onMessage(getApplicationContext(), getResources().getString(R.string.missing_mandatory_fields));
                } else {
                    createRoom();
                }
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void createRoom() {
//        progressDialog = new ProgressDialog(ManageRoomActivity.this,
//                R.style.AppTheme_Dark_Dialog);
//        progressDialog.setCanceledOnTouchOutside(false);
//        progressDialog.setIndeterminate(true);
//        progressDialog.setMessage(PROCESSING_MESSAGE);
//        progressDialog.show();
        showProgress();
        String ids;
        StringBuilder result = new StringBuilder();
        for (Contact contact : emailToLists) {
            result.append(contact.getId());
            result.append(",");
        }
        ids = result.length() > 0 ? result.substring(0, result.length() - 1) : "";

        String req_url = Constants.MANAGE_ROOM;

        String param = "?room_name=" + name.getText().toString() + "&room_phone=" + phone.getText().toString() + "&description=" + purpose.getText().toString() + "&initial_members=" + ids;
        NetworkManager.customJsonObjectRequest(
                ManageRoomActivity.this, req_url + param, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        progressDialog.dismiss();
                        hideProgress();
                        processResult(response);
                    }

                    @Override
                    public void onError(String message) {
//                        progressDialog.dismiss();
                        hideProgress();
                    }
                }, true);
    }

    private void processResult(JSONObject response) {
        try {
            if (Integer.parseInt(response.getString("result")) == 1) {
                JSONObject room_object = response.getJSONObject("room");
                if (room_object.has("room_id")) {
                    String id = room_object.getString("room_id");
                    String name = "";
                    if (room_object.has("room_name")) {
                        name = room_object.getString("room_name");
                    }
                    Intent intent = new Intent(getApplicationContext(), RoomDetailActivity.class);
                    intent.putExtra("_ID", id);
                    intent.putExtra("_NAME", name);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }
                Util.onMessage(getApplicationContext(), getResources().getString(R.string.room_created_success));
            } else {
                Util.onMessage(getApplicationContext(), getResources().getString(R.string.room_created_fail));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}