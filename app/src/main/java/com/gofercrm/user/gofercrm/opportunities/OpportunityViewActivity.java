package com.gofercrm.user.gofercrm.opportunities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.attachments.AttachmentActivity;
import com.gofercrm.user.gofercrm.entity.Comment;
import com.gofercrm.user.gofercrm.task.NewTaskActivity;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OpportunityViewActivity extends BaseActivity {

    ArrayList<OpportunityView> opporPage = new ArrayList<>();
    Opportunity oppor_obj = new Opportunity();
    EditText commentText;
    ImageButton opportunity_won_btn, opportunity_lost_btn;
    List comments = new ArrayList<Comment>();
    String OPPOR_ID;
    JSONObject result_obj;
    private OpportunityViewPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opportunity_view);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        opportunity_won_btn = findViewById(R.id.oppor_won_btn);
        opportunity_lost_btn = findViewById(R.id.oppor_lost_btn);

        opportunity_won_btn.setOnClickListener(view -> {
            if (result_obj != null) {
                Intent intent = new Intent(getApplicationContext(), OpportunityWonActivity.class);
                intent.putExtra("OPPORTUNITY_TO_WON", result_obj.toString());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        opportunity_lost_btn.setOnClickListener(view -> {
            if (result_obj != null) {
                Intent intent = new Intent(getApplicationContext(), OpportunityLostActivity.class);
                intent.putExtra("OPPORTUNITY_TO_LOST", result_obj.toString());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        Intent intent = getIntent();
        OPPOR_ID = intent.getStringExtra("OPPOR_ID");
        if (OPPOR_ID != null) {
            getOpportunityById(OPPOR_ID);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_opportunity_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_task: {
                Intent intent = new Intent(getApplicationContext(), NewTaskActivity.class);
                intent.putExtra("OPPORTUNITY_ID", OPPOR_ID);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
            }
            case R.id.action_attachment: {
                Intent intent = new Intent(getApplicationContext(), AttachmentActivity.class);
                intent.putExtra("ENTITY_ID", OPPOR_ID);
                intent.putExtra("ENTITY_TYPE", "opportunity");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
            }
            case R.id.action_delete:
                new AlertDialog.Builder(this)
                        .setTitle(getResources().getString(R.string.delete))
                        .setMessage(getResources().getString(R.string.delete_confirmation_msg))
                        .setIcon(android.R.drawable.ic_menu_delete)
                        .setPositiveButton(getResources().getString(R.string.yes), (dialog, whichButton) -> deleteOpportunityById(OPPOR_ID))
                        .setNegativeButton(getResources().getString(R.string.no), null).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public ArrayList<OpportunityView> getData() {
        return opporPage;
    }

    public List<Comment> getComments() {
        if (oppor_obj.getComments() != null) {
            comments = oppor_obj.getComments();
        }
        return comments;
    }

    public String getOpportunity_Id() {
        String oppor_id;
        oppor_id = OPPOR_ID != null ? OPPOR_ID : "";
        return oppor_id;
    }

    private void deleteOpportunityById(String opportunity_id) {
        JSONObject postData = new JSONObject();
        try {
            postData.put("action", "delete");
            postData.put("opportunity_id", opportunity_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        NetworkManager.customJsonObjectRequest(
                OpportunityViewActivity.this, Constants.OPPORTUNITY_MANAGE_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processResult(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("OPPOR. PROCESS ERROR", message);
                    }
                }, true);
    }

    private void processResult(JSONObject response) {
        try {
            if (Integer.parseInt(response.getString("result")) == 1) {
                Util.onMessageTop(getApplicationContext(), getResources().getString(R.string.oppo_deleted));
                Intent intent = new Intent(getApplicationContext(), OpportunityActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } else {
                Util.onMessageTop(getApplicationContext(), getResources().getString(R.string.oppo_delete_fail));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void processData(JSONObject data) {
        try {
            if (Integer.parseInt(data.getString("result")) == 1) {
                JSONObject result_Data = data.getJSONObject("data");
                JSONArray _oppors = result_Data.getJSONArray("opportunities");
                for (int i = 0; i < _oppors.length(); i++) {

                    result_obj = _oppors.getJSONObject(i);
                    List<Comment> comments_data = new ArrayList();
                    if (result_obj.has("comments_list")) {
                        Object comment_instance = result_obj.get("comments_list");
                        if (comment_instance instanceof JSONArray) {
                            JSONArray commentsArray = result_obj.getJSONArray("comments_list");
                            for (int j = 0; j < commentsArray.length(); j++) {
                                if (commentsArray.get(j) instanceof String) {
                                } else {
                                    JSONObject commentObj = commentsArray.getJSONObject(j);
                                    if (commentObj.has("comment")) {
                                        comments_data.add(new Comment(commentObj.getString("comment"), commentObj.getString("date"), commentObj.has("url") ? commentObj.getString("url") : "",
                                                commentObj.has("content_type") ? commentObj.getString("content_type") : ""));
                                    }
                                }
                            }
                        }
                    }
                    oppor_obj = new Opportunity();
                    if (comments_data.size() > 0) {
                        oppor_obj.setComments(comments_data);
                    }
                    if (result_obj.has("opportunity_id")) {
                        oppor_obj.setId(result_obj.getString("opportunity_id"));
                    }
                    if (result_obj.has("name")) {
                        oppor_obj.setName(result_obj.getString("name"));
                    }
                    if (result_obj.has("expected_close_date") && !result_obj.getString("expected_close_date").isEmpty()) {

                        oppor_obj.setClosedDate(result_obj.getLong("expected_close_date"));
                    }
                    if (result_obj.has("value_of_sale")) {
                        oppor_obj.setSaleValue(result_obj.getString("value_of_sale"));
                    }
                    if (result_obj.has("stage")) {
                        oppor_obj.setStage(result_obj.getString("stage"));
                    }
                    if (result_obj.has("created_ts") && !result_obj.getString("created_ts").isEmpty()) {
                        oppor_obj.setCreatedOn(result_obj.getLong("created_ts"));
                    }
                    if (result_obj.has("contact_id")) {
                        oppor_obj.setContact_id(result_obj.getString("contact_id"));
                    }
                    if (result_obj.has("product_id")) {
                        oppor_obj.setProduct_id(result_obj.getString("product_id"));
                    }
                    if (result_obj.has("contact_info")) {
                        JSONObject cInfo = result_obj.getJSONObject("contact_info");
                        oppor_obj.setContactName(cInfo.getString("name"));
                    }
                    if (result_obj.has("product_info")) {
                        JSONObject pInfo = result_obj.getJSONObject("product_info");
                        oppor_obj.setProductName(pInfo.getString("name"));
                    }
                    if (result_obj.has("business_need_description")) {
                        oppor_obj.setBusinessNeedDesc(result_obj.getString("business_need_description"));
                    }
                    if (result_obj.has("competitive_description")) {
                        oppor_obj.setCompetitiveDesc(result_obj.getString("competitive_description"));
                    }
                    renderView();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getOpportunityById(String oppor_id) {
        JSONObject postData = new JSONObject();
        try {
            postData.put("action", "get");
            postData.put("opportunity_id", oppor_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        NetworkManager.customJsonObjectRequest(
                OpportunityViewActivity.this, Constants.OPPORTUNITY_MANAGE_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processData(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("OPPOR. PROCESS ERROR", message);
                    }
                }, true);
    }

    private void renderView() {
        OpportunityView ov = null;
        ov = new OpportunityView("Name", oppor_obj.getName(), "", R.drawable.ic_account, 0);
        opporPage.add(ov);
        ov = new OpportunityView("Stage", oppor_obj.getStage(), "STAGE", 0, 0);
        opporPage.add(ov);
        ov = new OpportunityView("Sale Value", oppor_obj.getSaleValue(), "", R.drawable.currency_usd, 0);
        opporPage.add(ov);

        if (oppor_obj.getStage().toLowerCase().equals("closed_won")) {
            opportunity_won_btn.setVisibility(View.GONE);
        } else if (oppor_obj.getStage().toLowerCase().equals("closed_lost")) {
            opportunity_lost_btn.setVisibility(View.GONE);
        }

        String edit_pattern = "yyyy-MM-dd";
        SimpleDateFormat edditDateFormat = new SimpleDateFormat(edit_pattern);
        if (oppor_obj.getClosedDate() != null) {
            String close_date = edditDateFormat.format(new Date(oppor_obj.getClosedDate() * 1000));
            ov = new OpportunityView(getResources().getString(R.string.expected_close_date), close_date, "", R.drawable.ic_today, 0);
            opporPage.add(ov);
        }
        if (oppor_obj.getCreatedOn() != null) {
            String close_date = edditDateFormat.format(new Date(oppor_obj.getCreatedOn() * 1000));
            ov = new OpportunityView(getResources().getString(R.string.created_on), close_date, "", R.drawable.ic_today, 0);
            opporPage.add(ov);
        }
        if (oppor_obj.getBusinessNeedDesc() != null && !oppor_obj.getBusinessNeedDesc().isEmpty()) {
            ov = new OpportunityView(getResources().getString(R.string.business_need_description), oppor_obj.getBusinessNeedDesc(), "", R.drawable.ic_description, 0);
            opporPage.add(ov);
        }
        if (oppor_obj.getCompetitiveDesc() != null && !oppor_obj.getCompetitiveDesc().isEmpty()) {
            ov = new OpportunityView(getResources().getString(R.string.competitive_description), oppor_obj.getCompetitiveDesc(), "", R.drawable.ic_description, 0);
            opporPage.add(ov);
        }

        if (oppor_obj.getContact_id() != null) {
            ov = new OpportunityView(oppor_obj.getContactName(), oppor_obj.getContact_id(), "CONTACT", R.drawable.account_box_outline, 0);
            opporPage.add(ov);
        }
        if (oppor_obj.getProduct_id() != null) {
            ov = new OpportunityView(oppor_obj.getProductName(), oppor_obj.getProduct_id(), "PRODUCT", R.drawable.account_box_outline, 0);
            opporPage.add(ov);
        }
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(oppor_obj.getName() == null ?
                    getResources().getString(R.string.opportunity) : oppor_obj.getName());
        mSectionsPagerAdapter = new OpportunityViewPagerAdapter(getSupportFragmentManager());
        mViewPager = findViewById(R.id.leadView_viewPager_id);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        final FloatingActionButton fab = findViewById(R.id.lead_view_edit_id);
        fab.setOnClickListener(view -> {
            Intent leadIntent = new Intent(getApplicationContext(), ManageOpportunityActivity.class);
            leadIntent.putExtra("OPPORTUNITY", result_obj.toString());
            leadIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(leadIntent);
        });

        TabLayout tabLayout = findViewById(R.id.leadView_tabs_id);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout) {
            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        fab.show();
                        break;
                    case 1:
                        fab.hide();
                        break;
                }
            }
        });
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
    }
}