package com.gofercrm.user.gofercrm.util;

import android.app.Activity;

import com.gofercrm.user.gofercrm.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class TimeAgo_ {
    private static final String TAG = "TimeAgo";
    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;
    private static final int WEEK_MILLIS = 7 * DAY_MILLIS;

    public static String getTimeAgo(Activity activity, long time) {
        LogUtils.Print(TAG, "time => " + time);
        if (time < 1000000000000L) {
            time *= 1000;
        }

        long now = System.currentTimeMillis();
        LogUtils.Print(TAG, "now => " + now);
        if (time > now || time <= 0) {
            return activity.getResources().getString(R.string.just_now);
        }

        final long diff = now - time;
        LogUtils.Print(TAG, "diff => " + diff);
        if (diff < MINUTE_MILLIS) {
            return activity.getResources().getString(R.string.just_now);
        } else if (diff < HOUR_MILLIS) {
            long min = diff / MINUTE_MILLIS;
            if (min == 1) {
                return activity.getResources().getString(R.string._1_minute_ago);
            } else if (min < 5) {
                return activity.getResources().getString(R.string._5_minutes_ago);
            } else if (min > 5 && min < 30) {
                return min + " " + activity.getResources().getString(R.string.minutes_ago);
            } else {
                return min + " " + activity.getResources().getString(R.string.minutes_ago);
            }
        } else if (diff < DAY_MILLIS) {
            long hr = diff / HOUR_MILLIS;
            if (hr == 1) {
                return hr + " " + activity.getResources().getString(R.string.hour_ago);
            } else {
                return hr + " " + activity.getResources().getString(R.string.hours_ago);
            }
        } else if (diff < WEEK_MILLIS) {
            long day = diff / DAY_MILLIS;
            if (day == 1) {
                return day + " " + activity.getResources().getString(R.string.day_ago);
            } else {
                return day + " " + activity.getResources().getString(R.string.days_ago);
            }
        } else {
            SimpleDateFormat formatter = new SimpleDateFormat("E MMM dd,yyyy h:mm a", Locale.ENGLISH);
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(time);
            return formatter.format(calendar.getTime()) + " " + activity.getResources().getString(R.string.ago);
        }
    }

    public static boolean isActive(long time) {
        LogUtils.Print(TAG, "time => " + time);
        if (time < 1000000000000L) {
            time *= 1000;
        }

        long now = System.currentTimeMillis();
        LogUtils.Print(TAG, "now => " + now);
        if (time > now || time <= 0) {
            return true;
        }

        final long diff = now - time;
        LogUtils.Print(TAG, "diff => " + diff);
        if (diff < HOUR_MILLIS) {
            long min = diff / MINUTE_MILLIS;
            return min < 5;
        } else {
            return false;
        }
    }
}