package com.gofercrm.user.gofercrm.task.view.tab.general;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.task.view.TaskViewActivity;

public class TaskGeneralFragment extends Fragment {


    public TaskGeneralFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rv = inflater.inflate(R.layout.general_tab_task_view, container, false);
        TaskViewActivity activity = (TaskViewActivity) getActivity();
        RecyclerView recyclerView = rv.findViewById(R.id.task_view_recycle);

        TaskGeneralAdapter lGAdapter = new TaskGeneralAdapter(activity.getData(), getContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(lGAdapter);
        return rv;
    }
}