package com.gofercrm.user.gofercrm.opportunities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.gofercrm.user.gofercrm.ApplicationContext;
import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.entity.Comment;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class OpportunityActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    TextView no_records;
    boolean isScrolling = false;
    String last_id = null;
    boolean isMoreDataAvailable = true;
    boolean isOnSearch = false;
    SwipeRefreshLayout mSwipeRefreshLayout;
    private List<Opportunity> opportunities = new ArrayList<>();
    private RecyclerView recyclerView;
    private OpportunityListAdapter oAdapter;
    private ProgressBar paginationProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opportunity);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            JSONObject personal_dict = ((ApplicationContext) this.getApplication()).getPersonal_dict();
            if (personal_dict != null && personal_dict.has("opportunity")) {
                try {
                    getSupportActionBar().setTitle(personal_dict.getString("opportunity"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else {
                getSupportActionBar().setTitle(getResources().getString(R.string.opportunities));
            }
        }
        mSwipeRefreshLayout = findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        no_records = findViewById(R.id.no_records);

        recyclerView = findViewById(R.id.opportunity_list__recycle);
        oAdapter = new OpportunityListAdapter(opportunities, getApplicationContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(oAdapter);
        getOpportunities();

        paginationProgressBar = findViewById(R.id.opportunity_list_pagination_progress);

        FloatingActionButton fab = findViewById(R.id.new_opportunity);
        fab.setOnClickListener(view -> {
            Intent leadIntent = new Intent(getApplicationContext(), ManageOpportunityActivity.class);
            leadIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(leadIntent);
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
                    final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                            .getLayoutManager();
                    int totalItemCount = linearLayoutManager.getItemCount();
                    int lastVisibleItem = linearLayoutManager
                            .findLastVisibleItemPosition();
                    if (isMoreDataAvailable && !isOnSearch && !isScrolling && totalItemCount <= (lastVisibleItem + 5)) {
                        isScrolling = true;
                        getOpportunities();
                    }
                }
            }
        });
    }

    private void getOpportunities() {
        mSwipeRefreshLayout.setRefreshing(true);
        JSONObject postData = new JSONObject();
        try {
            postData.put("action", "get");
            postData.put("page_size", "10");
            if (last_id != null) {
                postData.put("last_id", last_id);
                paginationProgressBar.setVisibility(View.VISIBLE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        NetworkManager.customJsonObjectRequest(
                OpportunityActivity.this, Constants.OPPORTUNITY_MANAGE_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        processData(response, false);
                    }

                    @Override
                    public void onError(String message) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        LogUtils.Print("ERRROR=", message);
                    }
                }, true);
    }

    private void searchOpportunities(String query) {
        mSwipeRefreshLayout.setRefreshing(true);
        String url = Constants.SEARCH_OPPORTUNITY_URL + "?all_words=false&search_all=true&search_text=" + query;
        NetworkManager.customJsonObjectRequest(
                OpportunityActivity.this, url, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        processData(response, true);
                    }

                    @Override
                    public void onError(String message) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        LogUtils.Print(" Opportunity Error", message);
                    }
                }, false);
    }

    private void onLoaded() {
        if (opportunities.size() > 0) {
            paginationProgressBar.setVisibility(View.GONE);
            isScrolling = false;
            recyclerView.setVisibility(View.VISIBLE);
            oAdapter.notifyDataSetChanged();
            no_records.setVisibility(View.GONE);
        } else {
            no_records.setVisibility(View.VISIBLE);
        }
    }

    private void processData(JSONObject data, boolean isSearch) {
        try {
            Opportunity _oppor;
            if (Integer.parseInt(data.getString("result")) == 1) {
                JSONObject result_Data;
                if (isSearch) {
                    result_Data = data.getJSONObject("response");
                } else {
                    result_Data = data.getJSONObject("data");
                }

                last_id = result_Data.getString("last_id");
                JSONArray _oppors = result_Data.getJSONArray("opportunities");
                for (int i = 0; i < _oppors.length(); i++) {
                    JSONObject obj = _oppors.getJSONObject(i);
                    List<Comment> comments = new ArrayList();

                    JSONArray commentsArray = new JSONArray();
                    if (obj.has("comments")) {
                        Object comments_obj = obj.get("comments");
                        if (comments_obj instanceof JSONArray) {
                            commentsArray = (JSONArray) comments_obj;
                        }
                        for (int j = 0; j < commentsArray.length(); j++) {
                            JSONObject commentObj = commentsArray.getJSONObject(j);
                            comments.add(new Comment(commentObj.getString("comment"), commentObj.getString("date"), commentObj.has("url") ? commentObj.getString("url") : "",
                                    commentObj.has("content_type") ? commentObj.getString("content_type") : ""));
                        }
                    }
                    _oppor = new Opportunity();

                    if (obj.has("opportunity_id")) {
                        _oppor.setId(obj.getString("opportunity_id"));
                    }
                    if (obj.has("name")) {
                        _oppor.setName(obj.getString("name"));
                    }
                    if (obj.has("expected_close_date") && obj.getString("expected_close_date") != null && !obj.getString("expected_close_date").isEmpty()) {
                        _oppor.setClosedDate(obj.getLong("expected_close_date"));
                    }
                    if (obj.has("value_of_sale")) {
                        _oppor.setSaleValue("value_of_sale");
                    }
                    if (obj.has("stage")) {
                        _oppor.setStage(obj.getString("stage"));
                    }
                    opportunities.add(_oppor);
                }
                isMoreDataAvailable = _oppors.length() > 0;
                onLoaded();
            }
        } catch (JSONException e) {
            mSwipeRefreshLayout.setRefreshing(false);
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_lead, menu);
        SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView search = (SearchView) menu.findItem(R.id.lead_search).getActionView();
        search.setSearchableInfo(manager.getSearchableInfo(getComponentName()));
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                isOnSearch = true;
                no_records.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
                search.setIconified(true);
                opportunities.clear();
                last_id = null;
                searchOpportunities(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        search.setOnCloseListener(() -> {
            isOnSearch = false;
            last_id = null;
            opportunities.clear();
            recyclerView.setVisibility(View.GONE);
            getOpportunities();
            return false;
        });
        return true;
    }

    @Override
    public void onRefresh() {
        last_id = null;
        paginationProgressBar.setVisibility(View.GONE);
        getOpportunities();
    }
}