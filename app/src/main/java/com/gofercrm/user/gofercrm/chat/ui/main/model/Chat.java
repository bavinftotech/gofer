package com.gofercrm.user.gofercrm.chat.ui.main.model;

import java.util.List;
import java.util.Objects;

public class Chat {

    public Chat() {
    }

    private String _Id;
    private String mName;
    private String shortName;
    private String vanityName;
    private String mLastChat;
    private Long mTime;
    private String mImage;
    private boolean online;
    private int msg_count;
    private int friend;
    private String phone;
    private String email;
    private String virtual_phone;
    private String room_type;
    private List<Member> members;
    private int unReadCount;
    private String translateLanguage;
    private String timeZone;
    private long muteNotification;

    public String getName() {
        return mName;
    }

    public String getShortName() {
        return shortName;
    }

    public String getVanityName() {
        return vanityName;
    }

    public void setName(String name) {
        mName = name;
    }

    public void setShortName(String name) {
        shortName = name;
    }

    public void setVanityName(String name) {
        vanityName = name;
    }

    public String getLastChat() {
        return mLastChat;
    } //misnamed variable this represents status

    public void setLastChat(String lastChat) {
        mLastChat = lastChat;
    }

    public Long getTime() {
        return mTime;
    }

    public void setmTime(Long time) {
        mTime = time;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage = image;
    }

    public boolean getOnline(){
        return online;
    }

    public void setOnline(boolean on){
        online = on;
    }

    public String get_Id() {
        return _Id;
    }

    public void set_Id(String _Id) {
        this._Id = _Id;
    }

    public int getMsg_count() {
        return msg_count;
    }

    public void setMsg_count(int msg_count) {
        this.msg_count = msg_count;
    }

    public List<Member> getMembers() {
        return members;
    }

    public void setMembers(List<Member> members) {
        this.members = members;
    }

    public int getFriend() {
        return friend;
    }

    public void setFriend(int friend) {
        this.friend = friend;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getVirtual_phone() {
        return virtual_phone;
    }

    public void setVirtual_phone(String virtual_phone) {
        this.virtual_phone = virtual_phone;
    }

    public String getRoom_type() {
        return room_type;
    }

    public void setRoom_type(String room_type) {
        this.room_type = room_type;
    }

    public int getUnReadCount() {
        return unReadCount;
    }

    public void setUnReadCount(int unReadCount) {
        this.unReadCount = unReadCount;
    }

    public String getTranslateLanguage() {
        return translateLanguage;
    }

    public void setTranslateLanguage(String translateLanguage) {
        this.translateLanguage = translateLanguage;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public long getMuteNotification() {
        return muteNotification;
    }

    public void setMuteNotification(long muteNotification) {
        this.muteNotification = muteNotification;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Chat chat = (Chat) o;
        return Objects.equals(_Id, chat._Id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(_Id);
    }
}