package com.gofercrm.user.gofercrm.account;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.NavigationActivity;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.entity.Address;
import com.gofercrm.user.gofercrm.entity.Comment;
import com.gofercrm.user.gofercrm.entity.Email;
import com.gofercrm.user.gofercrm.entity.Phone;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AccountListActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    TextView no_account_records;
    boolean isScrolling = false;
    String last_id = null;
    boolean isMoreDataAvailable = true;
    boolean isOnSearch = false;
    SwipeRefreshLayout mSwipeRefreshLayout;
    private List<AccountList> accountLists = new ArrayList<>();
    private RecyclerView recyclerView;
    private AccountListAdapter aAdapter;
    private ProgressBar progressBar;
    private ProgressBar paginationProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lead_list);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(getResources().getString(R.string.accounts));
        }
        mSwipeRefreshLayout = findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        no_account_records = findViewById(R.id.no_lead_records);

        recyclerView = findViewById(R.id.lead_recycle);
        progressBar = findViewById(R.id.lead_list_progressBar);
        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        aAdapter = new AccountListAdapter(accountLists, getApplicationContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(aAdapter);
        prepareAccountData();

        paginationProgressBar = findViewById(R.id.lead_list_pagination_progress);

        FloatingActionButton fab = findViewById(R.id.lead_view_create_id);
        fab.setOnClickListener(view -> {
            Intent leadIntent = new Intent(getApplicationContext(), AccountProcessActivity.class);
            leadIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(leadIntent);
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NotNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NotNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
                    final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                            .getLayoutManager();
                    int totalItemCount = linearLayoutManager.getItemCount();
                    int lastVisibleItem = linearLayoutManager
                            .findLastVisibleItemPosition();
                    if (isMoreDataAvailable && !isOnSearch && !isScrolling && totalItemCount <= (lastVisibleItem + 5)) {
                        isScrolling = true;
                        prepareAccountData();
                    }
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, NavigationActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void prepareAccountData() {
        JSONObject postData = new JSONObject();
        try {
            postData.put("action", "get");
            postData.put("page_size", "20");
            if (last_id != null) {
                postData.put("last_id", last_id);
                paginationProgressBar.setVisibility(View.VISIBLE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        NetworkManager.customJsonObjectRequest(
                AccountListActivity.this, Constants.MANAGE_ACCOUNT_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        processData(response);
                    }

                    @Override
                    public void onError(String message) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        System.out.println("Error" + message);
                        LogUtils.Print("ERRROR=", message);
                    }
                }, true);
    }

    private void searchLeads(String query) {
        String url = Constants.SEARCH_ACCOUNT_URL + "?all_words=false&search_all=true&search_text=" + query;
        NetworkManager.customJsonObjectRequest(
                AccountListActivity.this, url, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processData(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ERRROR=", message);
                    }

                }, false);
    }

    private void onLoaded() {
        if (accountLists.size() > 0) {
            progressBar.setVisibility(View.GONE);
            paginationProgressBar.setVisibility(View.GONE);
            isScrolling = false;
            recyclerView.setVisibility(View.VISIBLE);
            aAdapter.notifyDataSetChanged();
            no_account_records.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.GONE);
            no_account_records.setVisibility(View.VISIBLE);
        }
    }

    private void processData(JSONObject data) {
        try {
            AccountList account;
            if (Integer.parseInt(data.getString("result")) == 1) {
                JSONObject result_Data = data.getJSONObject("data");
                last_id = result_Data.getString("last_id");
                JSONArray accounts = result_Data.getJSONArray("accounts");
                for (int i = 0; i < accounts.length(); i++) {

                    JSONObject obj = accounts.getJSONObject(i);
                    JSONArray emailArray = obj.getJSONArray("email_list");
                    List<Email> emails = new ArrayList<>();
                    for (int j = 0; j < emailArray.length(); j++) {
                        JSONObject emailObj = emailArray.getJSONObject(j);
                        emails.add(new Email(emailObj.getString("email"), emailObj.getString("type")));
                    }
                    JSONArray phoneArray = obj.getJSONArray("phone_list");
                    List<Phone> phones = new ArrayList<>();
                    for (int j = 0; j < phoneArray.length(); j++) {
                        JSONObject phoneObj = phoneArray.getJSONObject(j);
                        phones.add(new Phone(phoneObj.getString("phone"), phoneObj.getString("type")));
                    }
                    JSONArray addressArray = obj.getJSONArray("address_list");
                    List<Address> addresses = new ArrayList();
                    for (int j = 0; j < addressArray.length(); j++) {
                        JSONObject addressObj = addressArray.getJSONObject(j);
                        addresses.add(new Address(addressObj.getString("address"), addressObj.getString("type")));
                    }
                    List<Comment> comments = new ArrayList();
                    JSONArray commentsArray = new JSONArray();
                    if (obj.has("comments")) {
                        Object comments_obj = obj.get("comments");
                        if (comments_obj instanceof JSONArray) {
                            commentsArray = (JSONArray) comments_obj;
                        }
                        for (int j = 0; j < commentsArray.length(); j++) {
                            JSONObject commentObj = commentsArray.getJSONObject(j);
                            comments.add(new Comment(commentObj.getString("comment"), commentObj.getString("date"),
                                    commentObj.has("url") ? commentObj.getString("url") : "",
                                    commentObj.has("content_type") ? commentObj.getString("content_type") : ""));
                        }
                    }

//                    JSONObject lOObj = obj.getJSONObject("lead_owner");
//                    LeadOwner leadOwner = new LeadOwner(lOObj.getString("id"),
//                            lOObj.getString("name"));

                    account = new AccountList(obj.has("account_id") ? obj.getString("account_id") : obj.getString("id"),
                            obj.getString("name"),
                            obj.getString("status"),
                            obj.getString("file_as"),
                            obj.getString("source"),
                            comments,
                            addresses,
                            emails,
                            phones,
                            obj.getString("name")
                    );
                    accountLists.add(account);
                }
                isMoreDataAvailable = accounts.length() > 0;
                onLoaded();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_lead, menu);
        SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView search = (SearchView) menu.findItem(R.id.lead_search).getActionView();
        search.setSearchableInfo(manager.getSearchableInfo(getComponentName()));
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                isOnSearch = true;
                no_account_records.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                search.setIconified(true);
                accountLists.clear();
                last_id = null;
                searchLeads(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        search.setOnCloseListener(() -> {
            isOnSearch = false;
            last_id = null;
            accountLists.clear();
            progressBar.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            prepareAccountData();
            return false;
        });
        return true;
    }

    @Override
    public void onRefresh() {
        accountLists.clear();
        last_id = null;
        mSwipeRefreshLayout.setRefreshing(true);
        paginationProgressBar.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        prepareAccountData();
    }
}