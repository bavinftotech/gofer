package com.gofercrm.user.gofercrm.chat.ui.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.chat.ui.main.model.Member;
import com.gofercrm.user.gofercrm.chat.ui.main.model.Room;

import java.util.ArrayList;
import java.util.List;

public class RoomDetailAdapter extends RecyclerView.Adapter<RoomDetailAdapter.ViewHolder> {

    private Room mRoom;
    private List<Member> mArrayList = new ArrayList<>();
    private Context mContext;

    public RoomDetailAdapter(Context context, Room room) {
        setHasStableIds(true);
        if (room != null && room.getMembers() != null && room.getMembers().size() > 0) {
            this.mArrayList = room.getMembers();
        }
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.content_room_detail, parent, false);
        return new RoomDetailAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        Member member = mArrayList.get(position);
        viewHolder.tvName.setText(member.getmName());
        viewHolder.tvEmail.setText(member.getEmail());
        viewHolder.userPhoto.setVisibility(View.GONE);
        if (member.getmImage() != null && !member.getmImage().isEmpty() && !member.getmImage().equals("null")) {
            viewHolder.userPhoto.setVisibility(View.VISIBLE);
            viewHolder.tvInitial.setVisibility(View.GONE);
            RequestOptions options = new RequestOptions()
                    .circleCropTransform();
            Glide.with(mContext).load(member.getmImage()).apply(options).into(viewHolder.userPhoto);
        } else if (member.getmName() != null && !member.getmName().isEmpty()) {
            viewHolder.tvInitial.setText(String.valueOf(mArrayList.get(position).getmName().charAt(0)));
            viewHolder.tvInitial.setVisibility(View.VISIBLE);
        }
        if (member.getRole() == 1) {
            viewHolder.isAdmin.setVisibility(View.VISIBLE);
        } else {
            viewHolder.isAdmin.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName;
        public ImageView userPhoto;
        public TextView tvInitial, tvEmail;
        public ImageView isAdmin;
        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            tvName = itemLayoutView.findViewById(R.id.tv_user_name);
            isAdmin = itemLayoutView.findViewById(R.id.img_member_admin);
            tvInitial = itemLayoutView.findViewById(R.id.user_Initial);
            userPhoto = itemLayoutView.findViewById(R.id.iv_user_photo);
            tvEmail = itemLayoutView.findViewById(R.id.tv_member_email);
        }
    }
}