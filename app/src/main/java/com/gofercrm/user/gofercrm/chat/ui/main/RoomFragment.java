package com.gofercrm.user.gofercrm.chat.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.LauncherActivity;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.chat.ui.main.model.Chat;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.gofercrm.user.gofercrm.websocket.SharedObserver;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RoomFragment extends Fragment implements ChatAdapter.ClickListener {
    private static final String TAG = "RoomFragment";
    private static final String ARG_SECTION_NUMBER = "section_number";
    private List<Chat> data = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private RelativeLayout rlIndicator;
    private ChatAdapter mAdapter;
    private TextView tv_selection;
    private View viewIndicator;
    private TextInputLayout tlSearch;
    private ProgressBar progressBar;

    //share intent
    private boolean isFromShare = true;

    public static RoomFragment newInstance(int sectionNumber) {
        RoomFragment fragment = new RoomFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        SharedObserver.getSocketObserver().subscribe(this::processTyping);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ccc, null, false);
        progressBar = view.findViewById(R.id.progressBar);
        rlIndicator = view.findViewById(R.id.rlIndicator);
        tv_selection = view.findViewById(R.id.tv_selection);
        viewIndicator = view.findViewById(R.id.viewIndicator);
        mRecyclerView = view.findViewById(R.id.recyclerView);
        tlSearch = view.findViewById(R.id.tlSearch);
        tlSearch.setVisibility(View.GONE);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mAdapter = new ChatAdapter(getContext(), data, this, 2);
        mRecyclerView.setAdapter(mAdapter);
        if (data.size() == 0) {
            getContacts();
        }
        return view;
    }

    private void onLoaded() {
        mAdapter = new ChatAdapter(getContext(), data, this, 2);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClicked(int position) {
        Intent intent = new Intent(getContext(), Conversation.class);
        if (ChatPagerAdapter.intent != null && ChatPagerAdapter.intent.getBooleanExtra(LauncherActivity.IS_FROM_SHARE, false)) {
            intent.putExtra(LauncherActivity.IS_FROM_SHARE, ChatPagerAdapter.intent.getBooleanExtra(LauncherActivity.IS_FROM_SHARE, false));
            intent.putExtra(LauncherActivity.SHARE_URI, ChatPagerAdapter.intent.getStringExtra(LauncherActivity.SHARE_URI));
        }
        String id = data.get(position).get_Id();
        intent.putExtra("DATA", intent.getExtras());
        intent.putExtra("_ID", (Serializable) data.get(position).get_Id());
        intent.putExtra("_TYPE", (Serializable) 2);
        intent.putExtra("_NAME", data.get(position).getName());
        intent.putExtra("_SMS", data.get(position).getVirtual_phone() != null ?
                data.get(position).getVirtual_phone() : "");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getContext().startActivity(intent);
        ChatPagerAdapter.intent = null;
    }

    @Override
    public boolean onItemLongClicked(int position) {
        toggleSelection(position);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    private void toggleSelection(int position) {
        mAdapter.toggleSelection(position);
        if (mAdapter.getSelectedItemCount() > 0) {
            tv_selection.setVisibility(View.VISIBLE);
        } else
            tv_selection.setVisibility(View.GONE);
        getActivity().runOnUiThread(() -> tv_selection.setText(
                getResources().getString(R.string.delete) + " (" + mAdapter.getSelectedItemCount() + ")"));
    }

    private void getContacts() {
        progressBar.setVisibility(View.VISIBLE);
        NetworkManager.customJsonObjectGetRequest(
                getContext(), Constants.FRIENDS_FETCH_URL+"?contact_type_filter=room", null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressBar.setVisibility(View.GONE);
                        processData(response);
                    }

                    @Override
                    public void onError(String message) {
                        progressBar.setVisibility(View.GONE);
                        LogUtils.Print("ERRROR=", message);
                    }
                }, true);
    }

    private void processData(JSONObject response) {
        try {
            //LogUtils.Print(TAG, "response -->" + response);
            if (Integer.parseInt(response.getString("result")) == 1) {
                data = new ArrayList<>();
                JSONArray users = response.getJSONArray("users");
                JSONObject result_obj = new JSONObject();
                for (int i = 0; i < users.length(); i++) {
                    result_obj = users.getJSONObject(i);
                    //LogUtils.Print("TAG", "postData -> " + result_obj.toString());
                    if (result_obj.has("contact_type")
                            && result_obj.getString("contact_type").equals("room")) {

                        Chat chat = new Chat();
                        if (result_obj.has("id")) {
                            chat.set_Id(result_obj.getString("id"));
                        }
                        if (result_obj.has("room_type")) {
                            chat.setRoom_type(result_obj.getString("room_type"));
                        }
                        if (result_obj.has("name")) {
                            String name = result_obj.getString("name");
                            chat.setName(name);
                        }
                        if (result_obj.has("virtual_phone")) {
                            String virtual_phone = result_obj.getString("virtual_phone");
                            chat.setVirtual_phone(virtual_phone);
                        }

                        chat.setImage("room_image");
                        if (result_obj.has("is_active")) {
                            chat.setOnline(result_obj.getBoolean("is_active"));
                        }
                        if (result_obj.has("members")) {
                            JSONArray members = result_obj.getJSONArray("members");
                            int members_count = members.length();
                            chat.setLastChat(members_count > 1 ? members_count + " members" : members_count + " member");
                        }
                        data.add(chat);
                    }
                }
                onLoaded();
                if (data.size() == 0)
                    openIndicatorView();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void openIndicatorView() {
        rlIndicator.setOnClickListener(v -> rlIndicator.setVisibility(View.GONE));
        rlIndicator.setVisibility(View.VISIBLE);
    }

    private void processTyping(String response) {
        try {
            JSONObject input = new JSONObject(response);
            if (input.has("type") && input.getString("type").equals("room_message")) {
                String room_id = input.has("room_id") ? input.getString("room_id") : "";
                for (Chat user : data) {
                    if (user.get_Id().equals(room_id)) {
                        user.setMsg_count(1);
                        if (getActivity() != null)
                            getActivity().runOnUiThread(() -> {
                                onLoaded();
                            });
                        break;
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}