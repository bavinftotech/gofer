package com.gofercrm.user.gofercrm.chat.ui.main;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.widget.PopupMenu;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.chat.ui.main.model.ChatData;
import com.gofercrm.user.gofercrm.profile.ProfileActivity;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.ProgressDialog;
import com.gofercrm.user.gofercrm.util.Util;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ThreadAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int DATE = 0, YOU = 1, ME = 2;
    // The items to display in your RecyclerView
    private List<ChatData> items;
    private Context mContext;

    // Provide a suitable constructor (depends on the kind of data set)
    public ThreadAdapter(Context context, List<ChatData> items) {
        this.mContext = context;
        this.items = items;
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return this.items.size();
    }

    @Override
    public int getItemViewType(int position) {
        //More to come
        switch (items.get(position).getType()) {
            case "0":
                return DATE;
            case "1":
                return YOU;
            case "2":
                return ME;
        }
        return -1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        if (viewType == DATE) {
            View v1 = inflater.inflate(R.layout.layout_holder_date, viewGroup, false);
            viewHolder = new HolderDate(v1);
        } else {
            View v2 = inflater.inflate(R.layout.layout_holder_thread, viewGroup, false);
            viewHolder = new HolderThread(v2);
        }
        return viewHolder;
    }

    public void addItem(List<ChatData> item) {
        items.addAll(item);
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder.getItemViewType() == DATE) {
            HolderDate vh1 = (HolderDate) viewHolder;
            configureViewHolder1(vh1, position);
        } else {
            HolderThread vh = (HolderThread) viewHolder;
            configureViewHolder3(vh, position);
        }
    }

    private void configureViewHolder3(final HolderThread vh1, int position) {
        final ChatData chatData = items.get(position);
        vh1.getName().setText(items.get(position).getSender_name());
        vh1.getTime().setText(items.get(position).getTime());
        vh1.getChatText().setText(items.get(position).getText());
        if (chatData.getContent_type() != null && chatData.getAtt_url() != null) {
            vh1.getRl_thread().setVisibility(View.VISIBLE);
            vh1.getTitle().setText(chatData.getAtt_name());
            vh1.getSize().setText(chatData.getAtt_file_size());

            if (chatData.getContent_type().contains("image")) {
                RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.file)
                        .error(R.drawable.file);
                Glide.with(mContext).load(chatData.getAtt_url()).apply(options).into(vh1.getThumbnail());

            } else {
                Glide.with(mContext).load(chatData.getThumbnail()).into(vh1.getThumbnail());
                changeImageColor(chatData.getContent_type(), vh1.getThumbnail());
            }
            vh1.getOverflow().setOnClickListener(view -> showPopupMenu(vh1.getOverflow(), chatData));

        }
        if (chatData.getUser() != null) {
            if (chatData.getUser().getImage() != null &&
                    !chatData.getUser().getImage().isEmpty() &&
                    !chatData.getUser().getImage().equals("null")) {
                vh1.getThread_child_photo().setVisibility(View.VISIBLE);
                vh1.getThread_child_initial().setVisibility(View.GONE);
                RequestOptions options = new RequestOptions()
                        .centerCrop();
                Glide.with(mContext).load(chatData.getUser().getImage()).apply(options).into(vh1.getThread_child_photo());
                vh1.getThread_child_photo().setOnClickListener(v -> goToProfile(chatData));

            } else {
                vh1.getThread_child_photo().setVisibility(View.GONE);
                vh1.getThread_child_initial().setVisibility(View.VISIBLE);
                vh1.getThread_child_initial().setText(chatData.getUser().getFirst_name().charAt(0) + "" + chatData.getUser().getLast_name().charAt(0));
                vh1.getThread_child_initial().setOnClickListener(v -> goToProfile(chatData));
            }
        }
    }

    private void configureViewHolder1(HolderDate vh1, int position) {
        vh1.getDate().setText(items.get(position).getText());
    }

    private void changeImageColor(String type, ImageView image) {
        if (type == null) {
            return;
        }
        if (type.contains("pdf")) {
            image.setColorFilter(ContextCompat.getColor(mContext, R.color.pdf));
        } else if (type.contains("msword")) {
            image.setColorFilter(ContextCompat.getColor(mContext, R.color.msword));
        } else if (type.contains("audio")) {
            image.setColorFilter(ContextCompat.getColor(mContext, R.color.colorPrimary));
        } else if (type.contains("video")) {
            image.setColorFilter(ContextCompat.getColor(mContext, R.color.colorAccent));
        } else if (type.contains("text")) {
            image.setColorFilter(ContextCompat.getColor(mContext, R.color.yellow));
        } else if (type.contains("csv")) {
            image.setColorFilter(ContextCompat.getColor(mContext, R.color.csv));
        } else if (type.contains("xml")) {
            image.setColorFilter(ContextCompat.getColor(mContext, R.color.xml));
        } else if (type.contains("excel")) {
            image.setColorFilter(ContextCompat.getColor(mContext, R.color.excel));
        } else {
            image.setColorFilter(ContextCompat.getColor(mContext, R.color.yellow));
        }
    }

    private void goToProfile(ChatData data) {
        Intent intent = new Intent(mContext, ProfileActivity.class);
        intent.putExtra("USER_ID", data.getUser().getId());
        intent.putExtra("NAME", data.getUser().getFirst_name() + "" + data.getUser().getLast_name());
        intent.putExtra("IMAGE", data.getUser().getImage());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

    private void showPopupMenu(View view, ChatData attachment) {
        // inflate menu
        PopupMenu popup = new PopupMenu(mContext, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.attachment_message_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new MyMenuItemClickListener(attachment));
        popup.show();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class DownloadFile extends AsyncTask<String, String, String> {

        //        private ProgressDialog progressDialog;
        private String fileName;
        private String folder;
        private boolean isDownloaded;

        DownloadFile(String fileName) {
            this.fileName = fileName;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            this.progressDialog = new ProgressDialog(mContext);
//            progressDialog.setCanceledOnTouchOutside(false);
//            this.progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//            this.progressDialog.setCancelable(false);
//            this.progressDialog.show();
            ProgressDialog.getInstance().show(mContext);
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection connection = url.openConnection();
                connection.connect();
                int lengthOfFile = connection.getContentLength();
                InputStream input = new BufferedInputStream(url.openStream(), 8192);
                String timestamp = new SimpleDateFormat("yyyy-MM-dd-HH-mm").format(new Date());
                fileName = timestamp + "_" + fileName;

                //Default Download directory
                folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();

                OutputStream output = new FileOutputStream(folder + "/" + fileName);

                byte[] data = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    publishProgress("" + (int) ((total * 100) / lengthOfFile));
                    output.write(data, 0, count);
                }
                output.flush();
                output.close();
                input.close();
                return mContext.getResources().getString(R.string.downloaded_at_) + " " + folder + "/" + fileName;
            } catch (Exception e) {
                e.printStackTrace();
                LogUtils.Print("Error: ", e.getMessage());
            }
            return mContext.getResources().getString(R.string.something_went_wrong);
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
//            progressDialog.setProgress(Integer.parseInt(progress[0]));
            ProgressDialog.getInstance().dismiss();
        }

        @Override
        protected void onPostExecute(String message) {
//            this.progressDialog.dismiss();
            ProgressDialog.getInstance().dismiss();
            Toast.makeText(mContext,
                    message, Toast.LENGTH_LONG).show();
        }
    }

    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {
        private static final int REQUEST_EXTERNAL_STORAGE = 1;
        ChatData attachment;
        private String[] PERMISSIONS_STORAGE = {
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        };

        public MyMenuItemClickListener(ChatData attachment) {
            this.attachment = attachment;
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            Activity activity = (Activity) mContext;
            switch (menuItem.getItemId()) {
                case R.id.action_download:
                    int permission = ActivityCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    if (permission != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(
                                activity,
                                PERMISSIONS_STORAGE,
                                REQUEST_EXTERNAL_STORAGE
                        );
                    } else {
                        new DownloadFile(attachment.getAtt_name()).execute(attachment.getAtt_url());
                    }
                    return true;
                case R.id.action_share: {
                    shareTextUrl(attachment.getAtt_name(), attachment.getAtt_url());
                    return true;
                }
                case R.id.action_copy_url: {
                    Util.setClipboard(mContext, attachment.getAtt_url());
                }
                default:
            }
            return false;
        }

        private void shareTextUrl(String name, String text) {
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("text/plain");
            share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
            share.putExtra(Intent.EXTRA_SUBJECT, mContext.getResources().getString(R.string.share_url));
            share.putExtra(Intent.EXTRA_TEXT, text);
            share.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(Intent.createChooser(share, mContext.getResources().getString(R.string.share_link_)));
        }
    }
}