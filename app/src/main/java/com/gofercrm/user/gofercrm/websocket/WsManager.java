package com.gofercrm.user.gofercrm.websocket;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;

import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.chat.ui.main.BulletEventListener;
import com.gofercrm.user.gofercrm.chat.ui.main.UserStatusChangeListener;
import com.gofercrm.user.gofercrm.chat.video.group.openvcall.model.ConstantApp;
import com.gofercrm.user.gofercrm.chat.video.single.InComingCallActivity;
import com.gofercrm.user.gofercrm.chat.video.single.VideoChatViewActivity;
import com.gofercrm.user.gofercrm.login.LoginActivity;
import com.gofercrm.user.gofercrm.tenantcomment.ToastActivity;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.websocket.listener.WsStatusListener;
import com.gofercrm.user.gofercrm.websocket.model.DataSocketMessage;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

public class WsManager implements IWsManager {
    private static final String TAG = "WsManager";
    private final static int RECONNECT_INTERVAL = 10 * 1000;
    private final static long RECONNECT_MAX_TIME = 180 * 1000;
    private static WeakReference<VideoChatViewActivity> mVideoChatViewActivityRef;
    private static UserStatusChangeListener listener;
    private static BulletEventListener bulletEventListener;
    private static BulletEventListener bulletHomeEventListener;
    private final Context mContext;
    private final String wsUrl;
    private WebSocket mWebSocket;
    private OkHttpClient mOkHttpClient;
    private Request mRequest;
    private int mCurrentStatus = WsStatus.DISCONNECTED;
    private final boolean isNeedReconnect;
    private boolean isManualClose = false;
    private WsStatusListener wsStatusListener;
    private final Lock mLock;
    private final Handler wsMainHandler = new Handler(Looper.getMainLooper());
    private int reconnectCount = 0;
    private final Runnable reconnectRunnable = new Runnable() {
        @Override
        public void run() {
            if (wsStatusListener != null) {
                wsStatusListener.onReconnect();
            }
            buildConnect();
        }
    };
    private final WebSocketListener mWebSocketListener = new WebSocketListener() {
        @Override
        public void onOpen(WebSocket webSocket, final Response response) {
            mWebSocket = webSocket;
            setCurrentStatus(WsStatus.CONNECTED);
            connected();
            if (wsStatusListener != null) {
                if (Looper.myLooper() != Looper.getMainLooper()) {
                    wsMainHandler.post(() -> wsStatusListener.onOpen(response));
                } else {
                    wsStatusListener.onOpen(response);
                }
            }
        }

        @Override
        public void onMessage(WebSocket webSocket, final ByteString bytes) {
            if (wsStatusListener != null) {
                if (Looper.myLooper() != Looper.getMainLooper()) {
                    wsMainHandler.post(() -> wsStatusListener.onMessage(bytes));
                } else {
                    wsStatusListener.onMessage(bytes);
                }
            }
        }

        @Override
        public void onMessage(WebSocket webSocket, final String text) {//fix the overloading of message attribute in JSON
            //{"type": "video_voip", "payload": {}}
            LogUtils.Print(TAG, "WsManager onMessage --> " + text);
            try {
                //check "type=message" socket event and handle it before other events.
                JSONObject jsonObject = new JSONObject(text);
                if (jsonObject.has("type") && jsonObject.getString("type").equals("message")) {
                    if (wsStatusListener != null) {
                        if (Looper.myLooper() != Looper.getMainLooper()) {
                            wsMainHandler.post(() -> wsStatusListener.onMessage(text));
                        } else {
                            wsStatusListener.onMessage(text);
                        }
                    }
                    return;
                }
                if (jsonObject.has("type") && jsonObject.getString("type").equals("weather")) {
                    if (jsonObject.has("data")) {
                        JSONObject jsonData = jsonObject.getJSONObject("data");
                        if (jsonData.has("locations")) {
                            JSONObject jsonLocation = jsonData.getJSONObject("locations");
                            Iterator iterator = jsonLocation.keys();
                            while (iterator.hasNext()) {
                                String keyLocation = (String) iterator.next();
                                LogUtils.Print(TAG, "KEY -> " + keyLocation);
                                if (jsonLocation.has(keyLocation)) {
                                    SharedPreferences preferences = mContext.getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                                    JSONObject dataWeather = jsonLocation.getJSONObject(keyLocation);
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.putString("weather_name", dataWeather.getString("name"));
                                    if (dataWeather.has("values")) {
                                        editor.putString("weather", dataWeather.getString("values"));
                                        editor.apply();
                                    }
                                    if (bulletHomeEventListener != null) {
                                        bulletHomeEventListener.onBulletEventReceived(3, "");
                                    }
                                }
                            }
                        }
                    }
                    return;
                }
                DataSocketMessage message = new GsonBuilder().create().fromJson(text, DataSocketMessage.class);
                if (message != null && message.getType() != null) {
                    switch (message.getType()) {
                        case "error":
                            if (message.getErrorCode() != null &&
                                    message.getErrorCode().equals("invalid_client_connection_id")) {
                                SharedPreferences preferences = mContext.getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.clear();
                                editor.apply();
                                Intent intent = new Intent(mContext, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                mContext.startActivity(intent);
                                return;
                            }
                            break;
                        case "video_voip":
                            if (message.getPayload() != null &&
                                    message.getPayload().getChannelName() != null &&
                                    !message.getPayload().getChannelName().equals("")) {
                                if (message.getPayload().getCallType().equals("personal")) {
                                    Intent intent = new Intent(mContext, InComingCallActivity.class);
                                    intent.putExtra("_NAME", message.getPayload().getName());
                                    intent.putExtra("_USER_ID", "");
                                    intent.putExtra("_OPO_USER_ID", "");
                                    intent.putExtra("_CHANNEL_NAME", message.getPayload().getChannelName());
                                    intent.putExtra("_FROM", 0);
                                    intent.putExtra("_CALL_TYPE", InComingCallActivity.fromPersonal);
                                    intent.putExtra("_IMAGE", "");
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    mContext.startActivity(intent);
                                } else {
                                    Intent intent = new Intent(mContext, InComingCallActivity.class);
                                    intent.putExtra("_NAME", message.getPayload().getName());
                                    intent.putExtra("_USER_ID", "");
                                    intent.putExtra("_OPO_USER_ID", "");
                                    intent.putExtra("_FROM", 0);
                                    intent.putExtra("_CALL_TYPE", InComingCallActivity.fromGroup);
                                    intent.putExtra("_IMAGE", "");
                                    intent.putExtra(ConstantApp.ACTION_KEY_CHANNEL_NAME, message.getPayload().getChannelName());
                                    intent.putExtra(ConstantApp.ACTION_KEY_ENCRYPTION_KEY, "xdL_encr_key_");
                                    intent.putExtra(ConstantApp.ACTION_KEY_ENCRYPTION_MODE, "AES-256-XTS");
                                    mContext.startActivity(intent);
                                }
                            }
                            break;
                        case "video_hangup":
                            LogUtils.Print("========ATTEMPT DESTROY PERSONAL VIDEO CALL INTENT", "Personal Call Hangup");
                            if (message.getPayload() != null &&
                                    message.getPayload().getChannelName() != null &&
                                    !message.getPayload().getChannelName().equals("")) {
                                if (message.getPayload().getCallType().equals("personal")) {
                                    //If you received an incoming call and the other party ended, destroy InComingCallActivity
                                    if (InComingCallActivity.activity != null) {
                                        InComingCallActivity.destroyActivity();
                                    }

                                    //===If you initiated the call and the other party ended/rejected call, destroy the VideoChatViewActivity
                                    if (mVideoChatViewActivityRef != null) {
                                        mVideoChatViewActivityRef.get().finish();
                                    }
                                    LogUtils.Print("========DESTROY PERSONAL VIDEO CALL INTENT", "Personal Call Hangup");
                                } else {
                                    if (message.getSenderName() != null &&
                                            !message.getSenderName().equals("")) {
                                        Handler mainHandler = new Handler(mContext.getMainLooper());
                                        Runnable myRunnable = () -> Util.makeToast("User " + message.getSenderName() + " has left the Video Call");
                                        mainHandler.post(myRunnable);
                                    }
                                    LogUtils.Print("========NOTIFY GROUP VIDEO CALL DEPARTURE", "Someone left the group");
                                }
                            }
                            break;
                        case "user_status_busy":
                        case "user_status_available":
                            if (message.getMessage() != null &&
                                    message.getMessage().getData() != null &&
                                    message.getMessage().getData().getId() != null &&
                                    !message.getMessage().getData().getId().equals("")) {
                                if (listener != null) {
                                    listener.onUserStatusChanges(
                                            message.getType().equals("user_status_available") ? 1 : 0,
                                            message.getMessage().getData().getId());
                                }
                            }
                            break;
                        case "BULLETIN_COMMENT":
                            if (bulletHomeEventListener != null) {
                                bulletHomeEventListener.onBulletEventReceived(1, "");
                            }
                            if (bulletEventListener != null) {
                                bulletEventListener.onBulletEventReceived(1, "");
                            } else {
                                Intent intent = new Intent(mContext, ToastActivity.class);
                                intent.putExtra("data", mContext.getResources().getString(R.string.new_bulletin_broadcast));
                                intent.putExtra("from", ToastActivity.forBulletinBroadCast);
                                mContext.startActivity(intent);
                            }
                            break;
                        case "incoming_call":
                        case "check_presence_status":
                            if (bulletHomeEventListener != null) {
                                bulletHomeEventListener.onBulletEventReceived(2, text);
                            }
                            break;
                        case "contact_req_received":
                        case "contact_req_accepted":
                        case "contact_req_rejected":
                            if (listener != null) {
                                listener.onUpdateHappen();
                            }
                            break;
                        case "user_translation_added ":
                        case "user_translation_removed ":
                            LogUtils.Print(TAG, "== TRANSLATION UPDATED ==");
                            break;
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (wsStatusListener != null) {
                if (Looper.myLooper() != Looper.getMainLooper()) {
                    wsMainHandler.post(() -> wsStatusListener.onMessage(text));
                } else {
                    wsStatusListener.onMessage(text);
                }
            }
        }

        @Override
        public void onClosing(WebSocket webSocket, final int code, final String reason) {
            if (wsStatusListener != null) {
                if (Looper.myLooper() != Looper.getMainLooper()) {
                    wsMainHandler.post(() -> wsStatusListener.onClosing(code, reason));
                } else {
                    wsStatusListener.onClosing(code, reason);
                }
            }
        }

        @Override
        public void onClosed(WebSocket webSocket, final int code, final String reason) {
            if (wsStatusListener != null) {
                if (Looper.myLooper() != Looper.getMainLooper()) {
                    wsMainHandler.post(() -> wsStatusListener.onClosed(code, reason));
                } else {
                    wsStatusListener.onClosed(code, reason);
                }
            }
        }

        @Override
        public void onFailure(WebSocket webSocket, final Throwable t, final Response response) {
            LogUtils.Print(TAG, t.getMessage());
            tryReconnect();
            if (wsStatusListener != null) {
                if (Looper.myLooper() != Looper.getMainLooper()) {
                    wsMainHandler.post(() -> wsStatusListener.onFailure(t, response));
                } else {
                    wsStatusListener.onFailure(t, response);
                }
            }
        }
    };

    public WsManager(Builder builder) {
        mContext = builder.mContext;
        wsUrl = builder.wsUrl;
        isNeedReconnect = builder.needReconnect;
        mOkHttpClient = builder.mOkHttpClient;
        this.mLock = new ReentrantLock();
    }

    public static void updateActivity(VideoChatViewActivity activity) {
        mVideoChatViewActivityRef = new WeakReference<>(activity);
        //System.out.println("%%%%%%%%%%%%%%%%%%%%%%%% mVideoChatViewActivityRef set!");
    }

    public static void setOnUserStatusChangeListener(UserStatusChangeListener changeListener) {
        listener = changeListener;
    }

    public static void setOnBulletEventListener(BulletEventListener listener) {
        bulletEventListener = listener;
    }

    public static void setOnBulletHomeEventListener(BulletEventListener listener) {
        bulletHomeEventListener = listener;
    }

    private void initWebSocket() {
        if (mOkHttpClient == null) {
            mOkHttpClient = new OkHttpClient.Builder()
                    .retryOnConnectionFailure(true)
                    .build();
        }
        if (mRequest == null) {
            mRequest = new Request.Builder()
                    .url(wsUrl)
                    .build();
        }
        mOkHttpClient.dispatcher().cancelAll();
        try {
            mLock.lockInterruptibly();
            try {
                mOkHttpClient.newWebSocket(mRequest, mWebSocketListener);
            } finally {
                mLock.unlock();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public WebSocket getWebSocket() {
        return mWebSocket;
    }

    public void setWsStatusListener(WsStatusListener wsStatusListener) {
        this.wsStatusListener = wsStatusListener;
    }

    @Override
    public synchronized boolean isWsConnected() {
        return mCurrentStatus == WsStatus.CONNECTED;
    }

    @Override
    public synchronized int getCurrentStatus() {
        return mCurrentStatus;
    }

    @Override
    public synchronized void setCurrentStatus(int currentStatus) {
        this.mCurrentStatus = currentStatus;
    }

    @Override
    public void startConnect() {
        isManualClose = false;
        buildConnect();
    }

    @Override
    public void stopConnect() {
        isManualClose = true;
        disconnect();
    }

    private void tryReconnect() {
        if (!isNeedReconnect | isManualClose) {
            return;
        }
        if (!isNetworkConnected(mContext)) {
            setCurrentStatus(WsStatus.DISCONNECTED);
            return;
        }
        setCurrentStatus(WsStatus.RECONNECT);
        long delay = reconnectCount * RECONNECT_INTERVAL;
        wsMainHandler
                .postDelayed(reconnectRunnable, delay > RECONNECT_MAX_TIME ? RECONNECT_MAX_TIME : delay);
        reconnectCount++;
    }

    private void cancelReconnect() {
        wsMainHandler.removeCallbacks(reconnectRunnable);
        reconnectCount = 0;
    }

    private void connected() {
        cancelReconnect();
    }

    private void disconnect() {
        if (mCurrentStatus == WsStatus.DISCONNECTED) {
            return;
        }
        cancelReconnect();
        if (mOkHttpClient != null) {
            mOkHttpClient.dispatcher().cancelAll();
        }
        if (mWebSocket != null) {
            boolean isClosed = mWebSocket.close(WsStatus.CODE.NORMAL_CLOSE, WsStatus.TIP.NORMAL_CLOSE);
            if (!isClosed) {
                if (wsStatusListener != null) {
                    wsStatusListener.onClosed(WsStatus.CODE.ABNORMAL_CLOSE, WsStatus.TIP.ABNORMAL_CLOSE);
                }
            }
        }
        setCurrentStatus(WsStatus.DISCONNECTED);
    }

    private synchronized void buildConnect() {
        if (!isNetworkConnected(mContext)) {
            setCurrentStatus(WsStatus.DISCONNECTED);
            return;
        }
        switch (getCurrentStatus()) {
            case WsStatus.CONNECTED:
            case WsStatus.CONNECTING:
                break;
            default:
                setCurrentStatus(WsStatus.CONNECTING);
                initWebSocket();
        }
    }

    @Override
    public boolean sendMessage(String msg) {
        return send(msg);
    }

    @Override
    public boolean sendMessage(ByteString byteString) {
        return send(byteString);
    }

    private boolean send(Object msg) {
        boolean isSend = false;
        if (mWebSocket != null && mCurrentStatus == WsStatus.CONNECTED) {
            if (msg instanceof String) {
                isSend = mWebSocket.send((String) msg);
            } else if (msg instanceof ByteString) {
                isSend = mWebSocket.send((ByteString) msg);
            }
            if (!isSend) {
                tryReconnect();
            }
        }
        return isSend;
    }

    private boolean isNetworkConnected(Context context) {
        if (context != null) {
            ConnectivityManager mConnectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetworkInfo = mConnectivityManager
                    .getActiveNetworkInfo();
            if (mNetworkInfo != null) {
                return mNetworkInfo.isAvailable();
            }
        }
        return false;
    }

    public static final class Builder {
        private final Context mContext;
        private String wsUrl;
        private boolean needReconnect = true;
        private OkHttpClient mOkHttpClient;

        public Builder(Context val) {
            mContext = val;
        }

        public Builder wsUrl(String val) {
            wsUrl = val;
            return this;
        }

        public Builder client(OkHttpClient val) {
            mOkHttpClient = val;
            return this;
        }

        public Builder needReconnect(boolean val) {
            needReconnect = val;
            return this;
        }

        public WsManager build() {
            return new WsManager(this);
        }
    }
}