package com.gofercrm.user.gofercrm;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.core.app.NotificationCompat;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.gofercrm.user.gofercrm.account.AccountViewActivity;
import com.gofercrm.user.gofercrm.appointments.AppointmentsActivity;
import com.gofercrm.user.gofercrm.chat.ui.main.Conversation;
import com.gofercrm.user.gofercrm.chat.video.group.openvcall.model.ConstantApp;
import com.gofercrm.user.gofercrm.chat.video.single.InComingCallActivity;
import com.gofercrm.user.gofercrm.clients.ClientViewActivity;
import com.gofercrm.user.gofercrm.lead.LeadViewActivity;
import com.gofercrm.user.gofercrm.login.LoginActivity;
import com.gofercrm.user.gofercrm.opportunities.OpportunityViewActivity;
import com.gofercrm.user.gofercrm.task.NewTaskActivity;
import com.gofercrm.user.gofercrm.util.DeviceInfo;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.SharedPreferenceData;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleySingleton;
import com.gofercrm.user.gofercrm.websocket.WSListener;
import com.gofercrm.user.gofercrm.websocket.WSSingleton;
import com.gofercrm.user.gofercrm.websocket.WsManager;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class LauncherActivity extends BaseActivity implements WSListener {
    private static final String TAG = "Launcher";
    static SharedPreferences sharedPref;
    static String token = "";

    //share intent
    private boolean isFromShare = false;
    private Uri receiveUri = Uri.parse("");
    public static String IS_FROM_SHARE = "isf";
    public static String IS_FROM_N100 = "ifn100";
    public static String SHARE_URI = "su";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        clearNotification();
        setContentView(R.layout.activity_launcher);
        getDataFromIntent();
        isTokenValid(getIntent());
    }

    private void getDataFromIntent() {
        Intent receivedIntent = getIntent();
        String receivedAction = receivedIntent.getAction();
        String receivedType = receivedIntent.getType();
        LogUtils.Print(TAG, "receivedIntent --> " + receivedIntent);
        LogUtils.Print(TAG, "receivedAction --> " + receivedAction);
        LogUtils.Print(TAG, "receivedType --> " + receivedType);
        if (receivedAction != null && receivedAction.equals(Intent.ACTION_SEND)) {
            isFromShare = true;
            if (receivedType != null && receivedType.startsWith("image/")) {
                receiveUri = (Uri) receivedIntent
                        .getParcelableExtra(Intent.EXTRA_STREAM);
            } else if (receivedType != null && receivedType.startsWith("video/")) {
                receiveUri = (Uri) receivedIntent
                        .getParcelableExtra(Intent.EXTRA_STREAM);
            } else if (receivedType != null && receivedType.startsWith("application/")) {
                receiveUri = (Uri) receivedIntent
                        .getParcelableExtra(Intent.EXTRA_STREAM);
            }
            setIntent(null);
        }
        LogUtils.Print(TAG, "receiveUri --> " + receiveUri);
    }

    private void processIntent(Intent intent) {
        if (intent != null && intent.getExtras() != null) {
            Bundle extras = intent.getExtras();
            JSONObject json = new JSONObject();
            Set<String> keys = extras.keySet();
            for (String key : keys) {
                try {
                    json.put(key, JSONObject.wrap(extras.get(key)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (json.has("message")) {
                try {
                    String notificationData = json.getString("message");
                    showDataNotification(new JSONObject(notificationData));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Intent navigate = new Intent(this, NavigationActivity.class);
                if (isFromShare) {
                    navigate.putExtra(IS_FROM_SHARE, isFromShare);
                    navigate.putExtra(SHARE_URI, receiveUri.toString());
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }
                navigate.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(navigate);
            }
        }
    }

    private void isTokenValid(Intent intent) {
        sharedPref = getApplicationContext().getSharedPreferences("LOGIN", MODE_PRIVATE);
        token = sharedPref.getString("USER_TOKEN", "");
        if (token != null && !token.isEmpty()) {
            StringRequest request = new StringRequest(Request.Method.POST,
                    Constants.USR_BY_TOKEN_URL,
                    response -> processResponse(response, intent), e -> e.printStackTrace()) {
                @Override
                public Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("token", token);
                    return params;
                }
            };
            RequestQueue queue = VolleySingleton.getInstance(getApplicationContext()).getRequestQueue();
            queue.add(request);
        } else {
            navigate(false, intent);
        }
    }

    private void navigate(boolean isLoggedIn, Intent intent) {
        Intent navigate;
        if (isLoggedIn && (intent == null || intent.getExtras() == null)) {
            WsManager ws = WSSingleton.getInstanceWM(getApplicationContext());
            ws.startConnect();
            navigate = new Intent(this, NavigationActivity.class);
            if (isFromShare) {
                navigate.putExtra(IS_FROM_SHARE, true);
                navigate.putExtra(SHARE_URI, receiveUri.toString());
            }
            navigate.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(navigate);
        } else if (isLoggedIn && intent.getExtras() != null) {
            WsManager ws = WSSingleton.getInstanceWM(getApplicationContext());
            ws.startConnect();
            processIntent(intent);
        } else {
            navigate = new Intent(this, LoginActivity.class);
            navigate.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(navigate);
        }
    }

    private void processResponse(String response, Intent intent) {
        JSONObject object;
        try {
            object = new JSONObject(response);
            if (object.has("result") && Integer.parseInt(object.getString("result")) == 1) {
                navigate(true, intent);
                getFCMToken();
            } else {
                navigate(false, intent);
            }
        } catch (
                JSONException e) {
            e.printStackTrace();
        }
    }

    private void getFCMToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        LogUtils.Print(TAG, "getInstanceId failed" + task.getException());
                        return;
                    }
                    String token = task.getResult().getToken();
                    SharedPreferenceData.getInstance(LauncherActivity.this).setFCMToken(token);
                    updateProfile(token);
                    String msg = getString(R.string.msg_token_fmt, token);
                    LogUtils.Print(TAG, msg);
                });
    }

    private void updateProfile(String fcm_token) {
        JSONObject postData = new JSONObject();
        String deviceId = DeviceInfo.getDeviceID(getApplicationContext());
        String req = "{\"" + deviceId + "\":\"" + fcm_token + "\"}";
        String connection_id = SharedPreferenceData.getInstance(getApplicationContext()).getClientConnectionID();
        try {
            postData.put("push_notification_dict", req);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        NetworkManager.customJsonObjectRequest(
                LauncherActivity.this, Constants.UPDATE_PROFILE + "?push_notification_dict=" + req + "&os=android&connection_id=" + connection_id, null,
                new VolleyResponseListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtils.Print("RESPONSE", response.toString());
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("UPDATE PROFILE ERROR", message);
                    }

                }, false);
    }

    @Override
    public void onResult(String message) {

    }

    private void clearNotification() {
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.cancelAll();
    }

    private void showDataNotification(JSONObject notificationData) {
        String type = "", body = "", title = "", id = "", sender_id = "", sender_name = "", sender_image = "", room_id = "", room_name = "", msg_text = "";
        String sub_type = "", avatar_url = "", channel_name = "", call_type = "";
        Intent intent;
        JSONObject json;
        NotificationCompat.Builder mBuilder;
        if (notificationData != null) {
            try {
                json = notificationData;
                if (json.has("data")) {
                    JSONObject data = json.getJSONObject("data");
                    if (data.has("type")) {
                        type = data.getString("type");
                    }
                    if (data.has("channel_name")) {
                        channel_name = data.getString("channel_name");
                    }
                    if (data.has("call_type")) {
                        call_type = data.getString("call_type");
                    }
                    if (data.has("sub_type")) {
                        sub_type = data.getString("sub_type");
                    }
                    if (data.has("body")) {
                        body = data.getString("body");
                    }
                    if (data.has("title")) {
                        title = data.getString("title");
                    }
                    if (data.has("id")) {
                        id = data.getString("id");
                    }
                    if (data.has("sender_id")) {
                        sender_id = data.getString("sender_id");
                    }
                    if (data.has("room_id")) {
                        room_id = data.getString("room_id");
                    }
                    if (data.has("sender_nickname")) {
                        sender_name = data.getString("sender_nickname");
                    }
                    if (data.has("room_name")) {
                        room_name = data.getString("room_name");
                    }
                    if (data.has("avatar_url")) {
                        sender_image = data.getString("avatar_url");
                    }
                    if (data.has("message")) {
                        msg_text = data.getString("message");
                    }
                    if (data.has("avatar_url")) {
                        avatar_url = data.getString("avatar_url");
                    }
                    if (type.equals("N001")) {
                        intent = new Intent(this, LeadViewActivity.class);
                        intent.putExtra("LEAD_ID", id);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    } else if (type.equals("N002")) {
                        intent = new Intent(this, ClientViewActivity.class);
                        intent.putExtra("CLIENT_ID", id);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    } else if (type.equals("N003")) {
                        intent = new Intent(this, NewTaskActivity.class);
                        intent.putExtra("TASK_ID", id);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    } else if (type.equals("N004")) {
                        intent = new Intent(this, OpportunityViewActivity.class);
                        intent.putExtra("OPPOR_ID", id);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    } else if (type.equals("N005")) {
                        intent = new Intent(this, AccountViewActivity.class);
                        intent.putExtra("ACCOUNT_ID", id);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    } else if (type.equals("N006")) {
                        intent = new Intent(this, AppointmentsActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    } else if (type.equals("N007")) {
                        intent = new Intent(this, AppointmentsActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    } else if (type.equals("N010") && !sender_id.isEmpty()) {
                        intent = new Intent(getApplicationContext(), Conversation.class);
                        intent.putExtra("_ID", sender_id);
                        intent.putExtra("_TYPE", (Serializable) 1);
                        intent.putExtra("_NAME", sender_name);
                        intent.putExtra("_IMAGE", sender_image);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                    } else if (type.equals("N100") && !sender_id.isEmpty()) {
                        intent = new Intent(this, NavigationActivity.class);
                        intent.putExtra(IS_FROM_N100, true);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    } else if (type.equals("N009") && !room_id.isEmpty()) {
                        intent = new Intent(getApplicationContext(), Conversation.class);
                        intent.putExtra("_ID", room_id);
                        intent.putExtra("_TYPE", (Serializable) 2);
                        intent.putExtra("_NAME", room_name);
                        intent.putExtra("_IMAGE", sender_image);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    } else if (type.equals("voip_message") || type.equals("video_voip")) {
                        sharedPref = getApplicationContext().getSharedPreferences("LOGIN", MODE_PRIVATE);
                        sharedPref.getString("USER_NAME", "");
                        if (sender_name != null &&
                                channel_name != null &&
                                !channel_name.equals("")) {
                            if (call_type.equals("personal")) {
                                intent = new Intent(getApplicationContext(), InComingCallActivity.class);
                                intent.putExtra("_NAME", sender_name);
                                intent.putExtra("_LOGIN_USER_NAME", sharedPref.getString("USER_NAME", ""));
                                intent.putExtra("_USER_ID", "");
                                intent.putExtra("_OPO_USER_ID", sender_id);
                                intent.putExtra("_CHANNEL_NAME", channel_name);
                                intent.putExtra("_FROM", 0);
                                intent.putExtra("_CALL_TYPE", InComingCallActivity.fromPersonal);
                                intent.putExtra("_IMAGE", avatar_url);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                getApplicationContext().startActivity(intent);
                            } else {
                                intent = new Intent(getApplicationContext(), InComingCallActivity.class);
                                intent.putExtra("_NAME", sender_name);
                                intent.putExtra("_LOGIN_USER_NAME", sharedPref.getString("USER_NAME", ""));
                                intent.putExtra("_USER_ID", "");
                                intent.putExtra("_OPO_USER_ID", sender_id);
                                intent.putExtra("_CHANNEL_NAME", channel_name);
                                intent.putExtra("_FROM", 0);
                                intent.putExtra("_CALL_TYPE", InComingCallActivity.fromGroup);
                                intent.putExtra("_IMAGE", avatar_url);
                                intent.putExtra(ConstantApp.ACTION_KEY_CHANNEL_NAME, channel_name);
                                intent.putExtra(ConstantApp.ACTION_KEY_ENCRYPTION_KEY, "xdL_encr_key_");
                                intent.putExtra(ConstantApp.ACTION_KEY_ENCRYPTION_MODE, "AES-256-XTS");
                                getApplicationContext().startActivity(intent);
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}