package com.gofercrm.user.gofercrm.appointments;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gofercrm.user.gofercrm.R;
import com.tokenautocomplete.TokenCompleteTextView;

public class ContactsCompletionView extends TokenCompleteTextView<Contact> {
    public ContactsCompletionView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected View getViewForObject(Contact contact) {
        LayoutInflater l = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        TextView view = (TextView) l.inflate(R.layout.contact_token, (ViewGroup) getParent(), false);
        view.setText(contact.getName());
        return view;
    }

    @Override
    protected Contact defaultObject(String completionText) {
        int index = completionText.indexOf('@');
        if (index == -1) {
            return new Contact(completionText,completionText, completionText.replace(" ", "") + "@example.com","");
        } else {
            return new Contact(completionText,completionText.substring(0, index), completionText,"");
        }
    }
}