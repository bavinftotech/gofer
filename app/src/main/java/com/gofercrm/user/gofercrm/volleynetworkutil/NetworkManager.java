package com.gofercrm.user.gofercrm.volleynetworkutil;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.annotation.Nullable;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.gofercrm.user.gofercrm.login.LoginActivity;
import com.gofercrm.user.gofercrm.util.LogUtils;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class NetworkManager {
    private static SharedPreferences sharedPref;
    private static String token;

    public static void customJsonObjectRequest(Context context, String url, @Nullable JSONObject jsonRequest,
                                               final VolleyResponseListener listener, final boolean isJSON) {

        sharedPref = context.getSharedPreferences("LOGIN", MODE_PRIVATE);
        token = sharedPref.getString("USER_TOKEN", "");
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, url, jsonRequest, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        listener.onResponse(response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //if (error instanceof TimeoutError || error instanceof NoConnectionError)
                        if (error instanceof AuthFailureError) {
                            SharedPreferences preferences = context.getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.clear();
                            editor.apply();
                            Intent intent = new Intent(context, LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);
                            return;
                        }
                        listener.onError(error.toString());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                LogUtils.Print("TAG", "token -> " + "Bearer " + token);
                headers.put("Authorization", "Bearer " + token);
                if (isJSON) {
                    headers.put("Content-Type", "application/json; charset=utf-8");
                } else {
                    headers.put("Content-Type", "application/x-www-form-urlencoded");
                }
                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(9000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    public static void customJsonObjectRequest(Context context, String url, @Nullable JSONObject jsonRequest,
                                               final VolleyResponseListener listener,
                                               final boolean isJSON, String token) {

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, url, jsonRequest,
                        listener::onResponse,
                        error -> {
                            //if (error instanceof TimeoutError || error instanceof NoConnectionError)
                            if (error instanceof AuthFailureError) {
                                SharedPreferences preferences = context.getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.clear();
                                editor.apply();
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                context.startActivity(intent);
                                return;
                            }
                            listener.onError(error.toString());
                        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers;
                headers = new HashMap<>();
                LogUtils.Print("TAG", "token -> " + "Bearer " + token);
                headers.put("Authorization", "Bearer " + token);
                if (isJSON) {
                    headers.put("Content-Type", "application/json; charset=utf-8");
                } else {
                    headers.put("Content-Type", "application/x-www-form-urlencoded");
                }
                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(9000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    public static void customJsonObjectGetRequest(Context context, String url, @Nullable JSONObject jsonRequest,
                                                  final VolleyResponseListener listener, final boolean isJSON) {
        sharedPref = context.getSharedPreferences("LOGIN", MODE_PRIVATE);
        token = sharedPref.getString("USER_TOKEN", "");
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, jsonRequest, listener::onResponse, error -> {
                    if (error instanceof AuthFailureError) {
                        SharedPreferences preferences = context.getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.clear();
                        editor.apply();
                        Intent intent = new Intent(context, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                        return;
                    }
                    listener.onError(error.toString());
                }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers;
                headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + token);
                if (isJSON) {
                    headers.put("Content-Type", "application/json; charset=utf-8");
                } else {
                    headers.put("Content-Type", "application/x-www-form-urlencoded");
                }
                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(6000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    public static void tokenRequest(Context context, String url, @Nullable JSONObject jsonRequest,
                                    final VolleyResponseListener listener) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, url, jsonRequest, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        listener.onResponse(response);

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof AuthFailureError) {
                            SharedPreferences preferences = context.getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.clear();
                            editor.apply();
                            Intent intent = new Intent(context, LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);
                            return;
                        }
                        listener.onError(error.toString());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };
        VolleySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }
}