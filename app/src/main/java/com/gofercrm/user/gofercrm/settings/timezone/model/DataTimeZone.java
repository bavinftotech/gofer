package com.gofercrm.user.gofercrm.settings.timezone.model;

public class DataTimeZone {
    private String timeZone;
    private boolean select;

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }
}