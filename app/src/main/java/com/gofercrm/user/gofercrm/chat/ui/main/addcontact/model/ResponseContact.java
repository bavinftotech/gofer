package com.gofercrm.user.gofercrm.chat.ui.main.addcontact.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ResponseContact{

	@SerializedName("result")
	private int result;

	@SerializedName("users")
	private List<DataContact> users;

	public void setResult(int result){
		this.result = result;
	}

	public int getResult(){
		return result;
	}

	public void setUsers(List<DataContact> users){
		this.users = users;
	}

	public List<DataContact> getUsers(){
		return users;
	}
}