package com.gofercrm.user.gofercrm.settings;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;

import com.gofercrm.user.gofercrm.ApplicationContext;
import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.settings.deleteaccount.DeleteAccountActivity;
import com.gofercrm.user.gofercrm.settings.language.LanguageActivity;
import com.gofercrm.user.gofercrm.settings.privacy.PrivacyActivity;
import com.gofercrm.user.gofercrm.settings.timezone.TimeZoneActivity;
import com.gofercrm.user.gofercrm.util.DialogUtils;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.SharedPreferenceData;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SettingActivity extends BaseActivity implements View.OnClickListener {

    String DEFAULT_CALENDAR_ID;
    TextView calendar_default_value, tvLanguage, tvTimeZone;
    LinearLayout llLanguage, llTimeZone, llDeleteAccount, llPrivacy;
    CalendarBSFragment calendarBSFragment;
    ImageView setting_calendar_refresh_img_right;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.settings));
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        findViewById();
        setData();

        setting_calendar_refresh_img_right.setOnClickListener(view ->
                new AlertDialog.Builder(SettingActivity.this)
                        .setTitle(getResources().getString(R.string.refresh))
                        .setMessage(getResources().getString(R.string.refresh_confirmation))
                        .setIcon(R.drawable.refresh)
                        .setPositiveButton(getResources().getString(R.string.yes), (dialog, whichButton) -> refreshCalendars())
                        .setNegativeButton(getResources().getString(R.string.no), null).show());

        LinearLayout default_calendar = findViewById(R.id.set_dafault_calendar_layout);
        default_calendar.setOnClickListener(view -> {
            FragmentManager fm = getSupportFragmentManager();
            calendarBSFragment = new CalendarBSFragment();
            calendarBSFragment.setCurrentObj(calendarBSFragment);
            calendarBSFragment.show(fm, calendarBSFragment.getTag());
        });

        getCalendars();
    }

    private void findViewById() {
        setting_calendar_refresh_img_right = findViewById(R.id.setting_calendar_refresh_img_right);
        calendar_default_value = findViewById(R.id.calendar_default_value_id);
        llTimeZone = findViewById(R.id.llTimeZone);
        tvTimeZone = findViewById(R.id.tvTimeZone);
        llLanguage = findViewById(R.id.llLanguage);
        tvLanguage = findViewById(R.id.tvLanguage);
        llDeleteAccount = findViewById(R.id.llDeleteAccount);
        llPrivacy = findViewById(R.id.llPrivacy);
    }

    private void setData() {
        tvTimeZone.setText(ApplicationContext.preferences.getString("_TIMEZONE"));
        setLanguage();
        llLanguage.setOnClickListener(this);
        llTimeZone.setOnClickListener(this);
        llDeleteAccount.setOnClickListener(this);
        llPrivacy.setOnClickListener(this);
    }

    private void setLanguage() {
        String[] arrLanguage = getResources().getStringArray(R.array.strLanguage);
        String[] arrLanguageVal = getResources().getStringArray(R.array.strLanguageVal);
        String languageCode = ApplicationContext.preferences.getString(ApplicationContext.key_lang);
        String language = "English";
        for (int i = 0; i < arrLanguageVal.length; i++) {
            if (languageCode.equalsIgnoreCase(arrLanguageVal[i])) {
                language = arrLanguage[i];
                break;
            }
        }
        tvLanguage.setText(language);
    }

    @Override
    public void onClick(View view) {
        if (view == llLanguage) {
            Intent intent = new Intent(this, LanguageActivity.class);
            startActivityForResult(intent, 111);
        } else if (view == llTimeZone) {
            Intent intent = new Intent(this, TimeZoneActivity.class);
            intent.putExtra("_TIMEZONE", ApplicationContext.preferences.getString("_TIMEZONE"));
            startActivityForResult(intent, 222);
        } else if (view == llDeleteAccount) {
            if (SharedPreferenceData.getInstance(this).isTenantAdmin()) {
                startActivity(new Intent(this, DeleteAccountActivity.class));
            } else {
                DialogUtils.showInformationDialog(this, null,
                        getResources().getString(R.string.delete_account_user_message));
            }
        } else if (view == llPrivacy) {
            startActivity(new Intent(this, PrivacyActivity.class));
        }
    }

    private void refreshCalendars() {
        NetworkManager.customJsonObjectRequest(
                SettingActivity.this, Constants.CALENDAR_REFRESH_ALL_URL, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processResult(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ERRROR=", message);
                    }
                }, true);
    }

    public void refreshData() {
        getCalendars();
    }

    private void getCalendars() {
        JSONObject postData = new JSONObject();
        try {
            postData.put("action", "get");
            postData.put("page_size", "20");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        NetworkManager.customJsonObjectRequest(
                SettingActivity.this, Constants.CALENDAR_LIST_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processData(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ERRROR=", message);
                    }
                }, true);
    }

    private void processData(JSONObject data) {
        try {
            JSONArray calendars = data.getJSONArray("calendars");
            for (int i = 0; i < calendars.length(); i++) {
                JSONObject obj = calendars.getJSONObject(i);
                if (obj.getBoolean("is_default")) {
                    DEFAULT_CALENDAR_ID = obj.getString("calendarID");
                    calendar_default_value.setText(DEFAULT_CALENDAR_ID);
                    break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void processResult(JSONObject response) {
        try {
            if (Integer.parseInt(response.getString("result")) == 1) {
                Util.onMessageTop(getApplicationContext(), getResources().getString(R.string.refresh_success));
            } else {
                Util.onMessageTop(getApplicationContext(), getResources().getString(R.string.refresh_fail));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void updateUserTimeZone(String timezone) {
        NetworkManager.customJsonObjectRequest(this, Constants.UPDATE_PROFILE +
                        "?timeZone=" + timezone, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtils.Print("updateUserTimeZone", response.toString());
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("updateUserTimeZone", message);
                    }
                }, false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 222) {
            if (resultCode == RESULT_OK) {
                if (data != null && data.hasExtra("timzone")) {
                    String timeZone = data.getStringExtra("timzone");
                    updateUserTimeZone(timeZone);
                    tvTimeZone.setText(timeZone);
                    ApplicationContext.preferences.putString("_TIMEZONE", timeZone);
                }
            }
        } else if (requestCode == 111) {
            if (resultCode == RESULT_OK) {
                setLanguage();
            }
        }
    }
}