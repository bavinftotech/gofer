package com.gofercrm.user.gofercrm.chat.ui.main;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.R;

public class HolderAttachment extends RecyclerView.ViewHolder {

    private TextView title, desc, size;
    private ImageView thumbnail, overflow;

    public HolderAttachment(View view) {
        super(view);
        title = view.findViewById(R.id.title);
        desc = view.findViewById(R.id.count);
        size = view.findViewById(R.id.size);
        thumbnail = view.findViewById(R.id.thumbnail);
        overflow = view.findViewById(R.id.overflow);
    }

    public TextView getTitle() {
        return title;
    }

    public void setTitle(TextView title) {
        this.title = title;
    }

    public TextView getDesc() {
        return desc;
    }

    public void setDesc(TextView desc) {
        this.desc = desc;
    }

    public TextView getSize() {
        return size;
    }

    public void setSize(TextView size) {
        this.size = size;
    }

    public ImageView getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(ImageView thumbnail) {
        this.thumbnail = thumbnail;
    }

    public ImageView getOverflow() {
        return overflow;
    }

    public void setOverflow(ImageView overflow) {
        this.overflow = overflow;
    }
}