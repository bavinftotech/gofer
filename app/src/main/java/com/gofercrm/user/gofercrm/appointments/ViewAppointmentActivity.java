package com.gofercrm.user.gofercrm.appointments;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.appointments.adapter.AppointmentViewAdapter;
import com.gofercrm.user.gofercrm.appointments.model.DataAppointment;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

public class ViewAppointmentActivity extends BaseActivity {

    private RecyclerView recyclerView;
    private ArrayList<DataAppointment> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_appointment);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.appointments_details));
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getDataFromIntent();
        findViewById();
        setUpAdapterView();
    }

    private void getDataFromIntent() {
        Intent intent = getIntent();
        String strAppointmentArray = intent.getStringExtra("DATA");
        Gson gson = new Gson();
        TypeToken<ArrayList<DataAppointment>> token = new TypeToken<ArrayList<DataAppointment>>() {
        };
        list = gson.fromJson(strAppointmentArray, token.getType());
    }

    private void findViewById() {
        recyclerView = findViewById(R.id.rv);
    }

    private void setUpAdapterView() {
        AppointmentViewAdapter adapter = new AppointmentViewAdapter(list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }
}