package com.gofercrm.user.gofercrm.account;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.clients.ClientViewActivity;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.MyViewHolder> {

    private List<AccountContact> contactLists;
    private Context ctx;

    public ContactListAdapter(List<AccountContact> contactLists, Context ctx) {
        this.contactLists = contactLists;
        this.ctx = ctx;
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.account_contact_list_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(@NotNull MyViewHolder holder, final int position) {
        final AccountContact contact = contactLists.get(position);
        if (contact.getTitle() != null) {
            holder.title.setText(Util.capitalize(contact.getTitle()));
            holder.contactInitial.setText(Util.initialChar(contact.getTitle()));
        }
        holder.delete_contact.setOnClickListener(view -> new AlertDialog.Builder(view.getRootView().getContext())
                .setTitle(ctx.getResources().getString(R.string.delete))
                .setMessage(ctx.getResources().getString(R.string.delete_confirmation_msg))
                .setIcon(android.R.drawable.ic_menu_delete)
                .setPositiveButton(ctx.getResources().getString(R.string.delete), (dialog, whichButton) -> deleteContact(contact))
                .setNegativeButton(ctx.getResources().getString(R.string.no), null).show());

        holder.email.setText(contact.getEmail());
        holder.phone.setText(contact.getPhone());

        holder.itemView.setOnClickListener(view -> {
            Intent intent = new Intent(ctx, ClientViewActivity.class);
            intent.putExtra("CLIENT_ID", (Serializable) contactLists.get(position).getId());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            ctx.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return contactLists.size();
    }

    private void deleteContact(final AccountContact contact) {
        JSONObject postData = new JSONObject();
        try {
            postData.put("action", "delete");
            postData.put("contact_id", contact.getId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        NetworkManager.customJsonObjectRequest(
                ctx, Constants.MANAGE_CONTACT_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processResult(response, contact);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ERRROR=", message);
                    }
                }, true);
    }

    private void processResult(JSONObject response, AccountContact contact) {
        try {
            if (Integer.parseInt(response.getString("result")) == 1) {
                contactLists.remove(contact);
                notifyDataSetChanged();
                Util.onMessageTop(ctx, ctx.getResources().getString(R.string.contact_deleted));
            } else {
                Util.onMessageTop(ctx, ctx.getResources().getString(R.string.contact_deleted_failed));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, email, phone, contactInitial;
        public ImageView delete_contact;

        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.name);
            email = view.findViewById(R.id.email);
            phone = view.findViewById(R.id.phone);
            delete_contact = view.findViewById(R.id.account_contact_delete);
            contactInitial = view.findViewById(R.id.leadInitial);
        }
    }
}