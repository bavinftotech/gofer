package com.gofercrm.user.gofercrm.fcm;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.VibrationEffect;
import android.os.Vibrator;

import androidx.core.app.NotificationCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.FutureTarget;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.LauncherActivity;
import com.gofercrm.user.gofercrm.NavigationActivity;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.account.AccountViewActivity;
import com.gofercrm.user.gofercrm.appointments.AppointmentsActivity;
import com.gofercrm.user.gofercrm.chat.ui.main.Conversation;
import com.gofercrm.user.gofercrm.chat.video.group.openvcall.model.ConstantApp;
import com.gofercrm.user.gofercrm.chat.video.single.InComingCallActivity;
import com.gofercrm.user.gofercrm.chat.video.single.VideoChatViewActivity;
import com.gofercrm.user.gofercrm.clients.ClientViewActivity;
import com.gofercrm.user.gofercrm.lead.LeadViewActivity;
import com.gofercrm.user.gofercrm.opportunities.OpportunityViewActivity;
import com.gofercrm.user.gofercrm.task.NewTaskActivity;
import com.gofercrm.user.gofercrm.util.DeviceInfo;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.SharedPreferenceData;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class FireMessagingService extends FirebaseMessagingService {

    private static final String TAG = "FCM Service";
    static SharedPreferences sharedPref;
    static String token = "";
    private static final int LED_NOTIFICATION_ID = 0;
    public static String strMsgType = "", strMsgID = "";
    private boolean needToShowPushNotification = true;
    private static WeakReference<VideoChatViewActivity> mVideoChatViewActivityRef;

    public static void updateActivity(VideoChatViewActivity activity) {
        mVideoChatViewActivityRef = new WeakReference<>(activity);
        System.out.println("%%%%%%%%%%%%%%%%%%%%%%%% FireMessagingService mVideoChatViewActivityRef set!");
    }

    @Override
    public void onMessageReceived(@NotNull RemoteMessage remoteMessage) {
        //LogUtils.Print(TAG, "FMS remoteMessage --> " + remoteMessage);
        LogUtils.Print(TAG, "FMS remoteMessage --> getNotification()" + remoteMessage.getNotification());
        LogUtils.Print(TAG, "FMS remoteMessage getData()--> " + remoteMessage.getData());
        /*if (remoteMessage.getNotification() != null) {
            String message = remoteMessage.getNotification().getBody();
            if (isForeground(getApplicationContext())) {
                //if in forground then your operation
                // if app is running them
            } else {
                //if in background then perform notification operation
                sendNotification(message);
            }
        }*/
        showDataNotification(remoteMessage);
    }

    private static boolean isForeground(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        assert am != null;
        List<ActivityManager.RunningAppProcessInfo> tasks = am.getRunningAppProcesses();
        final String packageName = context.getPackageName();
        for (ActivityManager.RunningAppProcessInfo appProcess : tasks) {
            if (ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND == appProcess.importance && packageName.equals(appProcess.processName)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onNewToken(String token) {
        LogUtils.Print(TAG, "Refreshed_token: " + token);
        sharedPref = getApplicationContext().getSharedPreferences("LOGIN", MODE_PRIVATE);
        SharedPreferenceData.getInstance(getApplicationContext()).setFCMToken(token);
        token = sharedPref.getString("USER_TOKEN", "");
        if (!token.isEmpty()) {
            sendRegistrationToServer(token);
        }
    }

    private void sendRegistrationToServer(String fcm_token) {
        JSONObject postData = new JSONObject();
        try {
            String deviceId = DeviceInfo.getDeviceID(getApplicationContext());
            String req = "{\"" + deviceId + "\":\"" + fcm_token + "\"}";
            postData.put("push_notification_dict", req);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        NetworkManager.customJsonObjectRequest(
                getApplicationContext(), Constants.UPDATE_PROFILE, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtils.Print("RESPONSE", response.toString());
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("UPDATE PROFILE ERROR", message);
                    }
                }, true);
    }

    //    {"payload":{"channel_name":"5b1744418b578622c7ed86205b1a41d342e02e349587fc5b","name":"Priyank Gandhi","call_type":"personal"},"type":"video_voip"}
    private void showDataNotification(RemoteMessage remoteMessage) {
        String type = "", body = "", title = "", id = "", sender_id = "", sender_name = "", sender_image = "", timeZone = "", room_id = "", room_name = "", msg_text = "";
        String sub_type = "", avatar_url = "", channel_name = "", call_type = "";
        Intent intent;
        FutureTarget futureTarget = null;
        JSONObject json;
        PendingIntent pendingIntent = null;
        NotificationCompat.Builder mBuilder;
        remoteMessage.getData();
        if (remoteMessage.getData().size() > 0) {
            try {
                json = new JSONObject(remoteMessage.getData().toString());
                LogUtils.Print("NOTIFICATION", json.toString());
                if (json.has("message")) {
                    Boolean playRawNotificationSound = false;
                    JSONObject message = json.getJSONObject("message");
                    JSONObject data = message.getJSONObject("data");
                    if (data.has("type")) {
                        type = data.getString("type");
                    }
                    if (data.has("channel_name")) {
                        channel_name = data.getString("channel_name");
                    }
                    if (data.has("call_type")) {
                        call_type = data.getString("call_type");
                    }
                    if (data.has("sub_type")) {
                        sub_type = data.getString("sub_type");
                    }
                    if (data.has("body")) {
                        body = data.getString("body");
                    }
                    if (data.has("title")) {
                        title = data.getString("title");
                    }
                    if (data.has("id")) {
                        id = data.getString("id");
                    }
                    if (data.has("sender_id")) {
                        sender_id = data.getString("sender_id");
                    }
                    if (data.has("room_id")) {
                        room_id = data.getString("room_id");
                    }

                    if (data.has("name")) {
                        sender_name = data.getString("name");
                    }
                    if (data.has("timeZone")) {
                        timeZone = data.getString("timeZone");
                    }
                    if (data.has("sender_nickname")) {
                        sender_name = data.getString("sender_nickname");
                    }
                    if (data.has("room_name")) {
                        room_name = data.getString("room_name");
                    }
                    if (data.has("avatar_url")) {
                        sender_image = data.getString("avatar_url");
                    }
                    if (data.has("message")) {
                        msg_text = data.getString("message");
                    }
                    if (data.has("avatar_url")) {
                        avatar_url = data.getString("avatar_url");
                    }
                    String channelId = getString(R.string.default_notification_channel_id);
                    NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                    mBuilder = new NotificationCompat.Builder(getApplicationContext(), channelId)
                            .setContentTitle(title.equals("") ? getApplicationContext().getResources().getString(R.string.notification_from_)
                                    + " " + sender_name : title)
                            .setContentText(body.equals("") ? msg_text : body)
                            .setSmallIcon(getNotificationSmallIcon())
                            .setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.mipmap.ic_launcher))
                            .setDefaults(Notification.DEFAULT_SOUND).setAutoCancel(true)
                            .setPriority(NotificationCompat.PRIORITY_DEFAULT);

                    if (!avatar_url.isEmpty()) {
                        futureTarget = Glide.with(this)
                                .asBitmap()
                                .load(avatar_url)
                                .submit();
                    }
                    if (type.equals("N001")) {
                        intent = new Intent(this, LeadViewActivity.class);
                        intent.putExtra("LEAD_ID", id);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        pendingIntent = PendingIntent.getActivity(this, 0, intent,
                                PendingIntent.FLAG_ONE_SHOT);
                    } else if (type.equals("N002")) {
                        intent = new Intent(this, ClientViewActivity.class);
                        intent.putExtra("CLIENT_ID", id);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        pendingIntent = PendingIntent.getActivity(this, 0, intent,
                                PendingIntent.FLAG_ONE_SHOT);
                    } else if (type.equals("N003")) {
                        intent = new Intent(this, NewTaskActivity.class);
                        intent.putExtra("TASK_ID", id);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        pendingIntent = PendingIntent.getActivity(this, 0, intent,
                                PendingIntent.FLAG_ONE_SHOT);
                    } else if (type.equals("N004")) {
                        intent = new Intent(this, OpportunityViewActivity.class);
                        intent.putExtra("OPPOR_ID", id);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        pendingIntent = PendingIntent.getActivity(this, 0, intent,
                                PendingIntent.FLAG_ONE_SHOT);
                    } else if (type.equals("N005")) {
                        intent = new Intent(this, AccountViewActivity.class);
                        intent.putExtra("ACCOUNT_ID", id);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        pendingIntent = PendingIntent.getActivity(this, 0, intent,
                                PendingIntent.FLAG_ONE_SHOT);
                    } else if (type.equals("N006")) {
                        intent = new Intent(this, AppointmentsActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        pendingIntent = PendingIntent.getActivity(this, 0, intent,
                                PendingIntent.FLAG_ONE_SHOT);
                    } else if (type.equals("N007")) {
                        intent = new Intent(this, AppointmentsActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        pendingIntent = PendingIntent.getActivity(this, 0, intent,
                                PendingIntent.FLAG_ONE_SHOT);
                    }else if (type.equals("N100")) {
                        intent = new Intent(this, NavigationActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra(LauncherActivity.IS_FROM_N100, true);
                        pendingIntent = PendingIntent.getActivity(this, 0, intent,
                                PendingIntent.FLAG_ONE_SHOT);
                    } else if (type.equals("N010") && !sender_id.isEmpty()) {
                        if (strMsgType.equals("N010") && strMsgID.equals(sender_id)) {
                            needToShowPushNotification = false;
                        }
                        intent = new Intent(getApplicationContext(), Conversation.class);
                        intent.putExtra("_ID", sender_id);
                        intent.putExtra("_TYPE", (Serializable) 1);
                        intent.putExtra("_NAME", sender_name);
                        intent.putExtra("_IMAGE", sender_image);
                        intent.putExtra("_TIMEZONE", timeZone);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        pendingIntent = PendingIntent.getActivity(this, 0, intent,
                                PendingIntent.FLAG_ONE_SHOT);
                        mBuilder.setLights(Color.GREEN, 5000, 1000);
                        //mBuilder.setOngoing(true);
                    } else if (type.equals("N009") && !room_id.isEmpty()) {
                        if (strMsgType.equals("N009") && strMsgID.equals(room_id)) {
                            needToShowPushNotification = false;
                        }
                        intent = new Intent(getApplicationContext(), Conversation.class);
                        intent.putExtra("_ID", room_id);
                        intent.putExtra("_TYPE", (Serializable) 2);
                        intent.putExtra("_NAME", room_name);
                        intent.putExtra("_IMAGE", sender_image);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        pendingIntent = PendingIntent.getActivity(this, 0, intent,
                                PendingIntent.FLAG_ONE_SHOT);
                        mBuilder.setLights(Color.WHITE, 5000, 1000);
                    } else if (type.equals("voip_message") || type.equals("video_voip")) {
                        needToShowPushNotification = false;
                        //Vibration
//                        mBuilder.setVibrate(new long[]{1000, 1000, 1000, 1000});
                        Vibrator vibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                        if (Build.VERSION.SDK_INT >= 26) {
                            ((Vibrator) Objects.requireNonNull(getSystemService(VIBRATOR_SERVICE))).vibrate(VibrationEffect.createOneShot(1500, VibrationEffect.DEFAULT_AMPLITUDE));
                        } else {
                            vibrator.vibrate(new long[]{1000, 500, 1000, 500, 1000, 500, 1000, 500, 1000}, -1);
                        }
//                        mBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
                        //LED
                        mBuilder.setLights(Color.RED, 5000, 1000);

                        sharedPref = getApplicationContext().getSharedPreferences("LOGIN", MODE_PRIVATE);
                        sharedPref.getString("USER_NAME", "");
                        if (sender_name != null &&
                                channel_name != null &&
                                !channel_name.equals("")) {
                            if (call_type.equals("personal")) {
                                intent = new Intent(getApplicationContext(), InComingCallActivity.class);
                                intent.putExtra("_NAME", sender_name);
                                intent.putExtra("_LOGIN_USER_NAME", sharedPref.getString("USER_NAME", ""));
                                intent.putExtra("_USER_ID", "");
                                intent.putExtra("_OPO_USER_ID", sender_id);
                                intent.putExtra("_CHANNEL_NAME", channel_name);
                                intent.putExtra("_FROM", 0);
                                intent.putExtra("_CALL_TYPE", InComingCallActivity.fromPersonal);
                                intent.putExtra("_IMAGE", avatar_url);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                getApplicationContext().startActivity(intent);
                            } else {
                                intent = new Intent(getApplicationContext(), InComingCallActivity.class);
                                intent.putExtra("_NAME", sender_name);
                                intent.putExtra("_LOGIN_USER_NAME", sharedPref.getString("USER_NAME", ""));
                                intent.putExtra("_USER_ID", "");
                                intent.putExtra("_OPO_USER_ID", sender_id);
                                intent.putExtra("_CHANNEL_NAME", channel_name);
                                intent.putExtra("_FROM", 0);
                                intent.putExtra("_CALL_TYPE", InComingCallActivity.fromGroup);
                                intent.putExtra("_IMAGE", avatar_url);
                                intent.putExtra(ConstantApp.ACTION_KEY_CHANNEL_NAME, channel_name);
                                intent.putExtra(ConstantApp.ACTION_KEY_ENCRYPTION_KEY, "xdL_encr_key_");
                                intent.putExtra(ConstantApp.ACTION_KEY_ENCRYPTION_MODE, "AES-256-XTS");
                                getApplicationContext().startActivity(intent);
                            }
                            playRawNotificationSound = true;
                        }
                    } else if (type.equals("video_hangup")) {
                        needToShowPushNotification = false;
                        if (sender_name != null &&
                                channel_name != null &&
                                !channel_name.equals("")) {
                            if (call_type.equals("personal")) {
                                //If you received an incoming call and the other party ended, destroy InComingCallActivity
                                if (InComingCallActivity.activity != null) {
                                    InComingCallActivity.destroyActivity();
                                }

                                //===If you initiated the call and the other party ended/rejected call, destroy the VideoChatViewActivity
                                if (mVideoChatViewActivityRef != null) {
                                    mVideoChatViewActivityRef.get().finish();
                                }
                                LogUtils.Print("========DESTROY PERSONAL VIDEO CALL INTENT", "Personal Call Hangup");
                            } else {
                                if (!sender_name.equals("")) {
                                    Handler mainHandler = new Handler(getApplicationContext().getMainLooper());
                                    String finalSender_name = sender_name;
                                    Runnable myRunnable = () -> Util.makeToast(finalSender_name + " " +
                                            getResources().getString(R.string.has_left_the_video_call));
                                    mainHandler.post(myRunnable);
                                }
                                LogUtils.Print("========NOTIFY GROUP VIDEO CALL DEPARTURE", "Someone left the group");
                            }
                        }
                    }

                    //No need to show push notification to user.
                    if (!needToShowPushNotification) return;

                    if (pendingIntent != null) {
                        mBuilder.setContentIntent(pendingIntent);
                    }
                    if (futureTarget != null) {
                        try {
                            mBuilder.setLargeIcon((Bitmap) futureTarget.get());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        NotificationChannel channel = new NotificationChannel(channelId,
                                "GOFER_CRM_CHANNEL",
                                NotificationManager.IMPORTANCE_DEFAULT);
                        channel.enableLights(true);
                        channel.setLightColor(Color.RED);
                        if (playRawNotificationSound) {
                            channel.enableLights(true);
                            channel.setLightColor(Color.RED);
                            channel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
                            channel.enableVibration(true);
                            channel.setImportance(NotificationManager.IMPORTANCE_HIGH);

                        }
                        assert notificationManager != null;
                        notificationManager.createNotificationChannel(channel);
                    }

//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                        mBuilder.setPriority(NotificationManager.IMPORTANCE_MAX);
//                    } else {
//                        mBuilder.setPriority(Notification.PRIORITY_HIGH);
//                    }
                    assert notificationManager != null;
                    notificationManager.notify(createID(), mBuilder.build());

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private int getNotificationSmallIcon() {
        return R.mipmap.ic_launcher_transparent;
    }

    private void playRawNotificationSound() {
        try {
//            Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
//                    + "://" + getApplicationContext().getPackageName() + "/raw/notification");
            Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getApplicationContext().getPackageName() + "/" + R.raw.phone_ring);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), alarmSound);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int createID() {
        Date now = new Date();
        return Integer.parseInt(new SimpleDateFormat("HHmmssSSS", Locale.US).format(now));
    }

    private void showNotification(RemoteMessage remoteMessage) {
        LogUtils.Print("DATA=", remoteMessage.toString());
        String channelId = getString(R.string.default_notification_channel_id);
        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext(), channelId)
                .setContentTitle(remoteMessage.getNotification().getTitle())
                .setContentText(remoteMessage.getNotification().getBody())
                .setSmallIcon(R.drawable.ic_launcher)
                .setDefaults(Notification.DEFAULT_SOUND);

        if (remoteMessage.getNotification().getIcon() != null && !remoteMessage.getNotification().getIcon().isEmpty()) {
            mBuilder.setSmallIcon(Integer.parseInt(remoteMessage.getNotification().getIcon()));
        }

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            NotificationChannel channel = new NotificationChannel(channelId,
//                    "Gofer CRM",
//                    NotificationManager.IMPORTANCE_DEFAULT);
//            notificationManager.createNotificationChannel(channel);
//        }channel

        notificationManager.notify(0, mBuilder.build());
    }
}