package com.gofercrm.user.gofercrm.chat.ui.main;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.icu.util.Calendar;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.LauncherActivity;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.appointments.NewEventActivity;
import com.gofercrm.user.gofercrm.attachments.AttachmentActivity;
import com.gofercrm.user.gofercrm.chat.ui.main.adapter.AutoCompleteAdapter;
import com.gofercrm.user.gofercrm.chat.ui.main.model.AllLanguages;
import com.gofercrm.user.gofercrm.chat.ui.main.model.ChatData;
import com.gofercrm.user.gofercrm.chat.ui.main.model.Member;
import com.gofercrm.user.gofercrm.chat.ui.main.model.MessageData;
import com.gofercrm.user.gofercrm.chat.ui.main.model.Room;
import com.gofercrm.user.gofercrm.chat.video.group.openvcall.model.ConstantApp;
import com.gofercrm.user.gofercrm.chat.video.group.openvcall.ui.CallActivity;
import com.gofercrm.user.gofercrm.chat.video.single.VideoChatViewActivity;
import com.gofercrm.user.gofercrm.entity.User;
import com.gofercrm.user.gofercrm.fcm.FireMessagingService;
import com.gofercrm.user.gofercrm.implementor.DialogButtonClickListener;
import com.gofercrm.user.gofercrm.profile.ProfileActivity;
import com.gofercrm.user.gofercrm.util.DialogUtils;
import com.gofercrm.user.gofercrm.util.FileUtil;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.ScalingUtilities;
import com.gofercrm.user.gofercrm.util.SharedPreferenceData;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.gofercrm.user.gofercrm.websocket.SharedObserver;
import com.gofercrm.user.gofercrm.websocket.WSSingleton;
import com.gofercrm.user.gofercrm.websocket.WsManager;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.jaiselrahman.filepicker.activity.FilePickerActivity;
import com.jaiselrahman.filepicker.config.Configurations;
import com.jaiselrahman.filepicker.model.MediaFile;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.stream.Collectors;

import io.reactivex.disposables.Disposable;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static java.util.Map.Entry.comparingByKey;
import static java.util.stream.Collectors.toMap;

public class Conversation extends BaseActivity implements View.OnClickListener {
    private static final String TAG = "Conversation";
    private static final int READ_REQUEST_CODE = 42;
    private static final int FILE_REQUEST_CODE = 228;
    private static final int UPDATE_ATTACHMENT = 229;
    private final int CLIENT = 0, CONNECTION = 1, ROOM = 2;
    List<ChatData> data = new ArrayList<>();
    String _ID, _NAME, _IMAGE, _STATUS, _SMS, _LANG, _TIMEZONE;
    int _Type;
    long _MUTE_NOT = 0L;
    List<Member> _MEMBERS = new ArrayList<>();
    Room _ROOM;
    Member current_member;
    SharedPreferences sharedPref;
    String token, LOGIN_USER_ID, LOGIN_USER_NAME;
    String searchText;
    String file_upload_result;
    Toolbar toolbar;
    String edit_msg_id;
    MenuItem menu_notifcation = null;
    MenuItem menu_delete = null;
    MenuItem menu_attachments = null;
    ImageView ivDefaultLanguage;
    TextView tvSelectedLanguage;
    int selItem = 0;
    private RecyclerView mRecyclerView;
    private ConversationAdapter mAdapter;
    private EditText text;
    private TextView send;
    private TextView quote_text, quote_text_header;
    private LinearLayout quote_text_rl;
    private CoordinatorLayout coordinatorLayout;
    private TextView tv_selection;
    private boolean isEdit;
    private boolean text_on_edit;
    private Disposable subscribe;
    private Switch switchSMS;
    private ProgressBar paginationProgressBar;
    private ProgressBar progressBar;
    private FloatingActionButton fab;
    //pagination
    private String last_id = null;
    private int pageSize = 200;
    private boolean isMoreDataAvailable = true;
    private boolean isScrolling = false;
    //time
    private TextView tvTime;
    //Languages
    private ConstraintLayout clSearch;
    private ImageView ivDeleteLang;
    private AutoCompleteTextView etSearchLang;
    private ArrayList<AllLanguages> allLanguages = new ArrayList<>();
    private BroadcastReceiver _broadcastReceiver;
    private SimpleDateFormat _sdfWatchTime = new SimpleDateFormat("hh:mm a");
    private StringBuilder postFix = new StringBuilder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);
        findViewById();
        Intent intent = getIntent();
        _Type = intent.getIntExtra("_TYPE", 2); //CLIENT = 0, CONNECTION = 1, ROOM = 2

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView = findViewById(R.id.recyclerView);
//        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
//        linearLayoutManager.setStackFromEnd(true);
        mRecyclerView.setLayoutManager(linearLayoutManager);

        mAdapter = new ConversationAdapter(this, data, _Type);
        mRecyclerView.setAdapter(mAdapter);
        subscribe = SharedObserver.getSocketObserver().subscribe(s -> {
            processTyping(s);
            receivedData(s);
        });

        sharedPref = getApplicationContext().getSharedPreferences("LOGIN", MODE_PRIVATE);
        token = sharedPref.getString("USER_TOKEN", "");
        LOGIN_USER_ID = SharedPreferenceData.getInstance(getApplicationContext()).getLoggedInUserID();
        LOGIN_USER_NAME = SharedPreferenceData.getInstance(getApplicationContext()).getUserName();
        coordinatorLayout = findViewById(R.id
                .attachment_coordinator_layout_id);

        _NAME = intent.getStringExtra("_NAME"); //Represents whom we are chatting with (the other user)
        _IMAGE = intent.getStringExtra("_IMAGE");
        _ID = intent.getStringExtra("_ID");
        _SMS = intent.getStringExtra("_SMS");
        if (_SMS == null) {
            _SMS = "";//Assign default string
        }
        FireMessagingService.strMsgID = _ID;
        LogUtils.Print(TAG, "_ID --> " + _ID);
        _STATUS = intent.getStringExtra("_STATUS");
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(_NAME);
//            toolbar.setTitle(_NAME);
            if (intent.hasExtra("_LAST_SEEN")) {
                if (intent.getLongExtra("_LAST_SEEN", 0) != 0) {
                    getSupportActionBar().setSubtitle(getResources().getString(R.string.last_seen) + " " +
                            Util.timeAgo(intent.getLongExtra("_LAST_SEEN", 0)));
                }
            }
        }

        mAdapter.setToolbar_name(_NAME);
        if (intent.getStringExtra("_TIMEZONE") != null)
            _TIMEZONE = intent.getStringExtra("_TIMEZONE");
        if (_ID != null && _Type == ROOM) {
            FireMessagingService.strMsgType = "N009";
            last_id = null;
            getRoomMessage();
        } else if (_ID != null && _Type == CLIENT) {
            if (intent.getStringExtra("_LANG") != null)
                _LANG = intent.getStringExtra("_LANG");
            last_id = null;
            getConnectionMessage();
        } else if (_ID != null && _Type == CONNECTION) {
            if (intent.getStringExtra("_LANG") != null)
                _LANG = intent.getStringExtra("_LANG");
            _MUTE_NOT = intent.getLongExtra("_MUTE_NOT", 0L);
            _STATUS = intent.getStringExtra("_STATUS");
            FireMessagingService.strMsgType = "N010";
            WsManager.setOnUserStatusChangeListener((type, strId) -> {
                if (data != null && data.size() > 0) {
                    for (int i = 0; i < data.size(); i++) {
                        if (_ID.equals(strId)) {
                            _STATUS = type == 1 ? "Available" : "Busy";
                        }
                    }
                }
            });
            last_id = null;
            getConnectionMessage();
        }

        switchSMS = findViewById(R.id.switchSMS);
        if (_Type == CONNECTION || _Type == CLIENT) {
            if (sharedPref.getBoolean("softphone_enabled", false) &&
                    !sharedPref.getString("sip_phone", "").equals("")) {
                switchSMS.setVisibility(View.VISIBLE);
                switchSMS.setChecked(false);
            }
        }
        if (_Type == ROOM && !_SMS.equals("")) {
            switchSMS.setVisibility(View.VISIBLE);
            switchSMS.setChecked(false);
        }
        send = findViewById(R.id.bt_send);
        send.setText(getResources().getString(R.string.send));
        switchSMS.setOnCheckedChangeListener((compoundButton, b) -> send.setText(b ? getResources().getString(R.string.SMS) :
                getResources().getString(R.string.send)));

        text = findViewById(R.id.et_message);
        mRecyclerView.addOnLayoutChangeListener((v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom) -> {
            if (bottom < oldBottom) {
                if (mRecyclerView.getAdapter() != null &&
                        mRecyclerView.getAdapter().getItemCount() > 0) {
                    mRecyclerView.postDelayed(() -> mRecyclerView.scrollToPosition(
                            mRecyclerView.getAdapter().getItemCount() - 1), 100);
                }
            }
        });

        send.setOnClickListener(view -> {
            String quote = quote_text.getText().toString();
            String txtToBeSent = text.getText().toString();
            if (!txtToBeSent.equals("") && edit_msg_id != null
                    && !edit_msg_id.equals("")) {
                for (ChatData chat : data) {
                    if (chat != null && edit_msg_id.equals(chat.getId())) {
                        chat.setText(txtToBeSent);
                        chat.setEdit(1);
                        chat.setUser_message_type(switchSMS.isChecked() ? "sms" : "");
                        break;
                    }
                }
//                    mAdapter.addItem(data);
                mAdapter.notifyDataSetChanged();
            } else if (!txtToBeSent.equals("") && (edit_msg_id == null || edit_msg_id.equals(""))) {
                List<ChatData> send_data = new ArrayList<ChatData>();
                ChatData item = new ChatData();
                item.setTime(getResources().getString(R.string.just_now));
                item.setType("2");
                item.setRead(1);
                item.setUser_message_type(switchSMS.isChecked() ? "sms" : "");
                if (quote != null && !quote.isEmpty()) {
                    item.setQuotedText(quote);
                }
                item.setText(text.getText().toString());
//                    send_data.add(item);
                data.add(item);
//                    mAdapter.addItem(send_data);
                runOnUiThread(() -> mRecyclerView.scrollToPosition(data.size() - 1));
            }
            sendMessage(_Type, _ID, txtToBeSent, quote, null);
            text.setText("");
            hideQuote();
        });
        text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                notifyTyping();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        ImageButton btn_attachment = findViewById(R.id.bt_attachment);
        btn_attachment.setOnClickListener(v -> openFilePickerDialog());

        quote_text = findViewById(R.id.tv_quoted_text_id);
        quote_text_rl = findViewById(R.id.rl_quote_layout);
        quote_text_header = findViewById(R.id.quote_text_header);

        ImageButton btn_quote_cancel = findViewById(R.id.cancel_quote_btn);
        btn_quote_cancel.setOnClickListener(v -> hideQuote());
        if (_ID != null && _Type == ROOM) {
            getRoomInfo(_ID);
        }
        getLanguagesFromAssets();
        setOpoUserTime();

        //Upload file from share intent
        boolean isFromShare = intent.getBooleanExtra(LauncherActivity.IS_FROM_SHARE, false);
        if (isFromShare) {
            String stringExtra = intent.getStringExtra(LauncherActivity.SHARE_URI);
            try {
                if (stringExtra != null && !stringExtra.equals("")) {
                    Uri receiveUri = Uri.parse(stringExtra);
                    String mimeType = FileUtil.getMimeType(getApplicationContext(), receiveUri);
                    String fileName = FileUtil.getFileName(getApplicationContext(), receiveUri);
                    uploadAfterCompressImage(receiveUri, fileName, mimeType);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        WsManager ws = WSSingleton.getInstanceWM(getApplicationContext());
        if (!ws.isWsConnected()) {
            ws.startConnect();
        }

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NotNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NotNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
                    final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                            .getLayoutManager();
                    int totalItemCount = linearLayoutManager.getItemCount();
                    int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (totalItemCount > 0) {
                        fab.setVisibility((totalItemCount - lastVisibleItem) > 30 ? View.VISIBLE : View.GONE);
                        if (isMoreDataAvailable && !isScrolling && lastVisibleItem <= 20) {
                            isScrolling = true;
                            if (searchText == null || searchText.equals("")) {
                                if (_ID != null && _Type == ROOM) {
                                    getRoomMessage();
                                } else if (_ID != null && _Type == CLIENT) {
                                    getConnectionMessage();
                                } else if (_ID != null && _Type == CONNECTION) {
                                    getConnectionMessage();
                                }
                            } else {
                                searchMessage();
                            }
                        }
                    }
                }
            }
        });
    }

    private void findViewById() {
        toolbar = findViewById(R.id.toolbar);
        etSearchLang = findViewById(R.id.etSearchLang);
        clSearch = findViewById(R.id.clSearch);
        ivDeleteLang = findViewById(R.id.ivDeleteLang);
        ivDeleteLang.setOnClickListener(this);
        tvTime = findViewById(R.id.tvTime);
        progressBar = findViewById(R.id.progressBar);
        paginationProgressBar = findViewById(R.id.paginationProgressBar);
        fab = findViewById(R.id.fab);
        fab.setOnClickListener(this);
    }

    private void setOpoUserTime() {
        //set current timezone if not come from previous screen.
        if ((_Type == CLIENT || _Type == CONNECTION) &&
                (_TIMEZONE == null || _TIMEZONE.equals(""))) {
            _TIMEZONE = sharedPref.getString("_TIMEZONE", TimeZone.getDefault().getDisplayName());
        }
        //set time into header.
        if ((_TIMEZONE != null) && !_TIMEZONE.equals("")) {
            Calendar currentdate = Calendar.getInstance();
            TimeZone obj = TimeZone.getTimeZone(_TIMEZONE);
            _sdfWatchTime.setTimeZone(obj);
            if (obj.getDisplayName() != null && !obj.getDisplayName().equals("")) {
                String[] textName = obj.getDisplayName().split(" ");
                for (String s : textName) {
                    postFix.append(s.substring(0, 1));
                }
            }
            tvTime.setText(_sdfWatchTime.format(currentdate.getTime()) + " " + postFix);
            tvTime.setVisibility(View.VISIBLE);
        } else {
            tvTime.setVisibility(View.GONE);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (_Type != ROOM) {
            _broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context ctx, Intent intent) {
                    if (intent.getAction().compareTo(Intent.ACTION_TIME_TICK) == 0)
                        tvTime.setText(_sdfWatchTime.format(new Date()) + " " + postFix);
                }
            };
            registerReceiver(_broadcastReceiver, new IntentFilter(Intent.ACTION_TIME_TICK));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        FireMessagingService.strMsgType = "";
        FireMessagingService.strMsgID = "";
    }

    /*@Override
        public void onStart() {
            super.onStart();
            Intent intent = getIntent();
            _NAME = intent.getStringExtra("_NAME");
            LogUtils.Print("Conversation Start with " + _NAME, "+ ON START +");
        }
    */
    @Override
    public void onStop() {
        super.onStop();
//        Intent intent = getIntent();
        //_NAME = intent.getStringExtra("_NAME");
//        _ID = intent.getStringExtra("_ID");
        // LogUtils.Print("Conversation Stopped with " + _NAME, "+ ON STOP +");
        if (_ID != null && _Type != ROOM) {
            setUserAppPresenceStatus(false, _ID);
        }
        if (_Type != ROOM) {
            if (_broadcastReceiver != null)
                unregisterReceiver(_broadcastReceiver);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
//        Intent intent = getIntent();
        //_NAME = intent.getStringExtra("_NAME");
        //LogUtils.Print("Conversation Resumed with " + _NAME, "+ ON RESUME +");
        if (_ID != null && _Type != ROOM) {
            setUserAppPresenceStatus(true, _ID);
        }
        if (_STATUS != null) {

            if (_STATUS.equals("Available")) {
                runOnUiThread(() -> {
                    toolbar.setTitleTextColor(Color.WHITE);
                });
            } else if (_STATUS.equals("Busy")) {
                runOnUiThread(() -> {
                    toolbar.setTitleTextColor(Color.RED);
                });
            }
        }
    }

    private void hideQuote() {
        quote_text_rl.setVisibility(View.GONE);
        quote_text.setText("");
        quote_text_header.setText("");
        edit_msg_id = "";
        text.setText("");
    }

    public void setQuoteText(String text) {
        quote_text_header.setText(getResources().getString(R.string.quote_message));
        quote_text_rl.setVisibility(View.VISIBLE);
        quote_text.setText(text);
    }

    public String getMsgText() {
        return text.getText().toString();
    }

    public void setMsgText(String textToEdit, String msg_id) {
        text.setText(textToEdit);
        text.setSelection(text.getText().length());
        quote_text.setText(textToEdit);
        quote_text_header.setText(getResources().getString(R.string.edit_message));
        quote_text_rl.setVisibility(View.VISIBLE);
        edit_msg_id = msg_id;
        text.requestFocus();
    }

    private void openFilePickerDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.add_a_file));
        builder.setItems(new CharSequence[]{
                        getResources().getString(R.string.camera_video),
                        getResources().getString(R.string.file)},
                (dialog, which) -> {
                    switch (which) {
                        case 0: {
                            //Camera & Video
                            dialog.cancel();
                            Intent intent = new Intent(this, FilePickerActivity.class);
                            intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
                                    .setCheckPermission(true)
                                    .setShowImages(true)
                                    .setShowVideos(true)
                                    .setShowAudios(true)
                                    .setShowFiles(true)
                                    .enableImageCapture(true)
                                    .enableVideoCapture(true)
                                    .setSingleChoiceMode(true)
                                    .build());
                            startActivityForResult(intent, FILE_REQUEST_CODE);
                        }
                        break;
                        case 1: {
                            //File
                            dialog.cancel();
                            performFileSearch();
                        }
                        break;
                    }
                });
        builder.create().show();
        builder.setCancelable(false);
    }

    public void performFileSearch() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        startActivityForResult(intent, READ_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                ArrayList<MediaFile> files;
                if (data != null) {
                    if (data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES) != null) {
                        files = data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES);
                        if (files == null || files.size() == 0) {
                            return;
                        }
                        Uri uri = files.get(0).getUri();
                        String mimeType = FileUtil.getMimeType(getApplicationContext(), uri);
                        String fileName = FileUtil.getFileName(getApplicationContext(), uri);
                        uploadAfterCompressImage(uri, fileName, mimeType);
                    }
                }
            }
        } else if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri uri = null;
            if (data != null) {
                uri = data.getData();
                String mimeType = FileUtil.getMimeType(getApplicationContext(), uri);
                String fileName = FileUtil.getFileName(getApplicationContext(), uri);
                uploadAfterCompressImage(uri, fileName, mimeType);
            }
        } else if (requestCode == UPDATE_ATTACHMENT && resultCode == Activity.RESULT_OK) {
            if (_ID != null && _Type == ROOM) {
                last_id = null;
                getRoomMessage();
            } else if (_ID != null && (_Type == CLIENT || _Type == CONNECTION)) {
                last_id = null;
                getConnectionMessage();
            }
        }
    }

    private void uploadAfterCompressImage(Uri uri, final String file_name, String file_type) {
        if (Util.isImageGreaterThan1MB(this, uri, file_name)) {
            LogUtils.Print(TAG, "path --> " + uri);
            compressImage(getRealPathFromURI(uri), uri, file_name, file_type);
        } else {
            uploadFile(uri, file_name, file_type);
        }
    }

    private String getRealPathFromURI(Uri uri) {
        String yourRealPath = "";
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            yourRealPath = cursor.getString(columnIndex);
        } else {
            yourRealPath = uri.toString();
        }
        cursor.close();
        return yourRealPath;
    }

    private void uploadFile(Uri uri, final String file_name, String file_type) {
        final Snackbar snackbar = Snackbar.make(coordinatorLayout,
                getResources().getString(R.string.a_file_is_being_uploaded), Snackbar.LENGTH_LONG);
        snackbar.show();

        OkHttpClient httpClient = new OkHttpClient();
        RequestBody requestBody = null;
        try {
            File file = FileUtil.from(getApplicationContext(), uri);
            requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("filename", file_name)
                    .addFormDataPart("attachment", file_name, RequestBody.create(MediaType.parse(file_type),
                            file)).build();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Request request = new Request.Builder()
                .url(Constants.UPLOAD_ATTACHMENT_URL)
                .addHeader("Authorization", "Bearer " + token)
                .post(requestBody)
                .build();
        showProgress();
        httpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                hideProgress();
                e.printStackTrace();
                LogUtils.Print("ERORRORR", "error in getting response using async okhttp call");
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                hideProgress();
                ResponseBody responseBody = response.body();
                if (!response.isSuccessful()) {
                    throw new IOException("Error response " + response);
                } else {
                    file_upload_result = responseBody.string();
                    try {
                        new Handler(Looper.getMainLooper()).post(() -> sendAttachment(file_upload_result, file_name));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private int DESIREDWIDTH = 512;
    private int DESIREDHEIGHT = 512;

    private void compressImage(String path, Uri uri, String file_name, String file_type) {
        String strMyImagePath;
        Bitmap scaledBitmap;
        try {
            Bitmap unscaledBitmap = ScalingUtilities.decodeFile(path, DESIREDWIDTH, DESIREDHEIGHT, ScalingUtilities.ScalingLogic.FIT);
            if (!(unscaledBitmap.getWidth() <= DESIREDWIDTH && unscaledBitmap.getHeight() <= DESIREDHEIGHT)) {
                scaledBitmap = ScalingUtilities.createScaledBitmap(unscaledBitmap, DESIREDWIDTH, DESIREDHEIGHT, ScalingUtilities.ScalingLogic.FIT);
            } else {
                unscaledBitmap.recycle();
                uploadFile(uri, file_name, file_type);
                return;
            }

            String extr = getExternalFilesDir(null).getAbsolutePath();
            File mFolder = new File(extr + "/TMMFOLDER");
            if (!mFolder.exists()) {
                mFolder.mkdirs();
            }
            File f = new File(mFolder.getAbsolutePath(), "tmp.png");
            strMyImagePath = f.getAbsolutePath();
            FileOutputStream fos;
            try {
                fos = new FileOutputStream(f);
                scaledBitmap.compress(Bitmap.CompressFormat.PNG, 75, fos);
                fos.flush();
                fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            scaledBitmap.recycle();
        } catch (Throwable e) {
            e.getMessage();
            uploadFile(uri, file_name, file_type);
            return;
        }
        if (strMyImagePath == null) {
            uploadFile(uri, file_name, file_type);
            return;
        }
        LogUtils.Print(TAG, "path --> " + path);
        uploadFile(Uri.fromFile(new File(strMyImagePath)), file_name, file_type);
    }

    private void sendAttachment(String data, String file_name) {
        try {
            JSONObject response = new JSONObject(data);
            LogUtils.Print(TAG, "response --> " + response);
            String attachment_id = "";
            if (Integer.parseInt(response.getString("result")) == 1) {
                List<ChatData> chat_data = new ArrayList<>();
                ChatData item = new ChatData();
                item.setTime(getResources().getString(R.string.just_now));
                item.setType("2");
                item.setRead(1);
                item.setAtt_name(file_name);
                if (response.has("content_type")) {
                    item.setContent_type(response.getString("content_type"));
                }
                if (response.has("att_id")) {
                    attachment_id = response.getString("att_id");
                }
                if (response.has("size")) {
                    try {
                        item.setAtt_file_size(FileUtil.getStringSizeLengthFile(response.getLong("size")));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (response.has("link")) {
                    item.setAtt_url(response.getString("link"));
                }
                item.setText(text.getText().toString());
                chat_data.add(item);
                mAdapter.addItem(chat_data);
                if (mRecyclerView.getAdapter() != null &&
                        mRecyclerView.getAdapter().getItemCount() > 0) {
                    mRecyclerView.scrollToPosition(mRecyclerView.getAdapter().getItemCount() - 1);
                }
                sendMessage(_Type, _ID, text.getText().toString(), quote_text.getText().toString(), attachment_id);
                text.setText("");
                hideQuote();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void updateChat() {
    }

    private void sendMessage(int type, String _ID, String text, String quoted_text, String attachment_id) {
        if (_ID != null && type == ROOM && edit_msg_id != null
                && !edit_msg_id.equals("") && !text.equals("")) {
            editRoomMessage(edit_msg_id, text);
        } else if (_ID != null && type == CLIENT && edit_msg_id != null
                && !edit_msg_id.equals("") && !text.equals("")) {
            editConnectionMessage(edit_msg_id, text);
        } else if (_ID != null && type == CONNECTION && edit_msg_id != null
                && !edit_msg_id.equals("") && !text.equals("")) {
            editConnectionMessage(edit_msg_id, text);
        } else if (_ID != null && type == ROOM) {
            sendRoomMessage(_ID, text, quoted_text, attachment_id);
        } else if (_ID != null && type == CLIENT) {
            sendConnectionMessage(_ID, text, quoted_text, attachment_id);
        } else if (_ID != null && type == CONNECTION) {
            sendConnectionMessage(_ID, text, quoted_text, attachment_id);
        }
    }

    private void sendRoomMessage(String room_id, String text, String quoted_text, String
            attachment_id) {
        String param = "?room_id=" + room_id + "&text=" + text + "&quoted_text=" + quoted_text;
        if (attachment_id != null) {
            param = param + "&attachment_id=" + attachment_id;
        }
        if (switchSMS.isChecked()) {
            param = param + "&user_message_type=sms";
        }
        String url = Constants.SEND_ROOM_MESSAGE + param;
        NetworkManager.customJsonObjectRequest(
                getApplicationContext(), url, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ERRROR=", message);
                    }
                }, true);
    }

    private void sendConnectionMessage(String _id, String text, String quoted_text,
                                       String att_id) {
        String param = "?to_id=" + _id + "&text=" + text + "&quoted_text=" + quoted_text;
        if (att_id != null) {
            param = param + "&type=file" + "&att_id=" + att_id;
        }
        if (switchSMS.isChecked()) {
            param = param + "&user_message_type=sms";
        }
        String url = Constants.SEND_CONNECTION_MESSAGE + param;
        NetworkManager.customJsonObjectRequest(
                getApplicationContext(), url, null,
                new VolleyResponseListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtils.Print(TAG, "response --> " + response);
                        try {
                            if (Integer.parseInt(response.getString("result")) == 0) {
                                JSONObject error_obj = response.getJSONObject("error");
                                Util.onMessage(Conversation.this, error_obj.has("message") ?
                                        error_obj.getString("message") : getResources().getString(R.string.server_err));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ERRROR=", message);
                    }
                }, true);
    }

    private void getRoomMessage() {
        String url = Constants.GET_ROOM_MESSAGES + "?room_id=" + _ID + "&page_size=" + pageSize;
        if (last_id != null) {
            url += "&last_id=" + last_id;
            paginationProgressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.VISIBLE);
            if (data.size() > 0) {
                data.clear();
                mAdapter.notifyDataSetChanged();
            }
        }
        LogUtils.Print(TAG, "url --> " + url);
        NetworkManager.customJsonObjectRequest(
                getApplicationContext(), url, null,
                new VolleyResponseListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        paginationProgressBar.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        processData(response);
                    }

                    @Override
                    public void onError(String message) {
                        paginationProgressBar.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        LogUtils.Print("ERRROR=", message);
                    }

                }, true);
    }

    private void getConnectionMessage() {
        String url = Constants.GET_CONNECTION_MESSAGES + "?version=2&from=" + _ID + "&page_size=" + pageSize;
        if (last_id != null) {
            url += "&last_id=" + last_id;
            paginationProgressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.VISIBLE);
            if (data.size() > 0) {
                data.clear();
                mAdapter.notifyDataSetChanged();
            }
        }
        LogUtils.Print(TAG, "url --> " + url);
        NetworkManager.customJsonObjectGetRequest(
                getApplicationContext(), url, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        paginationProgressBar.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        processData(response);
                    }

                    @Override
                    public void onError(String message) {
                        paginationProgressBar.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        LogUtils.Print("ERRROR=", message);
                    }
                }, true);

    }

    private void searchConnectionMessage() {
        String url = Constants.SEARCH_CONNECTION_MESSAGES + "?allwords=true&search_text=" + searchText + "&with_userid=" + _ID +
                "&page_size=" + pageSize;
        if (last_id != null) {
            url += "&last_id=" + last_id;
            paginationProgressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.VISIBLE);
            if (data.size() > 0) {
                data.clear();
                mAdapter.notifyDataSetChanged();
            }
        }
        NetworkManager.customJsonObjectRequest(
                getApplicationContext(), url, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        paginationProgressBar.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        processData(response);
                    }

                    @Override
                    public void onError(String message) {
                        paginationProgressBar.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        LogUtils.Print("ERRROR=", message);
                    }
                }, true);

    }

    private void searchRoomMessage() {
        String url = Constants.SEARCH_ROOM_MESSAGES + "?allwords=true&search_text=" + searchText + "&room_id=" + _ID + "&page_size=" + pageSize;
        if (last_id != null) {
            url += "&last_id=" + last_id;
            paginationProgressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.VISIBLE);
            if (data.size() > 0) {
                data.clear();
                mAdapter.notifyDataSetChanged();
            }
        }
        NetworkManager.customJsonObjectRequest(
                getApplicationContext(), url, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        paginationProgressBar.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        processData(response);
                    }

                    @Override
                    public void onError(String message) {
                        paginationProgressBar.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        LogUtils.Print("ERRROR=", message);
                    }
                }, true);
    }

    private void searchMessage() {
        if (_ID != null && _Type == ROOM) {
            searchRoomMessage();
        } else if (_ID != null && _Type == CLIENT) {
            searchConnectionMessage();
        } else if (_ID != null && _Type == CONNECTION) {
            searchConnectionMessage();
        }
    }

    public void editRoomMessage(String msg_id, String text) {
        String param = "?msgid=" + msg_id + "&text=" + text;
        String url = Constants.EDIT_ROOM_MESSAGE + param;
        NetworkManager.customJsonObjectRequest(
                getApplicationContext(), url, null,
                new VolleyResponseListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ERRROR=", message);
                    }

                }, true);
    }

    public void setUserAppPresenceStatus(Boolean isConversationActive, String userid) {
        String param = "?isAppActive=" + isConversationActive + "&connection_id=" + SharedPreferenceData.getInstance(this).getClientConnectionID() + "&to_userid=" + userid;
        String url = Constants.SET_USER_CONVERSATION_APP_STATUS + param;
        NetworkManager.customJsonObjectRequest(
                getApplicationContext(), url, null,
                new VolleyResponseListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ERRROR=", message);
                    }

                }, true);
    }

    public void editConnectionMessage(String msg_id, String text) {
        String param = "?msgid=" + msg_id + "&text=" + text;
        String url = Constants.EDIT_CONNECTION_MESSAGE + param;
        NetworkManager.customJsonObjectRequest(
                getApplicationContext(), url, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ERRROR=", message);
                    }
                }, true);
    }

    public void deleteRoomMessage(String msg_id) {
        String param = "?msgid=" + msg_id;
        String url = Constants.DELETE_ROOM_MESSAGE + param;
        NetworkManager.customJsonObjectRequest(
                getApplicationContext(), url, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ERRROR=", message);
                    }
                }, true);
    }

    public void deleteConnectionMessage(String msg_id) {
        String param = "?msgid=" + msg_id;
        String url = Constants.DELETE_CONNECTION_MESSAGE + param;
        NetworkManager.customJsonObjectRequest(
                getApplicationContext(), url, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ERRROR=", message);
                    }
                }, true);
    }

    private void processData(JSONObject response) {
        List<MessageData> msgs_array = new ArrayList<>();
        List<String> unread_messages = new ArrayList<>();
        try {
            if (Integer.parseInt(response.getString("result")) == 1) {
                JSONArray messages = response.getJSONArray("messages");
                JSONArray user_array = response.getJSONArray("users");
                JSONObject result_obj;
                for (int i = 0; i < messages.length(); i++) {
                    result_obj = messages.getJSONObject(i);
                    MessageData msgs = new MessageData();

                    if (result_obj.has("user_message_type")) {
                        msgs.setUser_message_type(result_obj.getString("user_message_type"));
                    } else {
                        msgs.setUser_message_type("");
                    }
                    if (result_obj.has("text")) {
                        msgs.setText(result_obj.getString("text"));
                    }

                    if (result_obj.has("id")) {
                        msgs.setId(result_obj.getString("id"));
                    }
                    if (result_obj.has("quoted_text")) {
                        msgs.setQuotedText(result_obj.getString("quoted_text"));
                    }
                    if (result_obj.has("content_type")) {
                        msgs.setContent_type(result_obj.getString("content_type"));
                    }
                    if (result_obj.has("att_url")) {
                        msgs.setAtt_url(result_obj.getString("att_url"));
                    }
                    if (result_obj.has("att_filesize")) {
                        msgs.setAtt_file_size(result_obj.getString("att_filesize"));
                    }
                    if (result_obj.has("att_filename")) {
                        msgs.setAtt_name(result_obj.getString("att_filename"));
                    }
                    if (result_obj.has("sender_name")) {
                        msgs.setSender_name(result_obj.getString("sender_name"));
                    }
                    if (result_obj.has("pdf_url")) {
                        msgs.setPdf_url(result_obj.getString("pdf_url"));
                    }
                    if (result_obj.has("date_time")) {

                        msgs.setDataTime(result_obj.getLong("date_time"));
                    }
                    if (result_obj.has("direction")) {
                        msgs.setType(result_obj.getString("direction"));
                    }
                    if (result_obj.has("room_id")) {
                        msgs.setRoom_id(result_obj.getString("room_id"));
                    }
                    if (result_obj.has("status")) {
                        msgs.setRead(result_obj.getInt("status"));
                    }
                    if (result_obj.has("edited")) {
                        msgs.setEdit(result_obj.getInt("edited"));
                    }
                    if (result_obj.has("child_messages")) {
                        JSONObject child_message_obj = result_obj.getJSONObject("child_messages");
                        msgs.setThread_count(child_message_obj.getInt("child_thread_count"));
                    }

                    if (result_obj.has("date_time")) {
                        String pattern = "dd MMMM yyyy";
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
                        String date = simpleDateFormat.format(new Date(result_obj.getLong("date_time") * 1000));
                        msgs.setFormattedDate(date);
                    }
                    if (result_obj.has("user_id")) {
                        JSONObject user = getPosition(user_array, "id", result_obj.getString("user_id"));
                        if (user != null) {
                            User user_obj = new User();
                            user_obj.setId(user.has("id") ? user.getString("id") : "");
                            user_obj.setFirst_name(user.has("first_name") ? user.getString("first_name") : "");
                            user_obj.setLast_name(user.has("last_name") ? user.getString("last_name") : "");
                            user_obj.setImage(user.has("avatar_url") ? user.getString("avatar_url") : "");
                            user_obj.setName(user.has("name") ? user.getString("name") : "");
                            user_obj.setCompany(user.has("company_name") ? user.getString("company_name") : "");
                            user_obj.setLastSeen(user.has("last_seen") ? Util.timeAgo(user.getLong("last_seen")) : "");
                            msgs.setUser(user_obj);
                        }
                    } else if (result_obj.has("sender_id")) {
                        JSONObject user = getPosition(user_array, "id", result_obj.getString("sender_id"));
                        if (user != null) {
                            User user_obj = new User();
                            user_obj.setId(user.has("id") ? user.getString("id") : "");
                            user_obj.setFirst_name(user.has("first_name") ? user.getString("first_name") : "");
                            user_obj.setLast_name(user.has("last_name") ? user.getString("last_name") : "");
                            user_obj.setImage(user.has("avatar_url") ? user.getString("avatar_url") : "");
                            user_obj.setName(user.has("name") ? user.getString("name") : "");
                            user_obj.setCompany(user.has("company_name") ? user.getString("company_name") : "");
                            user_obj.setLastSeen(user.has("last_seen") ? Util.timeAgo(user.getLong("last_seen")) : "");
                            msgs.setUser(user_obj);
                        }
                        if (LOGIN_USER_ID != null && LOGIN_USER_ID.equals(result_obj.getString("sender_id"))) {
                            msgs.setType("outgoing");
                        } else {
                            msgs.setType("incoming");
                        }
                    }
                    msgs_array.add(msgs);
                }

                Map<Calendar, List<MessageData>> grouped = msgs_array
                        .stream()
                        .collect(Collectors.groupingBy(x -> {
                            Calendar cal = Calendar.getInstance();
                            cal.setTimeInMillis(x.getDataTime() * 1000);
                            cal.set(Calendar.HOUR_OF_DAY, 0);
                            cal.set(Calendar.MINUTE, 0);
                            cal.set(Calendar.SECOND, 0);
                            cal.set(Calendar.MILLISECOND, 0);
                            return cal;
                        }));
                Map<Calendar, List<MessageData>> sorted = grouped.entrySet().stream()
                        .sorted(comparingByKey()).collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2, LinkedHashMap::new));

                List<ChatData> listChat = new ArrayList<>();
                for (Map.Entry<Calendar, List<MessageData>> entry : sorted.entrySet()) {

                    ChatData item = new ChatData();
                    item.setType("0");
                    String pattern = "E dd MMMM yyyy";
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
                    String date = simpleDateFormat.format(entry.getKey().getTime());
                    item.setText(date);
                    item.setTime("");
                    listChat.add(item);
                    List<MessageData> sortedList = entry.getValue().stream()
                            .sorted(Comparator.comparingLong(MessageData::getDataTime))
                            .collect(Collectors.toList());
                    for (MessageData d : sortedList) {
                        item = new ChatData();
                        item.setId(d.getId());
                        item.setText(d.getText());
                        item.setUser_message_type(d.getUser_message_type());
                        item.setAtt_url(d.getAtt_url());
                        if (d.getAtt_file_size() != null && !d.getAtt_file_size().isEmpty()) {
                            long num = Long.valueOf(d.getAtt_file_size());
                            item.setAtt_file_size(getResources().getString(R.string.size) + " : " + FileUtil.getStringSizeLengthFile(num));
                        } else {
                            item.setAtt_file_size(getResources().getString(R.string.size) + " : 0b");
                        }
                        if (d.getType() != null && !d.getType().isEmpty()) {
                            item.setType(d.getType().equals("outgoing") ? "2" : "1");
                        }
                        if (d.getType() != null && !d.getType().isEmpty() && d.getType().equals("incoming") && d.getRead() == 1) {
                            unread_messages.add(d.getId());
                        }

                        item.setAtt_name(d.getAtt_name());
                        item.setContent_type(d.getContent_type());
                        item.setThumbnail(Util.getThumbNails(d.getContent_type()));
                        item.setConnection_type(_Type);
                        item.setThread_count(d.getThread_count());
                        item.setSender_name(d.getSender_name());
                        item.setRoom_id(d.getRoom_id());
                        item.setQuotedText(d.getQuotedText());
                        item.setUser(d.getUser());
                        item.setRead(d.getRead());
                        item.setEdit(d.getEdit());
                        String patternh = "hh:mm a";
                        simpleDateFormat = new SimpleDateFormat(patternh);
                        date = simpleDateFormat.format(new Date(d.getDataTime() * 1000));
                        item.setTime(date);
                        listChat.add(item);
                    }
                }
                if (_Type == 2) {
                    LogUtils.Print("COIMG_TO_ROOM", "ROOM");
                    markRoomMessagesAsRead(unread_messages);
                } else {
                    LogUtils.Print("COIMG_TO_CONNECTION", "CONNECTION");
                    markAsRead(unread_messages);
                }
                LogUtils.Print(TAG, "listChat.size() -> " + listChat.size());
                isMoreDataAvailable = listChat.size() >= pageSize;
                if (response.has("last_id")) {
                    onLoaded(response.getString("last_id"), listChat);
                } else {
                    onLoaded(null, listChat);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void markAsRead(List<String> unReadMsgs) {
        if (unReadMsgs == null || unReadMsgs.size() == 0) {
            return;
        }
        StringBuilder msgid = new StringBuilder();
        for (String name : unReadMsgs) {
            msgid = msgid.length() > 0 ? msgid.append(",").append(name) : msgid.append(name);
        }
        String url = Constants.MARK_MESSAGE_AS_READ_URL + "?status=0&msgid=" + msgid.toString();
        NetworkManager.customJsonObjectRequest(
                getApplicationContext(), url, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtils.Print("IREAD=", response.toString());
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ERRROR=", message);
                    }
                }, true);
    }

    private void markRoomMessagesAsRead(List<String> unReadMsgs) {
        if (unReadMsgs == null || unReadMsgs.size() == 0) {
            return;
        }
        StringBuilder msgid = new StringBuilder();
        for (String name : unReadMsgs) {
            msgid = msgid.length() > 0 ? msgid.append(",").append(name) : msgid.append(name);
        }

        String url = Constants.MARK_ROOM_MESSAGE_AS_READ_URL + "?status=0&msgid=" + msgid.toString();
        NetworkManager.customJsonObjectRequest(
                getApplicationContext(), url, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtils.Print("IREAD=", response.toString());
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ERRROR=", message);
                    }
                }, true);
    }

    private void muteRoomNotification(String room_id, String user_id, boolean mute) {
        String url = Constants.MUTE_ROOM_NOTIFICATION + "?room_id=" + room_id + "&user_id=" + user_id + "&mute=" + mute;
        NetworkManager.customJsonObjectRequest(
                getApplicationContext(), url, null,
                new VolleyResponseListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        menu_notifcation.setTitle(mute ?
                                getResources().getString(R.string.notification_muted) :
                                getResources().getString(R.string.notification_un_muted));
                        current_member.setMute_notifications(mute);
                        Util.onMessage(getApplicationContext(), mute ?
                                getResources().getString(R.string.notification_muted)
                                : getResources().getString(R.string.notification_un_muted));
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ERRROR=", message);
                    }
                }, true);

    }

    private JSONObject getPosition(JSONArray jsonArray, String attribute, String value) throws
            JSONException {
        for (int index = 0; index < jsonArray.length(); index++) {
            JSONObject jsonObject = jsonArray.getJSONObject(index);
            if (jsonObject.getString("id").equals(value)) {
                return jsonObject;
            }
        }
        return null;
    }

    private void onLoaded(String lastId, List<ChatData> listChat) {
        mAdapter.setSearch_text(searchText);
        mAdapter.notifyDataSetChanged();
        isScrolling = false;
        int lastSize = data.size();
        data.addAll(0, listChat);
        LogUtils.Print(TAG, "LIST SIZE --> " + data.size());
        if (data.size() > 0) {
            if (last_id == null) {
                //first time load, scroll to end
                if (mRecyclerView.getAdapter() != null &&
                        mRecyclerView.getAdapter().getItemCount() > 0) {
                    mRecyclerView.scrollToPosition(mRecyclerView.getAdapter().getItemCount() - 1);
                }
            } else {
                //second time load, scroll to previous list size position
                if (mRecyclerView.getAdapter() != null &&
                        mRecyclerView.getAdapter().getItemCount() > 0) {
                    mRecyclerView.scrollToPosition(data.size() - lastSize);
                }
            }
        }
        last_id = lastId;
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_conversation, menu);
        final MenuItem menu_search = menu.findItem(R.id.search_messages);
        final MenuItem menu_members = menu.findItem(R.id.open_members);
        final MenuItem menu_photo = menu.findItem(R.id.user_photo);
        final MenuItem menuLanguage = menu.findItem(R.id.item_language);
        ivDefaultLanguage = menuLanguage.getActionView().findViewById(R.id.ivDefaultLanguage);
        ivDefaultLanguage.setOnClickListener(this);
        tvSelectedLanguage = menuLanguage.getActionView().findViewById(R.id.tvSelectedLanguage);
        tvSelectedLanguage.setOnClickListener(this);
        if (_LANG != null && !_LANG.equals("")) {
            ivDeleteLang.setVisibility(View.VISIBLE);
            ivDefaultLanguage.setVisibility(View.GONE);
            tvSelectedLanguage.setVisibility(View.VISIBLE);
            tvSelectedLanguage.setText(_LANG);
        } else {
            ivDeleteLang.setVisibility(View.GONE);
            ivDefaultLanguage.setVisibility(View.VISIBLE);
            tvSelectedLanguage.setVisibility(View.GONE);
        }
        menu_notifcation = menu.findItem(R.id.mute_notification);
        menu_delete = menu.findItem(R.id.menu_delete);
        menu_delete.setVisible(_Type == CONNECTION);
        menu_attachments = menu.findItem(R.id.menu_attachments);
        menu_attachments.setVisible(_Type == CONNECTION || _Type == ROOM);
        menu_notifcation.setVisible(_Type != CLIENT);
        if (_Type == CONNECTION) {
            menu_notifcation.setTitle(_MUTE_NOT == 0L ?
                    getResources().getString(R.string.mute_notifications) :
                    getResources().getString(R.string.unmute_notification));
        }
        final MenuItem menu_appointment = menu.findItem(R.id.create_appointment);
        if (_Type == ROOM) {
            menu_photo.setVisible(false);
            menu_members.setVisible(true);
            menu_appointment.setVisible(true);
            menuLanguage.setVisible(false);
        } else {
            menu_members.setVisible(false);
            menu_photo.setVisible(true);
            menu_appointment.setVisible(false);
            menuLanguage.setVisible(true);

            if (_IMAGE != null && !_IMAGE.isEmpty() && !_IMAGE.equals("null")) {
                RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .circleCropTransform();

                Glide
                        .with(this)
                        .load(_IMAGE)
                        .apply(options)
                        .into(new CustomTarget<Drawable>() {
                            @Override
                            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                                menu_photo.setIcon(resource);
                            }

                            @Override
                            public void onLoadCleared(@Nullable Drawable placeholder) {

                            }
                        });
            } else if (_NAME != null && !_NAME.isEmpty()) {
                menu_photo.setTitle(_NAME.charAt(0) + "");
            }
        }
        SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView search = (SearchView) menu_search.getActionView();
        search.setOnSearchClickListener(v -> setItemsVisibility(menu, menu_search, false));

        search.setSearchableInfo(manager.getSearchableInfo(getComponentName()));

        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchText = query;
                last_id = null;
                searchMessage();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        search.setOnCloseListener(() -> {
            searchText = null;
            if (_Type == ROOM) {
                menu_members.setVisible(true);
            } else {
                menu_photo.setVisible(true);
            }
            if (_ID != null && _Type == ROOM) {
                last_id = null;
                getRoomMessage();
            } else if (_ID != null && _Type == CLIENT) {
                last_id = null;
                getConnectionMessage();
            } else if (_ID != null && _Type == CONNECTION) {
                last_id = null;
                getConnectionMessage();
            }
            return false;
        });
        return true;
    }

    private void setItemsVisibility(Menu menu, MenuItem exception, boolean visible) {
        for (int i = 0; i < menu.size(); ++i) {
            MenuItem item = menu.getItem(i);
            if (item != exception) item.setVisible(visible);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.open_members) {
            Intent intent = new Intent(getApplicationContext(), RoomDetailActivity.class);
            intent.putExtra("_ID", _ID);
            intent.putExtra("_NAME", _NAME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        if (id == R.id.user_photo) {
            subscribe.dispose();
            Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
            intent.putExtra("USER_ID", _ID);
            intent.putExtra("NAME", _NAME);
            intent.putExtra("IMAGE", _IMAGE);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        if (id == R.id.mute_notification) {
            if (_Type == ROOM) {
                if (current_member != null) {
                    muteRoomNotification(_ID, LOGIN_USER_ID, !current_member.isMute_notifications());
                } else {
                    muteRoomNotification(_ID, LOGIN_USER_ID, true);
                }
            } else {
                //open popup
                openMuteNotificationPopup();
            }
        }
        if (id == R.id.create_appointment) {
            Intent intent = new Intent(this, NewEventActivity.class);
            intent.putExtra("ROOM_MEMBER", (Serializable) _MEMBERS);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        if (id == R.id.video_call) {
            if (_Type == CONNECTION) {
                if (!_STATUS.equals("Available")) {
                    Util.onMessage(this, _NAME + " " + getResources().getString(R.string.is_not_available));
                    return false;
                }
            }
            isUserEligibleForVideoCall();
        }
        if (id == R.id.item_language) {
            //OPEN LANGUAGE SEARCH BAR
            clSearch.setVisibility(clSearch.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
        }
        if (id == R.id.menu_delete) {
            DialogUtils.showConfirmationDialog(this, new DialogButtonClickListener() {
                @Override
                public void onPositiveButtonClick() {
                    removeChatMessages();
                }
            }, getResources().getString(R.string.delete_chat_confirmation).replace("X", "" + _NAME));
        }
        if (id == R.id.menu_attachments) {
            Intent intent = new Intent(getApplicationContext(), AttachmentActivity.class);
            intent.putExtra("ENTITY_KEY", _Type == CONNECTION ? "to_entity_id" : "room_id");
            intent.putExtra("FROM", _Type == CONNECTION ? "FROM_CONNECTION" : "FROM_ROOM");
            intent.putExtra("ENTITY_ID", _ID);
            intent.putExtra("ENTITY_TYPE", "message");
            startActivityForResult(intent, UPDATE_ATTACHMENT);
        }
        return super.onOptionsItemSelected(item);
    }

    private void openMuteNotificationPopup() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(Conversation.this);
        String[] items;
        if (_MUTE_NOT == 0L) {
            alertDialog.setTitle(getResources().getString(R.string.mute_notification_for));
            items = new String[]{getResources().getString(R.string.hours_8),
                    getResources().getString(R.string.week_1),
                    getResources().getString(R.string.year_1)};
        } else {
            items = new String[]{getResources().getString(R.string.unmute)};
        }
        selItem = 0;
        alertDialog.setSingleChoiceItems(items, selItem, (dialog, which) -> {
            switch (which) {
                case 0://hours_8
                    selItem = 0;
                    break;
                case 1://week_1
                    selItem = 1;
                    break;
                case 2://year_1
                    selItem = 2;
                    break;
            }
        });
        alertDialog.setPositiveButton("OK", (dialog, which) -> {
            if (_MUTE_NOT != 0L) {
                //call unmute API
                muteConnectionNotification(2);
            } else {
                // user clicked OK
                muteConnectionNotification(1);
            }
        });
        alertDialog.setNegativeButton("Cancel", null);

        AlertDialog alert = alertDialog.create();
        alert.setCanceledOnTouchOutside(false);
        alert.show();
    }

    /**
     * 1 = Add, 2 = remove
     *
     * @param from
     */
    private void muteConnectionNotification(int from) {
        JSONObject postData = new JSONObject();
        try {
            if (from == 1) {
                if (selItem == 0) {//hours_8
                    postData.put(_ID, System.currentTimeMillis() / 1000 + 8 * 3600);
                } else if (selItem == 1) {//week_1
                    postData.put(_ID, System.currentTimeMillis() / 1000 + (7 * 24 * 3600));
                } else {//year_1
                    postData.put(_ID, System.currentTimeMillis() / 1000 + (365 * 24 * 3600));
                }
            } else {
                postData.put(_ID, "");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String apiURL = Constants.UPDATE_PROFILE;
        apiURL += (from == 1) ? "?add_mute_chat_notification_dict=" : "?remove_mute_chat_notification_dict=";
        apiURL += postData;
        LogUtils.Print(TAG, "postData --> " + apiURL);
        showProgress();
        NetworkManager.customJsonObjectRequest(
                getApplicationContext(), apiURL, null,
                new VolleyResponseListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        hideProgress();
                        LogUtils.Print(TAG, "onResponse --> " + response);
                        try {
                            if (Integer.parseInt(response.getString("result")) == 1) {
                                //update mute time locally for change button.
                                if (from == 1) {
                                    _MUTE_NOT = 1L;
                                } else {
                                    _MUTE_NOT = 0L;
                                }
                                if (menu_notifcation != null)
                                    menu_notifcation.setTitle(_MUTE_NOT == 0L ?
                                            getResources().getString(R.string.mute_notifications) :
                                            getResources().getString(R.string.unmute_notification));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String message) {
                        hideProgress();
                        LogUtils.Print("ERRROR=", message);
                    }
                }, false);
    }

    private void removeChatMessages() {
        String url = Constants.MESSAGES_DELETE + "?userid=" + _ID;
        NetworkManager.customJsonObjectRequest(
                getApplicationContext(), url, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtils.Print(TAG, "response-> " + response);
                        try {
                            if (Integer.parseInt(response.getString("result")) == 1) {
                                if (data.size() > 0)
                                    data.clear();
                                mAdapter.notifyDataSetChanged();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ERRROR=", message);
                    }
                }, true);
    }

    private void isUserEligibleForVideoCall() {
        String url = Constants.VIDEO_ELIGIBILITY;
        JSONObject postData = new JSONObject();
        try {
            postData.put("userid", SharedPreferenceData.getInstance(getApplicationContext()).getLoggedInUserID());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        NetworkManager.customJsonObjectRequest(
                getApplicationContext(), url, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (_Type == ROOM) {
                            Intent intent = new Intent(Conversation.this, CallActivity.class);
                            intent.putExtra("_NAME", _NAME);
                            intent.putExtra("_USER_ID", LOGIN_USER_ID);
                            intent.putExtra("_LOGIN_USER_NAME", LOGIN_USER_NAME);
                            intent.putExtra("_OPO_USER_ID", _ID);
                            intent.putExtra("_FROM", 1);
                            intent.putExtra(ConstantApp.ACTION_KEY_CHANNEL_NAME, LOGIN_USER_ID + _ID);
                            intent.putExtra(ConstantApp.ACTION_KEY_ENCRYPTION_KEY, "xdL_encr_key_");
                            intent.putExtra(ConstantApp.ACTION_KEY_ENCRYPTION_MODE, "AES-256-XTS");
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(Conversation.this, VideoChatViewActivity.class);
                            intent.putExtra("_NAME", _NAME);
                            intent.putExtra("_USER_ID", LOGIN_USER_ID);
                            intent.putExtra("_LOGIN_USER_NAME", LOGIN_USER_NAME);
                            intent.putExtra("_OPO_USER_ID", _ID);
                            intent.putExtra("_CHANNEL_NAME", LOGIN_USER_ID + _ID);
                            intent.putExtra("_FROM", 1);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    }

                    @Override
                    public void onError(String message) {
                        Util.onMessage(Conversation.this, message);
                        LogUtils.Print("ERRROR=", message);
                    }
                }, true);
    }

    private void processTyping(String response) {
        try {
            JSONObject input = new JSONObject(response);
            if (input.has("type") && input.getString("type").equals("activity_typing")) {
                String sender_id = input.has("sender_id") ? input.getString("sender_id") : "";
                if (sender_id.equals(_ID)) {
                    runOnUiThread(() -> {
                        toolbar.setSubtitle(getResources().getString(R.string.typing_));
                        toolbar.postDelayed(() -> toolbar.setSubtitle(getResources().getString(R.string.reading_)), 5000);
                    });
                }
            } else if (input.has("type") && input.getString("type").equals("contact_logout")) {
                String user_id = input.has("user_id") ? input.getString("user_id") : "";
                if (user_id.equals(_ID)) {
                    runOnUiThread(() -> toolbar.setSubtitle(""));
                }
            } else if (input.has("type") && input.getString("type").equals("contact_appinactive")) {
                String user_id = input.has("user_id") ? input.getString("user_id") : "";
                if (user_id.equals(_ID)) {
                    runOnUiThread(() -> toolbar.setSubtitle(""));
                }
            } else if (input.has("type") && input.getString("type").equals("contact_appactive")) {
                String user_id = input.has("user_id") ? input.getString("user_id") : "";
                if (user_id.equals(_ID)) {
                    runOnUiThread(() -> toolbar.setSubtitle(getResources().getString(R.string.reading)));
                }
            } else if (input.has("type") && input.getString("type").equals("contact_login")) {
                String user_id = input.has("user_id") ? input.getString("user_id") : "";
                if (user_id.equals(_ID)) {
                    runOnUiThread(() -> toolbar.setSubtitle(""));
                }
            } else if (input.has("type") && input.getString("type").equals("user_status_available")) {
                String user_id = input.has("user_id") ? input.getString("user_id") : "";
                if (user_id.equals(_ID)) {
                    runOnUiThread(() -> toolbar.setTitleTextColor(Color.WHITE));
                }
            } else if (input.has("type") && input.getString("type").equals("user_status_busy")) {
                String user_id = input.has("user_id") ? input.getString("user_id") : "";
                if (user_id.equals(_ID)) {
                    runOnUiThread(() -> toolbar.setTitleTextColor(Color.RED));
                }
            } else if (input.has("type") && input.getString("type").equals("message_read")) {
                String recipient_id = input.has("recipient_id") ? input.getString("recipient_id") : "";

                if (_ID.equals(recipient_id)) {
                    for (ChatData message : data) {
                        message.setRead(0);
                    }
                }
                runOnUiThread(() -> {
                    mAdapter.notifyDataSetChanged();
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void receivedData(String response) {
        LogUtils.Print("CONV: SOCKET MESSAGE RECEIVED RESPONSE=", response);
        try {
            JSONObject input = new JSONObject(response);
            if (input.has("type") && (input.getString("type").equals("message") || input.getString("type").equals("room_message"))) {
                LogUtils.Print(TAG, "sender_id -> " + input.get("sender_id"));
                LogUtils.Print(TAG, "_ID -> " + _ID);
                if (_Type == CONNECTION && input.has("sender_id") &&
                        !input.get("sender_id").equals(_ID)) {
                    return;
                }
                if (_Type == ROOM && input.has("room_id") &&
                        !input.get("room_id").equals(_ID)) {
                    return;
                }
                List<ChatData> data = new ArrayList<ChatData>();
                ChatData item = new ChatData();
                item.setSender_name(input.has("sender_nickname") ? input.getString("sender_nickname") : "");
                String pattern = "hh:mm";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
                String date = simpleDateFormat.format(new Date().getTime());
                item.setTime(date);
                item.setType("1"); //0 date 1 You 2 Me
                item.setRoom_id(input.has("room_id") ? input.getString("room_id") : "");
                item.setContent_type(input.has("content_type") ? input.getString("content_type") : "");
                item.setAtt_url(input.has("att_url") ? input.getString("att_url") : "");
                item.setAtt_file_size(FileUtil.getStringSizeLengthFile(input.has("att_filesize") ? input.getLong("att_filesize") : 0));
                item.setText(input.has("message") ? input.getString("message") : "");
                User user = new User();
                user.setId(input.has("sender_id") ? input.getString("sender_id") : "");
                user.setImage(input.has("avatar_url") ? input.getString("avatar_url") : "");
                user.setName(input.has("sender_nickname") ? input.getString("sender_nickname") : "");
                if (input.has("sender_nickname")) {
                    String[] strName = input.getString("sender_nickname").split(" ");
                    user.setFirst_name(strName.length > 0 ? strName[0] : "");
                    user.setLast_name(strName.length > 1 ? strName[1] : "");
                }
                item.setUser(user);
                data.add(item);
                List<String> unread_messages = new ArrayList<>();
                if (input.has("recipient_msgid")) {
                    unread_messages.add(input.getString("recipient_msgid"));
                    markAsRead(unread_messages);
                }
                mAdapter.addItem(data);
                mAdapter.notifyDataSetChanged();
                runOnUiThread(() -> {
//                    runOnUiThread(() -> mRecyclerView.scrollToPosition(data.size() - 1));
                    if (mRecyclerView.getAdapter() != null &&
                            mRecyclerView.getAdapter().getItemCount() > 0) {
                        mRecyclerView.scrollToPosition(mRecyclerView.getAdapter().getItemCount() - 1);
                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void notifyTyping() {
        JSONObject intro_data = new JSONObject();
        try {
            intro_data.put("type", "activity_typing");
            intro_data.put("sender_id", LOGIN_USER_ID);
            intro_data.put("recipient_id", _ID);
            WsManager ws = WSSingleton.getInstanceWM(getApplicationContext());
            if (ws.isWsConnected()) {
                ws.sendMessage(intro_data.toString());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getRoomInfo(String room_id) {
        String url = Constants.GET_ROOM_INFO + "?room_id=" + room_id;
        NetworkManager.customJsonObjectRequest(
                getApplicationContext(), url, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processRoomData(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ERRROR=", message);
                    }
                }, true);
    }

    private void processRoomData(JSONObject response) {
        _ROOM = new Room();
        try {
            if (Integer.parseInt(response.getString("result")) == 1) {
                JSONObject room_object = response.getJSONObject("room");
                if (room_object.has("room_name")) {
                    _ROOM.setName(room_object.getString("room_name"));
                }
                if (room_object.has("description")) {
                    _ROOM.setDesc(room_object.getString("description"));
                }
                if (room_object.has("room_id")) {
                    _ROOM.set_Id(room_object.getString("room_id"));
                }
                if (room_object.has("room_phone")) {
                    _ROOM.setPhone(room_object.getString("room_phone"));
                }
                if (room_object.has("updated_at")) {
                    _ROOM.setUpdatedOn(room_object.getLong("updated_at"));
                }
                if (room_object.has("members")) {
                    List<Member> members = new ArrayList<>();
                    JSONObject member_object = new JSONObject();
                    JSONArray room_members_response = room_object.getJSONArray("members");
                    for (int i = 0; i < room_members_response.length(); i++) {

                        member_object = room_members_response.getJSONObject(i);
                        Member member = new Member();

                        if (member_object.has("member_role")) {
                            member.setRole(member_object.getInt("member_role"));
                        }
                        if (member_object.has("mute_notifications")) {
                            member.setMute_notifications(member_object.getBoolean("mute_notifications"));
                        }
                        if (member_object.has("user_id")) {
                            member.set_Id(member_object.getString("user_id"));
                        }
                        if (member_object.has("member_email")) {
                            member.setEmail(member_object.getString("member_email"));
                        }
                        if (member_object.has("avatar_url")) {
                            member.setmImage(member_object.getString("avatar_url"));
                        }
                        if (member_object.has("member_profile")) {
                            String name = "";
                            JSONObject profile = member_object.getJSONObject("member_profile");
                            if (profile.has("first_name")) {
                                name = profile.getString("first_name");
                            }
                            if (profile.has("last_name")) {
                                name = name + " " + profile.getString("last_name");
                            }
                            member.setmName(name);
                            member.set_Id(member_object.getString("user_id"));
                        }
                        members.add(member);
                    }
                    _ROOM.setMembers(members);
                }
                _MEMBERS = _ROOM.getMembers();
                List<Member> filteredMembers = _MEMBERS.stream()
                        .filter(x -> x.get_Id().equals(LOGIN_USER_ID))
                        .collect(Collectors.toList());
                if (filteredMembers.size() > 0) {
                    current_member = filteredMembers.get(0);
                }
                if (menu_notifcation != null) {
                    if (current_member.isMute_notifications()) {
                        menu_notifcation.setIcon(R.drawable.bell_off);
                        menu_notifcation.setTitle(getResources().getString(R.string.notification_muted));
                    } else {
                        menu_notifcation.setIcon(R.drawable.bell);
                        menu_notifcation.setTitle(getResources().getString(R.string.mute_notification));
                    }
                }
            }
        } catch (JSONException e) {
            e.getMessage();
            LogUtils.Print("Error", e.toString());
        }
    }

    @Override
    public void onBackPressed() {
        subscribe.dispose();
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        subscribe.dispose();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onClick(View view) {
        if (view == ivDeleteLang) {
            DialogUtils.showConfirmationDialog(this, () -> updateUserLanguage(null), getResources().getString(R.string.delete_language_confirmation));
        } else if (view == ivDefaultLanguage) {
            clSearch.setVisibility(clSearch.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
        } else if (view == tvSelectedLanguage) {
            clSearch.setVisibility(clSearch.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
        } else if (view == fab) {
            if (mRecyclerView.getAdapter() != null &&
                    mRecyclerView.getAdapter().getItemCount() > 0) {
                mRecyclerView.postDelayed(() -> mRecyclerView.scrollToPosition(
                        mRecyclerView.getAdapter().getItemCount() - 1), 100);
            }
        }
    }

    /**
     * PARSING JASON FROM ASSETS
     */
    private void getLanguagesFromAssets() {
        int position = -1;
        try {
            JSONArray jsonArray = new JSONArray(loadJSONFromAsset());
            allLanguages = new ArrayList<>();
            AllLanguages languages;
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                languages = new AllLanguages();
                languages.setAlpha2(jsonObject.has("alpha2") ? jsonObject.getString("alpha2") : "");
                languages.setEnglish(jsonObject.has("English") ? jsonObject.getString("English") : "");
                if (_LANG != null && jsonObject.has("alpha2") && _LANG.equals(jsonObject.get("alpha2"))) {
                    position = i;
                    languages.setSelect(true);
                } else {
                    languages.setSelect(false);
                }
                allLanguages.add(languages);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        setUpAdapterView(position);
    }

    private String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getAssets().open("codebeautify.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private void setUpAdapterView(int selPosition) {
        AutoCompleteAdapter adapter = new AutoCompleteAdapter(this, android.R.layout.simple_dropdown_item_1line, allLanguages);
        etSearchLang.setAdapter(adapter);
        if (selPosition != -1)
            etSearchLang.setText(allLanguages.get(selPosition).getEnglish(), false);
        etSearchLang.setOnItemClickListener((parent, arg1, position, arg3) -> {
            Object item = parent.getItemAtPosition(position);
            if (item instanceof AllLanguages) {
                AllLanguages data = (AllLanguages) item;
                DialogUtils.showConfirmationDialog(this, new DialogButtonClickListener() {
                            @Override
                            public void onPositiveButtonClick() {
                                updateUserLanguage(data);
                            }

                            @Override
                            public void onNegativeButtonClick() {

                            }
                        }, getResources().getString(R.string.change_language_confirmation)
                                .replace("XFLName", _NAME)
                                .replace("ZLang", data.getEnglish()),
                        getResources().getString(R.string.yes),
                        getResources().getString(R.string.no));
            }
        });
    }

    private void updateUserLanguage(AllLanguages data) {
        JSONObject postData = new JSONObject();
        try {
            JSONArray jsonArray = new JSONArray();
            jsonArray.put("");
            jsonArray.put(data == null ? "" : data.getAlpha2());
            postData.put(_ID, jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String strURLPrefix = data == null ? "remove_translation_settings_dict" : "add_translation_settings_dict";
        LogUtils.Print("updateUserLanguage -> ", postData.toString());
        NetworkManager.customJsonObjectRequest(this,
                Constants.UPDATE_PROFILE + "?" + strURLPrefix + "=" + postData, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtils.Print(TAG, response.toString());
                        Util.hideSoftKeyboard(etSearchLang);
                        try {
                            if (Integer.parseInt(response.getString("result")) == 1) {
                                clSearch.setVisibility(View.GONE);
                                if (data == null) {
                                    _LANG = "";
                                    tvSelectedLanguage.setVisibility(View.GONE);
                                    ivDefaultLanguage.setVisibility(View.VISIBLE);
                                    ivDeleteLang.setVisibility(View.GONE);
                                    Util.makeToast(getResources().getString(R.string.delete_language_success));
                                    etSearchLang.setText("", false);
                                } else {
                                    ivDefaultLanguage.setVisibility(View.GONE);
                                    _LANG = data.getAlpha2();
                                    tvSelectedLanguage.setVisibility(View.VISIBLE);
                                    tvSelectedLanguage.setText(_LANG);
                                    ivDeleteLang.setVisibility(View.VISIBLE);
                                    Util.makeToast(getResources().getString(R.string.change_language_success));
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print(TAG, message);
                    }
                }, false);
    }
}