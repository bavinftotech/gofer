package com.gofercrm.user.gofercrm.settings.timezone;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.settings.timezone.model.DataTimeZone;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class TimeZoneActivity extends BaseActivity implements TimeZoneAdapter.ClickListener {

    //view
    private EditText etSearch;
    private RecyclerView rv;

    //adapter
    private TimeZoneAdapter adp;
    private List<DataTimeZone> list = new ArrayList<>();
    private List<DataTimeZone> listAll = new ArrayList<>();
    private String _TIMEZONE = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_zone);
        getDataFromIntent();
        findViewById();
        setUpAdapterView();
        main();
    }

    private void getDataFromIntent() {
        if (getIntent() != null && getIntent().hasExtra("_TIMEZONE")) {
            _TIMEZONE = getIntent().getStringExtra("_TIMEZONE");
        }
    }

    private void findViewById() {
        etSearch = findViewById(R.id.etSearch);
        rv = findViewById(R.id.rv);
    }

    private void setUpAdapterView() {
        try {
            JSONArray jsonArray = new JSONArray(loadJSONFromAsset());
            list = new ArrayList<>();
            listAll = new ArrayList<>();
            DataTimeZone timeZone;
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                timeZone = new DataTimeZone();
                timeZone.setTimeZone(jsonObject.has("timezone") ? jsonObject.getString("timezone") : "");
                timeZone.setSelect(_TIMEZONE != null && jsonObject.has("timezone") && _TIMEZONE.equals(jsonObject.get("timezone")));
                list.add(timeZone);
                listAll.add(timeZone);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this));
        adp = new TimeZoneAdapter(list, this);
        rv.setAdapter(adp);
    }

    private void main() {
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                list.clear();
                if (charSequence.toString().length() == 0) {
                    list.addAll(listAll);
                } else {
                    for (int j = 0; j < listAll.size(); j++) {
                        if (listAll.get(j).getTimeZone().toLowerCase().contains(charSequence.toString().toLowerCase())) {
                            list.add(listAll.get(j));
                        }
                    }
                }
                adp.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etSearch.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                hideSoftKeyboard(v);
                return true;
            }
            return false;
        });
    }

    private String loadJSONFromAsset() {
        String json;
        try {
            InputStream is = getAssets().open("timzone.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    /**
     * RECYCLER VIEW ITEM CLICK
     *
     * @param position
     */
    @Override
    public void onItemClicked(int position) {
        if (list.get(position).isSelect()) return;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).isSelect()) {
                list.get(i).setSelect(false);
                adp.notifyItemChanged(i);
                break;
            }
        }
        list.get(position).setSelect(!list.get(position).isSelect());
        adp.notifyItemChanged(position);

        Intent intent = new Intent();
        intent.putExtra("timzone", list.get(position).getTimeZone());
        setResult(RESULT_OK, intent);
        finish();
    }

    /**
     * CLOSE BUTTON CLICK
     *
     * @param view
     */
    public void onCloseButtonClick(View view) {
        hideSoftKeyboard(etSearch);
        finish();
    }

    private void hideSoftKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null)
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}