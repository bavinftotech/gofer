package com.gofercrm.user.gofercrm.settings.language.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.implementor.RecyclerViewClickListener;
import com.gofercrm.user.gofercrm.settings.language.model.DataLanguage;

import java.util.List;

public class LanguageAdapter extends RecyclerView.Adapter<LanguageAdapter.CommentsViewHolder> {

    private List<DataLanguage> list;
    private Activity activity;

    public LanguageAdapter(List<DataLanguage> list, Activity activity) {
        this.list = list;
        this.activity = activity;
    }

    @NonNull
    @Override
    public LanguageAdapter.CommentsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_language, parent, false);
        return new LanguageAdapter.CommentsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull LanguageAdapter.CommentsViewHolder holder, int position) {
        DataLanguage data = list.get(position);
        holder.tv.setText(data.getLanguage());
        holder.tv.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                data.isSelect() ? R.drawable.ic_check : 0, 0);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class CommentsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView tv;

        public CommentsViewHolder(View view) {
            super(view);
            tv = view.findViewById(R.id.tv);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view == itemView) {
                if (getAdapterPosition() != RecyclerView.NO_POSITION) {
                    if (listener != null) {
                        listener.onItemClick(0, getAdapterPosition(), view);
                    }
                }
            }
        }
    }

    private RecyclerViewClickListener listener;

    public void setOnRecyclerViewItemClickListener(RecyclerViewClickListener clickListener) {
        listener = clickListener;
    }
}