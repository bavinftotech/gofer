package com.gofercrm.user.gofercrm.weather;

import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.PopupWindow;

import androidx.appcompat.app.AppCompatActivity;

import com.gofercrm.user.gofercrm.R;


public class WeatherActivity extends AppCompatActivity {

    private PopupWindow mPopupWindow;
    private boolean isPopupShowing = false;
    private int cord_left = 0;
    private int cord_bottom = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Window w = getWindow();
//        w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
//                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
//
//        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.O) {
//            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
//        } else {
//            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//        }
        setContentView(R.layout.activity_weather);
        cord_left = getIntent().getIntExtra("cord_left", 0);
        cord_bottom = getIntent().getIntExtra("cord_bottom", 0);
        openPopup();
    }

    private void closePopup() {
        if (mPopupWindow != null) {
            if (mPopupWindow.isShowing()) {
                mPopupWindow.dismiss();
                isPopupShowing = false;
                mPopupWindow = null;
            }
        }
    }

    /**
     * Open calendar popup
     */
    private void openPopup() {
        if (mPopupWindow != null) {
            if (!isPopupShowing)
                openFilterDialog();
        } else {
            openFilterDialog();
        }
    }

    /**
     * OPEN FILTER DIALOG
     */
    public void openFilterDialog() {
        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);

        View customView = inflater.inflate(R.layout.activity_weather_popup, null);
        mPopupWindow = new PopupWindow(
                customView,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
//        mPopupWindow.setAnimationStyle(R.style.CustomAnimationTopToBottom);

        // Closes the popup window when touch outside.
        mPopupWindow.setOutsideTouchable(false);
        mPopupWindow.setFocusable(true);
        // Removes default background.
        mPopupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        mPopupWindow.setOnDismissListener(() -> {
            isPopupShowing = false;
            onBackPressed();
        });

        try {
            new Handler(Looper.getMainLooper()).postDelayed(() ->
//                            mPopupWindow.showAtLocation(customView, Gravity.TOP | Gravity.END,
//                                    cord_left, cord_bottom),
                            mPopupWindow.showAsDropDown(customView, 0, 0, Gravity.END),
                    200L);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        closePopup();
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        closePopup();
    }
}