package com.gofercrm.user.gofercrm.account;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AccountContactsFragment extends Fragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private RecyclerView recyclerView;
    private List<AccountContact> contactLists = new ArrayList<>();
    private AccountViewActivity activity;
    private String last_id = null;
    private ContactListAdapter cAdapter;
    private ContactBSFragment bottomSheetFragment;

    private boolean isScrolling = false;
    private boolean isMoreDataAvailable = true;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ProgressBar paginationProgressBar;
    private TextView no_contact_records;

    public AccountContactsFragment() {
    }

    public static AccountContactsFragment newInstance(int sectionNumber) {
        AccountContactsFragment fragment = new AccountContactsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rv = inflater.inflate(R.layout.contact_tab_account_view, container, false);
        activity = (AccountViewActivity) getActivity();

        mSwipeRefreshLayout = rv.findViewById(R.id.ac_swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

//        bottomAppBar = rv.findViewById(R.id.acbottomAppBar);
        bottomSheetFragment = new ContactBSFragment();
        //    private BottomAppBar bottomAppBar;
        FloatingActionButton fab = rv.findViewById(R.id.account_contact_fab);
        fab.setOnClickListener(view -> {
            bottomSheetFragment.setCurrentObj(bottomSheetFragment);
            bottomSheetFragment.show(getFragmentManager(), bottomSheetFragment.getTag());
        });

        paginationProgressBar = rv.findViewById(R.id.contact_list_pagination_progress);
        no_contact_records = rv.findViewById(R.id.no_lead_records);
        last_id = null;
        getContacts();

        recyclerView = rv.findViewById(R.id.account_contacts_recycler);
        recyclerView.setVisibility(View.GONE);
        cAdapter = new ContactListAdapter(contactLists, activity.getApplicationContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(cAdapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NotNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NotNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
                    final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                            .getLayoutManager();
                    int totalItemCount = linearLayoutManager.getItemCount();
                    int lastVisibleItem = linearLayoutManager
                            .findLastVisibleItemPosition();
                    if (isMoreDataAvailable && !isScrolling && totalItemCount <= (lastVisibleItem + 5)) {
                        isScrolling = true;
                        getContacts();
                    }
                }
            }
        });
        return rv;
    }

    private void getContacts() {
        mSwipeRefreshLayout.setRefreshing(true);
        JSONObject postData = new JSONObject();
        try {
            postData.put("action", "get");
            postData.put("page_size", "20");
            postData.put("account_id", activity.getAccount_Id());
            if (last_id != null) {
                postData.put("last_id", last_id);
                paginationProgressBar.setVisibility(View.VISIBLE);
            } else {
                contactLists.clear();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        NetworkManager.customJsonObjectRequest(
                Objects.requireNonNull(getContext()), Constants.GET_ACCOUNT_CONTACTS_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        processData(response);
                    }

                    @Override
                    public void onError(String message) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        LogUtils.Print("ERRROR=", message);
                    }
                }, true);
    }

    private void processData(JSONObject data) {
        try {
            AccountContact contact;
            if (Integer.parseInt(data.getString("result")) == 1) {
                JSONObject result_Data = data.getJSONObject("data");
                last_id = result_Data.getString("last_id");
                JSONArray contacts = result_Data.getJSONArray("contacts");
                for (int i = 0; i < contacts.length(); i++) {
                    JSONObject obj = contacts.getJSONObject(i);
                    contact = new AccountContact();
                    contact.setId(obj.getString("id"));
                    contact.setAccount_id(obj.getString("account_id"));
                    contact.setFirst_name(obj.getString("first_name"));
                    contact.setLast_name(obj.getString("last_name"));
                    contact.setCompany_name(obj.getString("company_name"));
                    contact.setEmail(obj.getString("email"));
                    contact.setPhone(obj.getString("phone"));
                    contact.setTitle(obj.getString("title"));
                    contact.setStatus(obj.getString("status"));
                    contactLists.add(contact);
                }
                isMoreDataAvailable = contacts.length() > 0;
                onLoaded();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void onLoaded() {
        if (contactLists.size() > 0) {
            paginationProgressBar.setVisibility(View.GONE);
            isScrolling = false;
            recyclerView.setVisibility(View.VISIBLE);
            cAdapter.notifyDataSetChanged();
            no_contact_records.setVisibility(View.GONE);
        } else {
            no_contact_records.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
    }

    @Override
    public void onRefresh() {
        contactLists.clear();
        last_id = null;
        paginationProgressBar.setVisibility(View.GONE);
        getContacts();
    }
}