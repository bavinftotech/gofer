package com.gofercrm.user.gofercrm.lead;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.NavigationActivity;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.entity.Address;
import com.gofercrm.user.gofercrm.entity.Comment;
import com.gofercrm.user.gofercrm.entity.Email;
import com.gofercrm.user.gofercrm.entity.LeadOwner;
import com.gofercrm.user.gofercrm.entity.MatchPreference;
import com.gofercrm.user.gofercrm.entity.Phone;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class LeadListActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    TextView no_lead_records;
    boolean isScrolling = false;
    String last_id = null;
    boolean isMoreDataAvailable = true;
    boolean isOnSearch = false;
    SwipeRefreshLayout mSwipeRefreshLayout;
    private List<LeadList> leadLists = new ArrayList<>();
    private RecyclerView recyclerView;
    private LeadListAdapter lAdapter;
    private ProgressBar progressBar;
    private ProgressBar paginationProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lead_list);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(getResources().getString(R.string.leads));
        }
        mSwipeRefreshLayout = findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        no_lead_records = findViewById(R.id.no_lead_records);

        recyclerView = findViewById(R.id.lead_recycle);
        progressBar = findViewById(R.id.lead_list_progressBar);
        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        lAdapter = new LeadListAdapter(leadLists, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(lAdapter);

        prepareLeadData();

        paginationProgressBar = findViewById(R.id.lead_list_pagination_progress);

        FloatingActionButton fab = findViewById(R.id.lead_view_create_id);
        fab.setOnClickListener(view -> {
            Intent leadIntent = new Intent(getApplicationContext(), LeadProcessActivity.class);
            leadIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(leadIntent);
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NotNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(@NotNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

                    final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                            .getLayoutManager();
                    int totalItemCount = linearLayoutManager.getItemCount();
                    int lastVisibleItem = linearLayoutManager
                            .findLastVisibleItemPosition();
                    if (isMoreDataAvailable && !isOnSearch && !isScrolling && totalItemCount <= (lastVisibleItem + 5)) {
                        isScrolling = true;
                        prepareLeadData();
                    }
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, NavigationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void prepareLeadData() {
        JSONObject postData = new JSONObject();
        try {
            postData.put("action", "get");
            postData.put("page_size", "20");
            if (last_id != null) {
                postData.put("last_id", last_id);
                paginationProgressBar.setVisibility(View.VISIBLE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        NetworkManager.customJsonObjectRequest(
                LeadListActivity.this, Constants.LEAD_LIST_URL, postData,
                new VolleyResponseListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        processData(response);
                    }

                    @Override
                    public void onError(String message) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        LogUtils.Print("ERRROR=", message);
                    }

                }, true);
    }

    private void searchLeads(String query) {
        String url = Constants.SEARCH_LEAD_URL + "?all_words=false&search_all=true&search_text=" + query;
        NetworkManager.customJsonObjectRequest(
                LeadListActivity.this, url, null,
                new VolleyResponseListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        processData(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("LEAD SEARCH ERROR=", message);
                    }
                }, false);
    }

    private void onLoaded() {
        if (leadLists.size() > 0) {
            progressBar.setVisibility(View.GONE);
            paginationProgressBar.setVisibility(View.GONE);
            isScrolling = false;
            recyclerView.setVisibility(View.VISIBLE);
            lAdapter.notifyDataSetChanged();
            no_lead_records.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.GONE);
            no_lead_records.setVisibility(View.VISIBLE);
        }
    }

    private void processData(JSONObject data) {
        try {
            LeadList lead;
            if (Integer.parseInt(data.getString("result")) == 1) {
                JSONObject result_Data = data.getJSONObject("data");
                last_id = result_Data.getString("last_id");
                JSONArray leads = result_Data.getJSONArray("lead_contacts");
                for (int i = 0; i < leads.length(); i++) {

                    JSONObject obj = leads.getJSONObject(i);
                    JSONArray emailArray = obj.getJSONArray("email_list");
                    List<Email> emails = new ArrayList<>();
                    for (int j = 0; j < emailArray.length(); j++) {
                        JSONObject emailObj = emailArray.getJSONObject(j);
                        emails.add(new Email(emailObj.getString("email"), emailObj.getString("type")));
                    }
                    JSONArray phoneArray = obj.getJSONArray("phone_list");
                    List<Phone> phones = new ArrayList<>();
                    for (int j = 0; j < phoneArray.length(); j++) {
                        JSONObject phoneObj = phoneArray.getJSONObject(j);
                        phones.add(new Phone(phoneObj.getString("phone"), phoneObj.getString("type")));
                    }
                    JSONArray addressArray = obj.getJSONArray("address_list");
                    List<Address> addresses = new ArrayList();
                    for (int j = 0; j < addressArray.length(); j++) {
                        JSONObject addressObj = addressArray.getJSONObject(j);
                        addresses.add(new Address(addressObj.getString("address"), addressObj.getString("type")));
                    }
                    List<Comment> comments = new ArrayList();

                    JSONArray commentsArray = new JSONArray();
                    if (obj.has("comments")) {
                        Object comments_obj = obj.get("comments");
                        if (comments_obj instanceof JSONArray) {
                            commentsArray = (JSONArray) comments_obj;
                        }

                        for (int j = 0; j < commentsArray.length(); j++) {
                            try {
                                JSONObject commentObj = commentsArray.getJSONObject(j);
                                comments.add(new Comment(commentObj.getString("comment"), commentObj.getString("date"), commentObj.has("url") ? commentObj.getString("url") : "",
                                        commentObj.has("content_type") ? commentObj.getString("content_type") : ""));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    JSONObject lOObj = obj.getJSONObject("lead_owner");
                    LeadOwner leadOwner = new LeadOwner(lOObj.getString("id"),
                            lOObj.getString("name"));

                    JSONObject mpObj = obj.getJSONObject("match_preferences_dict");
                    MatchPreference mp = new MatchPreference();
                    if (mpObj.has("lease_length")) {
                        mp.setLease_length(Integer.parseInt(mpObj.getString("lease_length")));
                    }
                    if (mpObj.has("bedrooms")) {
                        mp.setBeds(mpObj.getInt("bedrooms"));
                    }
                    if (mpObj.has("bedrooms")) {
                        mp.setBeds(mpObj.getInt("bedrooms"));
                    }
                    if (mpObj.has("bathrooms")) {
                        mp.setBathrooms(mpObj.getInt("bathrooms"));
                    }
                    if (mpObj.has("budget")) {
                        mp.setBudget(mpObj.getDouble("budget"));
                    }

                    lead = new LeadList(obj.getString("id"),
                            obj.getString("name"),
                            obj.getString("lead_status"),
                            obj.getString("file_as"),
                            obj.getString("lead_source"),
                            comments,
                            addresses,
                            emails,
                            phones,
                            leadOwner,
                            mp,
                            obj.getString("contact_id"),
                            obj.getBoolean("marketing_campaign_enabled"),
                            obj.getLong("created_ts"),
                            obj.getLong("status_update_date")
                    );
                    leadLists.add(lead);

                }
                isMoreDataAvailable = leads.length() > 0;
                onLoaded();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_lead, menu);
        SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView search = (SearchView) menu.findItem(R.id.lead_search).getActionView();
        search.setSearchableInfo(manager.getSearchableInfo(getComponentName()));

        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                isOnSearch = true;

                no_lead_records.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                leadLists.clear();
                last_id = null;
                search.setIconified(true);
                searchLeads(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        search.setOnCloseListener(() -> {
            isOnSearch = false;
            refreshLeadList();
            return false;
        });
        return true;
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(true);
        refreshLeadList();
    }

    private void refreshLeadList() {
        last_id = null;
        leadLists.clear();
        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        prepareLeadData();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LogUtils.Print("requestCode --> ", "" + requestCode);
        LogUtils.Print("resultCode --> ", "" + resultCode);
        if (requestCode == 222) {
            if (resultCode == RESULT_OK) {
                refreshLeadList();
            }
        }
    }
}