package com.gofercrm.user.gofercrm.volleynetworkutil;

import org.json.JSONObject;

public interface VolleyResponseListener {
    void onError(String message);

    void onResponse(JSONObject response);
}