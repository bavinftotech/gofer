package com.gofercrm.user.gofercrm.account;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class AccountViewPagerAdapter extends FragmentPagerAdapter {

    public AccountViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return AccountGeneralFragment.newInstance(0);
            case 1:
                return AccountCommentsFragment.newInstance(1);
            case 2:
                return AccountContactsFragment.newInstance(2);
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }
}