package com.gofercrm.user.gofercrm.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.gofercrm.user.gofercrm.ApplicationContext;
import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.NavigationActivity;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.passwordmanagement.ResetPasswordActivity;
import com.gofercrm.user.gofercrm.tenantcomment.ToastActivity;
import com.gofercrm.user.gofercrm.util.DeviceInfo;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.SharedPreferenceData;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleySingleton;
import com.gofercrm.user.gofercrm.websocket.WSListener;
import com.gofercrm.user.gofercrm.websocket.WSSingleton;
import com.gofercrm.user.gofercrm.websocket.WsManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class LoginActivity_ extends BaseActivity implements View.OnClickListener, WSListener {
    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;
    private static final int RC_SIGN_IN = 1;

    ImageView ivContactUs, ivInfo;
    EditText _emailText;
    EditText _passwordText;
    Button _loginButton;
    TextView _forgot_passwordBtn;
    TextView _signupLink;
    //    ProgressDialog progressDialog;
    SharedPreferences sharedPref;
    //    SignInButton signInButton;
    String REFRESH_TOKEN;

//    GoogleSignInClient mGoogleSignInClient;

    GoogleSignInOptions gso;

    Map<String, String> personal_dict = new HashMap<>();

    private GoogleSignInClient mGoogleSignInClient;
    private ImageView ivGoogle;
    public static final int REQUEST_GOOGLE_LOGIN = 610;

    @Override
    public void onStart() {
        super.onStart();
//        progressDialog = new ProgressDialog(this);
//        progressDialog.setCanceledOnTouchOutside(false);
//        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                .requestScopes(new Scope(Scopes.APP_STATE))
//                .requestServerAuthCode(getString(R.string.server_client_id_dev))
//                .requestIdToken(getString(R.string.server_client_id_dev))
//                .requestEmail()
//                .build();
//        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
//        mGoogleSignInClient.silentSignIn()
//                .addOnCompleteListener(this, new OnCompleteListener<GoogleSignInAccount>() {
//                    @Override
//                    public void onComplete(@NonNull Task<GoogleSignInAccount> task) {
//                        handleSignInResult(task);
//                    }
//                });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_);
//        signInButton = findViewById(R.id.google_sign_in_button);
//        signInButton.setSize(SignInButton.SIZE_WIDE);
//        TextView textView = (TextView) signInButton.getChildAt(0);
//        textView.setText("Sign in with Google");
//        signInButton.setOnClickListener(this);

        ivContactUs = findViewById(R.id.ivContactUs);
        ivInfo = findViewById(R.id.ivInfo);
        _emailText = findViewById(R.id.input_email);
        _passwordText = findViewById(R.id.input_password);
        _loginButton = findViewById(R.id.btn_login);
        sharedPref = getSharedPreferences("LOGIN", MODE_PRIVATE);

        _loginButton.setOnClickListener(v -> login());

        _forgot_passwordBtn = findViewById(R.id.fp_btn_id);
        _forgot_passwordBtn.setOnClickListener(view -> {
            Intent fpintent = new Intent(getApplicationContext(), ResetPasswordActivity.class);
            fpintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(fpintent);
        });

        ivGoogle = findViewById(R.id.ivGoogle);
        ivGoogle.setOnClickListener(this);
        ivContactUs.setOnClickListener(this);
        ivInfo.setOnClickListener(this);
        setUpGoogle();
    }

    public void login() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null && getCurrentFocus() != null && getCurrentFocus().getWindowToken() != null) {
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                    InputMethodManager.RESULT_UNCHANGED_SHOWN);
        }
        LogUtils.Print(TAG, "ILogin");

        if (!validate()) {
            onLoginFailed(getResources().getString(R.string.login_failed));
            return;
        }

        _loginButton.setEnabled(false);
//        progressDialog.setMessage("Authenticating...");
//        progressDialog.show();
        showProgress();

        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();
        try {
            performLoginOperation(email, password);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        // disable going back to the MainActivity
        moveTaskToBack(true);
    }

    public void onLoginSuccess() {
        _loginButton.setEnabled(true);
//        finish();
    }

    public void onLoginFailed(String text) {
        Toast.makeText(getBaseContext(), text, Toast.LENGTH_LONG).show();
        _loginButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError(getResources().getString(R.string.email_invalid));
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (password.isEmpty()) {
            _passwordText.setError(getResources().getString(R.string.password_is_empty));
            valid = false;
        } else {
            _passwordText.setError(null);
        }
        return valid;
    }

    private void performLoginOperation(final String email, final String password) throws Exception {
        StringRequest request = new StringRequest(Request.Method.POST,
                Constants.Login_URL,
                response -> {
                    processResponse(response);
                    LogUtils.Print("Login response", response);
//                    progressDialog.dismiss();
                    hideProgress();
                }, e -> {
            LogUtils.Print("Login Output", e.toString());
//            progressDialog.dismiss();
            hideProgress();
            onLoginFailed(getResources().getString(R.string.error_in_login));
            e.printStackTrace();
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("password", password);
                params.put("os_version", DeviceInfo.getAndroidVersion());
                params.put("os", "android");
                params.put("device_id", DeviceInfo.getDeviceID(getApplicationContext()));
                params.put("device_meta", DeviceInfo.getDeviceName());
                params.put("is_mobile", "true");
                return params;
            }
        };
        request.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 500000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 500000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        RequestQueue queue = VolleySingleton.getInstance(getApplicationContext()).getRequestQueue();
        queue.add(request);
    }

    private void processResponse(String response) {
        try {
            JSONObject object = new JSONObject(response);
            if (Integer.parseInt(object.getString("result")) == 1) {
                JSONObject userObject = object.getJSONObject("user");
                saveDataAndNavigateToNextScreen(userObject);
            } else {
                onLoginFailed(getResources().getString(R.string.invalid_credentials));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void saveDataAndNavigateToNextScreen(JSONObject userObject) {
        try {
//            JSONObject userObject = object.getJSONObject("user");
            //int accountLevel = userObject.getInt("account_level");
            String userName = userObject.getString("name");
            String userFullName = userName;
            boolean is_tenant_admin = userObject.getBoolean("is_tenant_admin");
            String email = userObject.getString("email");
            String imageURL = "";
            String tenant_id = "";
            String tenantName = "";
            String tenantImageURL = "";
            String client_connection_id = "";
            boolean softphone_enabled = false;
            String sip_phone = "";
            String virtual_phone = "";

            if (userObject.has("softphone_enabled")) {
                softphone_enabled = userObject.getBoolean("softphone_enabled");
            }
            if (userObject.has("virtual_phone")) {
                virtual_phone = userObject.getString("virtual_phone");
            }
            if (userObject.has("sip_creds_dict")) {
                JSONObject obj = userObject.getJSONObject("sip_creds_dict");
                if (obj.has("sip_phone")) {
                    sip_phone = obj.getString("sip_phone");
                }
            }
            if (userObject.has("tenant_id")) {
                tenant_id = userObject.getString("tenant_id");
            }
            if (userObject.has("avatar_url")) {
                imageURL = userObject.getString("avatar_url");
                LogUtils.Print(TAG, "imageURL --> " + imageURL);
            }
            if (userObject.has("client_connection_id")) {
                client_connection_id = userObject.getString("client_connection_id");
            }
            if (userObject.has("formatted_name")) {
                userFullName = userObject.getString("formatted_name");
            }
            if (userObject.has("tenant_dict")) {
                JSONObject tenantObject = userObject.getJSONObject("tenant_dict");
                tenantName = tenantObject.getString("company_name");
                if (tenantName.length() == 0) {
                    tenantName = userFullName;
                }
                tenantImageURL = tenantObject.getString("avatar_url");
                if (tenantImageURL.length() == 0) {
                    tenantImageURL = imageURL;
                }

                if (tenantObject.has("personalization_dict")) {
                    JSONObject personalization_dict = tenantObject.getJSONObject("personalization_dict");
                    sharedPref.edit().putString("PERSONAL_DICT", personalization_dict.toString()).apply();
                    if (personalization_dict.has("display_labels")) {
                        JSONObject pd = personalization_dict.getJSONObject("display_labels");
                        ((ApplicationContext) this.getApplication()).setPersonal_dict(pd);
                        personal_dict = Util.toMap(pd);
                    }
                }
            }

            if (userObject.has("calendars")) {
                JSONArray calendars = userObject.getJSONArray("calendars");
                if (calendars.length() > 0) {
                    JSONObject jsonObject = (JSONObject) calendars.get(0);
                    if (jsonObject.has("provider")) {
                        sharedPref.edit().putString("PROVIDER", jsonObject.getString("provider")).apply();
                    }
                    if (jsonObject.has("provider_uid")) {
                        sharedPref.edit().putString("PROVIDERID", jsonObject.getString("provider_uid")).apply();
                    }
                }

            }

            if (userObject.has("status"))
                sharedPref.edit().putString("STATUS",
                        userObject.getString("status")).apply();
            if (userObject.has("preferred_language")) {
                String lang = userObject.getString("preferred_language");
                if (!sharedPref.getString(ApplicationContext.key_lang, "").equals(lang)) {
                    ApplicationContext.updateResources(this, lang);
                }
            }
            if (userObject.has("timeZone"))
                sharedPref.edit().putString("_TIMEZONE", userObject.getString("timeZone")).apply();

            if (userObject.has("public_profile")) {
                JSONObject jsonObject = userObject.getJSONObject("public_profile");
                if (jsonObject.has("cell_visibility"))
                    sharedPref.edit().putBoolean("cell_visibility", jsonObject.getBoolean("cell_visibility")).apply();
                if (jsonObject.has("email_visibility"))
                    sharedPref.edit().putBoolean("email_visibility", jsonObject.getBoolean("email_visibility")).apply();
                if (jsonObject.has("display_on_website"))
                    sharedPref.edit().putBoolean("display_on_website", jsonObject.getBoolean("display_on_website")).apply();
                if (jsonObject.has("allow_contact_public_profile"))
                    sharedPref.edit().putBoolean("allow_contact_public_profile", jsonObject.getBoolean("allow_contact_public_profile")).apply();
            }

//            Util.makeToast(userObject.getJSONObject("module_access").toString());
            if (userObject.has("module_access")) {
                JSONObject objModuleAccess = userObject.getJSONObject("module_access");
                if (objModuleAccess.has("all") && objModuleAccess.getString("all").equals("*")) {
                    sharedPref.edit().putBoolean("ACCESS_LEAD", true).apply();
                    sharedPref.edit().putBoolean("ACCESS_CONTACT", true).apply();
                    sharedPref.edit().putBoolean("ACCESS_TASK", true).apply();
                    sharedPref.edit().putBoolean("ACCESS_ACCOUNT", true).apply();
                    sharedPref.edit().putBoolean("ACCESS_OPPORTUNITY", true).apply();
                } else {
                    sharedPref.edit().putBoolean("ACCESS_LEAD", setModuleAccess(objModuleAccess, "lead")).apply();
                    sharedPref.edit().putBoolean("ACCESS_CONTACT", setModuleAccess(objModuleAccess, "contact")).apply();
                    sharedPref.edit().putBoolean("ACCESS_TASK", setModuleAccess(objModuleAccess, "task")).apply();
                    sharedPref.edit().putBoolean("ACCESS_ACCOUNT", setModuleAccess(objModuleAccess, "account")).apply();
                    sharedPref.edit().putBoolean("ACCESS_OPPORTUNITY", setModuleAccess(objModuleAccess, "opportunity")).apply();
                }
            }
            sharedPref.edit().putString("TENANT_NAME", tenantName).apply();
            sharedPref.edit().putString("CLIENT_CONNECTION_ID", client_connection_id).apply();
            sharedPref.edit().putString("tenant_id", tenant_id).apply();
            sharedPref.edit().putString("TENANT_IMAGE_URL", tenantImageURL).apply();
            //sharedPref.edit().putInt("ACCOUNT_LEVEL", accountLevel).apply();
            sharedPref.edit().putString("USER_NAME", userName).apply();
            sharedPref.edit().putString("USER_FORMATTED_NAME", userFullName).apply();
            sharedPref.edit().putString("USER_EMAIL", email).apply();
            sharedPref.edit().putBoolean("IS_TENANT_ADMIN", is_tenant_admin).apply();
            sharedPref.edit().putString("USER_ID", userObject.getString("id")).apply();
            sharedPref.edit().putBoolean("IS_ADMIN", userObject.getBoolean("is_tenant_admin")).apply();
            sharedPref.edit().putString("USER_TOKEN", userObject.getString("token")).apply();
            sharedPref.edit().putString("USER_IMAGEURL", imageURL).apply();
            sharedPref.edit().putBoolean("softphone_enabled", softphone_enabled).apply();
            sharedPref.edit().putString("sip_phone", sip_phone).apply();
            sharedPref.edit().putString("virtual_phone", virtual_phone).apply();
//            ((ApplicationContext) this.getApplication()).setPersonal_dict(personal_dict);
            WsManager ws = WSSingleton.getInstanceWM(getApplicationContext());
            ws.startConnect();
            getFCMToken();
            Intent intent = new Intent(getApplicationContext(), NavigationActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private boolean setModuleAccess(JSONObject jsonObject, String moduleName) {
        try {
            if (jsonObject.has(moduleName)) {
                StringBuilder strArr = new StringBuilder();
                JSONArray arr = jsonObject.getJSONArray(moduleName);
                if (arr.length() > 0) {
                    for (int i = 0; i < arr.length(); i++) {
                        strArr.append(arr.getString(i));
                    }
                }
                return strArr.toString().equals("CRUD");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        if (v == ivGoogle) {
            signIn();
        } else if (v == ivContactUs) {
            startActivity(new Intent(this, ContactUsActivity.class));
        } else if (v == ivInfo) {
            Intent intent = new Intent(this, ToastActivity.class);
            intent.putExtra("data", getResources().getString(R.string.login_info));
            intent.putExtra("from", ToastActivity.forLoginInfo);
            startActivity(intent);
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    }

    private void getUserImage(final GoogleSignInAccount account) {
//        progressDialog.setMessage("Authenticating...");
//        progressDialog.show();
        showProgress();
        OkHttpClient client = new OkHttpClient();
        RequestBody requestBody = new FormBody.Builder()
                .add("grant_type", "authorization_code")
                .add("client_id", getString(R.string.server_client_id_dev))
                .add("client_secret", "iecl5JzpRSAIWEr_D08B8Ant")
                .add("redirect_uri", "")
                .add("code", account.getServerAuthCode())
                .build();
        final okhttp3.Request request = new okhttp3.Request.Builder()
                .url("https://www.googleapis.com/oauth2/v4/token")
                .post(requestBody)
                .build();
        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LogUtils.Print("GOOGLE_TOKENS_ERROR", e.toString());
            }

            @Override
            public void onResponse(Call call, okhttp3.Response response) throws IOException {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String rt = "";
                    if (jsonObject.has("refresh_token")) {
                        rt = jsonObject.getString("refresh_token");
                    }

                    final String message = jsonObject.toString(5);
                    checkTokenOnServer(jsonObject.getString("id_token"),
                            jsonObject.getString("access_token")
                            , rt);
                } catch (JSONException e) {
//                    progressDialog.dismiss();
                    hideProgress();
                    e.printStackTrace();
                }
            }
        });
    }

    private void setLoginMethod(int signInIdentifier) {
        if (sharedPref != null) {
            sharedPref.edit().putInt("SIGN_IN_IDENTIFIER", signInIdentifier).apply();
        }
    }

    private void checkTokenOnServer(final String id_token, final String
            access_token, final String refresh_token) {
        String URL = Constants.TOKEN_REQUEST_URL + "?state={" + "\"acct_type\"" + ":0}&access_token=" + access_token + "&id_token=" + id_token + "&refresh_token=" + refresh_token;
        OkHttpClient client = new OkHttpClient();
        final okhttp3.Request request = new okhttp3.Request.Builder()
                .url(URL)
                .get()
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                LogUtils.Print("GOOGLE_TOKENS_ERROR", e.toString());
//                progressDialog.dismiss();
                hideProgress();
            }

            @Override
            public void onResponse(Call call, okhttp3.Response response) throws IOException {
//                progressDialog.dismiss();
                hideProgress();
                String result = response.body().string();
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (Integer.parseInt(jsonObject.getString("result")) == 1) {
                        saveDataAndNavigateToNextScreen(jsonObject);
                    } else {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                if (jsonObject.has("msg")) {
                                    try {
                                        Util.onMessage(LoginActivity_.this, jsonObject.getString("msg"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    Util.onMessage(LoginActivity_.this, getResources().getString(R.string.server_err));
                                }
                            }
                        });
                    }

//                    JSONObject msgObject = jsonObject.getJSONObject("msg");
//                    Iterator<?> keys = msgObject.keys();
//                    String keyID = "";
//                    while (keys.hasNext()) {
//                        String key = (String) keys.next();
//                        if (msgObject.get(key) instanceof JSONObject) {
//                            keyID = key;
//                        }
//                        break;
//                    }
//                    JSONObject userObject = msgObject.getJSONObject(keyID);
//                    String userName = userObject.getString("name");
//                    String email = userObject.getString("email");
//                    String imageURL = userObject.getString("picture");
//
//                    sharedPref.edit().putString("USER_NAME", userName).apply();
//                    sharedPref.edit().putString("USER_EMAIL", email).apply();
//                    sharedPref.edit().putString("USER_TOKEN", jsonObject.getString("ambivo_token")).apply();
//                    sharedPref.edit().putString("USER_IMAGEURL", imageURL).apply();
//                    Intent intent = new Intent(getApplicationContext(), NavigationActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(intent);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void getFCMToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        LogUtils.Print(TAG, "getInstanceId failed" + task.getException());
                        return;
                    }
                    // Get new Instance ID token
                    String token = task.getResult().getToken();
                    LogUtils.Print(TAG, "token --> " + token);
                    SharedPreferenceData.getInstance(LoginActivity_.this).setFCMToken(token);

                    updateProfile(token);
                    String msg = getString(R.string.msg_token_fmt, token);
                    LogUtils.Print(TAG, msg);
                });
    }

    private void updateProfile(String fcm_token) {
        JSONObject postData = new JSONObject();
        String deviceId = DeviceInfo.getDeviceID(getApplicationContext());
        String req = "{\"" + deviceId + "\":\"" + fcm_token + "\"}";
        try {
            postData.put("push_notification_dict", req);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        LogUtils.Print(TAG, "postData --> " + postData);
        NetworkManager.customJsonObjectRequest(
                LoginActivity_.this, Constants.UPDATE_PROFILE + "?push_notification_dict=" + req + "&connection_id =" + SharedPreferenceData.getInstance(getApplicationContext()).getClientConnectionID() + "&os=android" + "&os_version=" + DeviceInfo.getAndroidVersion() + "&device_meta=" + DeviceInfo.getDeviceName(), null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtils.Print("RESPONSE", response.toString());
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("UPDATE PROFILE ERROR", message);
                    }
                }, false);
    }

    @Override
    public void onResult(String message) {

    }

    private void setUpGoogle() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                .requestIdToken("887708581481-nm9jtvt6i5dvd8js57vlkok5l568mvdd.apps.googleusercontent.com")
//                .requestIdToken("887708581481-b1as274si6quevfuugrqusk7bbqtl45n.apps.googleusercontent.com")
                .requestIdToken(Constants.getClientId())
                .requestId()
                .requestEmail()
                .requestProfile()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    private void signIn() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(new Scope("https://www.googleapis.com/auth/calendar.readonly"))
                .requestScopes(new Scope("https://www.googleapis.com/auth/calendar.events"))
                .requestServerAuthCode(Constants.clientId3)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, REQUEST_GOOGLE_LOGIN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_GOOGLE_LOGIN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(@NonNull Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            if (account != null) {
                String authCode = account.getServerAuthCode();
                LogUtils.Print(TAG, "authCode" + authCode);
                signUpTenantCB(authCode);
            } else {
                Util.onMessage(this, getResources().getString(R.string.server_err));
            }
        } catch (ApiException e) {
            LogUtils.Print(TAG, "signInResult:failed code=" + e.getStatusCode());
            Util.onMessage(this, getResources().getString(R.string.server_err));
        }
    }

    private void signUpTenantCB(String authCode) {
        String URL = Constants.googleSignupTenantCB +
                "?code=" + authCode +
                "&mobile_client_type=android" +
                "&mobile_client_id=" + Constants.getClientId() +
                "&os=android" +
                "&os_version=" + DeviceInfo.getAndroidVersion() +
                "&device_meta=" + DeviceInfo.getDeviceName();
        LogUtils.Print(TAG, "URL --> " + URL);
        showProgress();
        NetworkManager.customJsonObjectRequest(this, URL, null, new VolleyResponseListener() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                hideProgress();
                LogUtils.Print(TAG, jsonObject.toString());
                try {
                    if (Integer.parseInt(jsonObject.getString("result")) == 1) {
                        saveDataAndNavigateToNextScreen(jsonObject);
                    } else {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                if (jsonObject.has("msg")) {
                                    try {
                                        Util.onMessage(LoginActivity_.this, jsonObject.getString("msg"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    Util.onMessage(LoginActivity_.this, getResources().getString(R.string.server_err));
                                }
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String message) {
                hideProgress();
                LogUtils.Print(TAG, message);
            }
        }, false);
    }

    private void updateUI(GoogleSignInAccount account) {
//        getGoogleExtraTokens(account);
        checkTokenOnServer(account.getIdToken(),
                ""
                , "");
    }


    private void getGoogleExtraTokens(final GoogleSignInAccount account) {
//        progressDialog.setMessage("Authenticating...");
//        progressDialog.show();
        showProgress();
        OkHttpClient client = new OkHttpClient();
        RequestBody requestBody = new FormBody.Builder()
                .add("grant_type", "authorization_code")
                .add("client_id", getString(R.string.server_client_id_dev))
                .add("client_secret", "iecl5JzpRSAIWEr_D08B8Ant")
                .add("redirect_uri", "")
                .add("code", account.getServerAuthCode())
                .build();
        final okhttp3.Request request = new okhttp3.Request.Builder()
                .url("https://www.googleapis.com/oauth2/v4/token")
                .post(requestBody)
                .build();
        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                LogUtils.Print("GOOGLE_TOKENS_ERROR", e.toString());
            }

            @Override
            public void onResponse(Call call, okhttp3.Response response) throws IOException {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String rt = "";
                    if (jsonObject.has("refresh_token")) {
                        rt = jsonObject.getString("refresh_token");
                    }

                    final String message = jsonObject.toString(5);
                    checkTokenOnServer(jsonObject.getString("id_token"),
                            jsonObject.getString("access_token")
                            , rt);
                } catch (JSONException e) {
//                    progressDialog.dismiss();
                    hideProgress();
                    e.printStackTrace();
                }
            }
        });
    }
}