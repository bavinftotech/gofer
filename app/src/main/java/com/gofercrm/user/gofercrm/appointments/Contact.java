package com.gofercrm.user.gofercrm.appointments;

import java.io.Serializable;

public class Contact implements Serializable {
    private String id;
    private String name;
    private String email;
    private String phone;
    private boolean select;

    public Contact(String _id, String n, String e, String p) {
        id = _id;
        name = n;
        email = e;
        phone = p;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    @Override
    public String toString() {
        return name;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }
}