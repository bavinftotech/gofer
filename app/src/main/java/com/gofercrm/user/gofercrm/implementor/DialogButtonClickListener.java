package com.gofercrm.user.gofercrm.implementor;


public interface DialogButtonClickListener {
    void onPositiveButtonClick();
    default void onNegativeButtonClick(){};
}
