package com.gofercrm.user.gofercrm.task.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.appointments.Contact;
import com.gofercrm.user.gofercrm.databinding.ContentTaskUserFilterBinding;
import com.gofercrm.user.gofercrm.implementor.RecyclerViewClickListener;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class TaskUserFilterAdapter extends RecyclerView.Adapter<TaskUserFilterAdapter.OpportunityListViewHolder> {

    private List<Contact> contacts;
    private Activity activity;

    public TaskUserFilterAdapter(List<Contact> contacts, Activity activity) {
        this.contacts = contacts;
        this.activity = activity;
    }

    @NotNull
    @Override
    public OpportunityListViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        ContentTaskUserFilterBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.content_task_user_filter, parent, false);
        return new OpportunityListViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(OpportunityListViewHolder holder, final int position) {
        final Contact data = contacts.get(position);
        holder.binding.setData(data);
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public class OpportunityListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ContentTaskUserFilterBinding binding;

        public OpportunityListViewHolder(ContentTaskUserFilterBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view == itemView) {
                if (getAdapterPosition() != RecyclerView.NO_POSITION) {
                    if (listener != null) {
                        listener.onItemClick(0, getAdapterPosition(), view);
                    }
                }
            }
        }
    }

    private RecyclerViewClickListener listener;

    public void setOnRecyclerViewItemClickListener(RecyclerViewClickListener clickListener) {
        listener = clickListener;
    }
}