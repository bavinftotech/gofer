package com.gofercrm.user.gofercrm.lead.upload;

public interface DialogUserInputListener {
    void onDialogUserInput(String strText);

    default void onDialogUserCancelInput(){};
}