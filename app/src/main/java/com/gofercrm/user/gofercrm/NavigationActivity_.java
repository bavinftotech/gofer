package com.gofercrm.user.gofercrm;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.provider.Settings;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gofercrm.user.gofercrm.account.AccountListActivity;
import com.gofercrm.user.gofercrm.account.AccountProcessActivity;
import com.gofercrm.user.gofercrm.appointments.AppointmentsActivity;
import com.gofercrm.user.gofercrm.appointments.EventsAdapter;
import com.gofercrm.user.gofercrm.appointments.NewEventActivity;
import com.gofercrm.user.gofercrm.chat.ChatActivity;
import com.gofercrm.user.gofercrm.chat.ui.main.BulletEventListener;
import com.gofercrm.user.gofercrm.clients.ClientListActivity;
import com.gofercrm.user.gofercrm.clients.ClientProcessActivity;
import com.gofercrm.user.gofercrm.implementor.DialogButtonClickListener;
import com.gofercrm.user.gofercrm.lead.LeadListActivity;
import com.gofercrm.user.gofercrm.lead.LeadProcessActivity;
import com.gofercrm.user.gofercrm.login.LoginActivity;
import com.gofercrm.user.gofercrm.opportunities.ManageOpportunityActivity;
import com.gofercrm.user.gofercrm.opportunities.OpportunityActivity;
import com.gofercrm.user.gofercrm.settings.SettingActivity;
import com.gofercrm.user.gofercrm.settings.language.LanguageActivity;
import com.gofercrm.user.gofercrm.streaming.ui.StreamingActivity;
import com.gofercrm.user.gofercrm.streaming.ui.model.DataSteaming;
import com.gofercrm.user.gofercrm.task.NewTaskActivity;
import com.gofercrm.user.gofercrm.task.TaskActivity;
import com.gofercrm.user.gofercrm.tenantcomment.TenantCommentActivity;
import com.gofercrm.user.gofercrm.util.DataBindingUtils;
import com.gofercrm.user.gofercrm.util.DeviceInfo;
import com.gofercrm.user.gofercrm.util.DialogUtils;
import com.gofercrm.user.gofercrm.util.FileUtil;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.SharedPreferenceData;
import com.gofercrm.user.gofercrm.util.TimeAgo_;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.gofercrm.user.gofercrm.websocket.WSSingleton;
import com.gofercrm.user.gofercrm.websocket.WsManager;
import com.google.android.material.navigation.NavigationView;
import com.jaiselrahman.filepicker.activity.FilePickerActivity;
import com.jaiselrahman.filepicker.config.Configurations;
import com.jaiselrahman.filepicker.model.MediaFile;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.threeten.bp.LocalDate;
import org.threeten.bp.temporal.TemporalAdjusters;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class NavigationActivity_ extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {
    private static final String TAG = "NavigationActivity";
    SharedPreferences sharedPref;
    boolean doubleBackToExitPressedOnce = false;
    String startDate;
    String endDate;
    List<JSONObject> event_objects = new ArrayList<>();
    EventsAdapter eventsAdapter;
    RecyclerView event_recycler_view;
    TextView landing_no_events, tvTenantMessage, tvTenantTitle;
    CardView cardTenantMsg;
    ProgressBar navigation_events_progressBar;
    ImageView refreshEvents, ivRefreshTenant;
    LinearLayout llRefresh;
    ImageView new_appointment_btn, new_lead_btn, new_task_btn, new_client_btn, new_opportunity_btn, new_account_btn;
    ImageView imageProfile;
    //    LinearLayout llLeadsClients, llAppointmentsTasks, llAccountsOpportunities;
    private String strAvatarId = "";
    private String strAvatarURL = "";
    private JSONObject personal_dict;
    private LinearLayout llContainer;

    //Streaming
    private MenuItem menuStreaming;
    private static DataSteaming dataSteaming;
    private static String LIVE_PLAYER_MEDIA_URL_SOURCE = "";
    private int STREAMING_REQUEST_CODE = 100;
    public static MediaPlayer myMediaPlayer;
    private ImageView ivPlayPause;
    private boolean isAudioCompleted = false;
    private static int stateEmpty = 0;
    private static int statePlaying = 1;
    private static int statePause = 2;
    private static int stateStop = 3;
    private static int playerState = stateEmpty;
    //PopupWindow
    private PopupWindow mPopupWindow;
    private boolean isPopupShowing = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.home));
        setSupportActionBar(toolbar);
        getDataFromIntent();
        personal_dict = ((ApplicationContext) this.getApplication()).getPersonal_dict();
        sharedPref = getSharedPreferences("LOGIN", MODE_PRIVATE);
        llContainer = findViewById(R.id.llContainer);
        refreshEvents = findViewById(R.id.refreshEvents);
        llRefresh = findViewById(R.id.llRefresh);
        refreshEvents.setOnClickListener(this);
        llRefresh.setOnClickListener(this);

        new_appointment_btn = findViewById(R.id.new_appointment_btn);
        new_appointment_btn.setOnClickListener(this);

        CardView cardLeads = findViewById(R.id.cardLeads);
        cardLeads.setVisibility(sharedPref.getBoolean("ACCESS_LEAD", false) ? View.VISIBLE : View.GONE);
        CardView cardClients = findViewById(R.id.cardClients);
        cardClients.setVisibility(sharedPref.getBoolean("ACCESS_CONTACT", false) ? View.VISIBLE : View.GONE);
        CardView cardTasks = findViewById(R.id.cardTasks);
        cardTasks.setVisibility(sharedPref.getBoolean("ACCESS_TASK", false) ? View.VISIBLE : View.GONE);
        CardView cardAccounts = findViewById(R.id.cardAccounts);
        cardAccounts.setVisibility(sharedPref.getBoolean("ACCESS_ACCOUNT", false) ? View.VISIBLE : View.GONE);
        CardView cardOpportunities = findViewById(R.id.cardOpportunities);
        cardOpportunities.setVisibility(sharedPref.getBoolean("ACCESS_OPPORTUNITY", false) ? View.VISIBLE : View.GONE);

//        llLeadsClients = findViewById(R.id.llLeadsClients);
//        llAppointmentsTasks = findViewById(R.id.llAppointmentsTasks);
//        llAccountsOpportunities = findViewById(R.id.llAccountsOpportunities);
//        llLeadsClients.setVisibility(SharedPreferenceData.getInstance(this).isFullAccessAccount() ?
//                View.VISIBLE : View.GONE);
//        llAccountsOpportunities.setVisibility(SharedPreferenceData.getInstance(this).isFullAccessAccount() ?
//                View.VISIBLE : View.GONE);

        new_lead_btn = findViewById(R.id.new_lead_btn);
        new_lead_btn.setOnClickListener(this);

        new_task_btn = findViewById(R.id.new_task_btn);
        new_task_btn.setOnClickListener(this);

        new_client_btn = findViewById(R.id.new_client_btn);
        new_client_btn.setOnClickListener(this);

        new_opportunity_btn = findViewById(R.id.new_opportunity_btn);
        new_opportunity_btn.setOnClickListener(this);

        new_account_btn = findViewById(R.id.new_account_btn);
        new_account_btn.setOnClickListener(this);

        TextView tvLead = findViewById(R.id.tvLead);
        TextView tvClients = findViewById(R.id.tvClients);
        TextView tvTasks = findViewById(R.id.tvTasks);
        TextView tvAccounts = findViewById(R.id.tvAccounts);
        TextView tvOpportunities = findViewById(R.id.tvOpportunities);
        if (personal_dict != null) {
            try {
                if (personal_dict.has("lead"))
                    tvLead.setText(personal_dict.getString("lead"));
                if (personal_dict.has("contact"))
                    tvClients.setText(personal_dict.getString("contact"));
                if (personal_dict.has("tasks"))
                    tvTasks.setText(personal_dict.getString("tasks"));
                if (personal_dict.has("accounts"))
                    tvAccounts.setText(personal_dict.getString("accounts"));
                if (personal_dict.has("opportunity"))
                    tvOpportunities.setText(personal_dict.getString("opportunity"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        ImageView lead = findViewById(R.id.leadScreenMenu);
        navigation_events_progressBar = findViewById(R.id.navigation_events_progressBar);
        lead.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), LeadListActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplicationContext().startActivity(intent);
        });

        ImageView client = findViewById(R.id.clientScreenMenu);
        client.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), ClientListActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplicationContext().startActivity(intent);
        });

        ImageView appointment = findViewById(R.id.appointment_screen_menu);
        appointment.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), AppointmentsActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplicationContext().startActivity(intent);
        });

        ImageView task = findViewById(R.id.task_screen_menu);
        task.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), TaskActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplicationContext().startActivity(intent);
        });

        ImageView account = findViewById(R.id.account_screen_menu);
        account.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), AccountListActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplicationContext().startActivity(intent);
        });

        ImageView opportunity = findViewById(R.id.opportunity_screen_menu);
        opportunity.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), OpportunityActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplicationContext().startActivity(intent);
        });

//        LinearLayout newMeeting = (LinearLayout) findViewById(R.id.new_meeting_menu);
//        newMeeting.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getApplicationContext(), NewEventActivity.class);
//                getApplicationContext().startActivity(intent);
//            }
//        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.getMenu().findItem(R.id.nav_lead)
                .setVisible(sharedPref.getBoolean("ACCESS_LEAD", false));
        navigationView.getMenu().findItem(R.id.nav_client)
                .setVisible(sharedPref.getBoolean("ACCESS_CONTACT", false));
        navigationView.getMenu().findItem(R.id.nav_account)
                .setVisible(sharedPref.getBoolean("ACCESS_ACCOUNT", false));
        navigationView.getMenu().findItem(R.id.nav_tasks)
                .setVisible(sharedPref.getBoolean("ACCESS_TASK", false));
        navigationView.getMenu().findItem(R.id.nav_opportunities)
                .setVisible(sharedPref.getBoolean("ACCESS_OPPORTUNITY", false));

        Menu menu = navigationView.getMenu();
        MenuItem nav_device_ID = menu.findItem(R.id.nav_device_id);
        nav_device_ID.setTitle(getResources().getString(R.string.device_id_) + " " + DeviceInfo.getDeviceID(getApplicationContext()));

        MenuItem nav_version = menu.findItem(R.id.nav_share);
        nav_version.setTitle(getResources().getString(R.string.version_) + " " + BuildConfig.VERSION_NAME);

        View hView = navigationView.inflateHeaderView(R.layout.nav_header_navigation);
        TextView nav_user = hView.findViewById(R.id.nav_user_name);
        String name = sharedPref.getString("USER_NAME", "");
        nav_user.setText(name);

        TextView nav_email = hView.findViewById(R.id.nav_user_email);
        String email = sharedPref.getString("USER_EMAIL", "");
        nav_email.setText(email);

        String imageURL = sharedPref.getString("USER_IMAGEURL", "");
        imageProfile = hView.findViewById(R.id.nav_user_image);
        imageProfile.setOnClickListener(v -> {
            if (imageURL.equals("")) {
                openImageFilePicker();
            } else {
                DialogUtils.showConfirmationDialog(NavigationActivity_.this,
                        new DialogButtonClickListener() {
                            @Override
                            public void onPositiveButtonClick() {
                                openImageFilePicker();
                            }

                            @Override
                            public void onNegativeButtonClick() {

                            }
                        }, getResources().getString(R.string.update_pf_confirmation));
            }
        });

        if (!imageURL.equals("")) {
            Glide.with(this).load(imageURL).into(imageProfile);
        }
//        TextView user_name = (TextView)findViewById(R.id.user_name_id);
//        user_name.setText(name);
//
//        ImageView user_image = (ImageView)findViewById(R.id.user_image_id);
//        if (imageURL != null && imageURL!=null ) {
//            Glide.with(this).load(imageURL).into(user_image);
//        }

        TextView tenant_name = findViewById(R.id.tenant_name_id);
        String t_name = sharedPref.getString("TENANT_NAME", "");
        tenant_name.setText(t_name);

        TextView LabelTV = findViewById(R.id.tenant_name_label);
        String userFormattedName = sharedPref.getString("USER_FORMATTED_NAME", "");
        LabelTV.setText(userFormattedName);

        ImageView tenant_image = findViewById(R.id.tenant_image_id);
        String t_img_url = sharedPref.getString("TENANT_IMAGE_URL", "");
        if (t_img_url != null && tenant_image != null) {
            Glide.with(this).load(t_img_url).into(tenant_image);
        }

        navigationView.setNavigationItemSelectedListener(this);
        final LocalDate instance = LocalDate.now();
        startDate = instance.toString();
        endDate = instance.plusDays(1).with(TemporalAdjusters.lastDayOfMonth()).toString();

        startDate = startDate + "T00:00:00Z";
        endDate = endDate + "T23:59:59Z";
        event_recycler_view = findViewById(R.id.seven_days_event_recycler_view);
        landing_no_events = findViewById(R.id.land_no_events);
        landing_no_events.setVisibility(View.GONE);

        String cachedData = SharedPreferenceData.getInstance(getApplicationContext()).getEventData();
        JSONObject data;
        if (cachedData != null && !cachedData.isEmpty()) {
            try {
                data = new JSONObject(cachedData);
                processEvents(data);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            if (providerIsGoogle()) {
                getAccessToken();
            } else {
                prepareEvents(startDate, endDate);
            }
        }
        processPersonalization();
        getUserImage();
        requestPermissionSystemWindowAlert();

        tvTenantMessage = findViewById(R.id.tvTenantMessage);
        tvTenantTitle = findViewById(R.id.tvTenantTitle);
        tvTenantTitle.setVisibility(View.GONE);
        ivRefreshTenant = findViewById(R.id.ivRefreshTenant);
        cardTenantMsg = findViewById(R.id.cardTenantMsg);
        cardTenantMsg.setOnClickListener(this);
        ivRefreshTenant.setOnClickListener(this);
        getBulletinBoardComments();
    }

    private void getDataFromIntent() {
        Intent receiveIntent = getIntent();
        if (receiveIntent != null && receiveIntent.getBooleanExtra(LauncherActivity.IS_FROM_SHARE, false)) {
            Intent intent = new Intent(getApplicationContext(), ChatActivity.class);
            intent.putExtra(LauncherActivity.IS_FROM_SHARE, receiveIntent.getBooleanExtra(LauncherActivity.IS_FROM_SHARE, false));
            intent.putExtra(LauncherActivity.SHARE_URI, receiveIntent.getStringExtra(LauncherActivity.SHARE_URI));
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplicationContext().startActivity(intent);
            setIntent(null);
        }
    }

    private void cacheData(String eventData) {
        SharedPreferenceData.getInstance(getApplicationContext()).setEventData(eventData);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, getResources().getString(R.string.back_confirmation), Toast.LENGTH_SHORT).show();
            new Handler(Looper.getMainLooper()).postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.navigation, menu);
        menu.findItem(R.id.action_tenant_comment).setVisible(SharedPreferenceData.getInstance(this).isTenantAdmin());
        menuStreaming = menu.findItem(R.id.action_earphone_streaming);
        if (playerState == statePlaying) {
            menuStreaming.setIcon(R.drawable.ic_pause_round);
        } else if (playerState == statePause) {
            menuStreaming.setIcon(R.drawable.ic_play_round);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_new_meeting: {
                Intent intent = new Intent(this, NewEventActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
            }
            case R.id.action_chat: {
                Intent intent = new Intent(getApplicationContext(), ChatActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(intent);
                break;
            }
            case R.id.action_tenant_comment: {
                navigateToTenantScreen();
                break;
            }
            case R.id.action_earphone_streaming: {
                if (!LIVE_PLAYER_MEDIA_URL_SOURCE.equals("")) {
                    openStreamingDialog();
                } else {
                    Intent intent = new Intent(this, StreamingActivity.class);
                    startActivityForResult(intent, STREAMING_REQUEST_CODE);
                }
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_lead:
                Intent leadList = new Intent(this, LeadListActivity.class);
                leadList.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(leadList);
                break;
            case R.id.nav_client:
                Intent clientList = new Intent(this, ClientListActivity.class);
                clientList.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(clientList);
                break;
            case R.id.nav_appointments:
                Intent appointments = new Intent(this, AppointmentsActivity.class);
                appointments.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(appointments);
                break;
            case R.id.nav_tasks: {
                Intent intent = new Intent(getApplicationContext(), TaskActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(intent);
                break;
            }
            case R.id.nav_account: {
                Intent intent = new Intent(getApplicationContext(), AccountListActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(intent);
                break;
            }
            case R.id.nav_manage:
                logOut();
                break;
            case R.id.nav_opportunities: {
                Intent intent = new Intent(getApplicationContext(), OpportunityActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(intent);
                break;
            }
            case R.id.nav_chat: {
                Intent intent = new Intent(getApplicationContext(), ChatActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(intent);
                break;
            }
            case R.id.nav_settings: {
                Intent intent = new Intent(getApplicationContext(), SettingActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(intent);
                break;
            }
            case R.id.nav_language: {
                Intent intent = new Intent(getApplicationContext(), LanguageActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(intent);
                break;
            }
            case R.id.nav_bulletin_board: {
                navigateToTenantScreen();
                break;
            }
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void getUserImage() {
        String url = Constants.MY_DATA;
        NetworkManager.customJsonObjectRequest(
                NavigationActivity_.this, url, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //LogUtils.Print(TAG, "onResponse -==> " + response.toString());
                        try {
                            if (Integer.parseInt(response.getString("result")) == 1) {
                                JSONObject userObject = response.getJSONObject("user_data");
                                if (userObject.has("avatar")) {
                                    strAvatarId = userObject.getString("avatar");
                                    LogUtils.Print(TAG, "strAvatarId => " + strAvatarId);
                                }
                                if (userObject.has("avatar_url")) {
                                    strAvatarURL = userObject.getString("avatar_url");
                                    sharedPref.edit().putString("USER_IMAGEURL", strAvatarURL).apply();
                                    Glide.with(NavigationActivity_.this).load(strAvatarURL).into(imageProfile);
                                    LogUtils.Print(TAG, "strAvatarURL => " + strAvatarURL);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print(TAG, "message -> " + message);
                    }
                }, true);
    }

    private int FILE_REQUEST_CODE = 2282;

    private void openImageFilePicker() {
        Intent intent = new Intent(this, FilePickerActivity.class);
        intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
                .setCheckPermission(true)
                .setShowImages(true)
                .setShowVideos(false)
                .setSingleChoiceMode(true)
                .enableImageCapture(true)
                .build());
        startActivityForResult(intent, FILE_REQUEST_CODE);
    }

    private void uploadFile(Uri uri, String file_name, String file_type) {
        OkHttpClient httpClient = new OkHttpClient();
        RequestBody requestBody = null;
        try {
            File file = FileUtil.from(NavigationActivity_.this, uri);
            requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("avatar", file_name, RequestBody.create(MediaType.parse(file_type),
                            file)).build();
        } catch (Exception e) {
            e.printStackTrace();
        }

        String token = sharedPref.getString("USER_TOKEN", "");
        Request request = new Request.Builder()
                .url(Constants.CHANGE_AVATAR)
                .addHeader("Authorization", "Bearer " + token)
                .post(requestBody)
                .build();

        httpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                LogUtils.Print("ERORRORR", "error in getting response using async okhttp call");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                ResponseBody responseBody = response.body();
                if (!response.isSuccessful()) {
                    throw new IOException("Error response " + response);
                }
                String jsonData = responseBody.string();
                try {
                    JSONObject jsonObject = new JSONObject(jsonData);
                    if (Integer.parseInt(jsonObject.getString("result")) == 1) {
                        if (jsonObject.has("temp_url")) {
                            strAvatarURL = jsonObject.getString("temp_url");
                            sharedPref.edit().putString("USER_IMAGEURL", strAvatarURL).apply();
//                            Glide.with(NavigationActivity.this).load(strAvatarURL).into(imageProfile);
                            LogUtils.Print(TAG, "strAvatarURL => " + strAvatarURL);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void logOut() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.logout));
        builder.setIcon(android.R.drawable.ic_lock_power_off);
        builder.setMessage(getResources().getString(R.string.logout_confirmation));
        builder.setCancelable(false);

        View viewInflated = LayoutInflater.from(this)
                .inflate(R.layout.dialog_user_logout, null, false);

        final CheckBox chk = viewInflated.findViewById(R.id.chk);
        builder.setView(viewInflated);

        builder.setPositiveButton(getResources().getString(R.string.logout), (dialog, which) -> {
            logoutOnServer(chk.isChecked());
            //setUserActive("n", chk.isChecked());
            dialog.dismiss();
        });
        builder.setNegativeButton(android.R.string.cancel, (dialog, which) -> {
            dialog.cancel();
        });
        builder.show();

//        new AlertDialog.Builder(this)
//                .setTitle("Logout")
//                .setMessage("Do you really want to Logout?")
//                .setIcon(android.R.drawable.ic_lock_power_off)
//                .setPositiveButton(android.R.string.yes, (dialog, whichButton) -> {
//                    setUserActive("n");
////                        clearPreferences();
//                })
//                .setNegativeButton(android.R.string.no, null).show();

    }

    private void clearPreferences() {
        closeSocketWithMessage();
        SharedPreferences preferences = getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
        startWithClearStack(LoginActivity.class);
    }

    private void closeSocketWithMessage() {
        JSONObject intro_data = new JSONObject();
        try {
            intro_data.put("type", "self_logout");
            intro_data.put("user_id", SharedPreferenceData.getInstance(getApplicationContext()).getLoggedInUserID());
            intro_data.put("recipient_id", SharedPreferenceData.getInstance(getApplicationContext()).getClientConnectionID());
            WsManager ws = WSSingleton.getInstanceWM(getApplicationContext());
            if (ws.isWsConnected()) {
                ws.sendMessage(intro_data.toString());
            }
            ws.stopConnect();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void processPersonalization() {
//        Map<String, String> dict = ((ApplicationContext) this.getApplication()).getPersonal_dict();
//        if (dict == null) {
//            return;
//        }
//        String lead = dict.get("lead");
//        MenuItem menu;
//        if (lead != null && !lead.isEmpty()) {
//            menu = findViewById(R.id.nav_lead);
//            menu.setTitle(lead);
//        }
    }

    private boolean providerIsGoogle() {
        return SharedPreferenceData.getInstance(this).getProvider().equalsIgnoreCase("google");
    }

    private void getAccessToken() {
        navigation_events_progressBar.setVisibility(View.VISIBLE);
        String params = "?provider_uid=" + SharedPreferenceData.getInstance(this).getProviderId() +
                "&mobile_client_id=" + Constants.getClientId() +
                "&mobile_client_type=android";
        LogUtils.Print(TAG, "getAccessToken --> " + params);
        NetworkManager.customJsonObjectRequest(
                NavigationActivity_.this, Constants.GOOGLE_ACCESS_TOKEN + params, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        navigation_events_progressBar.setVisibility(View.GONE);
                        prepareEvents(startDate, endDate);
                    }

                    @Override
                    public void onError(String message) {
                        navigation_events_progressBar.setVisibility(View.GONE);
                        prepareEvents(startDate, endDate);
                    }
                }, true);
    }

    private void prepareEvents(String startDate, String endDate) {
        navigation_events_progressBar.setVisibility(View.VISIBLE);
        String params = "?startDateTime=" + startDate + "&endDateTime=" + endDate;
        NetworkManager.customJsonObjectRequest(
                NavigationActivity_.this, Constants.GET_EVENTS_URL + params, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        navigation_events_progressBar.setVisibility(View.GONE);
                        cacheData(response.toString());
                        processEvents(response);
                    }

                    @Override
                    public void onError(String message) {
                        navigation_events_progressBar.setVisibility(View.GONE);
                    }
                }, true);
    }

    private void processEvents(JSONObject response) {
        JSONArray eventsArray = new JSONArray();
        event_objects = new ArrayList<>();
        if (response.has("events_list")) {
            Object events = null;
            try {
                events = response.get("events_list");
                if (events instanceof JSONArray) {
                    eventsArray = (JSONArray) events;
                }
                for (int i = 0; i < eventsArray.length(); i++) {
                    JSONObject event_obj = eventsArray.getJSONObject(i);
                    JSONArray ea = event_obj.getJSONArray("events_list");
                    for (int j = 0; j < ea.length(); j++) {
                        JSONObject f_obj = ea.getJSONObject(j);
                        f_obj.put("calendarId", event_obj.getString("calendarId"));
                        event_objects.add(ea.getJSONObject(j));
                    }
                }
                if (event_objects.size() > 0) {
                    eventsAdapter = new EventsAdapter(event_objects, getApplicationContext(), true);

                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                    mLayoutManager = new LinearLayoutManager(
                            getApplicationContext(),
                            LinearLayoutManager.HORIZONTAL,
                            false
                    );
                    event_recycler_view.setLayoutManager(mLayoutManager);
                    event_recycler_view.setItemAnimator(new DefaultItemAnimator());
                    event_recycler_view.setAdapter(eventsAdapter);
                    eventsAdapter.notifyDataSetChanged();
                } else {
                    landing_no_events.setVisibility(View.VISIBLE);
                    event_recycler_view.setVisibility(View.GONE);
                }
            } catch (JSONException e) {
                landing_no_events.setVisibility(View.VISIBLE);
                event_recycler_view.setVisibility(View.GONE);
                e.printStackTrace();
            }
        } else {
            landing_no_events.setVisibility(View.VISIBLE);
            event_recycler_view.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        WsManager.setOnBulletHomeEventListener((type, text) -> runOnUiThread(this::getBulletinBoardComments));
    }

    @Override
    protected void onPause() {
        super.onPause();
        WsManager.setOnBulletHomeEventListener(null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        WsManager.setOnBulletHomeEventListener(null);
    }

    private void getBulletinBoardComments() {
        NetworkManager.customJsonObjectGetRequest(
                NavigationActivity_.this, Constants.manageTenantsComments, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processEventsTenantMsg(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print(TAG, "onError --> " + message);
                    }
                }, true);
    }

    private void processEventsTenantMsg(JSONObject response) {
        LogUtils.Print(TAG, "response --> " + response);
        try {
            if (Integer.parseInt(response.getString("result")) == 1) {
                JSONArray commentsList = response.getJSONArray("comments_list");
                if (commentsList.length() > 0) {
                    JSONObject obj = commentsList.getJSONObject(commentsList.length() - 1);
                    if (obj.has("comment")) {
                        cardTenantMsg.setVisibility(View.VISIBLE);
                        tvTenantTitle.setVisibility(View.VISIBLE);
                        tvTenantMessage.setTextColor(ContextCompat.getColor(this, android.R.color.holo_green_dark));
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            tvTenantMessage.setText(Html.fromHtml(obj.getString("comment"), Html.FROM_HTML_MODE_COMPACT));
                        } else {
                            tvTenantMessage.setText(Html.fromHtml(obj.getString("comment")));
                        }

                        String strMessage = "";
                        strMessage += commentsList.length() + (commentsList.length() == 1 ?
                                " " + getResources().getString(R.string.message_) :
                                " " + getResources().getString(R.string.messages_));
                        if (obj.has("date")) {
                            strMessage += " " + getResources().getString(R.string.last_message_) + " " +
                                    TimeAgo_.getTimeAgo(this, Long.parseLong(obj.getString("date")));
                        }
                        tvTenantTitle.setText(strMessage);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /*private void setUserActive(String isActive, boolean checked) {
        navigation_events_progressBar.setVisibility(View.VISIBLE);
        String params = "?isAppActive=" + isActive +
                "?connection_id=" + SharedPreferenceData.getInstance(this).getClientConnectionID() +
                "?everywhere=" + checked;
        LogUtils.Print(TAG, "Params ==> " + params);
        NetworkManager.customJsonObjectRequest(
                NavigationActivity.this, Constants.SET_USER_CONVERSATION_APP_STATUS + params, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        logoutOnServer(SharedPreferenceData.getInstance(getApplicationContext()).getClientConnectionID());
                    }

                    @Override
                    public void onError(String message) {
                        navigation_events_progressBar.setVisibility(View.GONE);
                    }
                }, true);
    }*/

    private void logoutOnServer(boolean checkedEverywhere) {
        navigation_events_progressBar.setVisibility(View.VISIBLE);
        String params = "?connection_id=" + SharedPreferenceData.getInstance(this).getClientConnectionID() + "&everywhere=" + checkedEverywhere;
        NetworkManager.customJsonObjectRequest(
                NavigationActivity_.this, Constants.LOGOUT_USER + params, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        clearPreferences();
                    }

                    @Override
                    public void onError(String message) {
                        navigation_events_progressBar.setVisibility(View.GONE);
                        clearPreferences();
                    }
                }, true);
        SharedPreferences preferences = getApplicationContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        getApplicationContext().startActivity(intent);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.new_appointment_btn: {
                Intent intent = new Intent(this, NewEventActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
            }
            case R.id.new_lead_btn: {
                Intent intent = new Intent(this, LeadProcessActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
            }
            case R.id.new_account_btn: {
                Intent intent = new Intent(this, AccountProcessActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
            }
            case R.id.new_task_btn: {
                Intent intent = new Intent(this, NewTaskActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
            }
            case R.id.new_opportunity_btn: {
                Intent intent = new Intent(this, ManageOpportunityActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
            }
            case R.id.new_client_btn: {
                Intent intent = new Intent(this, ClientProcessActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
            }
            case R.id.cardTenantMsg: {
                navigateToTenantScreen();
                break;
            }
            case R.id.llRefresh:
            case R.id.refreshEvents: {
                event_objects.clear();
                if (providerIsGoogle()) {
                    getAccessToken();
                } else {
                    prepareEvents(startDate, endDate);
                }
                break;
            }
            case R.id.ivRefreshTenant: {
                getBulletinBoardComments();
                break;
            }
        }
    }

    private void navigateToTenantScreen() {
        Intent intent = new Intent(getApplicationContext(), TenantCommentActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getApplicationContext().startActivity(intent);
    }

    private void requestPermissionSystemWindowAlert() {
        if (Settings.canDrawOverlays(this)) return;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            DialogUtils.showConfirmationDialog(this, new DialogButtonClickListener() {
                        @Override
                        public void onPositiveButtonClick() {
                            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                    Uri.parse("package:" + getPackageName()));
                            startActivityForResult(intent, 555);
                        }

                        @Override
                        public void onNegativeButtonClick() {

                        }
                    }, getResources().getString(R.string.alert_window_confirmation),
                    getResources().getString(R.string.allow),
                    getResources().getString(R.string.deny));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LogUtils.Print(TAG, "requestCode ->" + requestCode);
        LogUtils.Print(TAG, "resultCode ->" + resultCode);
        if (requestCode == FILE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                ArrayList<MediaFile> files;
                if (data != null) {
                    if (data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES) != null) {
                        files = data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES);
                        if (files == null || files.size() == 0) return;
                        Uri uri = files.get(0).getUri();
                        String mimeType = FileUtil.getMimeType(getApplicationContext(), uri);
                        String fileName = FileUtil.getFileName(getApplicationContext(), uri);
                        Glide.with(this).load(files.get(0).getUri()).into(imageProfile);
                        uploadFile(uri, fileName, mimeType);
                    }
                }
            }
        } else if (requestCode == 555) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                if (!Settings.canDrawOverlays(this)) {
                    LogUtils.Print(TAG, getResources().getString(R.string.notification_on_os_10));
                }
            }
        } else if (requestCode == STREAMING_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (data != null && data.hasExtra("DATA")) {
                    dataSteaming = data.getParcelableExtra("DATA");
                    if (dataSteaming != null) {
                        LIVE_PLAYER_MEDIA_URL_SOURCE = dataSteaming.getUrl();
                        setUpMediaPlayer();
                    }
                } else {
                    LIVE_PLAYER_MEDIA_URL_SOURCE = "";
                    menuStreaming.setIcon(R.drawable.earphone_streaming);
                    playerState = stateEmpty;
                }
            }
        }
    }

    private void setUpMediaPlayer() {
        LogUtils.Print(TAG, "LIVE_PLAYER_MEDIA_URL_SOURCE -> " + LIVE_PLAYER_MEDIA_URL_SOURCE);
        if (myMediaPlayer != null) {
            myMediaPlayer.stop();
            myMediaPlayer.release();
        }
        myMediaPlayer = new MediaPlayer();
        myMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        myMediaPlayer.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
        try {
            myMediaPlayer.setDataSource(getApplicationContext(), Uri.parse(LIVE_PLAYER_MEDIA_URL_SOURCE));
            myMediaPlayer.prepareAsync();
            playerState = statePlaying;
            setPlayingView();
        } catch (IOException e) {
            Util.makeToast(getResources().getString(R.string.channel_unavailable));
            e.printStackTrace();
        }
        myMediaPlayer.setOnPreparedListener(mediaPlayer -> {
            setPlayingView();
            mediaPlayer.start();
        });
        myMediaPlayer.setOnCompletionListener(mediaPlayer -> {
            LogUtils.Print("onCompletion", "onCompletion");
            playerState = stateStop;
            isAudioCompleted = true;
            setPauseView();
        });
    }

    /**
     * OPEN STREAMING DIALOG
     */
    public void openStreamingDialog() {
        if (mPopupWindow != null && isPopupShowing) return;
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

        View customView = inflater.inflate(R.layout.layout_streaming, null);
        mPopupWindow = new PopupWindow(
                customView,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        mPopupWindow.setAnimationStyle(R.style.customAnimationTopToBottom);

        ImageView imageView = customView.findViewById(R.id.iv);
        ImageView ivStop = customView.findViewById(R.id.ivStop);
        TextView tvName = customView.findViewById(R.id.tvName);
        ivPlayPause = customView.findViewById(R.id.ivPlayPause);
        ImageView ivSearch = customView.findViewById(R.id.ivSearch);

        ivStop.setVisibility((playerState == statePlaying || playerState == statePause) ? View.VISIBLE : View.GONE);
        if (dataSteaming != null) {
            DataBindingUtils.setStreamingIcon(imageView, dataSteaming.getLogoUrl());
            tvName.setText(dataSteaming.getChannel());
        }
        if (myMediaPlayer != null && myMediaPlayer.isPlaying()) {
            ivPlayPause.setImageResource(R.drawable.ic_pause_round);
        } else {
            ivPlayPause.setImageResource(R.drawable.ic_play_round);
        }
        ivPlayPause.setOnClickListener(view -> {
            if (myMediaPlayer.isPlaying()) {
                playerState = statePause;
                myMediaPlayer.pause();
                setPauseView();
            } else {
                playerState = statePlaying;
                if (isAudioCompleted) {
                    isAudioCompleted = false;
                    setUpMediaPlayer();
                } else {
                    myMediaPlayer.start();
                    setPlayingView();
                }
            }
            closePopup();
        });
        ivStop.setOnClickListener(view -> {
            playerState = stateStop;
            menuStreaming.setIcon(R.drawable.earphone_streaming);
            if (myMediaPlayer.isPlaying()) {
                myMediaPlayer.pause();
            }
            closePopup();
        });
        ivSearch.setOnClickListener(view -> {
            Intent intent = new Intent(NavigationActivity_.this, StreamingActivity.class);
            startActivityForResult(intent, STREAMING_REQUEST_CODE);
            closePopup();
        });

        // Closes the popup window when touch outside.
        mPopupWindow.setOutsideTouchable(true);
        mPopupWindow.setFocusable(true);
        // Removes default background.
        mPopupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        mPopupWindow.setOnDismissListener(() -> {
            isPopupShowing = false;
            closePopup();
        });

        new Handler(Looper.getMainLooper()).postDelayed(() ->
                        mPopupWindow.showAtLocation(llContainer, Gravity.END | Gravity.TOP,
                                0, (int) getResources().getDimension(R.dimen._60sdp)),
                200L);
    }

    private void setPauseView() {
        menuStreaming.setIcon(R.drawable.ic_play_round);
        if (ivPlayPause != null)
            ivPlayPause.setImageResource(R.drawable.ic_play_round);
    }

    private void setPlayingView() {
        menuStreaming.setIcon(R.drawable.ic_pause_round);
        if (ivPlayPause != null)
            ivPlayPause.setImageResource(R.drawable.ic_pause_round);
    }

    private void closePopup() {
        if (mPopupWindow != null) {
            if (mPopupWindow.isShowing()) {
                mPopupWindow.dismiss();
                isPopupShowing = false;
                mPopupWindow = null;
            }
        }
    }
}