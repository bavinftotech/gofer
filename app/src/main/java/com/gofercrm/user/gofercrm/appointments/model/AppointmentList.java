package com.gofercrm.user.gofercrm.appointments.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AppointmentList {

    @SerializedName("appointment_list")
    @Expose
    private List<DataAppointment> appointmentList = null;

    public List<DataAppointment> getAppointmentList() {
        return appointmentList;
    }

    public void setAppointmentList(List<DataAppointment> appointmentList) {
        this.appointmentList = appointmentList;
    }
}