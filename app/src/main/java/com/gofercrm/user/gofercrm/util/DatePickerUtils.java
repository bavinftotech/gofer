package com.gofercrm.user.gofercrm.util;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;

import com.gofercrm.user.gofercrm.implementor.DialogDateClickListener;
import com.google.firebase.crashlytics.internal.common.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DatePickerUtils {
    public static final String DATE_DD_MM_YYYY_FORMAT = "dd/MM/yyyy";
    public static final String TIME_HH_MM_SS_FORMAT = "HH:mm:ss";

    public static final int TIME_PICKER_INTERVAL = 5;
    private static final String TAG = "DatePickerUtils";
    // DATE PICKER
    private static Calendar calendar;
    private static int year, month, day;
    private static String strReturnDateFormat = DATE_DD_MM_YYYY_FORMAT;
    // TIME PICKER
    private static int hour, minute;
    private static String strReturnTimeFormat = TIME_HH_MM_SS_FORMAT;

    public static void getDefaultDateDialog(Context context, final DialogDateClickListener listener) {
        openCustomDateDialog(context, "", "", "",
                "", "", "", strReturnDateFormat, listener);
    }

    public static void getFormatedDateDialog(Context context, String strReturnDateFormat
            , final DialogDateClickListener listener) {
        openCustomDateDialog(context, "", "", "",
                "", "", "", strReturnDateFormat, listener);
    }

    public static void getSelectedDateDialog(Context context, String strSelDate, String strSelDateFormat,
                                             String strReturnDateFormat, final DialogDateClickListener listener) {
        openCustomDateDialog(context, "", "", "", "",
                strSelDate, strSelDateFormat, strReturnDateFormat, listener);
    }

    public static void getMinDateDialog(Context context, String strSelDate, String strSelDateFormat,
                                        String strMinDate, String strMinDateFormat,
                                        String strReturnDateFormat, final DialogDateClickListener listener) {
        openCustomDateDialog(context, strMinDate, strMinDateFormat, "", "",
                strSelDate, strSelDateFormat, strReturnDateFormat, listener);
    }

    public static void getMaxDateDialog(Context context, String strSelDate, String strSelDateFormat,
                                        String strMaxDate, String strMaxDateFormat,
                                        String strReturnDateFormat, final DialogDateClickListener listener) {
        openCustomDateDialog(context, "", "", strMaxDate, strMaxDateFormat,
                strSelDate, strSelDateFormat, strReturnDateFormat, listener);
    }

    public static void getMinMaxDateDialog(Context context, String strMinDate, String strMinDateFormat, String strSelDate, String strSelDateFormat,
                                           String strMaxDate, String strMaxDateFormat,
                                           String strReturnDateFormat, final DialogDateClickListener listener) {
        openCustomDateDialog(context, strMinDate, strMinDateFormat, strMaxDate, strMaxDateFormat,
                strSelDate, strSelDateFormat, strReturnDateFormat, listener);
    }

    private static void openCustomDateDialog(Context context,
                                             String strMinDate, String strMinDateFormat,
                                             String strMaxDate, String strMaxDateFormat,
                                             String strSelDate, String strSelDateFormat,
                                             String strReturnDateFormat, final DialogDateClickListener listener) {
        calendar = Calendar.getInstance(Locale.ENGLISH);
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        if (!strSelDate.equals("")) {
            try {
                Date date = new SimpleDateFormat(strSelDateFormat, Locale.ENGLISH)
                        .parse(strSelDate);
                long timeInMillis = 0;
                if (date != null) {
                    timeInMillis = date.getTime();
                }
                Calendar calendar = Calendar.getInstance(Locale.ENGLISH);
                calendar.setTimeInMillis(timeInMillis);
                year = calendar.get(Calendar.YEAR);
                month = calendar.get(Calendar.MONTH);
                day = calendar.get(Calendar.DAY_OF_MONTH);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        DatePickerDialog dpd = new DatePickerDialog(context,
                (view, year, monthOfYear, dayOfMonth) -> {
                    String date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                    try {
                        String strReturnDate = Util.getDateOnRequireFormat(date, DATE_DD_MM_YYYY_FORMAT,
                                strReturnDateFormat);
                        if (listener != null) {
                            LogUtils.Print(TAG, "Selected date ==> " + strReturnDate);
                            listener.onDateClick(strReturnDate);
                        }
                    } catch (Exception e) {
                        // TODO: handle exception
                        e.printStackTrace();
                    }
                }, year, (month), day);

        try {
            if (!strMinDate.equals("")) {
                Date selectedDate_min = new SimpleDateFormat(strMinDateFormat, Locale.ENGLISH)
                        .parse(strMinDate);
                dpd.getDatePicker().setMinDate(selectedDate_min.getTime());
            }

            if (!strMaxDate.equals("")) {
                Date selectedDate_max = new SimpleDateFormat(strMaxDateFormat, Locale.ENGLISH)
                        .parse(strMaxDate);
                dpd.getDatePicker().setMaxDate(selectedDate_max.getTime());
            }

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        dpd.setOnCancelListener(dialog -> {
            if (listener != null) {
                listener.onCancelClick();
            }
        });

        dpd.show();
    }
}