package com.gofercrm.user.gofercrm.chat.ui.main.model;

import com.gofercrm.user.gofercrm.entity.User;

import java.io.Serializable;

public class ChatData implements Serializable {
    private String type, text, time,room_id,user_message_type;

    private int edit,read;

    private int thumbnail,connection_type,thread_count;

    private String att_file_size,att_name, pdf_url,sender_name,att_url,content_type,quotedText,id;

    private User user;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAtt_file_size() {
        return att_file_size;
    }

    public void setAtt_file_size(String att_file_size) {
        this.att_file_size = att_file_size;
    }

    public String getPdf_url() {
        return pdf_url;
    }

    public void setPdf_url(String pdf_url) {
        this.pdf_url = pdf_url;
    }

    public String getSender_name() {
        return sender_name;
    }

    public void setSender_name(String sender_name) {
        this.sender_name = sender_name;
    }

    public String getAtt_url() {
        return att_url;
    }

    public void setAtt_url(String att_url) {
        this.att_url = att_url;
    }

    public String getContent_type() {
        return content_type;
    }

    public void setContent_type(String content_type) {
        this.content_type = content_type;
    }

    public String getQuotedText() {
        return quotedText;
    }

    public void setQuotedText(String quotedText) {
        this.quotedText = quotedText;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAtt_name() {
        return att_name;
    }

    public void setAtt_name(String att_name) {
        this.att_name = att_name;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }

    public int getConnection_type() {
        return connection_type;
    }

    public void setConnection_type(int connection_type) {
        this.connection_type = connection_type;
    }

    public int getThread_count() {
        return thread_count;
    }

    public void setThread_count(int thread_count) {
        this.thread_count = thread_count;
    }

    public String getRoom_id() {
        return room_id;
    }

    public void setRoom_id(String room_id) {
        this.room_id = room_id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getEdit() {
        return edit;
    }

    public void setEdit(int edit) {
        this.edit = edit;
    }

    public int getRead() {
        return read;
    }

    public void setRead(int read) {
        this.read = read;
    }

    public String getUser_message_type() {
        return user_message_type;
    }

    public void setUser_message_type(String user_message_type) {
        this.user_message_type = user_message_type;
    }
}