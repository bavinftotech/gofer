package com.gofercrm.user.gofercrm.settings.timezone;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.settings.timezone.model.DataTimeZone;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class TimeZoneAdapter extends RecyclerView.Adapter<TimeZoneAdapter.ViewHolder> {
    private List<DataTimeZone> list;
    public ClickListener listener;

    public TimeZoneAdapter(List<DataTimeZone> list, ClickListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_timezone, null);
        return new ViewHolder(itemLayoutView);
    }

    @Override
    public void onBindViewHolder(@NotNull ViewHolder holder, int position) {
        DataTimeZone data = list.get(position);
        holder.tv.setText(data.getTimeZone());
        holder.tv.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0,
                data.isSelect() ? R.drawable.ic_check : 0, 0);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tv;

        public ViewHolder(View view) {
            super(view);
            tv = view.findViewById(R.id.tv);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v == itemView) {
                if (getAdapterPosition() != RecyclerView.NO_POSITION) {
                    if (listener != null) {
                        listener.onItemClicked(getAdapterPosition());
                    }
                }
            }
        }
    }

    public interface ClickListener {
        void onItemClicked(int position);
    }
}