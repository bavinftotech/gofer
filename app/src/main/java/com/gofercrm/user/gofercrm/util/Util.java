package com.gofercrm.user.gofercrm.util;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Rect;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Patterns;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.gofercrm.user.gofercrm.ApplicationContext;
import com.gofercrm.user.gofercrm.R;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;
import org.threeten.bp.DateTimeUtils;
import org.threeten.bp.Instant;
import org.threeten.bp.ZonedDateTime;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Util {

    public static final List<Long> times = Arrays.asList(
            TimeUnit.DAYS.toMillis(365),
            TimeUnit.DAYS.toMillis(30),
            TimeUnit.DAYS.toMillis(1),
            TimeUnit.HOURS.toMillis(1),
            TimeUnit.MINUTES.toMillis(1),
            TimeUnit.SECONDS.toMillis(1));
    public static final List<String> timesString = Arrays.asList("year", "month", "day", "hour", "minute", "second");

    public static String capitalize(final String line) {
        String result = "";
        if (line != null && !line.isEmpty()) {
            result = Character.toUpperCase(line.charAt(0)) + line.substring(1);
        }
        return result;
    }

    public static String initialChar(final String line) {
        if (!line.isEmpty()) {
            return Character.toUpperCase(line.charAt(0)) + "";
        } else
            return "";
    }

    public static void onMessage(Context ctx, String text) {
        Toast.makeText(ctx, text, Toast.LENGTH_LONG).show();
    }

    public static void makeToast(String text) {
        Toast.makeText(ApplicationContext.ctx, text, Toast.LENGTH_SHORT).show();
    }

    public static void onMessageTop(Context ctx, String text) {
        Toast toast = Toast.makeText(ctx, text, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }

    public static String zFormatStringToDate(String _date) {
        String result = "";
        String pattern = "dd-MMM-yyyy h:mm a";

        String inputValue = _date;
        Instant timestamp = Instant.parse(inputValue);
        String zone = TimeZone.getDefault().getID();
        ZonedDateTime deviceTime = timestamp.atZone(org.threeten.bp.ZoneId.of(zone));
        System.out.println(deviceTime);
        Date date = DateTimeUtils.toDate(deviceTime.toInstant());
        DateFormat uiFormat = new SimpleDateFormat(pattern);
        result = uiFormat.format(date);
        return result;
    }

    public static void setClipboard(Context context, String text) {
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
            android.text.ClipboardManager clipboard = (android.text.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            clipboard.setText(text);
        } else {
            android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            android.content.ClipData clip = android.content.ClipData.newPlainText("Copied URL", text);
            clipboard.setPrimaryClip(clip);
            onMessage(context, context.getResources().getString(R.string.url_copied) + " " + text);
        }
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    private static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*+=]).{8,20})";

    public static boolean isValidPassword(final String password) {
        Pattern pattern;
        Matcher matcher;
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public static Map<String, String> toMap(JSONObject object) throws JSONException {
        Map<String, String> map = new HashMap<>();
        if (object != null) {
            Iterator<String> keysItr = object.keys();
            while (keysItr.hasNext()) {
                String key = keysItr.next();
                String value = object.getString(key);
                map.put(key, value);
            }
        }
        return map;
    }

    public static String formatDate(long milliseconds) {
        SimpleDateFormat sdf = new SimpleDateFormat("E MMM dd,yyyy h:mm a");
        return sdf.format(milliseconds * 1000);
    }

    public static String formatDate(double milliseconds) {
        SimpleDateFormat sdf = new SimpleDateFormat("E MMM dd,yyyy h:mm a");
        return sdf.format(milliseconds * 1000);
    }

    public static String weatherDate(double milliseconds) {
        SimpleDateFormat sdf = new SimpleDateFormat("EEE");
        return sdf.format(milliseconds);
    }

    public static String timeAgo(long duration) {
        return DateUtils.getRelativeTimeSpanString(duration * 1000, new Date().getTime()
                , DateUtils.MINUTE_IN_MILLIS).toString();
    }

    public static int getThumbNails(String type) {
        if (type == null)
            return 0;
        int thumbnail = R.drawable.file;
        if (type.contains("image")) {
            thumbnail = R.drawable.file_image;
        } else if (type.contains("pdf")) {
            thumbnail = R.drawable.file_pdf;
        } else if (type.contains("msword")) {
            thumbnail = R.drawable.file_word_box;
        } else if (type.contains("audio")) {
            thumbnail = R.drawable.audiobook;
        } else if (type.contains("video")) {
            thumbnail = R.drawable.file_video;
        } else if (type.contains("text")) {
            thumbnail = R.drawable.file;
        } else if (type.contains("csv")) {
            thumbnail = R.drawable.file_delimited;
        } else if (type.contains("xml")) {
            thumbnail = R.drawable.file_xml;
        } else if (type.contains("excel")) {
            thumbnail = R.drawable.file_excel;
        }
        return thumbnail;
    }

    public static void copyDataOnClipboard(Context context, String str) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("data", str);
        if (clipboard != null)
            clipboard.setPrimaryClip(clip);
    }

    public static void hideSoftKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) view.getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null)
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static String getCurrentDate(String strFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(strFormat);
        Date c = Calendar.getInstance().getTime();
        return sdf.format(c);
    }

    public static String getFutureMonthDate(String strFormat, int month) {
        SimpleDateFormat sdf = new SimpleDateFormat(strFormat);
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, month);
        return sdf.format(c.getTime());
    }

    public static String getNextHourCurrentDate(SimpleDateFormat strFormat) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR, c.get(Calendar.HOUR) + 1);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        return strFormat.format(c.getTime());
    }

    public static String get30MinAfterDate(SimpleDateFormat strFormat) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MINUTE, 30);
        return strFormat.format(c.getTime());
    }

    public static String get30MinAfter1HourDate(SimpleDateFormat strFormat) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR, c.get(Calendar.HOUR) + 1);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.add(Calendar.MINUTE, 30);
        return strFormat.format(c.getTime());
    }

    public static int getDateDifferenceInMonth(String strFrom, String strTo) {
        try {
            Date date1;
            Date date2;
            SimpleDateFormat dates = new SimpleDateFormat("MM/dd/yyyy");
            date1 = dates.parse(strFrom);
            date2 = dates.parse(strTo);
            long difference = Math.abs(date1.getTime() - date2.getTime());

            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;
            long daysInMilli = hoursInMilli * 24;
            long monthInMili = daysInMilli * 30;

            return (int) (difference / monthInMili);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return 0;
    }

    public static String getDateOnRequireFormat(String date,
                                                String givenformat, String resultformat) {
        if (date.equals("")) return date;
        String result = "";
        SimpleDateFormat sdf;
        SimpleDateFormat sdf1;
        try {
            if (date != null) {
                sdf = new SimpleDateFormat(givenformat, Locale.US);
                sdf1 = new SimpleDateFormat(resultformat, Locale.US);
                result = sdf1.format(sdf.parse(date));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        } finally {
            sdf = null;
            sdf1 = null;
        }
        return result;
    }

    public static String getExtension(String url) {
        String[] filenameArray = url.split("\\.");
        String extension = filenameArray[filenameArray.length - 1];
        return extension;
    }

    public static long getCurrentTimeMillis() {
        return System.currentTimeMillis();
    }

    /**
     * ***************************************************************************
     * ****************************** MESSAGE UTILS ******************************
     * ***************************************************************************
     */
    public static void setErrorOnTextInputLayout(TextInputLayout textInputLayout, String text) {
        if (textInputLayout != null) {
            if (text != null) {
                textInputLayout.setErrorEnabled(true);
                textInputLayout.setError(text);
            } else {
                textInputLayout.setError(null);
                textInputLayout.setErrorEnabled(false);
            }
        }
    }

    public static boolean isValidName(String name) {
        boolean check;
        Pattern p;
        Matcher m;

        String NAME_STRING = "[a-zA-z]+([ '-][a-zA-Z]+)*";

        p = Pattern.compile(NAME_STRING);

        m = p.matcher(name);
        check = m.matches();

        return check;
    }

    public static String GetDateOnRequireFormat(String date,
                                                String givenformat, String resultformat) {
        String result = "";
        SimpleDateFormat sdf;
        SimpleDateFormat sdf1;
        try {
            if (date != null) {
                sdf = new SimpleDateFormat(givenformat, Locale.US);
                sdf1 = new SimpleDateFormat(resultformat, Locale.US);
                result = sdf1.format(sdf.parse(date));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        } finally {
            sdf = null;
            sdf1 = null;
        }
        return result;
    }

    public static String getDate(String format) {
        Calendar cal = Calendar.getInstance();
        Date currentLocalTime = cal.getTime();
        DateFormat date = new SimpleDateFormat(format, Locale.getDefault());
        return date.format(currentLocalTime);
    }

    static String[] okFileExtensions = new String[]{
            "jpg",
            "png",
            "jpeg"
    };

    public static boolean isImageGreaterThan1MB(Context context, Uri uri, String file_name) {
        boolean isImage = false;
        File file = new File(uri.toString());
        for (String extension : okFileExtensions) {
            if (file_name.toLowerCase().endsWith(extension)) {
                isImage = true;
                break;
            }
        }
        LogUtils.Print("SIZE -> ", "" + getRealSizeFromUri(context, uri));
        return isImage && (getRealSizeFromUri(context, uri) / 1024) > 1;
    }

    private static int getRealSizeFromUri(Context context, Uri uri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Audio.Media.SIZE};
            cursor = context.getContentResolver().query(uri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.SIZE);
            cursor.moveToFirst();
            return Integer.parseInt(cursor.getString(column_index));
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    //SnackBar
    public static void showSnackBar(String text, View view) {
        if (view != null) {
            Snackbar snackbar;
            snackbar = Snackbar.make(view, text, Snackbar.LENGTH_SHORT);
            View snackBarView = snackbar.getView();
            snackBarView.setBackgroundColor(ContextCompat.getColor(view.getContext(), R.color.colorPrimary));
            TextView textView = snackBarView.findViewById(R.id.snackbar_text);
            textView.setTextColor(ContextCompat.getColor(view.getContext(), R.color.white));
            snackbar.show();
        }
    }

    //SnackBar
    public static void showSnackBar(String text, Activity activity) {
        if (activity != null && !activity.isFinishing() &&
                activity.findViewById(android.R.id.content) != null) {
            Snackbar snackbar;
            snackbar = Snackbar.make(activity.findViewById(android.R.id.content), text, Snackbar.LENGTH_SHORT);
            View snackBarView = snackbar.getView();
            snackBarView.setBackgroundColor(ContextCompat.getColor(activity, R.color.colorPrimary));
            TextView textView = snackBarView.findViewById(R.id.snackbar_text);
            textView.setTextColor(ContextCompat.getColor(activity, R.color.white));
            snackbar.show();
        }
    }

    public static String GetDeviceID(Context ctx) {
        return Settings.Secure.getString(ctx.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static double metersToMiles(double meters) {
        return meters / 1609.3440057765;
    }

    public static Rect locateView(View v) {
        int[] loc_int = new int[2];
        if (v == null) return null;
        try {
            v.getLocationOnScreen(loc_int);
        } catch (NullPointerException npe) {
            npe.printStackTrace();
            return null;
        }
        Rect location = new Rect();
        location.left = loc_int[0];
        location.top = loc_int[1];
        location.right = location.left + v.getWidth();
        location.bottom = location.top + v.getHeight();
        return location;
    }
}