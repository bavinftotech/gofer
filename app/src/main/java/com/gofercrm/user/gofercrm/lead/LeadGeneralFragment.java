package com.gofercrm.user.gofercrm.lead;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.R;

public class LeadGeneralFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";

    public LeadGeneralFragment() {
    }

    public static LeadGeneralFragment newInstance(int sectionNumber) {
        LeadGeneralFragment fragment = new LeadGeneralFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rv = inflater.inflate(R.layout.general_tab_lead_view, container, false);
        LeadViewActivity activity = (LeadViewActivity) getActivity();
        RecyclerView recyclerView = rv.findViewById(R.id.lead_view_recycle);

        LeadGeneralAdapter lGAdapter = new LeadGeneralAdapter(activity.getData(), getContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(lGAdapter);
        return rv;
    }
}