package com.gofercrm.user.gofercrm.chat.ui.main.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AllLanguages {
    @SerializedName("English")
    @Expose
    private String english;
    @SerializedName("alpha2")
    @Expose
    private String alpha2;

    private boolean select;

    public String getEnglish() {
        return english;
    }

    public void setEnglish(String english) {
        this.english = english;
    }

    public String getAlpha2() {
        return alpha2;
    }

    public void setAlpha2(String alpha2) {
        this.alpha2 = alpha2;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }

    public AllLanguages() {
    }

    @Override
    public String toString() {
        return english;
    }
}