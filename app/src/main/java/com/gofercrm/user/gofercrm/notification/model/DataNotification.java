package com.gofercrm.user.gofercrm.notification.model;

import org.json.JSONObject;

public class DataNotification {
    public static int notificationVideoCall = 1;
    public static int notificationConnectionRequest = 2;
    public static int notificationPastTask = 3;
    public static int notificationUpcomingTask = 4;
    private String userId;
    private int type;
    private String name;
    private String toName;
    private long time;
    private String image;
    private int friend;
    private JSONObject jsonObject;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public String getToName() {
        return toName;
    }

    public void setToName(String toName) {
        this.toName = toName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getFriend() {
        return friend;
    }

    public void setFriend(int friend) {
        this.friend = friend;
    }

    public JSONObject getJsonObject() {
        return jsonObject;
    }

    public void setJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }
}
