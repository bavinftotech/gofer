package com.gofercrm.user.gofercrm.tenantcomment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.R;

public class ToastActivity extends BaseActivity {

    public static int forBulletinBroadCast = 1;
    public static int forLoginInfo = 2;

    private int from = 0;
    private String data = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toast);
        getDataFromIntent();
        findViewById();
        setUpHandler();
    }

    private void getDataFromIntent() {
        if (getIntent() != null) {
            from = getIntent().getIntExtra("from", 0);
            data = getIntent().getStringExtra("data");
        }
    }

    private void findViewById() {
        TextView text = findViewById(R.id.text);
        if (data != null && !data.equals("")) {
            text.setText(data);
        }
        text.setOnClickListener(view -> {
            if (from == forBulletinBroadCast) {
                Intent intent = new Intent(ToastActivity.this, TenantCommentActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } else if (from == forLoginInfo) {
                finish();
            }
        });
    }

    private void setUpHandler() {
        new Handler(Looper.getMainLooper()).postDelayed(this::finish, 5000);
    }
}