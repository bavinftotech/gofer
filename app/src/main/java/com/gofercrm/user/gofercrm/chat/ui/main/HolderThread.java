package com.gofercrm.user.gofercrm.chat.ui.main;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.R;

public class HolderThread extends RecyclerView.ViewHolder {

    private TextView time, chatText, name, onlineView;
    private TextView title, desc, size, thread_child_initial;
    private ImageView thumbnail, overflow, thread_child_photo;
    private RelativeLayout rl_thread;

    public HolderThread(View v) {
        super(v);
        name = v.findViewById(R.id.thread_child_name);
        time = v.findViewById(R.id.thread_child_time);
        chatText = v.findViewById(R.id.thread_child_text);
        title = v.findViewById(R.id.title);
        desc = v.findViewById(R.id.count);
        size = v.findViewById(R.id.size);
        thumbnail = v.findViewById(R.id.thumbnail);
        overflow = v.findViewById(R.id.overflow);
        onlineView = v.findViewById(R.id.thread_online_indicator);
        thread_child_initial = v.findViewById(R.id.thread_child_initial);
        thread_child_photo = v.findViewById(R.id.thread_child_photo);
        rl_thread = v.findViewById(R.id.rl_thread);
    }

    public TextView getTime() {
        return time;
    }

    public void setTime(TextView time) {
        this.time = time;
    }

    public TextView getChatText() {
        return chatText;
    }

    public void setChatText(TextView chatText) {
        this.chatText = chatText;
    }

    public TextView getTitle() {
        return title;
    }

    public void setTitle(TextView title) {
        this.title = title;
    }

    public TextView getDesc() {
        return desc;
    }

    public void setDesc(TextView desc) {
        this.desc = desc;
    }

    public TextView getSize() {
        return size;
    }

    public void setSize(TextView size) {
        this.size = size;
    }

    public ImageView getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(ImageView thumbnail) {
        this.thumbnail = thumbnail;
    }

    public ImageView getOverflow() {
        return overflow;
    }

    public void setOverflow(ImageView overflow) {
        this.overflow = overflow;
    }

    public TextView getName() {
        return name;
    }

    public void setName(TextView name) {
        this.name = name;
    }

    public TextView getOnlineView() {
        return onlineView;
    }

    public void setOnlineView(TextView onlineView) {
        this.onlineView = onlineView;
    }

    public RelativeLayout getRl_thread() {
        return rl_thread;
    }

    public void setRl_thread(RelativeLayout rl_thread) {
        this.rl_thread = rl_thread;
    }

    public TextView getThread_child_initial() {
        return thread_child_initial;
    }

    public void setThread_child_initial(TextView thread_child_initial) {
        this.thread_child_initial = thread_child_initial;
    }

    public ImageView getThread_child_photo() {
        return thread_child_photo;
    }

    public void setThread_child_photo(ImageView thread_child_photo) {
        this.thread_child_photo = thread_child_photo;
    }
}