package com.gofercrm.user.gofercrm.util;

import android.util.Log;

import com.gofercrm.user.gofercrm.BuildConfig;

public class LogUtils {
    public static void Print(String tag, String text) {
        if (BuildConfig.DEBUG)
            Log.e(tag, "==========" + text);
    }
}