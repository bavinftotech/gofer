package com.gofercrm.user.gofercrm.settings.privacy;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import com.gofercrm.user.gofercrm.ApplicationContext;
import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.databinding.ActivityPrivacyBinding;
import com.gofercrm.user.gofercrm.util.DialogUtils;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;

import org.json.JSONObject;

public class PrivacyActivity extends BaseActivity {

    private ActivityPrivacyBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_privacy);
        binding.setActivity(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.privacy));
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());
        setUpHeaderView();
    }

    private void setUpHeaderView() {
        binding.switchEmail.setChecked(ApplicationContext.preferences.getBoolean("email_visibility"));
        binding.switchPhone.setChecked(ApplicationContext.preferences.getBoolean("cell_visibility"));
        binding.switchProfile.setChecked(ApplicationContext.preferences.getBoolean("display_on_website"));
        binding.switchContact.setChecked(ApplicationContext.preferences.getBoolean("allow_contact_public_profile"));
        binding.switchLastSeen.setChecked(ApplicationContext.preferences.getBoolean("last_active_visibility"));
        binding.groupContact.setVisibility(binding.switchProfile.isChecked() ? View.VISIBLE : View.GONE);
    }

    public void onViewClicked(View view) {
        if (view == binding.ivInfoEmail) {
            DialogUtils.showInformationDialog(this, null,
                    getResources().getString(R.string.email_visibility_desc));
        } else if (view == binding.ivInfoPhone) {
            DialogUtils.showInformationDialog(this, null,
                    getResources().getString(R.string.phone_visibility_desc));
        } else if (view == binding.ivInfoProfile) {
            DialogUtils.showInformationDialog(this, null,
                    getResources().getString(R.string.profile_visibility_desc));
        } else if (view == binding.ivInfoContact) {
            DialogUtils.showInformationDialog(this, null,
                    getResources().getString(R.string.allow_profile_contact_desc));
        }else if (view == binding.ivLastSeen) {
            DialogUtils.showInformationDialog(this, null,
                    getResources().getString(R.string.last_seen_visibility_desc));
        } else if (view == binding.switchEmail) {
            changePrivacyAPIRequest();
        } else if (view == binding.switchPhone) {
            changePrivacyAPIRequest();
        } else if (view == binding.switchProfile) {
            binding.groupContact.setVisibility(binding.switchProfile.isChecked() ? View.VISIBLE : View.GONE);
            changePrivacyAPIRequest();
        } else if (view == binding.switchContact) {
            changePrivacyAPIRequest();
        }else if (view == binding.switchLastSeen) {
            changePrivacyAPIRequest();
        }
    }

    private void changePrivacyAPIRequest() {
        String strURL = Constants.SET_PUBLIC_PROFILE;
        strURL += "?cell_visibility=" + (binding.switchPhone.isChecked() ? "true" : "false");
        strURL += "&email_visibility=" + (binding.switchEmail.isChecked() ? "true" : "false");
        strURL += "&display_on_website=" + (binding.switchProfile.isChecked() ? "true" : "false");
        strURL += "&allow_contact_public_profile=" + (binding.switchContact.isChecked() ? "true" : "false");
        strURL += "&last_active_visibility=" + (binding.switchLastSeen.isChecked() ? "true" : "false");
        LogUtils.Print("strURL--> ", "" + strURL);
        NetworkManager.customJsonObjectRequest(
                PrivacyActivity.this, strURL, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject userObject) {
                        LogUtils.Print("onResponse ", "" + userObject);
                        try {
                            if (userObject.has("public_profile")) {
                                JSONObject jsonObject = userObject.getJSONObject("public_profile");
                                if (jsonObject.has("cell_visibility"))
                                    ApplicationContext.preferences.putBoolean("cell_visibility", jsonObject.getBoolean("cell_visibility"));
                                if (jsonObject.has("email_visibility"))
                                    ApplicationContext.preferences.putBoolean("email_visibility", jsonObject.getBoolean("email_visibility"));
                                if (jsonObject.has("display_on_website"))
                                    ApplicationContext.preferences.putBoolean("display_on_website", jsonObject.getBoolean("display_on_website"));
                                if (jsonObject.has("allow_contact_public_profile"))
                                    ApplicationContext.preferences.putBoolean("allow_contact_public_profile", jsonObject.getBoolean("allow_contact_public_profile"));
                                if (jsonObject.has("last_active_visibility"))
                                    ApplicationContext.preferences.putBoolean("last_active_visibility", jsonObject.getBoolean("last_active_visibility"));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("onError=", message);
                        Util.showSnackBar(message, PrivacyActivity.this);
                    }
                }, false);
    }
}