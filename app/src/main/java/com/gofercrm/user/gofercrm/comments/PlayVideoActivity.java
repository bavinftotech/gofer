package com.gofercrm.user.gofercrm.comments;

import android.net.Uri;
import android.os.Bundle;
import android.view.Surface;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.R;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.decoder.DecoderCounters;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.exoplayer2.video.VideoRendererEventListener;
import com.huawei.multimedia.audiokit.utils.LogUtils;

public class PlayVideoActivity extends BaseActivity implements View.OnClickListener,
        VideoRendererEventListener {

    private static final String TAG = "PlayVideoActivity";

    //Header View
    private SimpleExoPlayerView simpleExoPlayerView;
    private SimpleExoPlayer player;

    private String strURL;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_video);
        getDataFromIntent();
        findViewById();
        setUpHeaderView();
        main();
    }

    private void getDataFromIntent() {
        strURL = getIntent().getStringExtra("DATA");
    }

    /**
     * findViews by Ids
     */
    private void findViewById() {

        LoadControl loadControl = new DefaultLoadControl();

        player = ExoPlayerFactory.newSimpleInstance(this, new DefaultTrackSelector(), loadControl);
        simpleExoPlayerView = new SimpleExoPlayerView(this);
        simpleExoPlayerView = findViewById(R.id.player_view1);

        simpleExoPlayerView.setUseController(true);
        simpleExoPlayerView.requestFocus();

        simpleExoPlayerView.setPlayer(player);
        Uri mp4VideoUri = Uri.parse(strURL);

        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this,
                Util.getUserAgent(this, getResources().getString(R.string.app_name)));
        MediaSource videoSource = new ProgressiveMediaSource.Factory(dataSourceFactory)
                .createMediaSource(mp4VideoUri);
        player.prepare(videoSource);

        player.addListener(new Player.EventListener() {

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                LogUtils.error(TAG, "Listener-onPlayerStateChanged..." + playbackState);
                if (playbackState == 4)
                    finish();
            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {
                LogUtils.error(TAG, "Listener-onPlayerError...");
                player.stop();
                player.prepare(videoSource);
                player.setPlayWhenReady(true);
            }
        });

        player.setRepeatMode(Player.REPEAT_MODE_OFF);
        player.setPlayWhenReady(true);
        player.setVideoDebugListener(this);
    }

    /**
     * Set header data
     */
    private void setUpHeaderView() {

    }

    /**
     * main
     */
    private void main() {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onVideoEnabled(DecoderCounters counters) {

    }

    @Override
    public void onVideoDecoderInitialized(String decoderName, long initializedTimestampMs, long initializationDurationMs) {

    }

    @Override
    public void onVideoInputFormatChanged(Format format) {

    }

    @Override
    public void onDroppedFrames(int count, long elapsedMs) {

    }

    @Override
    public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees, float pixelWidthHeightRatio) {
        LogUtils.error(TAG, "onVideoSizeChanged [" + " width: " + width + " height: " + height + "]");
    }

    @Override
    public void onRenderedFirstFrame(Surface surface) {

    }

    @Override
    public void onVideoDisabled(DecoderCounters counters) {

    }

    @Override
    protected void onStop() {
        super.onStop();
        LogUtils.error(TAG, "onStop()...");
    }

    @Override
    protected void onStart() {
        super.onStart();
        LogUtils.error(TAG, "onStart()...");
    }

    @Override
    protected void onResume() {
        super.onResume();
        LogUtils.error(TAG, "onResume()...");
    }

    @Override
    protected void onPause() {
        super.onPause();
        LogUtils.error(TAG, "onPause()...");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LogUtils.error(TAG, "onDestroy()...");
        player.release();
    }
}