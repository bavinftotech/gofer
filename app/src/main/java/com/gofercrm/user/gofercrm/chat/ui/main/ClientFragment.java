package com.gofercrm.user.gofercrm.chat.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.LauncherActivity;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.chat.ui.main.model.Chat;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.gofercrm.user.gofercrm.websocket.SharedObserver;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static java.util.Comparator.comparing;
import static java.util.Comparator.nullsLast;
import static java.util.Comparator.reverseOrder;

public class ClientFragment extends Fragment implements ChatAdapter.ClickListener {

    private static final String ARG_SECTION_NUMBER = "section_number";

    private RecyclerView mRecyclerView;
    private ChatAdapter mAdapter;
    private TextView tv_selection;
    private List<Chat> data = new ArrayList<>();
    private ProgressBar progressBar;

    //share intent
    private boolean isFromShare = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        SharedObserver.getSocketObserver().subscribe(s -> {
            processTyping(s);
        });
        super.onCreate(savedInstanceState);
    }

    public static ClientFragment newInstance(int sectionNumber) {
        ClientFragment fragment = new ClientFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ccc, null, false);
        progressBar = view.findViewById(R.id.progressBar);
        tv_selection = view.findViewById(R.id.tv_selection);
        mRecyclerView = view.findViewById(R.id.recyclerView);
        TextInputLayout tlSearch = view.findViewById(R.id.tlSearch);
        tlSearch.setVisibility(View.GONE);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mAdapter = new ChatAdapter(getContext(), data, this, 3);
        mRecyclerView.setAdapter(mAdapter);
        if (data.size() == 0) {
            getContacts();
        }
        return view;
    }

    private void onLoaded() {
        data.sort(comparing(
                Chat::getTime,
                nullsLast(reverseOrder())
        ));
        mAdapter = new ChatAdapter(getContext(), data, this, 3);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClicked(int position) {
        Intent intent = new Intent(getContext(), Conversation.class);
        if (ChatPagerAdapter.intent != null && ChatPagerAdapter.intent.getBooleanExtra(LauncherActivity.IS_FROM_SHARE, false)) {
            intent.putExtra(LauncherActivity.IS_FROM_SHARE, ChatPagerAdapter.intent.getBooleanExtra(LauncherActivity.IS_FROM_SHARE, false));
            intent.putExtra(LauncherActivity.SHARE_URI, ChatPagerAdapter.intent.getStringExtra(LauncherActivity.SHARE_URI));
        }
        intent.putExtra("DATA", intent.getExtras());
        intent.putExtra("_ID", (Serializable) data.get(position).get_Id());
        intent.putExtra("_TYPE", (Serializable) 0);
        intent.putExtra("_NAME", data.get(position).getName());
        intent.putExtra("_TIMEZONE", data.get(position).getTimeZone());
        if (data.get(position).getTranslateLanguage() != null)
            intent.putExtra("_LANG", data.get(position).getTranslateLanguage());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Objects.requireNonNull(getContext()).startActivity(intent);
        ChatPagerAdapter.intent = null;
    }

    @Override
    public boolean onItemLongClicked(int position) {
        toggleSelection(position);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    private void toggleSelection(int position) {
        mAdapter.toggleSelection(position);
        if (mAdapter.getSelectedItemCount() > 0) {
            tv_selection.setVisibility(View.VISIBLE);
        } else
            tv_selection.setVisibility(View.GONE);
        getActivity().runOnUiThread(() -> tv_selection.setText(
                getResources().getString(R.string.delete) +
                        " (" + mAdapter.getSelectedItemCount() + ")"));
    }

    private void getContacts() {
        progressBar.setVisibility(View.VISIBLE);
        NetworkManager.customJsonObjectGetRequest(
                //getContext(), Constants.CLIENTS_USERS_URL, null,
                getContext(), Constants.FRIENDS_FETCH_URL +"?contact_type_filter=client", null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressBar.setVisibility(View.GONE);
                        processData(response);
                    }

                    @Override
                    public void onError(String message) {
                        progressBar.setVisibility(View.GONE);
                        LogUtils.Print("ERRROR=", message);
                    }
                }, true);
    }

    private void processData(JSONObject response) {
        try {
            if (Integer.parseInt(response.getString("result")) == 1) {
                data = new ArrayList<>();
                JSONArray users = response.getJSONArray("users");
                JSONObject result_obj = new JSONObject();
                for (int i = 0; i < users.length(); i++) {
                    result_obj = users.getJSONObject(i);

                    Chat chat = new Chat();
                    if (result_obj.has("last_seen")) {
                        chat.setmTime(result_obj.getLong("last_seen"));
                    }
                    if (result_obj.has("first_name")) {
                        String name = result_obj.getString("first_name") + " " + result_obj.getString("last_name");
                        chat.setName(name);
                    }
                    if (result_obj.has("id")) {
                        chat.set_Id(result_obj.getString("id"));
                    }

                    chat.setImage(result_obj.getString("avatar_url"));
                    if (result_obj.has("is_active")) {
                        chat.setOnline(result_obj.getBoolean("is_active"));
                    }
                    chat.setLastChat(result_obj.getString("status"));
                    if (result_obj.has("translation_settings_dict")) {
                        JSONObject jsonObject = result_obj.getJSONObject("translation_settings_dict");
                        if (jsonObject.has(chat.get_Id())) {
                            JSONArray jsonArray = jsonObject.getJSONArray(chat.get_Id());
                            if (jsonArray.length() > 1) {
                                chat.setTranslateLanguage(jsonArray.get(1).toString());
                            }
                        }
                    }

                    if (result_obj.has("timeZone")) {
                        chat.setTimeZone(result_obj.getString("timeZone"));
                    }
                    data.add(chat);
                }
                onLoaded();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void processTyping(String response) {
        try {
            JSONObject input = new JSONObject(response);
            if (input.has("type") && input.getString("type").equals("contact_logout")) {
                String user_id = input.has("user_id") ? input.getString("user_id") : "";
                for (Chat user : data) {
                    if (user.get_Id().equals(user_id)) {
                        user.setOnline(false);
                        /*if (getActivity() != null)
                            getActivity().runOnUiThread(() -> {
                                onLoaded();
                            });*/
                        break;
                    }
                }
            } else if (input.has("type") && input.getString("type").equals("contact_login")) {
                String user_id = input.has("user_id") ? input.getString("user_id") : "";
                for (Chat user : data) {
                    if (user.get_Id().equals(user_id)) {
                        user.setOnline(true);
                        if (getActivity() != null)
                            getActivity().runOnUiThread(() -> {
                                onLoaded();

                            });
                        break;
                    }
                }
            } else if (input.has("type") && input.getString("type").equals("message")) {
                String user_id = input.has("sender_id") ? input.getString("sender_id") : "";
                for (Chat user : data) {
                    if (user.get_Id().equals(user_id)) {
                        user.setMsg_count(1);
                        if (getActivity() != null)
                            getActivity().runOnUiThread(() -> {
                                onLoaded();
                            });
                        break;
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}