package com.gofercrm.user.gofercrm.appointments.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.appointments.model.DataAppointment;
import com.gofercrm.user.gofercrm.databinding.RowAppointmentBinding;

import java.util.List;

public class AppointmentViewAdapter extends RecyclerView.Adapter<AppointmentViewAdapter.ViewHolder> {

    private List<DataAppointment> list;

    public AppointmentViewAdapter(List<DataAppointment> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RowAppointmentBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.row_appointment, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.setData(list.get(position));
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tv;
        private RowAppointmentBinding binding;

        public ViewHolder(RowAppointmentBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}