package com.gofercrm.user.gofercrm.attachments;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.util.FileUtil;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class AttachmentActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = "AttachmentActivity";
    private static final int READ_REQUEST_CODE = 42;
    SharedPreferences sharedPref;
    String token;
    String file_upload_result;
    SwipeRefreshLayout mSwipeRefreshLayout;
    TextView no_attachments;
    private RecyclerView recyclerView;
    private AttachmentAdapter adapter;
    private List<Attachment> attachmentList;
    private CoordinatorLayout coordinatorLayout;
    private String ENTITY_TYPE = "contact";
    private String ENTITY_ID = "5c2e3b2bc71d8e07ef55f54d";
    private String ENTITY_KEY = "entity_id";
    private String FROM = "";
    public boolean needToUpdate = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attachment);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(getResources().getString(R.string.attachments));
        }
        no_attachments = findViewById(R.id.no_attachments);

        mSwipeRefreshLayout = findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        Intent intent = getIntent();
        if (intent.getStringExtra("ENTITY_KEY") != null) {
            ENTITY_KEY = intent.getStringExtra("ENTITY_KEY");
        }
        if (intent.getStringExtra("FROM") != null) {
            FROM = intent.getStringExtra("FROM");
        }
        if (intent.getStringExtra("ENTITY_ID") != null) {
            ENTITY_ID = intent.getStringExtra("ENTITY_ID");
        }
        if (intent.getStringExtra("ENTITY_TYPE") != null) {
            ENTITY_TYPE = intent.getStringExtra("ENTITY_TYPE");
        }

        sharedPref = getApplicationContext().getSharedPreferences("LOGIN", MODE_PRIVATE);
        token = sharedPref.getString("USER_TOKEN", "");
        coordinatorLayout = findViewById(R.id
                .attachment_coordinator_layout_id);

        recyclerView = findViewById(R.id.attachment_recycler_view);

        attachmentList = new ArrayList<>();
        adapter = new AttachmentAdapter(this, attachmentList);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        getAttachments(ENTITY_TYPE, ENTITY_ID);
        FloatingActionButton fab = findViewById(R.id.file_upload_fab);
        fab.setOnClickListener(view -> performFileSearch());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        if (needToUpdate) {
            setResult(RESULT_OK);
            finish();
            return;
        }
        super.onBackPressed();
    }

    private void prepareAttachments(JSONObject response) {
        attachmentList.clear();
        String id = "", name = "", url = "", type = "", size = "", entity_type = "", entity_id = "";
        Long created_on = null;
        int thumbnail = R.drawable.file;
        Attachment attachment;
        try {
            if (Integer.parseInt(response.getString("result")) == 1) {
                JSONArray attachments_array = response.getJSONArray("attachment_info_list");
                for (int i = 0; i < attachments_array.length(); i++) {
                    JSONObject obj = attachments_array.getJSONObject(i);
                    if (obj.has("id")) {
                        id = obj.getString("id");

                    }
                    if (obj.has("name")) {
                        name = obj.getString("name");

                    }
                    if (obj.has("url")) {
                        url = obj.getString("url");

                    }
                    if (obj.has("size")) {
                        size = obj.getString("size");
                        if (size != null && !size.isEmpty()) {
                            long num = Long.valueOf(size);
                            size = FileUtil.getStringSizeLengthFile(num);
                        } else {
                            size = "0b";
                        }
                    }
                    if (obj.has("content_type")) {
                        type = obj.getString("content_type");

                    }
                    if (obj.has("created_ts")) {
                        created_on = obj.getLong("created_ts");
                    }
                    if (obj.has("entity_type")) {
                        entity_type = obj.getString("entity_type");
                    }
                    if (obj.has("entity_id")) {
                        entity_id = obj.getString("entity_id");
                    }

                    attachment = new Attachment(id, name, getThumbNails(type), "", url, type, created_on, entity_type, entity_id, size);
                    attachmentList.add(attachment);
                }
            }
        } catch (JSONException e) {
            onLoaded(e.getMessage());
            e.printStackTrace();
        }
        adapter.notifyDataSetChanged();
    }

    private void getAttachments(String entity_type, String entity_id) {
        mSwipeRefreshLayout.setRefreshing(true);
        JSONObject postData = new JSONObject();
        try {
            postData.put("action", "get");
//            postData.put("page_size", "100");
            postData.put("entity_type", entity_type);
            postData.put(ENTITY_KEY, entity_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        LogUtils.Print("postData--> ", "" + postData);
        NetworkManager.customJsonObjectRequest(
                AttachmentActivity.this, Constants.ATTACHMENT_LIST_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtils.Print("TAG", "response --> " + response);
                        prepareAttachments(response);
                        mSwipeRefreshLayout.setRefreshing(false);
                        onLoaded("");
                    }

                    @Override
                    public void onError(String message) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        onLoaded(message);
                        LogUtils.Print("ERRROR=", message);
                    }
                }, true);
    }

    @Override
    public void onRefresh() {
        getAttachments(ENTITY_TYPE, ENTITY_ID);
    }

    private void onLoaded(String message) {
        if (attachmentList.size() <= 0) {
            no_attachments.setVisibility(View.VISIBLE);
        } else if (message != null && !message.isEmpty() && attachmentList.size() <= 0) {
            no_attachments.setVisibility(View.VISIBLE);
            no_attachments.setText(message);
        } else if (attachmentList.size() > 0) {
            no_attachments.setVisibility(View.GONE);
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    private void uploadFile(Uri uri, String file_name, String file_type, String entity_type, String entity_id) {
        final Snackbar snackbar = Snackbar.make(coordinatorLayout, getResources().getString(R.string.a_file_is_being_uploaded), Snackbar.LENGTH_LONG);
        snackbar.show();

        OkHttpClient httpClient = new OkHttpClient();
        RequestBody requestBody = null;
        try {
            File file = FileUtil.from(AttachmentActivity.this, uri);
            requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("filename", file_name)
                    .addFormDataPart("attachment_entity_type", entity_type)
                    .addFormDataPart("attachment_entity_id", entity_id)
                    .addFormDataPart("attachment", file_name, RequestBody.create(MediaType.parse(file_type),
                            file)).build();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Request request = new Request.Builder()
                .url(Constants.UPLOAD_ATTACHMENT_URL)
                .addHeader("Authorization", "Bearer " + token)
                .post(requestBody)
                .build();

        showProgress();
        httpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                hideProgress();
                e.printStackTrace();
                LogUtils.Print("ERORRORR", "error in getting response using async okhttp call");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                hideProgress();
                ResponseBody responseBody = response.body();
                if (!response.isSuccessful()) {
                    throw new IOException("Error response " + response);
                }
                file_upload_result = responseBody.string();
                LogUtils.Print(TAG, "file_upload_result -> " + file_upload_result);
                try {
                    new Handler(Looper.getMainLooper()).post(() -> sendAttachment(file_upload_result, file_name));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void sendAttachment(String data, String file_name) {
        try {
            JSONObject response = new JSONObject(data);
            LogUtils.Print(TAG, "response --> " + response);
            if (Integer.parseInt(response.getString("result")) == 1) {
                String attachment_id = "";
                Attachment attachment;
                Long created_on = null;
                String id = "", name = "", url = "", type = "", size = "", entity_type = "", entity_id = "";
                if (response.has("att_id")) {
                    attachment_id = response.getString("att_id");
                }
                if (response.has("att_id")) {
                    id = response.getString("att_id");
                }
                name = file_name;
                if (response.has("url")) {
                    url = response.getString("url");
                }
                if (response.has("size")) {
                    size = response.getString("size");
                    if (!size.isEmpty()) {
                        long num = Long.valueOf(size);
                        size = FileUtil.getStringSizeLengthFile(num);
                    } else {
                        size = "0b";
                    }
                }
                if (response.has("content_type")) {
                    type = response.getString("content_type");

                }
                if (response.has("date")) {
                    created_on = response.getLong("date");
                }
                entity_type = ENTITY_TYPE;
                entity_id = ENTITY_ID;
                attachment = new Attachment(id, name, getThumbNails(type), "", url, type, created_on, entity_type, entity_id, size);
                needToUpdate = true;
                attachmentList.add(attachment);
                adapter.notifyItemInserted(attachmentList.size() - 1);
                no_attachments.setVisibility(View.GONE);
                if (FROM.equals("FROM_ROOM")) {
                    sendRoomMessage(attachment_id);
                } else if (FROM.equals("FROM_CONNECTION")) {
                    sendConnectionMessage(attachment_id);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void sendRoomMessage(String attachment_id) {
        String param = "?room_id=" + ENTITY_ID + "&text=&quoted_text=";
        if (attachment_id != null) {
            param = param + "&attachment_id=" + attachment_id;
        }
        String url = Constants.SEND_ROOM_MESSAGE + param;
        NetworkManager.customJsonObjectRequest(
                getApplicationContext(), url, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtils.Print(TAG, "response --> " + response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print(TAG, "onError --> " + message);
                    }
                }, true);
    }

    private void sendConnectionMessage(String att_id) {
        String param = "?to_id=" + ENTITY_ID + "&text=&quoted_text=";
        if (att_id != null) {
            param = param + "&type=file" + "&att_id=" + att_id;
        }
        String url = Constants.SEND_CONNECTION_MESSAGE + param;
        NetworkManager.customJsonObjectRequest(
                getApplicationContext(), url, null,
                new VolleyResponseListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtils.Print(TAG, "response --> " + response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print(TAG, "onError --> " + message);
                    }
                }, true);
    }

    public void performFileSearch() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        startActivityForResult(intent, READ_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode,
                                 Intent resultData) {
        super.onActivityResult(requestCode, resultCode, resultData);
        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri uri = null;
            if (resultData != null) {
                uri = resultData.getData();
                String mimeType = FileUtil.getMimeType(getApplicationContext(), uri);
                String fileName = FileUtil.getFileName(getApplicationContext(), uri);
                uploadFile(uri, fileName, mimeType, ENTITY_TYPE, ENTITY_ID);
            }
        }
    }

    private int getThumbNails(String type) {
        int thumbnail = R.drawable.file;
        if (type.contains("image")) {
            thumbnail = R.drawable.file_image;
        } else if (type.contains("pdf")) {
            thumbnail = R.drawable.file_pdf;
        } else if (type.contains("msword")) {
            thumbnail = R.drawable.file_word_box;
        } else if (type.contains("audio")) {
            thumbnail = R.drawable.audiobook;
        } else if (type.contains("video")) {
            thumbnail = R.drawable.file_video;
        } else if (type.contains("text")) {
            thumbnail = R.drawable.file;
        } else if (type.contains("csv")) {
            thumbnail = R.drawable.file_delimited;
        } else if (type.contains("xml")) {
            thumbnail = R.drawable.file_xml;
        } else if (type.contains("excel")) {
            thumbnail = R.drawable.file_excel;
        }
        return thumbnail;
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }
}