package com.gofercrm.user.gofercrm.account;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.telephony.PhoneNumberUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.R;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class AccountGeneralAdapter extends RecyclerView.Adapter<AccountGeneralAdapter.MyViewHolder> {

    private List<AccountView> clientPageElements;
    private Context ctx;

    public AccountGeneralAdapter(List<AccountView> accountViewList, Context ctx) {
        this.clientPageElements = accountViewList;
        this.ctx = ctx;
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lead_view_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final AccountView client = clientPageElements.get(position);
        holder.label.setText(client.getLabel());
        if (client.getType().equals("PHONE") && !client.getValue().isEmpty()) {
            holder.value.setText(PhoneNumberUtils.formatNumber(client.getValue(), "US"));
        } else {
            holder.value.setText(client.getValue());
        }

        if (client.getLeftDrawable() != 0) {
            holder.leftImage.setImageResource(client.getLeftDrawable());
        }
        if (client.getRightDrawable() != 0) {
            holder.rightImage.setImageResource(client.getRightDrawable());
        }

        holder.itemView.setOnClickListener(view -> {
            if (client.getType().equals("PHONE") && !client.getValue().isEmpty()) {
                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.setData(Uri.parse("sms:" + client.getValue()));
                sendIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ctx.startActivity(sendIntent);

            } else if (client.getType().equals("EMAIL") && !client.getValue().isEmpty()) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                Uri data = Uri.parse("mailto:" + client.getValue());
                intent.setData(data);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ctx.startActivity(intent);

            } else if (client.getType().equals("ADDRESS") && !client.getValue().isEmpty()) {
                Uri gmmIntentUri = Uri.parse("google.navigation:q=" + client.getValue());
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                mapIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ctx.startActivity(mapIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return clientPageElements.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView label, value;
        public ImageView leftImage, rightImage;

        public MyViewHolder(View view) {
            super(view);
            label = view.findViewById(R.id.leadView_label_id);
            value = view.findViewById(R.id.lead_view_data_id);
            leftImage = view.findViewById(R.id.lead_img_btn_left);
            rightImage = view.findViewById(R.id.lead_img_btn_right);
        }
    }
}