package com.gofercrm.user.gofercrm.opportunityclientlead.add;

import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;

import androidx.databinding.DataBindingUtil;

import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.databinding.ActivityManageOpportunityClientBinding;
import com.gofercrm.user.gofercrm.implementor.DialogButtonClickListener;
import com.gofercrm.user.gofercrm.implementor.DialogDateClickListener;
import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.opportunities.Opportunity;
import com.gofercrm.user.gofercrm.opportunityclientlead.model.DataProperty;
import com.gofercrm.user.gofercrm.util.DatePickerUtils;
import com.gofercrm.user.gofercrm.util.DialogUtils;
import com.gofercrm.user.gofercrm.util.InputFilterMinMax;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ManageOpportunityClientActivity extends BaseActivity {

    private static final String TAG = "ManageOppotunityClientLeadActivity";

    String SELECTED_STAGE = "closed_won";

    String selected_property_id;

    String PROCESSING_MESSAGE;
    String SUCCESS_MESSAGE;

    //Data binding
    private ActivityManageOpportunityClientBinding binding;

    private Opportunity opportunity = null;

    private List<DataProperty> listProperty = new ArrayList<>();
    private List<String> strListProperty = new ArrayList<>();

    private List<String> strMarketingSource = new ArrayList<>();
    private String strMMDDYYYY = "MM/dd/yyyy";
    private String strYYYYMMDD = "yyyy-MM-dd";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_manage_opportunity_client);
        binding.setActivity(this);
        PROCESSING_MESSAGE = getResources().getString(R.string.creating_opportunity_);
        SUCCESS_MESSAGE = getResources().getString(R.string.opportunity_create_success);
        getDataFromIntent();
        setUpHeaderView();
        manageData();
        getProperty();
        getMarketingSource();
    }

    private void manageData() {
        radioButtons = new RadioButton[]{
                binding.radioDiscovery, binding.radioProspecting, binding.radioDeveloping,
                binding.radioNegotiationReview, binding.radioContract, binding.radioWon,
                binding.radioLost};

        if (opportunity != null && opportunity.getId() != null) {
            if (opportunity.getStage() != null && !opportunity.getStage().equals("")) {
                switch (opportunity.getStage()) {
                    case "closed_won":
                        binding.radioWon.setChecked(true);
                        onRadioButtonClicked(binding.radioWon);
                        break;
                    case "closed_lost":
                        binding.radioLost.setChecked(true);
                        onRadioButtonClicked(binding.radioLost);
                        break;
                    case "discovery":
                        binding.radioDiscovery.setChecked(true);
                        onRadioButtonClicked(binding.radioDiscovery);
                        break;
                    case "prospecting":
                        binding.radioProspecting.setChecked(true);
                        onRadioButtonClicked(binding.radioProspecting);
                        break;
                    case "developing":
                        binding.radioDeveloping.setChecked(true);
                        onRadioButtonClicked(binding.radioDeveloping);
                        break;
                    case "negotiation_review":
                        binding.radioNegotiationReview.setChecked(true);
                        onRadioButtonClicked(binding.radioNegotiationReview);
                        break;
                    case "contact":
                        binding.radioContract.setChecked(true);
                        onRadioButtonClicked(binding.radioContract);
                        break;
                    default:
                        binding.radioWon.setChecked(true);
                        onRadioButtonClicked(binding.radioWon);
                }
            }

            binding.etName.setText(opportunity.getName() != null ? opportunity.getName() : "");
            binding.etInvoiceAmount.setText(opportunity.getSaleValue() != null ? opportunity.getSaleValue() : "");
            SimpleDateFormat format = new SimpleDateFormat(strMMDDYYYY);
            String etExpectedMoveInDate = "", etExpectedMoveOutDate = "";
            if (opportunity.getClosedDate() != null) {
                etExpectedMoveInDate = format.format(new Date(opportunity.getClosedDate() * 1000));
                binding.etExpectedMoveInDate.setText(etExpectedMoveInDate);
            }
            if (opportunity.getRenewalDate() != null) {
                etExpectedMoveOutDate = format.format(new Date(opportunity.getRenewalDate() * 1000));
                binding.etExpectedMoveOutDate.setText(etExpectedMoveOutDate);
            }
            if (!etExpectedMoveInDate.equals("") && !etExpectedMoveOutDate.equals("")) {
                String strLease = "" + Util.getDateDifferenceInMonth(etExpectedMoveInDate, etExpectedMoveOutDate);
                binding.etLeaseTerm.setText(strLease);
            }
            binding.etMarketingRent.setText(opportunity.getMarket_rent_rate() != null ?
                    opportunity.getMarket_rent_rate() : "");
            binding.etUnitNumber.setText(opportunity.getUnit_number() != null ?
                    opportunity.getUnit_number() : "");
            binding.etRentalConcession.setText(opportunity.getRental_concession() != null ?
                    opportunity.getRental_concession() : "");

            if (opportunity.getLostClosedDate() != null) {
                String date = format.format(new Date(opportunity.getLostClosedDate() * 1000));
                binding.etCloseDate.setText(date);
            }
            if (opportunity.getLostFollowUpDate() != null) {
                String date = format.format(new Date(opportunity.getLostFollowUpDate() * 1000));
                binding.etFollowupDate.setText(date);
            }

            binding.etLossReason.setText(opportunity.getLoss_reason() != null ?
                    opportunity.getLoss_reason() : "");
            binding.etComments.setText(opportunity.getLossComments() != null ?
                    opportunity.getLossComments() : "");

        } else {
            binding.radioWon.setChecked(true);
            onRadioButtonClicked(binding.radioWon);

            binding.etName.setText((opportunity != null && opportunity.getUserName() != null ? opportunity.getUserName() : "")
                    + " " + getResources().getString(R.string.record_created) + " " + Util.getCurrentDate("MM-dd-yyyy"));
            binding.etExpectedMoveInDate.setText(Util.getCurrentDate(strMMDDYYYY));
            binding.etExpectedMoveOutDate.setText(Util.getFutureMonthDate(strMMDDYYYY, 12));
            binding.etLeaseTerm.setText("12");

            binding.etCloseDate.setText(Util.getCurrentDate(strMMDDYYYY));
            binding.etFollowupDate.setText(Util.getFutureMonthDate(strMMDDYYYY, 12));
        }

        binding.etLeaseTerm.setFilters(new InputFilter[]{new InputFilterMinMax(1, 18)});
        binding.etLeaseTerm.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!charSequence.toString().trim().isEmpty()) {
                    if (Integer.parseInt(charSequence.toString().trim()) >= 3 &&
                            Integer.parseInt(charSequence.toString().trim()) <= 18) {
                        binding.etExpectedMoveOutDate.setText(Util.getFutureMonthDate(strMMDDYYYY,
                                Integer.parseInt(charSequence.toString().trim())));
                    } else {
                        Util.makeToast(getResources().getString(R.string.lear_term_period_err));
                    }
                } else {
                    Util.makeToast(getResources().getString(R.string.lear_term_period_err));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void setUpHeaderView() {
        binding.toolbar.setTitle(
                (opportunity != null && opportunity.getId() != null ?
                        getResources().getString(R.string.update) :
                        getResources().getString(R.string.new_)) +
                        " " + getResources().getString(R.string.opportunity_for) + " " +
                        (opportunity != null && opportunity.getUserName() != null ? opportunity.getUserName() : ""));
        setSupportActionBar(binding.toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    /**
     * GET DATA FROM INTENT
     */
    private void getDataFromIntent() {
        if (getIntent() != null) {
            if (getIntent().getSerializableExtra("DATA") != null) {
                opportunity = (Opportunity) getIntent().getSerializableExtra("DATA");
            }
        }
    }

    private RadioButton[] radioButtons;

    public void onRadioButtonClicked(View view) {
        for (int i = 0; i < radioButtons.length; i++) {
            if (view != radioButtons[i]) {
                radioButtons[i].setChecked(false);
            }
        }
        switch (view.getId()) {
            case R.id.radio_won:
                SELECTED_STAGE = "closed_won";
                LogUtils.Print(TAG, "radio_won");
                binding.groupWon.setVisibility(View.VISIBLE);
                binding.groupLoss.setVisibility(View.GONE);
                break;
            case R.id.radio_lost:
                SELECTED_STAGE = "closed_lost";
                LogUtils.Print(TAG, "radio_lost");
                binding.groupWon.setVisibility(View.GONE);
                binding.groupLoss.setVisibility(View.VISIBLE);
                break;
            default:
                switch (view.getId()) {
                    case R.id.radio_discovery:
                        SELECTED_STAGE = "discovery";
                        break;
                    case R.id.radio_prospecting:
                        SELECTED_STAGE = "prospecting";
                        break;
                    case R.id.radio_developing:
                        SELECTED_STAGE = "developing";
                        break;
                    case R.id.radio_negotiation_review:
                        SELECTED_STAGE = "negotiation_review";
                        break;
                    case R.id.radio_contract:
                        SELECTED_STAGE = "closed_won";
                        break;
                    default:
                        SELECTED_STAGE = "discovery";
                }
                binding.groupWon.setVisibility(View.GONE);
                binding.groupLoss.setVisibility(View.GONE);
                LogUtils.Print(TAG, "other");
                break;
        }
    }

    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.etExpectedMoveInDate:
            case R.id.etExpectedMoveOutDate:
            case R.id.etCloseDate:
            case R.id.etFollowupDate:
                DatePickerUtils.getSelectedDateDialog(this, ((EditText) view).getText().toString().trim(),
                        strMMDDYYYY, strMMDDYYYY, new DialogDateClickListener() {
                            @Override
                            public void onDateClick(String date) {
                                LogUtils.Print("TAG", "date -> " + date);
                                ((EditText) view).setText(date);
                            }

                            @Override
                            public void onCancelClick() {

                            }
                        });
                break;
        }
    }


    public boolean validate() {
        if (binding.etName.getText().toString().trim().isEmpty()) {
            binding.tlName.setErrorEnabled(true);
            binding.tlName.setError(getResources().getString(R.string.name_empty_err_));
            return false;
        } else {
            binding.tlName.setError(null);
            binding.tlName.setErrorEnabled(false);
        }

        if (binding.actvProperties.getText().toString().trim().isEmpty()) {
            Util.onMessage(this, getResources().getString(R.string.properties_selection_err));
            return false;
        }

        if (binding.radioWon.isChecked()) {
            if (binding.etUnitNumber.getText().toString().trim().isEmpty()) {
                binding.tlUnitNumber.setErrorEnabled(true);
                binding.tlUnitNumber.setError(getResources().getString(R.string.unit_number_empty_err));
                return false;
            } else {
                binding.tlUnitNumber.setErrorEnabled(false);
                binding.tlUnitNumber.setError(null);
            }
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_oppo_lead_client, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (id == R.id.action_save) {
            if (validate()) {
                DialogUtils.showConfirmationDialog(this, new DialogButtonClickListener() {
                    @Override
                    public void onPositiveButtonClick() {
                        saveOpportunity();
                    }

                    @Override
                    public void onNegativeButtonClick() {

                    }
                }, getResources().getString(R.string.proceed_invoice_confirmation) + " " +
                        (binding.etInvoiceAmount.getText().toString().trim().isEmpty() ? "null?" :
                                binding.etInvoiceAmount.getText().toString().trim() + "?"));
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void saveOpportunity() {
        showProgress();
        JSONObject postData = new JSONObject();
        try {
            if (opportunity != null && opportunity.getId() != null) {
                postData.put("action", "update");
                postData.put("opportunity_id", opportunity.getId());
            } else {
                postData.put("action", "add");
            }
            postData.put("payload", requestData());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        LogUtils.Print(TAG, "postData --> " + postData.toString());
        NetworkManager.customJsonObjectRequest(
                this, Constants.OPPORTUNITY_MANAGE_URL, postData,
                new VolleyResponseListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtils.Print(TAG, "response --> " + response);
                        hideProgress();
                        processResult(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print(TAG, "message --> " + message);
                        hideProgress();
                    }
                }, true);
    }

    private JSONObject requestData() {
        JSONObject payload = new JSONObject();
        try {
            payload.put("name", binding.etName.getText().toString());
            payload.put("stage", SELECTED_STAGE);
            if (opportunity != null && opportunity.getContact_id() != null)
                payload.put("contact_id", opportunity.getContact_id());
            payload.put("expected_close_date",
                    Util.getDateOnRequireFormat(binding.etExpectedMoveInDate.getText().toString(),
                            strMMDDYYYY, strYYYYMMDD) + "T23:59:59.000Z");
            if (opportunity != null && opportunity.getProduct_id() != null)
                payload.put("product_id", opportunity.getProduct_id());
            payload.put("property_id", selected_property_id);
            payload.put("set_lead_status", "opportunity");
            payload.put("value_of_sale", binding.etInvoiceAmount.getText().toString());
            payload.put("comment", binding.etComment.getText().toString());
            payload.put("close_date", Util.getDateOnRequireFormat(binding.etCloseDate.getText().toString(),
                    strMMDDYYYY, strYYYYMMDD) + "T23:59:59.000Z");
            payload.put("followup_date", Util.getDateOnRequireFormat(binding.etFollowupDate.getText().toString(),
                    strMMDDYYYY, strYYYYMMDD) + "T23:59:59.000Z");
            payload.put("renewal_date", Util.getDateOnRequireFormat(binding.etExpectedMoveOutDate.getText().toString(),
                    strMMDDYYYY, strYYYYMMDD) + "T23:59:59.000Z");

            JSONObject closeDict = new JSONObject();
            closeDict.put("unit_number", binding.etUnitNumber.getText().toString().trim());
            closeDict.put("market_rent_rate", binding.etMarketingRent.getText().toString().trim());
            closeDict.put("marketing_source", binding.actvMarketingSource.getText().toString().trim());
            closeDict.put("source_name", binding.etSourceDetail.getText().toString().trim());
            closeDict.put("rental_concession", binding.etRentalConcession.getText().toString().trim());
            closeDict.put("comments", binding.etComments.getText().toString().trim());
            closeDict.put("loss_reason", binding.etLossReason.getText().toString().trim());
            payload.put("close_dict", closeDict);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return payload;
    }

    private void processResult(JSONObject response) {
        try {
            if (Integer.parseInt(response.getString("result")) == 1) {
                Util.onMessage(getApplicationContext(), SUCCESS_MESSAGE);
                setResult(RESULT_OK);
                finish();
            } else {
                JSONObject error_obj = response.getJSONObject("error");
                if (error_obj.has("reason")) {
                    Util.onMessage(getApplicationContext(), error_obj.getString("reason"));
                } else {
                    Util.onMessage(getApplicationContext(), getResources().getString(R.string.opportunity_save_success));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getProperty() {
        JSONObject postData = new JSONObject();
        try {
            postData.put("action", "get");
            postData.put("page_size", "100000");
            postData.put("is_active", true);
            postData.put("exclude_manual_data_source", false);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        NetworkManager.customJsonObjectRequest(
                this, Constants.PROPERTY_LIST, postData,
                new VolleyResponseListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtils.Print(TAG, "onResponse -> " + response);
                        processProperty(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print(TAG, "onError -> " + message);
                    }

                }, true);

    }

    private void processProperty(JSONObject response) {
        try {
            listProperty.clear();
            String id = "";
            String name = "";
            if (Integer.parseInt(response.getString("result")) == 1) {
                JSONArray properties = response.getJSONArray("properties");
                int selPos = 0;
                String selString = "";
                for (int i = 0; i < properties.length(); i++) {
                    JSONObject obj = properties.getJSONObject(i);
                    id = obj.getString("id");
                    name = obj.getString("name");
                    if (opportunity != null && opportunity.getProperty_id() != null) {
                        if (opportunity.getProperty_id().equals(id)) {
                            selPos = i;
                            selString = name;
                        }
                    }
                    DataProperty dataProperty = new DataProperty(id, name);
                    listProperty.add(dataProperty);
                    strListProperty.add(name);
                }

                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this,
                        R.layout.actv_drop_dwon, strListProperty);
                arrayAdapter.setDropDownViewResource(R.layout.spinner_drop_dwon);
                binding.actvProperties.setAdapter(arrayAdapter);
                binding.actvProperties.setThreshold(1);
                binding.actvProperties.setListSelection(selPos);
                binding.actvProperties.setText(selString, false);
                binding.actvProperties.setOnItemClickListener((adapterView, view, i, l) -> selected_property_id = listProperty.get(i).getId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getMarketingSource() {
        JSONObject postData = new JSONObject();
        try {
            postData.put("action", "get");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        NetworkManager.customJsonObjectRequest(
                this, Constants.MARKETING_SOURCE_LIST, postData,
                new VolleyResponseListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtils.Print(TAG, "onResponse -> " + response);
                        processMarketingSource(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print(TAG, "onError -> " + message);
                    }

                }, true);
    }

    private void processMarketingSource(JSONObject response) {
        try {
            strMarketingSource.clear();
            String name = "";
            if (Integer.parseInt(response.getString("result")) == 1) {
                JSONObject objData = response.getJSONObject("data");
                JSONArray properties = objData.getJSONArray("marketing_sources");
                int selPos = 0;
                String selString = "";
                for (int i = 0; i < properties.length(); i++) {
                    JSONObject obj = properties.getJSONObject(i);
                    name = obj.getString("name");
                    if (opportunity != null && opportunity.getMarketingSource() != null) {
                        if (opportunity.getMarketingSource().equalsIgnoreCase(name)) {
                            selPos = i;
                            selString = name;
                        }
                    }
                    strMarketingSource.add(name);
                }

                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this,
                        R.layout.actv_drop_dwon, strMarketingSource);
                arrayAdapter.setDropDownViewResource(R.layout.spinner_drop_dwon);
                binding.actvMarketingSource.setAdapter(arrayAdapter);
                binding.actvMarketingSource.setThreshold(1);
                binding.actvMarketingSource.setSelection(selPos);
                binding.actvMarketingSource.setText(selString, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}