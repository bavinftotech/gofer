package com.gofercrm.user.gofercrm.lead;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.comments.CommentAttachmentActivity;
import com.gofercrm.user.gofercrm.comments.CommentsAdapter;
import com.gofercrm.user.gofercrm.entity.Comment;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.SharedPreferenceData;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LeadCommentsFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final int REQUEST_CODE = 100;
    private ImageButton sendComment;
    private EditText commentText;
    private LeadViewActivity activity;
    private RecyclerView recyclerView;
    private CommentsAdapter commentAdapter;
    private ImageButton btnAttach;

    public LeadCommentsFragment() {
    }

    public static LeadCommentsFragment newInstance(int sectionNumber) {
        LeadCommentsFragment fragment = new LeadCommentsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rv = inflater.inflate(R.layout.comments_tab_lead_view, container, false);
        activity = (LeadViewActivity) getActivity();
        recyclerView = rv.findViewById(R.id.comments_recycler);
        commentText = rv.findViewById(R.id.commentsText);

        sendComment = rv.findViewById(R.id.btn_update_comment);
        sendComment.setOnClickListener(v -> {
            String comment = commentText.getText().toString().trim();
            if (comment.isEmpty()) {
                Util.makeToast(getResources().getString(R.string.comment_empty));
                return;
            }
            String lead_id = activity.lead_obj.getId();
            updateComment(lead_id, comment);

            Comment objComment = new Comment(
                    SharedPreferenceData.getInstance(getActivity()).loginSharedPref.getString("USER_FORMATTED_NAME", "") + ": " + comment,
                    String.valueOf(Util.getCurrentTimeMillis()),
                    "", "");
            activity.getComments().add(objComment);
            commentText.setText("");
            commentAdapter.notifyDataSetChanged();
            recyclerView.post(() -> recyclerView.smoothScrollToPosition(commentAdapter.getItemCount() - 1));
        });

        commentAdapter = new CommentsAdapter(activity.getComments(), getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(commentAdapter);
        if (activity.getComments().size() > 1) {
            recyclerView.smoothScrollToPosition(activity.getComments().size() - 1);
        }

        btnAttach = rv.findViewById(R.id.btnAttach);
        btnAttach.setOnClickListener(view -> {
            Intent intent1 = new Intent(getActivity(), CommentAttachmentActivity.class);
            intent1.putExtra("TYPE", "lead");
            intent1.putExtra("ID", activity.getID());
            intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent1);
        });

        btnAttach = rv.findViewById(R.id.btnAttach);
        btnAttach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(getActivity(), CommentAttachmentActivity.class);
                intent1.putExtra("attachment_entity_type", "contact");
                intent1.putExtra("attachment_entity_id", activity.lead_obj.getId());
                startActivityForResult(intent1, REQUEST_CODE);
            }
        });
        return rv;
    }

    public void updateComment(String lead_id, String comments) {
        if (lead_id == null || lead_id.isEmpty() || comments == null || comments.isEmpty()) {
            return;
        }
        JSONObject postData = new JSONObject();
        try {
            postData.put("action", "update");
            postData.put("lead_id", lead_id);
            JSONObject commentReq = new JSONObject();
            commentReq.put("comments", comments);
            postData.put("payload", commentReq);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        NetworkManager.customJsonObjectRequest(
                getActivity(), Constants.LEAD_LIST_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        processResult(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("LEAD  COMMENT ERROR", message);
                    }
                }, true);
    }

    private void processResult(JSONObject response) {
        try {
            if (Integer.parseInt(response.getString("result")) == 1) {
                commentText.setText("");
                manipulateComments(response);
                Util.onMessage(getActivity().getApplicationContext(), getResources().getString(R.string.comment_updates_success));
            } else {
                Util.onMessage(getActivity().getApplicationContext(), getResources().getString(R.string.comment_updates_fail));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void manipulateComments(JSONObject response) {
        JSONObject dataObj;
        JSONArray commentsArray;
        Comment comment;
        try {
            dataObj = response.getJSONObject("data");
            if (dataObj != null) {
                commentsArray = dataObj.getJSONArray("comments");
                if (commentsArray.length() >= 1) {
                    JSONObject obj = (JSONObject) commentsArray.get(commentsArray.length() - 1);
                    comment = new Comment(obj.getString("comment"), obj.getString("date"),
                            obj.has("url") ? obj.getString("url") : "",
                            obj.has("content_type") ? obj.getString("content_type") : "");
                    activity.getComments().add(comment);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null && data.hasExtra("DATA")) {
                    Comment comment = (Comment) data.getSerializableExtra("DATA");
                    activity.getComments().add(comment);
                    commentAdapter.notifyItemInserted(activity.getComments().size() - 1);
                    if (activity.getComments().size() > 1) {
                        new Handler(Looper.getMainLooper()).postDelayed(() -> recyclerView.smoothScrollToPosition(activity.getComments().size() - 1), 200);
                    }
                }
            }
        }
    }
}