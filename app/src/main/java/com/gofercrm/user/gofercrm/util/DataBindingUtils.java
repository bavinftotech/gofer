package com.gofercrm.user.gofercrm.util;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.databinding.BindingAdapter;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.notification.model.DataNotification;

import java.util.List;

public class DataBindingUtils {

    public static final String DATE_FULL_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    public static final String DATE_DD_MMM_YYYY_H_MM_A_FORMAT = "dd-MMM-yyyy hh:mm a";
    public static final String DATE_EEE = "EEE";

    @BindingAdapter("setColorScheme")
    public static void setColorScheme(SwipeRefreshLayout swipeRefreshLayout, int color) {
        try {
            swipeRefreshLayout.setColorSchemeColors(color);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @BindingAdapter({"setDateFormat"})
    public static void setDateFormat(TextView view, String strDate) {
        view.setText(Util.GetDateOnRequireFormat(strDate,
                DATE_FULL_FORMAT, DATE_DD_MMM_YYYY_H_MM_A_FORMAT));
    }

    @BindingAdapter({"setStringArray"})
    public static void setStringArray(TextView view, List<String> list) {
        if (list == null || list.size() == 0) {
            view.setText("-");
        } else {
            StringBuilder strText = new StringBuilder();
            for (int i = 0; i < list.size(); i++) {
                strText.append(list.get(i));
                if (i != (list.size() - 1))
                    strText.append(",\n");
            }
            view.setText(strText.toString());
        }
    }

    @BindingAdapter({"setDistance"})
    public static void setDistance(TextView textView, int distance) {
        if (distance == 0) {
            textView.setText(distance + " " + textView.getResources().getString(R.string.mile));
        } else {
            textView.setText(String.format("%.2f", Util.metersToMiles(distance)) + " " + textView.getResources().getString(R.string.miles));
        }
    }

    @BindingAdapter({"setStreamingIcon"})
    public static void setStreamingIcon(ImageView view, String strImg) {
        if (strImg == null || strImg.equals("")) {
            view.setImageResource(R.drawable.earphone_streaming);
            view.setColorFilter(ContextCompat.getColor(view.getContext(), R.color.colorPrimary),
                    android.graphics.PorterDuff.Mode.MULTIPLY);
        } else {
//            RequestOptions requestOptions = new RequestOptions();
//            requestOptions = requestOptions.transform(new CircleCrop());
            Glide.with(view.getContext())
                    .load(strImg)
//                    .apply(requestOptions)
                    .placeholder(R.drawable.earphone_streaming)
                    .into(view);
            view.setColorFilter(null);
        }
    }

    @BindingAdapter({"setNotificationIcon"})
    public static void setNotificationIcon(ImageView view, DataNotification data) {
        if (data.getType() == DataNotification.notificationPastTask ||
                data.getType() == DataNotification.notificationUpcomingTask) {
            view.setVisibility(View.INVISIBLE);
        } else if (data.getImage() == null || data.getImage().equals("")) {
            view.setImageResource(R.drawable.ic_account_circle);
            view.setVisibility(View.VISIBLE);
        } else {
            view.setVisibility(View.VISIBLE);
            RequestOptions requestOptions = new RequestOptions();
            requestOptions = requestOptions.transform(new CircleCrop());
            Glide.with(view.getContext())
                    .load(data.getImage())
                    .apply(requestOptions)
                    .placeholder(R.drawable.ic_account_circle)
                    .into(view);
        }
    }

    @BindingAdapter({"setTaskIcon"})
    public static void setTaskIcon(ImageView view, DataNotification data) {
        if (data.getType() == DataNotification.notificationPastTask ||
                data.getType() == DataNotification.notificationUpcomingTask) {
            view.setVisibility(View.VISIBLE);
        } else {
            view.setVisibility(View.INVISIBLE);
        }
    }

    @BindingAdapter("setNotificationName")
    public static void setNotificationName(TextView textView, DataNotification dataNotification) {
        try {
            String strName = dataNotification.getName();
            strName += " " + (dataNotification.getType() == DataNotification.notificationVideoCall ?
                    textView.getResources().getString(R.string.missed_called_to_you) :
                    dataNotification.getType() == DataNotification.notificationConnectionRequest ?
                            (dataNotification.getFriend() == 2 ? textView.getResources().getString(R.string._s_request) :
                                    textView.getResources().getString(R.string.sent_request_to_you)) : "");
            Spannable wordtoSpan = new SpannableString(strName);
            wordtoSpan.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 0, dataNotification.getName().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            textView.setText(wordtoSpan);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}