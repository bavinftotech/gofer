package com.gofercrm.user.gofercrm.opportunityclientlead.list;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.clients.ClientList;
import com.gofercrm.user.gofercrm.entity.Comment;
import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.opportunities.Opportunity;
import com.gofercrm.user.gofercrm.opportunityclientlead.add.ManageOpportunityClientActivity;
import com.gofercrm.user.gofercrm.opportunityclientlead.list.adapter.OpportunityLeadClientAdapter;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OpportunityLeadClientActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = "OpportunityLeadClientActivity";
    private TextView no_records;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private List<Opportunity> opportunities = new ArrayList<>();
    private RecyclerView recyclerView;
    private OpportunityLeadClientAdapter oAdapter;

    private ClientList dataClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opportunity_lead_client);
        getDataFromIntent();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(dataClient.getName() + "'"+getResources().getString(R.string.opportunity));
        }
        mSwipeRefreshLayout = findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        no_records = findViewById(R.id.no_records);

        recyclerView = findViewById(R.id.opportunity_list__recycle);
        oAdapter = new OpportunityLeadClientAdapter(opportunities, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(oAdapter);
        OpportunityLeadClientAdapter.setOnRecyclerViewItemClickListener((flag, position, view) -> copyOpportunity(position));
        getOpportunities();

        FloatingActionButton fab = findViewById(R.id.new_opportunity);
        fab.setOnClickListener(view -> {
            Intent leadIntent = new Intent(getApplicationContext(), ManageOpportunityClientActivity.class);
            leadIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            Opportunity opportunity = new Opportunity();
            opportunity.setUserName(dataClient.getName());
            leadIntent.putExtra("DATA", opportunity);
            startActivityForResult(leadIntent, 228);
        });
    }

    /**
     * GET DATA FROM INTENT
     */
    private void getDataFromIntent() {
        if (getIntent() != null) {
            if (getIntent().getSerializableExtra("DATA") != null) {
                dataClient = (ClientList) getIntent().getSerializableExtra("DATA");
            }
        }
    }

    private void getOpportunities() {
        mSwipeRefreshLayout.setRefreshing(true);
        JSONObject postData = new JSONObject();
        try {
            postData.put("contact_id", dataClient.getId());
            postData.put("find_all", "true");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        LogUtils.Print(TAG, "postData --> " + postData);
        NetworkManager.customJsonObjectRequest(
                this, Constants.OPPORTUNITY_FIND_ONE, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtils.Print(TAG, "onResponse -> " + response);
                        mSwipeRefreshLayout.setRefreshing(false);
                        processData(response, false);
                    }

                    @Override
                    public void onError(String message) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        LogUtils.Print(TAG, "onError -> " + message);
                    }
                }, true);
    }

    private void onLoaded() {
        if (opportunities.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            oAdapter.notifyDataSetChanged();
            no_records.setVisibility(View.GONE);
        } else {
            no_records.setVisibility(View.VISIBLE);
        }
    }

    private void processData(JSONObject data, boolean isSearch) {
        try {
            Opportunity _oppor;
            if (Integer.parseInt(data.getString("result")) == 1) {
                JSONObject result_Data;
                if (isSearch) {
                    result_Data = data.getJSONObject("response");
                } else {
                    result_Data = data.getJSONObject("data");
                }

                JSONArray _oppors = data.getJSONArray("opportunities_list");
                for (int i = 0; i < _oppors.length(); i++) {
                    JSONObject obj = _oppors.getJSONObject(i);
                    List<Comment> comments = new ArrayList();

                    JSONArray commentsArray = new JSONArray();
                    if (obj.has("comments")) {
                        Object comments_obj = obj.get("comments");
                        if (comments_obj instanceof JSONArray) {
                            commentsArray = (JSONArray) comments_obj;
                        }
                        for (int j = 0; j < commentsArray.length(); j++) {
                            JSONObject commentObj = commentsArray.getJSONObject(j);
                            comments.add(new Comment(commentObj.getString("comment"), commentObj.getString("date"), commentObj.has("url") ? commentObj.getString("url") : "",
                                    commentObj.has("content_type") ? commentObj.getString("content_type") : ""));
                        }
                    }
                    _oppor = new Opportunity();

                    if (obj.has("opportunity_id")) {
                        _oppor.setId(obj.getString("opportunity_id"));
                    }
                    if (obj.has("property_id")) {
                        _oppor.setProperty_id(obj.getString("property_id"));
                    }
                    if (obj.has("contact_id")) {
                        _oppor.setContact_id(obj.getString("contact_id"));
                    }
                    if (obj.has("product_id")) {
                        _oppor.setProduct_id(obj.getString("product_id"));
                    }
                    if (obj.has("name")) {
                        _oppor.setName(obj.getString("name"));
                    }
                    if (obj.has("expected_close_date") && obj.getString("expected_close_date") != null && !obj.getString("expected_close_date").isEmpty()) {
                        _oppor.setClosedDate(obj.getLong("expected_close_date"));
                    }
                    if (obj.has("close_date") && obj.getString("close_date") != null && !obj.getString("expected_close_date").isEmpty()) {
                        _oppor.setLostClosedDate(obj.getLong("close_date"));
                    }
                    if (obj.has("followup_date") && obj.getString("followup_date") != null && !obj.getString("expected_close_date").isEmpty()) {
                        _oppor.setLostFollowUpDate(obj.getLong("followup_date"));
                    }
                    if (obj.has("created_ts") && obj.getString("created_ts") != null &&
                            !obj.getString("created_ts").isEmpty()) {
                        _oppor.setOpenDate(obj.getLong("created_ts"));
                    }
                    if (obj.has("timestamp") && obj.getString("timestamp") != null &&
                            !obj.getString("timestamp").isEmpty()) {
                        _oppor.setRenewalDate(obj.getLong("timestamp"));
                    }
                    if (obj.has("value_of_sale")) {
                        _oppor.setSaleValue(obj.getString("value_of_sale"));
                    }
                    if (obj.has("stage")) {
                        _oppor.setStage(obj.getString("stage"));
                    }
                    if (obj.has("property_info")) {
                        JSONObject objProperty = obj.getJSONObject("property_info");
                        if (objProperty.has("name")) {
                            _oppor.setPropertyName(objProperty.getString("name"));
                        }
                    }
                    if (obj.has("contact_info")) {
                        JSONObject objContact = obj.getJSONObject("contact_info");
                        if (objContact.has("name")) {
                            _oppor.setUserName(objContact.getString("name"));
                        }
                    }
                    if (obj.has("close_dict")) {
                        JSONObject objSource = obj.getJSONObject("close_dict");
                        if (objSource.has("marketing_source")) {
                            _oppor.setMarketingSource(objSource.getString("marketing_source"));
                        }
                        if (objSource.has("market_rent_rate")) {
                            _oppor.setMarket_rent_rate(objSource.getString("market_rent_rate"));
                        }
                        if (objSource.has("unit_number")) {
                            _oppor.setUnit_number(objSource.getString("unit_number"));
                        }
                        if (objSource.has("source_name")) {
                            _oppor.setSource_name(objSource.getString("source_name"));
                        }
                        if (objSource.has("rental_concession")) {
                            _oppor.setRental_concession(objSource.getString("rental_concession"));
                        }
                        if (objSource.has("loss_reason") && objSource.getString("loss_reason") != null && !obj.getString("expected_close_date").isEmpty()) {
                            _oppor.setLoss_reason(objSource.getString("loss_reason"));
                        }
                        if (objSource.has("comments") && objSource.getString("comments") != null && !obj.getString("expected_close_date").isEmpty()) {
                            _oppor.setLossComments(objSource.getString("comments"));
                        }

                    }
                    opportunities.add(_oppor);
                }
                onLoaded();
            }
        } catch (JSONException e) {
            mSwipeRefreshLayout.setRefreshing(false);
            e.printStackTrace();
        }
    }

    @Override
    public void onRefresh() {
        opportunities.clear();
        oAdapter.notifyDataSetChanged();
        getOpportunities();
    }

    private void copyOpportunity(int position) {
        showProgress();
        Opportunity opportunity = opportunities.get(position);
        JSONObject postData = new JSONObject();
        try {
            postData.put("action", "add");
            postData.put("payload", requestData(opportunity));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        LogUtils.Print(TAG, "postData --> " + postData.toString());
        NetworkManager.customJsonObjectRequest(
                this, Constants.OPPORTUNITY_MANAGE_URL, postData,
                new VolleyResponseListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtils.Print(TAG, "response --> " + response);
                        hideProgress();
                        processResult(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print(TAG, "message --> " + message);
                        hideProgress();
                    }
                }, true);
    }

    private JSONObject requestData(Opportunity opportunity) {
        JSONObject payload = new JSONObject();
        String strMMDDYYYY = "MM/dd/yyyy";
        SimpleDateFormat format = new SimpleDateFormat(strMMDDYYYY);
        try {
            payload.put("name", opportunity.getUserName() + " RENEWAL ON " + Util.getCurrentDate("MM-dd-yyyy"));
            payload.put("stage", opportunity.getStage());
            if (opportunity != null && opportunity.getContact_id() != null)
                payload.put("contact_id", opportunity.getContact_id());
            if (opportunity.getClosedDate() != null)
                payload.put("expected_close_date", format.format(new Date(opportunity.getClosedDate() * 1000)) + "T23:59:59.000Z");
            if (opportunity != null && opportunity.getProduct_id() != null)
                payload.put("product_id", opportunity.getProduct_id());
            payload.put("property_id", opportunity.getProperty_id());
            payload.put("set_lead_status", "opportunity");
            payload.put("value_of_sale", opportunity.getSaleValue());
            payload.put("comment", "");

            if (opportunity.getLostClosedDate() != null)
                payload.put("close_date", format.format(new Date(opportunity.getLostClosedDate() * 1000)) + "T23:59:59.000Z");
            if (opportunity.getLostFollowUpDate() != null)
                payload.put("followup_date", format.format(new Date(opportunity.getLostFollowUpDate() * 1000)) + "T23:59:59.000Z");
            if (opportunity.getRenewalDate() != null)
                payload.put("getRenewalDate", format.format(new Date(opportunity.getRenewalDate() * 1000)) + "T23:59:59.000Z");

            JSONObject closeDict = new JSONObject();
            closeDict.put("unit_number", opportunity.getUnit_number());
            closeDict.put("market_rent_rate", opportunity.getMarket_rent_rate());
            closeDict.put("marketing_source", opportunity.getMarketingSource());
            closeDict.put("source_name", opportunity.getSource_name());
            closeDict.put("rental_concession", opportunity.getRental_concession());
            closeDict.put("comments", opportunity.getComments());
            closeDict.put("loss_reason", opportunity.getLoss_reason());
            payload.put("close_dict", closeDict);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return payload;
    }

    private void processResult(JSONObject response) {
        try {
            if (Integer.parseInt(response.getString("result")) == 1) {
                Util.onMessage(getApplicationContext(), getResources().getString(R.string.opportunity_create_success));
                opportunities.clear();
                oAdapter.notifyDataSetChanged();
                getOpportunities();
            } else {
                JSONObject error_obj = response.getJSONObject("error");
                if (error_obj.has("reason")) {
                    Util.onMessage(getApplicationContext(), error_obj.getString("reason"));
                } else {
                    Util.onMessage(getApplicationContext(), getResources().getString(R.string.opportunity_save_success));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 228) {
            if (resultCode == Activity.RESULT_OK) {
                opportunities.clear();
                oAdapter.notifyDataSetChanged();
                getOpportunities();
            }
        }
    }
}