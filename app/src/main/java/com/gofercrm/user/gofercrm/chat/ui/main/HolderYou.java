package com.gofercrm.user.gofercrm.chat.ui.main;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.R;

public class HolderYou extends RecyclerView.ViewHolder {

    private TextView time, chatText, quotedText, edit;
    private TextView title, desc, size, youInitial;
    private ImageView thumbnail, overflow, userImage, expanded_image, image_read, ivSMS, ivPlay;
    private CardView cardView;
    private Button threadBtn;
    private RelativeLayout container;

    public HolderYou(View v) {
        super(v);
        time = v.findViewById(R.id.tv_time);
        chatText = v.findViewById(R.id.tv_chat_text);
        title = v.findViewById(R.id.title);
        desc = v.findViewById(R.id.count);
        size = v.findViewById(R.id.size);
        thumbnail = v.findViewById(R.id.thumbnail);
        overflow = v.findViewById(R.id.overflow);
        cardView = v.findViewById(R.id.card_view_you);
        threadBtn = v.findViewById(R.id.tv_thread);
        userImage = v.findViewById(R.id.you_pic_id);
        expanded_image = v.findViewById(R.id.expanded_image);
        container = v.findViewById(R.id.container);
        youInitial = v.findViewById(R.id.holder_you_initial);
        quotedText = v.findViewById(R.id.tv_quoted_text_id);
        edit = v.findViewById(R.id.tv_edit);
        image_read = v.findViewById(R.id.image_read);
        ivSMS = v.findViewById(R.id.ivSMS);
        ivPlay = v.findViewById(R.id.ivPlay);
    }

    public TextView getTime() {
        return time;
    }

    public void setTime(TextView time) {
        this.time = time;
    }

    public TextView getChatText() {
        return chatText;
    }

    public void setChatText(TextView chatText) {
        this.chatText = chatText;
    }

    public TextView getTitle() {
        return title;
    }

    public void setTitle(TextView title) {
        this.title = title;
    }

    public TextView getDesc() {
        return desc;
    }

    public void setDesc(TextView desc) {
        this.desc = desc;
    }

    public TextView getSize() {
        return size;
    }

    public void setSize(TextView size) {
        this.size = size;
    }

    public ImageView getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(ImageView thumbnail) {
        this.thumbnail = thumbnail;
    }

    public ImageView getOverflow() {
        return overflow;
    }

    public void setOverflow(ImageView overflow) {
        this.overflow = overflow;
    }

    public CardView getCardView() {
        return cardView;
    }

    public void setCardView(CardView cardView) {
        this.cardView = cardView;
    }

    public Button getThreadBtn() {
        return threadBtn;
    }

    public void setThreadBtn(Button threadBtn) {
        this.threadBtn = threadBtn;
    }

    public ImageView getUserImage() {
        return userImage;
    }

    public void setUserImage(ImageView userImage) {
        this.userImage = userImage;
    }

    public ImageView getExpanded_image() {
        return expanded_image;
    }

    public void setExpanded_image(ImageView expanded_image) {
        this.expanded_image = expanded_image;
    }

    public RelativeLayout getContainer() {
        return container;
    }

    public void setContainer(RelativeLayout container) {
        this.container = container;
    }

    public TextView getYouInitial() {
        return youInitial;
    }

    public void setYouInitial(TextView youInitial) {
        this.youInitial = youInitial;
    }

    public TextView getQuotedText() {
        return quotedText;
    }

    public void setQuotedText(TextView quotedText) {
        this.quotedText = quotedText;
    }

    public TextView getEdit() {
        return edit;
    }

    public void setEdit(TextView edit) {
        this.edit = edit;
    }

    public ImageView getImage_read() {
        return image_read;
    }

    public void setImage_read(ImageView image_read) {
        this.image_read = image_read;
    }

    public ImageView getIvSMS() {
        return ivSMS;
    }

    public void setIvSMS(ImageView ivSMS) {
        this.ivSMS = ivSMS;
    }

    public ImageView getIvPlay() {
        return ivPlay;
    }

    public void setIvPlay(ImageView ivPlay) {
        this.ivPlay = ivPlay;
    }
}