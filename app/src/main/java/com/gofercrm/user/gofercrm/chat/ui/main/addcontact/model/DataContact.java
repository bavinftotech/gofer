package com.gofercrm.user.gofercrm.chat.ui.main.addcontact.model;

import com.google.gson.annotations.SerializedName;

public class DataContact {

    @SerializedName("country")
    private String country;

    @SerializedName("avatar_url")
    private String avatarUrl;

    @SerializedName("city")
    private String city;

    @SerializedName("company_name")
    private String companyName;

    @SerializedName("friend")
    private int friend;

    @SerializedName("name")
    private String name;

    @SerializedName("public_profile_url")
    private String publicProfileUrl;

    @SerializedName("id")
    private String id;

    @SerializedName("friend_description")
    private String friendDescription;

    @SerializedName("email")
    private String email;

    @SerializedName("phone")
    private String phone;

    @SerializedName("distance")
    private double distance;

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public int getFriend() {
		return friend;
	}

	public void setFriend(int friend) {
		this.friend = friend;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPublicProfileUrl() {
		return publicProfileUrl;
	}

	public void setPublicProfileUrl(String publicProfileUrl) {
		this.publicProfileUrl = publicProfileUrl;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFriendDescription() {
		return friendDescription;
	}

	public void setFriendDescription(String friendDescription) {
		this.friendDescription = friendDescription;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}
}