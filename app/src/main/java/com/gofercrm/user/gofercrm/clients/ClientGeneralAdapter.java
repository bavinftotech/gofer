package com.gofercrm.user.gofercrm.clients;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.telephony.PhoneNumberUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.util.Util;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class ClientGeneralAdapter extends RecyclerView.Adapter<ClientGeneralAdapter.MyViewHolder> {

    private List<ClientView> clientPageElements;
    private Context ctx;

    public ClientGeneralAdapter(List<ClientView> clientViewList, Context ctx) {
        this.clientPageElements = clientViewList;
        this.ctx = ctx;
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lead_view_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final ClientView client = clientPageElements.get(position);
        holder.label.setText(client.getLabel());
        if (client.getType().equals("PHONE") && !client.getValue().isEmpty()) {
            holder.value.setText(PhoneNumberUtils.formatNumber(client.getValue(), "US"));
        } else {
            holder.value.setText(client.getValue());
        }

        if (client.getLeftDrawable() != 0) {
            holder.leftImage.setImageResource(client.getLeftDrawable());
        }
        if (client.getRightDrawable() != 0) {
            holder.rightImage.setImageResource(client.getRightDrawable());
        }

        holder.itemView.setOnClickListener(view -> {
            if (client.getType().equals("PHONE") && !client.getValue().isEmpty()) {
                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.setData(Uri.parse("sms:" + client.getValue()));
                sendIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ctx.startActivity(sendIntent);

            } else if (client.getType().equals("EMAIL") && !client.getValue().isEmpty()) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                Uri data = Uri.parse("mailto:" + client.getValue());
                intent.setData(data);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ctx.startActivity(intent);

            } else if (client.getType().equals("ADDRESS") && !client.getValue().isEmpty()) {
                Uri gmmIntentUri = Uri.parse("google.navigation:q=" + client.getValue());
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                mapIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ctx.startActivity(mapIntent);
            }
        });

        holder.itemView.setOnLongClickListener(view -> {
            if (client.getType().equals("PHONE") && !client.getValue().isEmpty()) {
                Util.copyDataOnClipboard(ctx, client.getValue());
                Util.onMessage(ctx, ctx.getResources().getString(R.string.phone_number_copied));
            } else if (client.getType().equals("EMAIL") && !client.getValue().isEmpty()) {
                Util.copyDataOnClipboard(ctx, client.getValue());
                Util.onMessage(ctx, ctx.getResources().getString(R.string.email_copied));
            }
            return true;
        });
    }

    @Override
    public int getItemCount() {
        return clientPageElements.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView label, value;
        public ImageView leftImage, rightImage;

        public MyViewHolder(View view) {
            super(view);
            label = view.findViewById(R.id.leadView_label_id);
            value = view.findViewById(R.id.lead_view_data_id);
            leftImage = view.findViewById(R.id.lead_img_btn_left);
            rightImage = view.findViewById(R.id.lead_img_btn_right);
        }
    }
}