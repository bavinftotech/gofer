package com.gofercrm.user.gofercrm.profile;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.databinding.ActivityProfileBinding;
import com.gofercrm.user.gofercrm.profile.sendmessage.SendAMessageActivity;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;

import org.json.JSONException;
import org.json.JSONObject;

public class ProfileActivity extends BaseActivity {

    private ActivityProfileBinding binding;
    private String email, phone, fb, google;
    private String USER_ID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_profile);
        binding.setActivity(this);
        USER_ID = getIntent().getStringExtra("USER_ID");
        getUserDetails(USER_ID);
    }

    private void getUserDetails(String _Id) {
        showProgress();
        String url = Constants.GET_USER_DATA + "?userid=" + _Id;
        NetworkManager.customJsonObjectGetRequest(
                getApplicationContext(), url, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideProgress();
                        LogUtils.Print("onResponse ", response.toString());
                        processData(response);
                    }

                    @Override
                    public void onError(String message) {
                        hideProgress();
                        LogUtils.Print("onError ", message);
                    }
                }, true);
    }

    private void processData(JSONObject response) {
        try {
            if (Integer.parseInt(response.getString("result")) == 1) {
                JSONObject user = response.getJSONObject("public_profile");

                if (user.has("tenant_avatar_url")) {
                    RequestOptions requestOptions = new RequestOptions();
                    requestOptions = requestOptions.transform(new CircleCrop());
                    Glide.with(this)
                            .load(user.getString("tenant_avatar_url"))
                            .apply(requestOptions)
                            .into(binding.iv);
                }
                if (user.has("avatar_url")) {
                    Glide.with(this)
                            .load(user.getString("avatar_url"))
                            .into(binding.ivUser);
                }

                if (user.has("cell_visibility") && user.getBoolean("cell_visibility")
                        && user.has("phone") && !user.getString("phone").equals("")) {
                    phone = PhoneNumberUtils.formatNumber(user.getString("phone"), "US");
                    binding.ivCall.setVisibility(View.VISIBLE);
                }

                if (user.has("email_visibility") && user.getBoolean("email_visibility")
                        && user.has("email") && !user.getString("email").equals("")) {
                    email = user.getString("email");
                    binding.ivEmail.setVisibility(View.VISIBLE);
                }

                if (user.has("facebook_url") && user.getString("facebook_url") != null &&
                        !user.getString("facebook_url").equalsIgnoreCase("null") &&
                        !user.getString("facebook_url").equals("")) {
                    fb = user.getString("facebook_url");
                    binding.ivFB.setVisibility(View.VISIBLE);
                }

                if (user.has("googleplus_url") && user.getString("googleplus_url") != null &&
                        !user.getString("googleplus_url").equalsIgnoreCase("null") &&
                        !user.getString("googleplus_url").equals("")) {
                    google = user.getString("facebook_url");
                    binding.ivGoogle.setVisibility(View.VISIBLE);
                }

                String strName = "";
                if (user.has("first_name"))
                    strName += user.getString("first_name");
                if (user.has("last_name"))
                    strName += " " + user.getString("last_name");
                binding.tvName.setText(strName);

                if (user.has("description")) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        binding.tvDesc.setText(Html.fromHtml(user.getString("description"), Html.FROM_HTML_MODE_COMPACT));
                    } else {
                        binding.tvDesc.setText(Html.fromHtml(user.getString("description")));
                    }
                    binding.tvDesc.setMovementMethod(new ScrollingMovementMethod());
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void onViewClicked(View view) {
        if (view == binding.ivCall) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + phone));
            startActivity(intent);
        } else if (view == binding.ivEmail) {
            Intent intentMail = new Intent(Intent.ACTION_SEND);
            intentMail.setType("message/rfc822");
            intentMail.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
            intentMail.putExtra(Intent.EXTRA_SUBJECT, "");
            intentMail.putExtra(Intent.EXTRA_TEXT, "");
            try {
                startActivity(intentMail);
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(ProfileActivity.this, getResources().getString(R.string.no_email_for_share), Toast.LENGTH_SHORT).show();
            }
        } else if (view == binding.ivFB) {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(fb));
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
        } else if (view == binding.ivGoogle) {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(google));
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
        } else if (view == binding.ivSendMessage) {
            Intent intent = new Intent(this, SendAMessageActivity.class);
            intent.putExtra("_ID", USER_ID);
            startActivity(intent);
        }
    }
}