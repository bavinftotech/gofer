package com.gofercrm.user.gofercrm.comments;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.github.chrisbanes.photoview.PhotoView;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.entity.Comment;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.ProgressDialog;
import com.gofercrm.user.gofercrm.util.Util;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.CommentsViewHolder> {

    private List<Comment> comments;
    private Activity activity;

    private String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final int REQUEST_EXTERNAL_STORAGE = 1;

    public CommentsAdapter(List<Comment> comments, Activity activity) {
        this.comments = comments;
        this.activity = activity;
    }

    @NonNull
    @Override
    public CommentsAdapter.CommentsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.comments_list, parent, false);
        return new CommentsAdapter.CommentsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentsAdapter.CommentsViewHolder holder, int position) {
        try {
            final Comment comment = comments.get(position);
            holder.commentDate.setText(Util.formatDate(Double.parseDouble(comment.getDate())));
            // holder.commentText.setText(comment.getComment());
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                holder.commentText.setText(fromHtml(comment.getComment(), Html.FROM_HTML_MODE_COMPACT));
//            } else {
//                holder.commentText.setText(fromHtml(comment.getComment()));
//            }
//            Linkify.addLinks(holder.commentText, Linkify.ALL);
            holder.commentText.loadDataWithBaseURL("", comment.getComment(),
                    "text/html", "UTF-8", "");


            if (comment.getContent_type() != null &&
                    !comment.getContent_type().equals("")) {
                if (comment.getContent_type().startsWith("video")) {
                    holder.rlAttachments.setVisibility(View.VISIBLE);
                    holder.ivPlay.setVisibility(View.VISIBLE);
                    holder.ivPlay.setImageResource(R.drawable.ic_play);
                    holder.iv.setVisibility(View.VISIBLE);
                    Glide.with(activity).load(comment.getUrl()).into(holder.iv);
                    holder.ivDownload.setVisibility(View.GONE);
                } else if (comment.getContent_type().startsWith("image")) {
                    holder.rlAttachments.setVisibility(View.VISIBLE);
                    holder.ivPlay.setVisibility(View.GONE);
                    holder.iv.setVisibility(View.VISIBLE);
                    Glide.with(activity).load(comment.getUrl()).into(holder.iv);
                    holder.ivDownload.setVisibility(View.GONE);
                } else if (comment.getContent_type().startsWith("audio")) {
                    holder.rlAttachments.setVisibility(View.VISIBLE);
                    holder.iv.setVisibility(View.GONE);
                    holder.ivPlay.setVisibility(View.VISIBLE);
                    holder.ivPlay.setImageResource(R.drawable.ic_play);
                    holder.iv.setImageResource(R.drawable.audiobook);
                    holder.ivDownload.setVisibility(View.GONE);
                } else if (comment.getContent_type().startsWith("text")) {
                    holder.rlAttachments.setVisibility(View.VISIBLE);
                    holder.iv.setVisibility(View.GONE);
                    holder.ivPlay.setVisibility(View.VISIBLE);
                    holder.ivPlay.setImageResource(R.drawable.file);
                    holder.ivDownload.setVisibility(View.VISIBLE);
                } else if (comment.getContent_type().startsWith("application")) {
                    holder.rlAttachments.setVisibility(View.VISIBLE);
                    holder.iv.setVisibility(View.GONE);
                    holder.ivPlay.setVisibility(View.VISIBLE);
                    holder.ivPlay.setImageResource(R.drawable.file);
                    holder.ivDownload.setVisibility(View.VISIBLE);
                } else {
                    holder.rlAttachments.setVisibility(View.GONE);
                }
            } else {
                holder.rlAttachments.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return comments.size();
    }

    public class CommentsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView commentDate;
        public WebView commentText;
        private ImageView iv, ivPlay, ivDownload;
        private RelativeLayout rlAttachments;

        public CommentsViewHolder(View view) {
            super(view);
            commentDate = view.findViewById(R.id.id_comments_date);
            commentText = view.findViewById(R.id.id_comments_text);
            commentText.getSettings().setJavaScriptEnabled(true);

            rlAttachments = view.findViewById(R.id.rlAttachments);
            iv = view.findViewById(R.id.iv);
            ivPlay = view.findViewById(R.id.ivPlay);
            ivDownload = view.findViewById(R.id.ivDownload);

            rlAttachments.setOnClickListener(this);
            ivDownload.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (getAdapterPosition() != RecyclerView.NO_POSITION) {
                if (view == ivDownload) {
                    int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    if (permission != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(
                                activity,
                                PERMISSIONS_STORAGE,
                                REQUEST_EXTERNAL_STORAGE
                        );
                    } else {
                        new DownloadFile(activity.getResources().getString(R.string.app_name))
                                .execute(comments.get(getAdapterPosition()).getUrl());
                    }
                } else {
                    if (comments.get(getAdapterPosition()).getContent_type().startsWith("video")) {
                        Intent intent = new Intent(activity, PlayVideoActivity.class);
                        intent.putExtra("DATA", comments.get(getAdapterPosition()).getUrl());
                        activity.startActivity(intent);
                    } else if (comments.get(getAdapterPosition()).getContent_type().startsWith("audio")) {
                        Intent intent = new Intent(activity, MainAudioPlayer.class);
                        intent.putExtra("DATA", comments.get(getAdapterPosition()).getUrl());
                        activity.startActivity(intent);
                    } else if (comments.get(getAdapterPosition()).getContent_type().startsWith("image")) {
                        loadPhoto(comments.get(getAdapterPosition()).getUrl());
//                    } else if (comments.get(getAdapterPosition()).getContent_type().startsWith("text")) {
//                        Intent intent = new Intent(activity, WebViewActivity.class);
//                        intent.putExtra("DATA", comments.get(getAdapterPosition()));
//                        activity.startActivity(intent);
//                    } else if (comments.get(getAdapterPosition()).getContent_type().startsWith("application")) {
//                        Intent intent = new Intent(activity, WebViewActivity.class);
//                        intent.putExtra("DATA", comments.get(getAdapterPosition()));
//                        activity.startActivity(intent);
                    }
                }
            }
        }
    }

    private void loadPhoto(String image_src) {
        AlertDialog.Builder imageDialog = new AlertDialog.Builder(activity);
        LayoutInflater imageInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View image_layout = imageInflater.inflate(R.layout.custom_full_image, null, false);
        PhotoView image = image_layout.findViewById(R.id.fullimage);
        Glide.with(activity).load(image_src).into(image);
        imageDialog.setView(image_layout);
        imageDialog.setPositiveButton(activity.getResources().getString(R.string.close), (dialog, which) -> dialog.dismiss());
        imageDialog.create();
        imageDialog.show();
    }

    private class DownloadFile extends AsyncTask<String, String, String> {
        //        private ProgressDialog progressDialog;
        private String fileName;
        private String folder;
        private boolean isDownloaded;

        DownloadFile(String fileName) {
            this.fileName = fileName;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            this.progressDialog = new ProgressDialog(mContext);
//            progressDialog.setCanceledOnTouchOutside(false);
//            this.progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//            this.progressDialog.setCancelable(false);
//            this.progressDialog.show();
            ProgressDialog.getInstance().show(activity);
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection connection = url.openConnection();
                connection.connect();
                int lengthOfFile = connection.getContentLength();
                InputStream input = new BufferedInputStream(url.openStream(), 8192);
                String timestamp = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new Date());
                fileName = timestamp + "_" + fileName;

                //Default Download directory
                folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();

                OutputStream output = new FileOutputStream(folder + "/" + fileName);

                byte[] data = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    publishProgress("" + (int) ((total * 100) / lengthOfFile));
                    output.write(data, 0, count);
                }
                output.flush();
                output.close();
                input.close();
                return activity.getResources().getString(R.string.downloaded_at_) + " " + folder + "/" + fileName;
            } catch (Exception e) {
                LogUtils.Print("Error: ", e.getMessage());
            }
            return activity.getResources().getString(R.string.something_went_wrong);
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
//            progressDialog.setProgress(Integer.parseInt(progress[0]));
            ProgressDialog.getInstance().dismiss();
        }

        @Override
        protected void onPostExecute(String message) {
//            this.progressDialog.dismiss();
            ProgressDialog.getInstance().dismiss();
            Toast.makeText(activity,
                    message, Toast.LENGTH_LONG).show();
        }
    }
}