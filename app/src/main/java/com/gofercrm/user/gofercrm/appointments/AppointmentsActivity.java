package com.gofercrm.user.gofercrm.appointments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.calendardecorators.EventDecorator;
import com.gofercrm.user.gofercrm.calendardecorators.MySelectorDecorator;
import com.gofercrm.user.gofercrm.calendardecorators.OneDayDecorator;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.SharedPreferenceData;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.threeten.bp.LocalDate;
import org.threeten.bp.format.DateTimeFormatter;
import org.threeten.bp.temporal.TemporalAdjusters;

import java.util.ArrayList;
import java.util.List;

public class AppointmentsActivity extends BaseActivity implements OnDateSelectedListener, OnMonthChangedListener {

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("EEE, d MMM yyyy");
    private final OneDayDecorator oneDayDecorator = new OneDayDecorator();
    MaterialCalendarView widget;
    String startDate;
    String endDate;
    List<JSONObject> event_objects;
    RecyclerView recyclerView;
    EventsAdapter eventsAdapter;
    List<JSONObject> selected_day_events = new ArrayList<>();
    ProgressBar progressBar;
    TextView emptyView;
    TextView events_count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointments);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        emptyView = findViewById(R.id.empty_view);
        events_count = findViewById(R.id.events_count);

        widget = findViewById(R.id.calendarView);

        recyclerView = findViewById(R.id.event_recycle);
        progressBar = findViewById(R.id.appointments_progress_bar);
        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);

        eventsAdapter = new EventsAdapter(selected_day_events, getApplicationContext(), false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(eventsAdapter);

        FloatingActionButton fab = findViewById(R.id.fab_newevent_btn);
        fab.setOnClickListener(view -> {
            Intent new_event = new Intent(getApplicationContext(), NewEventActivity.class);
            new_event.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(new_event);
        });
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        widget.setOnDateChangedListener(this);
        widget.setShowOtherDates(MaterialCalendarView.SHOW_ALL);

        final LocalDate instance = LocalDate.now();
        startDate = instance.minusMonths(6).with(TemporalAdjusters.firstDayOfMonth()).toString();
        endDate = instance.plusMonths(6).with(TemporalAdjusters.lastDayOfMonth()).toString();

        startDate = startDate + "T00:00:00Z";
        endDate = endDate + "T23:59:59Z";

        widget.setSelectedDate(instance);
        widget.setOnDateChangedListener(this);
        widget.setOnMonthChangedListener(this);

//        final LocalDate min = LocalDate.of(instance.getYear(), Month.JANUARY, 1);
//        final LocalDate max = LocalDate.of(instance.getYear(), Month.DECEMBER, 31);
//
//        widget.state().edit().setMinimumDate(min).setMaximumDate(max).commit();

        widget.addDecorators(
                new MySelectorDecorator(this),
//                new HighlightWeekendsDecorator(),
                oneDayDecorator
        );
        if (providerIsGoogle()) {
            getAccessToken();
        } else {
            prepareEvents(startDate, endDate);
        }
    }

    @Override
    public void onDateSelected(@NonNull MaterialCalendarView widget,
                               @NonNull CalendarDay date,
                               boolean selected) {
        recyclerView.setVisibility(View.VISIBLE);
        emptyView.setVisibility(View.GONE);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(FORMATTER.format(date.getDate()));
        oneDayDecorator.setDate(date.getDate());
        widget.invalidateDecorators();

        selected_day_events = getEventsByDate(event_objects, date.getDate().toString());
        if (selected_day_events.size() == 0) {
            emptyView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            return;
        }
        events_count.setText(selected_day_events.size() + " " + getResources().getString(R.string.events_));
        eventsAdapter = new EventsAdapter(selected_day_events, getApplicationContext(), false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(eventsAdapter);
        eventsAdapter.notifyDataSetChanged();
    }

    @Override
    public void onMonthChanged(MaterialCalendarView widget, CalendarDay date) {
//        LogUtils.Print("DDAATTEE", date.getDate().toString());
//        LogUtils.Print("DDAATTEE1", getLastDateOfMonth(date.getDate().toString()));
    }

    private void getTodayEvents() {
        emptyView.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        selected_day_events = getEventsByDate(event_objects, LocalDate.now().toString());
        eventsAdapter = new EventsAdapter(selected_day_events, getApplicationContext(), false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(eventsAdapter);
        eventsAdapter.notifyDataSetChanged();
        if (selected_day_events.size() == 0) {
            emptyView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            events_count.setText(selected_day_events.size() + " " + getResources().getString(R.string.events_));
        }
    }

    private String getLastDateOfMonth(String input_date) {
        LocalDate convertedDate = LocalDate.parse(input_date);
        convertedDate = convertedDate.withDayOfMonth(
                convertedDate.getMonth().length(convertedDate.isLeapYear()));
        return convertedDate.toString();
    }

    private boolean providerIsGoogle() {
        return SharedPreferenceData.getInstance(this).getProvider().equalsIgnoreCase("google");
    }

    private void getAccessToken() {
        String params = "?provider_uid=" + SharedPreferenceData.getInstance(this).getProviderId() +
                "&mobile_client_id=" + Constants.getClientId() +
                "&mobile_client_type=android";
        NetworkManager.customJsonObjectRequest(
                AppointmentsActivity.this, Constants.GOOGLE_ACCESS_TOKEN + params, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        prepareEvents(startDate, endDate);
                    }

                    @Override
                    public void onError(String message) {
                        prepareEvents(startDate, endDate);
                    }
                }, true);
    }

    private void prepareEvents(String startDate, String endDate) {
        String params = "?startDateTime=" + startDate + "&endDateTime=" + endDate;
        NetworkManager.customJsonObjectRequest(
                AppointmentsActivity.this, Constants.GET_EVENTS_URL + params, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        recyclerView.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        processEvents(response);
                        getTodayEvents();
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ERRROR=", message);
                    }
                }, false);
    }

    private void processEvents(JSONObject response) {
        JSONArray eventsArray = new JSONArray();
        event_objects = new ArrayList<>();
        if (response.has("events_list")) {
            Object events;
            try {
                events = response.get("events_list");
                if (events instanceof JSONArray) {
                    eventsArray = (JSONArray) events;
                }
                for (int i = 0; i < eventsArray.length(); i++) {
                    JSONObject event_obj = eventsArray.getJSONObject(i);
                    JSONArray ea = event_obj.getJSONArray("events_list");
                    for (int j = 0; j < ea.length(); j++) {
                        JSONObject f_obj = ea.getJSONObject(j);
                        f_obj.put("calendarId", event_obj.getString("calendarId"));
                        event_objects.add(ea.getJSONObject(j));
                    }
                }
                final ArrayList<CalendarDay> dates = new ArrayList<>();

                for (int i = 0; i < event_objects.size(); i++) {
                    JSONObject event = event_objects.get(i);
                    String s_date, e_date;
                    try {
                        s_date = event.getString("start");
                        e_date = event.getString("end");
                        LocalDate event_on = LocalDate.parse(convertDateWithSubstring(s_date));
                        final CalendarDay day = CalendarDay.from(event_on);
                        dates.add(day);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                widget.addDecorator(new EventDecorator(Color.RED, dates));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private String convertDateWithSubstring(String date) {
        int iend = date.indexOf("T");
        String subString = "";
        if (iend != -1) {
            subString = date.substring(0, iend);
        }
        return subString;
    }

    private List<JSONObject> getEventsByDate(List<JSONObject> array, String match_string) {
        List<JSONObject> filtered = new ArrayList<>();
        if (array != null) {
            try {
                String date = "";
                for (int i = 0; i < array.size(); ++i) {
                    JSONObject obj = array.get(i);
                    String start = obj.getString("start");
                    if (start != null && !start.isEmpty()) {
                        date = convertDateWithSubstring(start);
                    }
                    if (date.equals(match_string)) {
                        filtered.add(obj);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return filtered;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_appointments, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_event_today) {
            final LocalDate instance = LocalDate.now();
            widget.setSelectedDate(instance);
            widget.setCurrentDate(instance);
        }
        return super.onOptionsItemSelected(item);
    }
}