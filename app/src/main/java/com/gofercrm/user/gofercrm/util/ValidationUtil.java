package com.gofercrm.user.gofercrm.util;

import android.util.Patterns;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.crashlytics.internal.common.Utils;

public class ValidationUtil {

    public static boolean checkEmptyView(EditText editText, TextInputLayout textInputLayout, String strErr) {
        if (editText.getText().toString().trim().isEmpty()) {
            Util.setErrorOnTextInputLayout(textInputLayout, strErr);
            return true;
        }
        Util.setErrorOnTextInputLayout(textInputLayout, null);
        return false;
    }

    public static boolean checkAlphaView(EditText editText, TextInputLayout textInputLayout,
                                         String strErr, String strErr1) {
        if (checkEmptyView(editText, textInputLayout, strErr)) {
            return true;
        } else if (!Util.isValidName(editText.getText().toString().trim())) {
            Util.setErrorOnTextInputLayout(textInputLayout, strErr1);
            return true;
        }
        Util.setErrorOnTextInputLayout(textInputLayout, null);
        return false;
    }

    public static boolean checkEmailView(EditText editText, TextInputLayout textInputLayout,
                                         String strErr, String strErr1) {
        if (checkEmptyView(editText, textInputLayout, strErr)) {
            return true;
        } else if (!Util.isValidEmail(editText.getText().toString().trim())) {
            Util.setErrorOnTextInputLayout(textInputLayout, strErr1);
            return true;
        }
        Util.setErrorOnTextInputLayout(textInputLayout, null);
        return false;
    }

    public static boolean checkPhoneView(EditText editText, TextInputLayout textInputLayout,
                                         String strErr) {
        if (checkEmptyView(editText, textInputLayout, strErr)) {
            return true;
        }
        Util.setErrorOnTextInputLayout(textInputLayout, null);
        return false;
    }

    public static boolean checkWebsiteView(EditText editText, TextInputLayout textInputLayout,
                                           String strErr, String strErr1) {
        if (checkEmptyView(editText, textInputLayout, strErr)) {
            return true;
        } else if (!Patterns.WEB_URL.matcher(editText.getText().toString().trim()).matches()) {
            Util.setErrorOnTextInputLayout(textInputLayout, strErr1);
            return true;
        }
        Util.setErrorOnTextInputLayout(textInputLayout, null);
        return false;
    }

    public static boolean checkPasswordWithLength(EditText editText, TextInputLayout textInputLayout,
                                                  String strErr, String strErr1) {
        if (checkEmptyView(editText, textInputLayout, strErr)) {
            return true;
        } else if (editText.getText().toString().trim().length() < 6) {
            Util.setErrorOnTextInputLayout(textInputLayout, strErr1);
            return true;
        }
        Util.setErrorOnTextInputLayout(textInputLayout, null);
        return false;
    }

    public static boolean checkPhoneWithLength(EditText editText, TextInputLayout textInputLayout,
                                               String strErr, String strErr1) {
        if (checkEmptyView(editText, textInputLayout, strErr)) {
            return true;
        } else if (editText.getText().toString().trim().length() < 8) {
            Util.setErrorOnTextInputLayout(textInputLayout, strErr1);
            return true;
        }
        Util.setErrorOnTextInputLayout(textInputLayout, null);
        return false;
    }

    public static boolean checkPassword(EditText etPassword, EditText etConfirmPassword,
                                        TextInputLayout tilPassword, TextInputLayout tilConfPassword,
                                        String strErr, String strPassLength, String strErr1, String strErr2) {
        if (checkEmptyView(etPassword, tilPassword, strErr)) {
            etPassword.requestFocus();
            return true;
        } else if (etPassword.getText().toString().trim().length() < 6) {
            Util.setErrorOnTextInputLayout(tilPassword, strPassLength);
            etPassword.requestFocus();
            return true;
        } else if (checkEmptyView(etConfirmPassword, tilConfPassword, strErr1)) {
            etConfirmPassword.requestFocus();
            return true;
        } else if (!etPassword.getText().toString().trim().equals(etConfirmPassword.getText().toString().trim())) {
            Util.setErrorOnTextInputLayout(tilConfPassword, strErr2);
            etConfirmPassword.requestFocus();
            return true;
        }
        Util.setErrorOnTextInputLayout(tilPassword, null);
        Util.setErrorOnTextInputLayout(tilConfPassword, null);
        return false;
    }
}
