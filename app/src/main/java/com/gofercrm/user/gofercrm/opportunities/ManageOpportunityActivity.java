package com.gofercrm.user.gofercrm.opportunities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;

import androidx.appcompat.widget.Toolbar;

import com.gofercrm.user.gofercrm.ApplicationContext;
import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.appointments.Contact;
import com.gofercrm.user.gofercrm.appointments.ContactAdapter;
import com.gofercrm.user.gofercrm.entity.Email;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.SharedPreferenceData;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.google.android.material.textfield.TextInputLayout;
import com.tokenautocomplete.TokenCompleteTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Random;

public class ManageOpportunityActivity extends BaseActivity implements View.OnClickListener,
        AdapterView.OnItemSelectedListener,
        TokenCompleteTextView.TokenListener<Contact> {

    final Calendar moveInDate = Calendar.getInstance();
    final Calendar closeDate = Calendar.getInstance();
    final Calendar moveOutDate = Calendar.getInstance();
    final Calendar followUpDate = Calendar.getInstance();
    String SELECTED_STAGE = "discovery";
    ArrayAdapter<Contact> contactsAdapter;
    AutoCompleteTextView contactsChip;
    List<Contact> contacts = new ArrayList<>();

    List<Contact> products = new ArrayList<>();
    ArrayAdapter<Contact> productsAdapter;
    AutoCompleteTextView productsChip;

    //    List<String> stages = new ArrayList<>();
//    ArrayAdapter stagesAdapter;
//    Spinner stageSpinner;
    AutoCompleteTextView propertiesChip;
    List<Contact> properties = new ArrayList<>();
    ArrayAdapter<Contact> propertiesAdapter;
    List<Contact> selected_properties = new ArrayList<>();
    Contact selected_product;
    Contact selected_contact;
    Contact selected_property;
    String selected_contact_id;
    String selected_product_id;
    String selected_property_id;
    String pattern = "dd-MMM-yyyy h:mm a";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
    String PROCESSING_MESSAGE;
    String SUCCESS_MESSAGE;
    EditText input_oppor_name, input_oppor_sale, input_lease_term,
            oppor_expected_close_date, oppor_expected_move_out, oppor_renewal_date, oppor_close_date, input_comments;
    //    ProgressDialog progressDialog;
    RadioGroup stage_rg;
    LinearLayout dynamicLinearLayout, oppor_renewal_date_layout_id, oppor_move_out_layout_id, lease_term_layout_id, oppor_close_date_layout_id;
    HashMap<String, Integer> dynamicFields = new HashMap<>();
    private String OPPORTUNITY_ID;
    private JSONObject OPPORTUNITY_EDIT_OBJ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_opportunity);
        PROCESSING_MESSAGE = getResources().getString(R.string.creating_opportunity_);
        SUCCESS_MESSAGE = getResources().getString(R.string.opportunity_create_success);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        oppor_renewal_date_layout_id = findViewById(R.id.oppor_renewal_date_layout_id);
        oppor_renewal_date_layout_id.setVisibility(View.GONE);
        oppor_move_out_layout_id = findViewById(R.id.oppor_move_out_layout_id);
        oppor_move_out_layout_id.setVisibility(View.GONE);

        lease_term_layout_id = findViewById(R.id.lease_term_layout_id);
        lease_term_layout_id.setVisibility(View.GONE);
        oppor_close_date_layout_id = findViewById(R.id.oppor_close_date_layout_id);
        oppor_close_date_layout_id.setVisibility(View.GONE);

//        stageSpinner = findViewById(R.id.oppor_stages);
//        stageSpinner.setOnItemSelectedListener(this);
//
//        stagesAdapter = new ArrayAdapter<String>(
//                this, android.R.layout.simple_spinner_item, stages);
//        stagesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        stageSpinner.setAdapter(stagesAdapter);

        productsAdapter = new ContactAdapter(this, R.layout.contact_ac_layout, products);
        productsChip = findViewById(R.id.oppor_service_id);
        productsChip.setThreshold(1);
        productsChip.setAdapter(productsAdapter);
        productsChip.setOnItemClickListener((adapterView, view, i, l) -> {
            selected_product = (Contact) adapterView.getItemAtPosition(i);
            selected_product_id = selected_product.getId();
        });

        contactsAdapter = new ContactAdapter(this, R.layout.contact_ac_layout, contacts);
        contactsChip = findViewById(R.id.oppor_contact_id);
        contactsChip.setThreshold(1);
        contactsChip.setAdapter(contactsAdapter);
        contactsChip.setOnItemClickListener((adapterView, view, i, l) -> {
            selected_contact = (Contact) adapterView.getItemAtPosition(i);
            selected_contact_id = selected_contact.getId();
        });

        propertiesAdapter = new ContactAdapter(this, R.layout.contact_ac_layout, properties);
        propertiesChip = findViewById(R.id.oppor_properties_id);
        propertiesChip.setAdapter(propertiesAdapter);
        propertiesChip.setOnItemClickListener((adapterView, view, i, l) -> {
            selected_property = (Contact) adapterView.getItemAtPosition(i);
            selected_property_id = selected_property.getId();
        });

        input_oppor_name = findViewById(R.id.input_oppor_name);
        input_oppor_sale = findViewById(R.id.input_oppor_sale);
        input_lease_term = findViewById(R.id.input_lease_term);
        input_comments = findViewById(R.id.input_comments);

        oppor_expected_close_date = findViewById(R.id.oppor_expected_close_date);

        final DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {
            moveInDate.set(Calendar.YEAR, year);
            moveInDate.set(Calendar.MONTH, monthOfYear);
            moveInDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            expectedCloseDate(moveInDate);
        };

        oppor_expected_close_date.setOnClickListener(v -> new DatePickerDialog(ManageOpportunityActivity.this, date, moveInDate
                .get(Calendar.YEAR), moveInDate.get(Calendar.MONTH),
                moveInDate.get(Calendar.DAY_OF_MONTH)).show());

        oppor_renewal_date = findViewById(R.id.oppor_renewal_date);

        final DatePickerDialog.OnDateSetListener r_date = (view, year, monthOfYear, dayOfMonth) -> {
            followUpDate.set(Calendar.YEAR, year);
            followUpDate.set(Calendar.MONTH, monthOfYear);
            followUpDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            followUpDate(followUpDate);
        };

        oppor_renewal_date.setOnClickListener(v -> new DatePickerDialog(ManageOpportunityActivity.this, r_date, followUpDate
                .get(Calendar.YEAR), followUpDate.get(Calendar.MONTH),
                followUpDate.get(Calendar.DAY_OF_MONTH)).show());

        oppor_expected_move_out = findViewById(R.id.oppor_move_out);

        final DatePickerDialog.OnDateSetListener m_date = (view, year, monthOfYear, dayOfMonth) -> {
            moveOutDate.set(Calendar.YEAR, year);
            moveOutDate.set(Calendar.MONTH, monthOfYear);
            moveOutDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            expectedMoveOutDate(moveOutDate);
        };

        oppor_expected_move_out.setOnClickListener(v -> new DatePickerDialog(ManageOpportunityActivity.this, m_date, moveOutDate
                .get(Calendar.YEAR), moveOutDate.get(Calendar.MONTH),
                moveOutDate.get(Calendar.DAY_OF_MONTH)).show());

        oppor_close_date = findViewById(R.id.oppor_close_date);

        final DatePickerDialog.OnDateSetListener c_date = (view, year, monthOfYear, dayOfMonth) -> {
            closeDate.set(Calendar.YEAR, year);
            closeDate.set(Calendar.MONTH, monthOfYear);
            closeDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            closeDate(closeDate);
        };

        oppor_close_date.setOnClickListener(v -> new DatePickerDialog(ManageOpportunityActivity.this, c_date, closeDate
                .get(Calendar.YEAR), closeDate.get(Calendar.MONTH),
                closeDate.get(Calendar.DAY_OF_MONTH)).show());

        stage_rg = findViewById(R.id.stage_radioGroup);

        stage_rg.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.radio_discovery: {
                    SELECTED_STAGE = "discovery";
                    lease_term_layout_id.setVisibility(View.GONE);
                    oppor_move_out_layout_id.setVisibility(View.GONE);
                    oppor_renewal_date_layout_id.setVisibility(View.GONE);
                    oppor_close_date_layout_id.setVisibility(View.GONE);
                }
                break;
                case R.id.radio_won: {
                    SELECTED_STAGE = "closed_won";
                    addDynamicFields(true);
                    lease_term_layout_id.setVisibility(View.VISIBLE);
                    oppor_move_out_layout_id.setVisibility(View.VISIBLE);
                    oppor_renewal_date_layout_id.setVisibility(View.GONE);
                    oppor_close_date_layout_id.setVisibility(View.GONE);
                }
                break;
                case R.id.radio_lost: {
                    SELECTED_STAGE = "closed_lost";
                    addDynamicFields(false);
                    oppor_move_out_layout_id.setVisibility(View.VISIBLE);
                    lease_term_layout_id.setVisibility(View.GONE);
                    oppor_renewal_date_layout_id.setVisibility(View.VISIBLE);
                    oppor_move_out_layout_id.setVisibility(View.GONE);
                    oppor_close_date_layout_id.setVisibility(View.VISIBLE);
                }
                break;
            }
        });

//        getStages();
        Intent intent = getIntent();
        OPPORTUNITY_ID = intent.getStringExtra("OPPORTUNITY");
        if (OPPORTUNITY_ID != null) {
            try {
                OPPORTUNITY_EDIT_OBJ = new JSONObject(OPPORTUNITY_ID);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JSONObject personal_dict_edit = ((ApplicationContext) this.getApplication()).getPersonal_dict();
            if (personal_dict_edit != null && personal_dict_edit.has("opportunity")) {
                try {
                    if (getSupportActionBar() != null)
                        getSupportActionBar().setTitle(getResources().getString(R.string.edit) + " " + personal_dict_edit.getString("opportunity"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                if (getSupportActionBar() != null)
                    getSupportActionBar().setTitle(getResources().getString(R.string.edit_opportunity));
            }
            PROCESSING_MESSAGE = getResources().getString(R.string.updating_opportunity);
            SUCCESS_MESSAGE = getResources().getString(R.string.opportunity_update_success);
            processData(OPPORTUNITY_EDIT_OBJ);
        } else {
            JSONObject personal_dict = ((ApplicationContext) this.getApplication()).getPersonal_dict();
            if (personal_dict != null && personal_dict.has("opportunity")) {
                try {
                    if (getSupportActionBar() != null)
                        getSupportActionBar().setTitle(getResources().getString(R.string.new_) + " " + personal_dict.getString("opportunity"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                if (getSupportActionBar() != null)
                    getSupportActionBar().setTitle(getResources().getString(R.string.new_opportunity));
            }
            followUpDate.add(Calendar.YEAR, 1);
            followUpDate(followUpDate);
            moveOutDate.add(Calendar.YEAR, 1);
            expectedMoveOutDate(moveOutDate);
            expectedCloseDate(moveInDate);
            closeDate(closeDate);

            getContacts(null);
            getCommunities(null);
            getProducts(null);
        }
    }

    private void addDynamicFields(boolean isWon) {
        String pref_obj = SharedPreferenceData.getInstance(getApplicationContext()).getPrefObj();
        dynamicLinearLayout = findViewById(R.id.dynamic_ll);
        EditText dynamicEditText;
        TextInputLayout titleWrapper;
        try {
            JSONObject obj = new JSONObject(pref_obj);
            JSONObject customFields = obj.getJSONObject("custom_fields");
            JSONObject close_dict = customFields.getJSONObject("opportunity_close_dict");
            JSONObject won_dict = customFields.getJSONObject("opportunity_won_dict");
            if (isWon) {
                dynamicLinearLayout.removeAllViews();
                Iterator keys = won_dict.keys();
                while (keys.hasNext()) {
                    String dynamicKey = (String) keys.next();
                    String value = won_dict.getString(dynamicKey);
                    dynamicEditText = new EditText(this);
                    dynamicEditText.setHint(value);
                    int id = new Random().nextInt();
                    dynamicFields.put(dynamicKey, id);
                    dynamicEditText.setId(id);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(20, 20, 20, 20);
                    dynamicEditText.setLayoutParams(params);
                    titleWrapper = new TextInputLayout(this);
                    titleWrapper.setLayoutParams(params);
                    titleWrapper.setHint(value);
                    dynamicLinearLayout.addView(titleWrapper);
                    titleWrapper.addView(dynamicEditText);
                }
            } else {
                dynamicLinearLayout.removeAllViews();
                Iterator keys = close_dict.keys();
                while (keys.hasNext()) {
                    String dynamicKey = (String) keys.next();
                    String value = close_dict.getString(dynamicKey);
                    dynamicEditText = new EditText(this);
                    dynamicEditText.setHint(value);
                    dynamicEditText.setTag(dynamicKey);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(20, 20, 20, 20);
                    dynamicEditText.setLayoutParams(params);
                    titleWrapper = new TextInputLayout(this);
                    titleWrapper.setLayoutParams(params);
                    titleWrapper.setHint(value);
                    dynamicLinearLayout.addView(titleWrapper);
                    titleWrapper.addView(dynamicEditText);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private JSONObject requestData() {
        JSONObject payload = new JSONObject();
        String pref_obj = SharedPreferenceData.getInstance(getApplicationContext()).getPrefObj();
        try {
            JSONObject obj = new JSONObject(pref_obj);
            JSONObject customFields = obj.getJSONObject("custom_fields");
            payload.put("name", input_oppor_name.getText().toString());
            payload.put("product_id", selected_product_id);
            payload.put("contact_id", selected_contact_id);
            JSONArray propertiesArray = new JSONArray();
//            for (Contact c : selected_properties) {
//                propertiesArray.put(c.getId());
//            }
            payload.put("product_ids_list", propertiesArray);
            payload.put("set_lead_status", "opportunity");
            payload.put("stage", SELECTED_STAGE);
            payload.put("value_of_sale", input_oppor_sale.getText().toString());
            payload.put("expected_close_date", oppor_expected_close_date.getText().toString() + "T23:59:59.000Z");
            payload.put("close_date", oppor_close_date.getText().toString() + "T23:59:59.000Z");
            payload.put("lease_term", input_lease_term.getText().toString());
            payload.put("comment", input_comments.getText().toString());
            payload.put("property_id", selected_property_id);
            //TODO
            if (SELECTED_STAGE.equals("closed_won")) {
                JSONObject close_fields = customFields.getJSONObject("opportunity_close_dict");
                JSONObject won_fields = customFields.getJSONObject("opportunity_won_dict");
                payload.put("renewal_date", oppor_expected_move_out.getText().toString() + "T23:59:59.000Z");
                JSONObject close_dict = new JSONObject();
                Iterator keys = close_fields.keys();
                while (keys.hasNext()) {
                    String dynamicKey = (String) keys.next();
                    int id = dynamicFields.get(dynamicKey);
//                    EditText ed = findViewById(id);
//                    close_dict.put(dynamicKey, ed.getText());
                }
            }
            if (SELECTED_STAGE.equals("closed_lost")) {
                payload.put("followup_date", oppor_renewal_date.getText().toString() + "T23:59:59.000Z");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return payload;
    }

    public boolean validate() {
        boolean valid = true;
        String oppor_name = input_oppor_name.getText().toString();
        if (input_oppor_name != null && oppor_name.isEmpty()) {
            input_oppor_name.setError(getResources().getString(R.string.name_empty_err_));
            valid = false;
        } else {
            input_oppor_name.setError(null);
        }

        if (selected_contact_id == null) {
            contactsChip.setError(getResources().getString(R.string.contact_empty_err));
            valid = false;
        } else {
            contactsChip.setError(null);
        }
        if (selected_product_id == null) {
            productsChip.setError("Product is Mandatory");
            valid = false;
        } else {
            productsChip.setError(null);
        }
        if (selected_property_id == null) {
            propertiesChip.setError(getResources().getString(R.string.property_empty_err));
            valid = false;
        } else {
            propertiesChip.setError(null);
        }
        return valid;
    }

    private void saveOpportunity() {
//        progressDialog = new ProgressDialog(ManageOpportunityActivity.this,
//                R.style.AppTheme_Dark_Dialog);
//        progressDialog.setCanceledOnTouchOutside(false);
//        progressDialog.setIndeterminate(true);
//        progressDialog.setMessage(PROCESSING_MESSAGE);
//        progressDialog.show();
        showProgress();
        JSONObject postData = new JSONObject();
        try {
            if (OPPORTUNITY_EDIT_OBJ != null) {
                postData.put("action", "update");
                postData.put("opportunity_id", OPPORTUNITY_EDIT_OBJ.getString("opportunity_id"));
            } else {
                postData.put("action", "add");
            }
            postData.put("payload", requestData());
            LogUtils.Print("PAYLOAD=", postData.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        showProgress();
        NetworkManager.customJsonObjectRequest(
                ManageOpportunityActivity.this, Constants.OPPORTUNITY_MANAGE_URL, postData,
                new VolleyResponseListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        hideProgress();
                        LogUtils.Print("onResponse ", response.toString());
                        processResult(response);
                    }

                    @Override
                    public void onError(String message) {
                        hideProgress();
                        LogUtils.Print("CLIENT ERROR", message);
                    }
                }, true);
    }

    private void processResult(JSONObject response) {
        try {
            if (Integer.parseInt(response.getString("result")) == 1) {
                Util.onMessage(getApplicationContext(), SUCCESS_MESSAGE);
                Intent intent = new Intent(getApplicationContext(), OpportunityActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } else {
                JSONObject error_obj = response.getJSONObject("error");
                if (error_obj.has("reason")) {
                    Util.onMessage(getApplicationContext(), error_obj.getString("reason"));
                } else {
                    Util.onMessage(getApplicationContext(), getResources().getString(R.string.opportunity_save_success));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_lead_process, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_save) {
            if (!validate()) {
                Util.onMessage(getApplicationContext(), getResources().getString(R.string.fields_value_in_correct));
            } else {
                saveOpportunity();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void getContacts(final Opportunity opportunity) {
        NetworkManager.customJsonObjectRequest(
                ManageOpportunityActivity.this, Constants.GET_CONTACTS_PICKLIST_URL, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processContacts(response, opportunity);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ERRROR=", message);
                    }
                }, false);
    }

    private void processContacts(JSONObject data, Opportunity opportunity) {
        try {
            contacts.clear();
            String id = "";
            String name = "";
            String email = "";
            if (Integer.parseInt(data.getString("result")) == 1) {
                JSONObject result_Data = data.getJSONObject("data");
                JSONArray leads = result_Data.getJSONArray("contacts");
                for (int i = 0; i < leads.length(); i++) {
                    JSONObject obj = leads.getJSONObject(i);
                    JSONArray emailArray = obj.getJSONArray("email_list");
                    List<Email> emails = new ArrayList<>();
                    for (int j = 0; j < emailArray.length(); j++) {
                        JSONObject emailObj = emailArray.getJSONObject(j);
                        email = emailObj.getString("email");
                    }
                    id = obj.getString("id");
                    name = obj.getString("name");
                    if (name != null) {
                        name = Util.capitalize(name);
                    }
                    if (opportunity != null) {
                        if (opportunity.getContact_id().equals(id)) {
                            contactsChip.setText(name);
                            selected_contact_id = opportunity.getContact_id();
                        }
                    }
                    Contact contact = new Contact(id, name, email, "");
                    contacts.add(contact);
                }
                contactsAdapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getOpportunityById(String oppor_id) {
        JSONObject postData = new JSONObject();
        try {
            postData.put("action", "get");
            postData.put("opportunity_id", oppor_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        NetworkManager.customJsonObjectRequest(
                ManageOpportunityActivity.this, Constants.OPPORTUNITY_MANAGE_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processData(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("OPPOR. PROCESS ERROR", message);
                    }
                }, true);
    }

    private void processData(JSONObject data) {
        try {
            Opportunity opportunity;
            if (data != null) {
                JSONObject obj = data;
                opportunity = new Opportunity();
                if (obj.has("opportunity_id")) {
                    opportunity.setId(obj.getString("opportunity_id"));
                }
                if (obj.has("name")) {
                    opportunity.setName(obj.getString("name"));
                }
                if (obj.has("business_need_description")) {
                    opportunity.setBusinessNeedDesc(obj.getString("business_need_description"));
                }
                if (obj.has("competitive_description")) {
                    opportunity.setCompetitiveDesc(obj.getString("competitive_description"));
                }
                if (obj.has("contact_id")) {
                    opportunity.setContact_id(obj.getString("contact_id"));
                }
                if (obj.has("product_id")) {
                    opportunity.setProduct_id(obj.getString("product_id"));
                }
                if (obj.has("property_id")) {
                    opportunity.setProperty_id(obj.getString("property_id"));
                }
                if (obj.has("expected_close_date") && obj.getString("expected_close_date") != null && !obj.getString("expected_close_date").isEmpty()) {
                    opportunity.setClosedDate(obj.getLong("expected_close_date"));
                }
                if (obj.has("stage")) {
                    opportunity.setStage(obj.getString("stage"));
                }
                if (obj.has("value_of_sale")) {
                    opportunity.setSaleValue(obj.getString("value_of_sale"));
                }
                if (obj.has("value_of_sale")) {
                    opportunity.setSaleValue(obj.getString("value_of_sale"));
                }
                if (obj.has("probability_pcnt")) {
                    opportunity.setProbability_percent(obj.getString("probability_pcnt"));
                }
                if (opportunity != null) {
                    updateView(opportunity);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void updateView(Opportunity opportunity) {
        String edit_pattern = "yyyy-MM-dd";
        SimpleDateFormat edditDateFormat = new SimpleDateFormat(edit_pattern);
        if (opportunity.getClosedDate() != null) {
            String close_date = edditDateFormat.format(new Date(opportunity.getClosedDate() * 1000));
            oppor_expected_close_date.setText(close_date);
        }
        input_oppor_name.setText(opportunity.getName());
//        String stage = opportunity.getStage();
//        if (stage != null) {
//            stage = Util.capitalize(stage);
//        }
//        stageSpinner.setSelection(stagesAdapter.getPosition(stage));
        input_lease_term.setText(opportunity.getProbability_percent());
        input_oppor_sale.setText(opportunity.getSaleValue());

        getProducts(opportunity);
        getCommunities(opportunity);
        getContacts(opportunity);
    }

    private void getCommunities(final Opportunity opportunity) {
        JSONObject postData = new JSONObject();
        try {
            postData.put("is_active", true);
            postData.put("page_size", 5000);
            postData.put("exclude_manual_data_source", false);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        NetworkManager.customJsonObjectRequest(
                ManageOpportunityActivity.this, Constants.GET_COMMUNITIES_PICKLIST_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processCommunities(response, opportunity);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ERRROR=", message);
                    }
                }, true);
    }

    private void processCommunities(JSONObject response, Opportunity opportunity) {
        try {
            properties.clear();
            String id = "";
            String name = "";
            String marketing_name = "";

            JSONArray propertyList = response.getJSONArray("properties");
            for (int i = 0; i < propertyList.length(); i++) {
                JSONObject obj = propertyList.getJSONObject(i);
                if (obj.has("id")) {
                    id = obj.getString("id");
                }
                if (obj.has("name")) {
                    name = obj.getString("name");
                }
                if (obj.has("marketing_name")) {
                    name = obj.getString("marketing_name");
                }
                if (name != null && !name.isEmpty()) {
                    name = Util.capitalize(name);
                }
                if (opportunity != null) {
                    if (opportunity.getProperty_id().equals(id)) {
                        propertiesChip.setText(name);
                        selected_property_id = id;
                    }
                }
                Contact contact = new Contact(id, name, marketing_name, "");
                properties.add(contact);
            }
            propertiesAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getProducts(final Opportunity opportunity) {

        NetworkManager.customJsonObjectRequest(
                ManageOpportunityActivity.this, Constants.GET_PRODUCT_LIST_URL, null,
                new VolleyResponseListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        processProducts(response, opportunity);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ERRROR=", message);
                    }

                }, false);

    }

    private void processProducts(JSONObject response, Opportunity opportunity) {
        try {
            products.clear();
            String id = "";
            String name = "";
            String email = "";
            if (Integer.parseInt(response.getString("result")) == 1) {
                JSONObject data = response.getJSONObject("data");
                JSONArray products_list = data.getJSONArray("product");

                for (int i = 0; i < products_list.length(); i++) {
                    JSONObject obj = products_list.getJSONObject(i);
                    id = obj.getString("id");
                    name = obj.getString("name");
                    if (name != null) {
                        name = Util.capitalize(name);
                    }
                    if (opportunity != null) {
                        if (opportunity.getProduct_id().equals(id)) {
                            productsChip.setText(name);
                            selected_product_id = opportunity.getProduct_id();
                        }
                    }
                    Contact contact = new Contact(id, name, name, "");
                    products.add(contact);
                }

                productsAdapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    private void getStages() {
//
//        JSONObject postData = new JSONObject();
//        try {
//            postData.put("action", "get");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        NetworkManager.customJsonObjectRequest(
//                ManageOpportunityActivity.this, Constants.GET_OPPORTUNITY_STAGE_LIST_URL, postData,
//                new VolleyResponseListener() {
//
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        processStages(response);
//                    }
//
//                    @Override
//                    public void onError(String message) {
//                        LogUtils.Print("ERRROR=", message);
//                    }
//
//                }, true);
//    }
//
//    private void processStages(JSONObject data) {
//        try {
//            stages.clear();
//            if (Integer.parseInt(data.getString("result")) == 1) {
//                JSONArray stage_list = data.getJSONArray("response");
//                for (int i = 0; i < stage_list.length(); i++) {
//                    String obj = stage_list.getString(i);
//                    if (obj != null) {
//                        if (obj.toLowerCase().equals("closed_lost") || obj.toLowerCase().equals("closed_won") || obj.toLowerCase().equals("discovery")) {
//                            obj = Util.capitalize(obj);
//                            stages.add(obj);
//                        }
//                    }
//
//                }
//                stagesAdapter.notifyDataSetChanged();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    private void expectedCloseDate(Calendar calendar) {
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        oppor_expected_close_date.setText(sdf.format(calendar.getTime()));
    }

    private void expectedMoveOutDate(Calendar calendar) {
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        oppor_expected_move_out.setText(sdf.format(calendar.getTime()));
    }

    private void followUpDate(Calendar calendar) {
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        oppor_renewal_date.setText(sdf.format(calendar.getTime()));
    }

    private void closeDate(Calendar calendar) {
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        oppor_close_date.setText(sdf.format(calendar.getTime()));
    }

    @Override
    public void onClick(View view) {
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        LogUtils.Print("VALUE=", adapterView.getSelectedItem().toString());
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onTokenAdded(Contact token) {
        selected_properties.add(token);
    }

    @Override
    public void onTokenRemoved(Contact token) {
        selected_properties.remove(token);
    }

    @Override
    public void onTokenIgnored(Contact token) {

    }
}