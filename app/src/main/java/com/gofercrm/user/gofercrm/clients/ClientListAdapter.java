package com.gofercrm.user.gofercrm.clients;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.telephony.PhoneNumberUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.chat.ui.main.Conversation;
import com.gofercrm.user.gofercrm.entity.Email;
import com.gofercrm.user.gofercrm.entity.Phone;
import com.gofercrm.user.gofercrm.opportunityclientlead.list.OpportunityLeadClientActivity;
import com.gofercrm.user.gofercrm.util.Util;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.List;

public class ClientListAdapter extends RecyclerView.Adapter<ClientListAdapter.MyViewHolder> {

    private List<ClientList> clientLists;
    private Activity activity;

    public ClientListAdapter(List<ClientList> clientList, Activity activity) {
        setHasStableIds(true);
        this.clientLists = clientList;
        this.activity = activity;
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lead_list_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final ClientList client = clientLists.get(position);
        holder.title.setText(!client.getName().isEmpty() ? Util.capitalize(client.getTitle()) : "");
        holder.leadInitial.setText(Util.initialChar(client.getTitle()));
        holder.status.setText(!client.getStatus().isEmpty() ? Util.capitalize(client.getStatus()) : "");
        switch (client.getStatus()) {
            case "open":
                holder.status.setTextColor(ContextCompat.getColor(activity, R.color.open));
                break;
            case "attempted":
                holder.status.setTextColor(ContextCompat.getColor(activity, R.color.attempted));
                break;
            case "contacted":
                holder.status.setTextColor(ContextCompat.getColor(activity, R.color.contacted));
                break;
            case "meeting-setup":
                holder.status.setTextColor(ContextCompat.getColor(activity, R.color.meetingsetup));
                break;
            case "disqualified":
                holder.status.setTextColor(ContextCompat.getColor(activity, R.color.disqualified));
                break;
            case "assigned":
                holder.status.setTextColor(ContextCompat.getColor(activity, R.color.assigned));
                break;
        }

        if (client.getFileAs().equals("mobile_app")) {
            holder.mobile_lead_id.setVisibility(View.VISIBLE);
            holder.mobile_lead_id.setOnClickListener(v -> {
                Intent intent = new Intent(activity, Conversation.class);
                intent.putExtra("_ID", client.getId());
                intent.putExtra("_TYPE", (Serializable) 0);
                intent.putExtra("_NAME", client.getName());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(intent);
            });
        } else {
            holder.mobile_lead_id.setVisibility(View.GONE);
        }
        holder.ivLead.setOnClickListener(v -> {
            Intent intent = new Intent(activity, OpportunityLeadClientActivity.class);
            intent.putExtra("DATA", clientLists.get(position));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activity.startActivity(intent);
        });
        if (client.getEmails().size() > 0) {
            Email email = client.getEmails().get(0);
            holder.email.setText(email.getEmail());
        } else {
            holder.email.setText("");
        }

        if (client.getPhones().size() > 0) {
            Phone phone = client.getPhones().get(0);
            holder.phone.setText(PhoneNumberUtils.formatNumber(phone.getPhone(), "US"));
        } else {
            holder.phone.setText("");
        }

        holder.itemView.setOnClickListener(view -> {
            Intent intent = new Intent(activity, ClientViewActivity.class);
            intent.putExtra("CLIENT_ID", (Serializable) clientLists.get(position).getId());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activity.startActivity(intent);

        });
    }

    @Override
    public int getItemCount() {
        return clientLists.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, email, phone, status, leadInitial;
        public ImageView mobile_lead_id, ivLead;

        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.name);
            email = view.findViewById(R.id.email);
            phone = view.findViewById(R.id.phone);
            status = view.findViewById(R.id.status);
            leadInitial = view.findViewById(R.id.leadInitial);
            mobile_lead_id = view.findViewById(R.id.mobile_lead_id);
            ivLead = view.findViewById(R.id.ivLead);
            ivLead.setVisibility(View.VISIBLE);
        }
    }
}