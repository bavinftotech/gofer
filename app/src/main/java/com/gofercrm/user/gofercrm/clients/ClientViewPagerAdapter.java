package com.gofercrm.user.gofercrm.clients;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import org.jetbrains.annotations.NotNull;

public class ClientViewPagerAdapter extends FragmentPagerAdapter {

    public ClientViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @NotNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return ClientGeneralFragment.newInstance(0);
            case 1:
                return ClientCommentsFragment.newInstance(1);
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}