package com.gofercrm.user.gofercrm.sendfax;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.databinding.ActivitySendFaxBinding;
import com.gofercrm.user.gofercrm.util.FileUtil;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.jaiselrahman.filepicker.activity.FilePickerActivity;
import com.jaiselrahman.filepicker.config.Configurations;
import com.jaiselrahman.filepicker.model.MediaFile;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import io.github.lizhangqu.coreprogress.ProgressHelper;
import io.github.lizhangqu.coreprogress.ProgressUIListener;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class SendFaxActivity extends BaseActivity {

    private static final String TAG = "SendFaxActivity";

    private ActivitySendFaxBinding binding;
    private static final int FILE_REQUEST_CODE = 228;
    private final int PICK_CONTACT = 1;
    private String token = "";
    private String temp_url = "";
    private Uri uri = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_send_fax);
        binding.setActivity(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.send_fax));
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());
        SharedPreferences sharedPref = getApplicationContext()
                .getSharedPreferences("LOGIN", MODE_PRIVATE);
        token = sharedPref.getString("USER_TOKEN", "");
        setUpHeaderView();
    }

    private void setUpHeaderView() {
        binding.etDocument.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 0 && uri != null) {
                    //remove selected attachment URI
                    uri = null;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    public void onContactClicked() {
        Intent intent = new Intent(this, FilePickerActivity.class);
        intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
                .setCheckPermission(true)
                .setShowImages(true)
                .setShowFiles(true)
                .setShowVideos(false)
                .build());
        startActivityForResult(intent, FILE_REQUEST_CODE);
    }

    public void onSubmitClicked() {
        if (binding.etFaxNumber.getText().toString().trim().isEmpty()) {
            binding.etFaxNumber.requestFocus();
            Util.showSnackBar(getResources().getString(R.string.fax_number_empty), SendFaxActivity.this);
        } else if (binding.etDocument.getText().toString().trim().isEmpty()) {
            binding.etDocument.requestFocus();
            Util.showSnackBar(getResources().getString(R.string.document_empty), SendFaxActivity.this);
        } else {
            if (uri != null) {
                String mimeType = FileUtil.getMimeType(getApplicationContext(), uri);
                String fileName = FileUtil.getFileName(getApplicationContext(), uri);
                uploadFile(uri, fileName, mimeType);
            } else {
                //download inserted url file
                if (!Patterns.WEB_URL.matcher(binding.etDocument.getText().toString().trim()).matches()) {
                    Util.makeToast(getResources().getString(R.string.valid_url));
                    return;
                }
                new DownloadFile().execute(binding.etDocument.getText().toString().trim());
            }
        }
    }

    private void sendFaxAPIRequest() {
        showProgress();
        String params = "?media_url=" + temp_url + "&to_num=" + binding.etFaxNumber.getText().toString().trim();
        NetworkManager.customJsonObjectRequest(
                SendFaxActivity.this, Constants.SEND_FAX_API + params, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideProgress();
                        LogUtils.Print("onResponse ", "" + response);
                        try {
                            if (Integer.parseInt(response.getString("result")) == 1) {
                                Util.onMessage(SendFaxActivity.this, getResources().getString(R.string.send_sms_success));
                                binding.etDocument.setText("");
                                binding.etFaxNumber.setText("");
                                Util.hideSoftKeyboard(binding.etDocument);
                                Util.hideSoftKeyboard(binding.etFaxNumber);
                            } else {
                                Util.onMessage(SendFaxActivity.this, getResources().getString(R.string.send_sms_failure));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Util.showSnackBar(getResources().getString(R.string.server_err), SendFaxActivity.this);
                        }
                    }

                    @Override
                    public void onError(String message) {
                        hideProgress();
                        LogUtils.Print("onError=", message);
                        Util.showSnackBar(message, SendFaxActivity.this);
                    }
                }, false);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LogUtils.Print("onActivityResult", "requestCode ->" + requestCode);
        LogUtils.Print("onActivityResult", "resultCode ->" + resultCode);
        if (requestCode == FILE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                ArrayList<MediaFile> files;
                if (data != null) {
                    if (data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES) != null) {
                        files = data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES);
                        if (files == null || files.size() == 0) return;
                        uri = files.get(0).getUri();
                        binding.etDocument.setText(FileUtil.getFileName(getApplicationContext(), uri));
                    }
                }
            }
        }
    }

    private void uploadFile(Uri uri, String file_name, String file_type) {
        try {
            File file = FileUtil.from(this, uri);
            uploadFile(file, file_name, file_type);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void uploadFile(File file, String file_name, String file_type) {
        //client
        OkHttpClient okHttpClient = new OkHttpClient();
        //request builder
        Request.Builder builder = new Request.Builder();
        builder.url(Constants.FILE_UPLOAD);
        builder.addHeader("Authorization", "Bearer " + token);

        //your original request body
        MultipartBody body = null;
        try {
            MultipartBody.Builder bodyBuilder = new MultipartBody.Builder();
            bodyBuilder.setType(MultipartBody.FORM);
            bodyBuilder.addFormDataPart("storefile_file_purpose", "store_file");
            bodyBuilder.addFormDataPart("storefile_description", file_name);
            bodyBuilder.addFormDataPart("storefile", file_name, RequestBody.create(MediaType.parse(file_type), file));
            body = bodyBuilder.build();
        } catch (Exception e) {
            e.printStackTrace();
            Util.onMessage(SendFaxActivity.this, getResources().getString(R.string.server_err));
        }

        //wrap your original request body with progress
        RequestBody requestBody = ProgressHelper.withProgress(body, new ProgressUIListener() {

            //if you don't need this method, don't override this methd. It isn't an abstract method, just an empty method.
            @Override
            public void onUIProgressStart(long totalBytes) {
                super.onUIProgressStart(totalBytes);
                LogUtils.Print("TAG", "onUIProgressStart:" + totalBytes);
            }

            @Override
            public void onUIProgressChanged(long numBytes, long totalBytes, float percent, float speed) {
                LogUtils.Print("TAG", "percent:" + percent);
            }

            //if you don't need this method, don't override this methd. It isn't an abstract method, just an empty method.
            @Override
            public void onUIProgressFinish() {
                super.onUIProgressFinish();
                LogUtils.Print("TAG", "onUIProgressFinish:");
            }
        });

        //post the wrapped request body
        builder.post(requestBody);
        //call
        Call call = okHttpClient.newCall(builder.build());
        //enqueue
        showProgress();
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                hideProgress();
                LogUtils.Print("TAG", "=============onFailure===============");
                e.printStackTrace();
                runOnUiThread(() -> Toast.makeText(SendFaxActivity.this, getResources().getString(R.string.server_err), Toast.LENGTH_SHORT).show());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                hideProgress();
                ResponseBody responseBody = response.body();
                if (!response.isSuccessful()) {
                    runOnUiThread(() -> Toast.makeText(SendFaxActivity.this, getResources().getString(R.string.server_err), Toast.LENGTH_SHORT).show());
                    throw new IOException("Error response " + response);
                }
                String jsonData = responseBody.string();
                try {
                    JSONObject jsonObject = new JSONObject(jsonData);
                    LogUtils.Print(TAG, "response jsonObject --> " + jsonObject);
//                    {"result":1,"file_id":"5f97061a163c29791171ff80","temp_url":"https:\/\/messaging-api.s3.amazonaws.com\/2d7cf8de-e858-430d-ae5e-bfd94a5d40da?response-content-disposition=attachment%3B%20filename%3Ddownload.jpeg&response-content-type=image%2Fjpeg&AWSAccessKeyId=AKIAJC47X2VDRXY5P3NQ&Signature=rnYfJ3QwRD7%2Ffu%2FV%2FQ9fdUVLEfU%3D&Expires=1603819419"}
                    if (Integer.parseInt(jsonObject.getString("result")) == 1) {
                        if (jsonObject.has("temp_url")) {
                            temp_url = jsonObject.getString("temp_url");
                        }
                        runOnUiThread(() -> sendFaxAPIRequest());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    LogUtils.Print(TAG, "Exception -->" + e.getMessage());
                    runOnUiThread(() -> Toast.makeText(SendFaxActivity.this, getResources().getString(R.string.server_err), Toast.LENGTH_SHORT).show());
                }
            }
        });
    }

    private class DownloadFile extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress();
        }

        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                if (!f_url[0].startsWith("http://") && !f_url[0].startsWith("https://")) {
                    f_url[0] = "http://" + f_url[0];
                }
                URL url = new URL(f_url[0]);
                URLConnection connection = url.openConnection();
                connection.connect();
                int lengthOfFile = connection.getContentLength();
                InputStream input = new BufferedInputStream(url.openStream(), 8192);
                String timestamp = new SimpleDateFormat("yyyy-MM-dd-HH-mm").format(new Date());
                String fileName = timestamp + "_" + "temp";

                //Default Download directory
                //        private ProgressDialog progressDialog;
                String folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
                File file = new File(folder + "/" + fileName);
                OutputStream output = new FileOutputStream(file.getAbsolutePath());

                byte[] data = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    publishProgress("" + (int) ((total * 100) / lengthOfFile));
                    output.write(data, 0, count);
                }
                output.flush();
                output.close();
                input.close();
                return file.getAbsolutePath();
            } catch (Exception e) {
                e.printStackTrace();
                runOnUiThread(() -> Toast.makeText(SendFaxActivity.this, getResources().getString(R.string.server_err), Toast.LENGTH_SHORT).show());
            }
            return "";
        }

        @Override
        protected void onPostExecute(String message) {
            hideProgress();
            if (!message.equals("")) {
                Uri uri = Uri.fromFile(new File(message));
                String mimeType = FileUtil.getMimeType(getApplicationContext(), uri);
                String fileName = FileUtil.getFileName(getApplicationContext(), uri);
                uploadFile(uri, fileName, mimeType);
            }
        }
    }
}