package com.gofercrm.user.gofercrm.clients;

import android.Manifest;
import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.NavigationActivity;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.entity.Address;
import com.gofercrm.user.gofercrm.entity.Comment;
import com.gofercrm.user.gofercrm.entity.Email;
import com.gofercrm.user.gofercrm.entity.MatchPreference;
import com.gofercrm.user.gofercrm.entity.Phone;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ClientListActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = "ClientListActivity";
    private final int PICK_CONTACT = 1;
    private final int REQUEST_CODE_ASK_PERMISSIONS = 123;
    TextView no_lead_records;
    boolean isScrolling = false;
    String last_id = null;
    boolean isMoreDataAvailable = true;
    boolean isOnSearch = false;
    SwipeRefreshLayout mSwipeRefreshLayout;
    String phoneNumber = "", emailAddress = "", address = "";
    String f_name = "", l_name = "", display_name = "";
    private List<ClientList> clientLists = new ArrayList<>();
    private RecyclerView recyclerView;
    private ClientListAdapter cAdapter;
    private ProgressBar progressBar;
    private ProgressBar paginationProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lead_list);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(getResources().getString(R.string.clients));
        }
        mSwipeRefreshLayout = findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        no_lead_records = findViewById(R.id.no_lead_records);

        recyclerView = findViewById(R.id.lead_recycle);
        progressBar = findViewById(R.id.lead_list_progressBar);
        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        cAdapter = new ClientListAdapter(clientLists, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(cAdapter);
        prepareClientData();

        paginationProgressBar = findViewById(R.id.lead_list_pagination_progress);

        FloatingActionButton fab = findViewById(R.id.lead_view_create_id);
        fab.setOnClickListener(view -> {
            Intent leadIntent = new Intent(getApplicationContext(), ClientProcessActivity.class);
            leadIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(leadIntent);
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
                    final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                            .getLayoutManager();
                    int totalItemCount = linearLayoutManager.getItemCount();
                    int lastVisibleItem = linearLayoutManager
                            .findLastVisibleItemPosition();
                    if (isMoreDataAvailable && !isOnSearch && !isScrolling && totalItemCount <= (lastVisibleItem + 5)) {
                        isScrolling = true;
                        prepareClientData();
                    }
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, NavigationActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void prepareClientData() {
        JSONObject postData = new JSONObject();
        try {
            postData.put("action", "get");
            postData.put("page_size", "20");
            if (last_id != null) {
                postData.put("last_id", last_id);
                paginationProgressBar.setVisibility(View.VISIBLE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        NetworkManager.customJsonObjectRequest(
                ClientListActivity.this, Constants.MANAGE_CONTACT_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtils.Print(TAG, "response --> " + response);
                        mSwipeRefreshLayout.setRefreshing(false);
                        processData(response);
                    }

                    @Override
                    public void onError(String message) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        LogUtils.Print("ERRROR=", message);
                    }
                }, true);
    }

    private void searchContacts(String query) {
        String url = Constants.SEARCH_CONTACT_URL + "?all_words=false&search_all=true&search_text=" + query;
        NetworkManager.customJsonObjectRequest(
                ClientListActivity.this, url, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtils.Print(TAG, "response --> " + response);
                        processData(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ERRROR=", message);
                    }
                }, false);
    }

    private void onLoaded() {
        if (clientLists.size() > 0) {
            progressBar.setVisibility(View.GONE);
            paginationProgressBar.setVisibility(View.GONE);
            isScrolling = false;
            recyclerView.setVisibility(View.VISIBLE);
            cAdapter.notifyDataSetChanged();
            no_lead_records.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.GONE);
            no_lead_records.setVisibility(View.VISIBLE);
        }
    }

    private void processData(JSONObject data) {
        try {
            ClientList client;
            if (Integer.parseInt(data.getString("result")) == 1) {
                JSONObject result_Data = data.getJSONObject("data");
                last_id = result_Data.getString("last_id");
                JSONArray clients = result_Data.getJSONArray("contacts");
                for (int i = 0; i < clients.length(); i++) {
                    JSONObject obj = clients.getJSONObject(i);
                    JSONArray emailArray = obj.getJSONArray("email_list");
                    List<Email> emails = new ArrayList<>();
                    for (int j = 0; j < emailArray.length(); j++) {
                        JSONObject emailObj = emailArray.getJSONObject(j);
                        emails.add(new Email(emailObj.getString("email"), emailObj.getString("type")));
                    }
                    JSONArray phoneArray = obj.getJSONArray("phone_list");
                    List<Phone> phones = new ArrayList<>();
                    for (int j = 0; j < phoneArray.length(); j++) {
                        JSONObject phoneObj = phoneArray.getJSONObject(j);
                        phones.add(new Phone(phoneObj.getString("phone"), phoneObj.getString("type")));
                    }
                    JSONArray addressArray = obj.getJSONArray("address_list");
                    List<Address> addresses = new ArrayList();
                    for (int j = 0; j < addressArray.length(); j++) {
                        JSONObject addressObj = addressArray.getJSONObject(j);
                        addresses.add(new Address(addressObj.getString("address"), addressObj.getString("type")));
                    }
                    List<Comment> comments = new ArrayList();
                    JSONArray commentsArray = new JSONArray();
                    if (obj.has("comments")) {
                        Object comments_obj = obj.get("comments");
                        if (comments_obj instanceof JSONArray) {
                            commentsArray = (JSONArray) comments_obj;
                        }

                        for (int j = 0; j < commentsArray.length(); j++) {
                            try {
                                JSONObject commentObj = commentsArray.getJSONObject(j);
                                comments.add(new Comment(commentObj.getString("comment"), commentObj.getString("date"), commentObj.has("url") ? commentObj.getString("url") : "",
                                        commentObj.has("content_type") ? commentObj.getString("content_type") : ""));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

//                    JSONObject lOObj = obj.getJSONObject("lead_owner");
//                    LeadOwner leadOwner = new LeadOwner(lOObj.getString("id"),
//                            lOObj.getString("name"));

                    JSONObject mpObj = obj.getJSONObject("match_preferences_dict");
                    MatchPreference mp = new MatchPreference();
                    if (mpObj.has("lease_length")) {
                        mp.setLease_length(Integer.parseInt(mpObj.getString("lease_length")));
                    }
                    if (mpObj.has("bedrooms")) {
                        mp.setBeds(mpObj.getInt("bedrooms"));
                    }
                    if (mpObj.has("bedrooms")) {
                        mp.setBeds(mpObj.getInt("bedrooms"));
                    }
                    if (mpObj.has("bathrooms")) {
                        mp.setBathrooms(mpObj.getInt("bathrooms"));
                    }
                    if (mpObj.has("budget")) {
                        mp.setBudget(mpObj.getDouble("budget"));
                    }

                    client = new ClientList(obj.getString("id"),
                            obj.getString("name"),
                            obj.getString("status"),
                            obj.getString("file_as"),
                            obj.getString("source"),
                            comments,
                            addresses,
                            emails,
                            phones,
                            obj.getString("name"),
                            mp,
                            obj.getBoolean("marketing_campaign_enabled"),
                            obj.getLong("created_ts"),
                            obj.getLong("status_update_date")
                    );
                    clientLists.add(client);
                }
                isMoreDataAvailable = clients.length() > 0;
                onLoaded();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_client, menu);
        SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView search = (SearchView) menu.findItem(R.id.lead_search).getActionView();
        search.setSearchableInfo(manager.getSearchableInfo(getComponentName()));
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                isOnSearch = true;
                no_lead_records.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                search.setIconified(true);
                clientLists.clear();
                last_id = null;
                searchContacts(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        search.setOnCloseListener(() -> {
            clientLists.clear();
            isOnSearch = false;
            last_id = null;
            progressBar.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            prepareClientData();
            return false;
        });
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.open_phonebook) {
            int hasWriteContactsPermission = checkSelfPermission(Manifest.permission.READ_CONTACTS);
            if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_CONTACTS},
                        REQUEST_CODE_ASK_PERMISSIONS);
            } else {
                Intent i = new Intent(Intent.ACTION_PICK);
                i.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                i.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivityForResult(i, PICK_CONTACT);

//                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivityForResult(intent, PICK_CONTACT);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        clientLists.clear();
        last_id = null;
        mSwipeRefreshLayout.setRefreshing(true);
        paginationProgressBar.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        prepareClientData();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_CONTACT) {
            if (resultCode == Activity.RESULT_OK) {
                Uri contactData = data.getData();
                Cursor c = getContentResolver().query(contactData, null, null, null, null);
                if (c.moveToFirst()) {
                    display_name = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    phoneNumber = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    emailAddress = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                    String street = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.STREET));
                    String city = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.CITY));
                    String country = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.COUNTRY));

                    if (street != null) {
                        address = street;
                    }
                    if (city != null) {
                        address = address + " " + city;
                    }
                    if (country != null) {
                        address = address + " " + country;
                    }

//                    String contactId = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));
//                    try {
//                        String hasPhone = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
//
//                        if (hasPhone.equalsIgnoreCase("1"))
//                            hasPhone = "true";
//                        else
//                            hasPhone = "false";
//
//                        if (Boolean.parseBoolean(hasPhone)) {
//                            Cursor phones = this.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
//                            while (phones.moveToNext()) {
//                                phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
//                            }
//                            phones.close();
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//
//                    String whereName = ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId;
//                    try {
//                        Cursor nameCur = this.getContentResolver().query(ContactsContract.Data.CONTENT_URI, null, whereName, null, ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME);
//                        while (nameCur.moveToNext()) {
//                            f_name = nameCur.getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME));
//                            l_name = nameCur.getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME));
//                            display_name = nameCur.getString(nameCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME));
//                        }
//                        nameCur.close();
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//
//                    // Find Email Addresses
//                    try {
//                        Cursor emails = this.getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = " + contactId, null, null);
//                        while (emails.moveToNext()) {
//                            emailAddress = emails.getString(emails.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
//                        }
//                        emails.close();
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//
//                    Uri postal_uri = ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_URI;
//                    try {
//                        Cursor postal_cursor = this.getContentResolver().query(postal_uri, null, ContactsContract.Data.CONTACT_ID + "=" + contactId, null, null);
//                        while (postal_cursor.moveToNext()) {
//                            String street = postal_cursor.getString(postal_cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.STREET));
//                            String city = postal_cursor.getString(postal_cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.CITY));
//                            String country = postal_cursor.getString(postal_cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.COUNTRY));
//
//                            if (street != null) {
//                                address = street;
//                            }
//                            if (city != null) {
//                                address = address + " " + city;
//                            }
//                            if (country != null) {
//                                address = address + " " + country;
//                            }
//                        }
//                        postal_cursor.close();
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }

                    new AlertDialog.Builder(this)
                            .setTitle(getResources().getString(R.string.create_client_from_address_book))
                            .setMessage(getResources().getString(R.string.create_client_confirmation) + " " + display_name + " ?")
                            .setIcon(R.drawable.account_outline)
                            .setPositiveButton(getResources().getString(R.string.yes), (dialog, whichButton) -> {
                                JSONObject contact = getData(display_name, f_name, l_name, emailAddress, phoneNumber, address);
                                exportContact(contact);
                            })
                            .setNegativeButton(getResources().getString(R.string.no), null).show();
                    prepareClientData();
                }
                c.close();
            }
        }
    }

    private JSONObject getData(String display_name, String first_name, String last_name, String email, String phone, String address) {
        JSONObject payload = new JSONObject();
        try {
            payload.put("name", display_name);
            payload.put("first_name", first_name);
            payload.put("last_name", last_name);

            JSONArray emailArray = new JSONArray();
            JSONObject emailObject = new JSONObject();
            emailObject.put("email", email);
            emailObject.put("type", "Work");
            emailArray.put(emailObject);
            payload.put("email_list", emailArray);

            JSONArray phoneArray = new JSONArray();
            JSONObject phoneObject = new JSONObject();
            phoneObject.put("phone", phone);
            phoneObject.put("type", "work");
            phoneArray.put(phoneObject);
            payload.put("phone_list", phoneArray);

            JSONArray addressArray = new JSONArray();
            JSONObject addressObject = new JSONObject();
            addressObject.put("address", address);
            addressObject.put("type", "work");
            addressArray.put(addressObject);
            payload.put("address_list", addressArray);

            payload.put("status", "open");
            payload.put("source", "Agent");
            payload.put("relationship", "Lead");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return payload;
    }

    private void exportContact(JSONObject paramObj) {
        Util.onMessage(getApplicationContext(), getResources().getString(R.string.contact_creating));
        JSONObject postData = new JSONObject();
        try {
            postData.put("action", "add");
            postData.put("payload", paramObj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        NetworkManager.customJsonObjectRequest(
                ClientListActivity.this, Constants.MANAGE_CONTACT_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processResult(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("EXPORT CONTACT ERROR", message);
                    }
                }, true);
    }

    private void processResult(JSONObject response) {
        try {
            if (Integer.parseInt(response.getString("result")) == 1) {
                Util.onMessage(getApplicationContext(), getResources().getString(R.string.contact_created));
            } else {
                Util.onMessage(getApplicationContext(), getResources().getString(R.string.contact_create_failed));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_ASK_PERMISSIONS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent i = new Intent(Intent.ACTION_PICK);
                i.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                i.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivityForResult(i, PICK_CONTACT);

//                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivityForResult(intent, PICK_CONTACT);
            } else {
                Util.onMessage(getApplicationContext(), getResources().getString(R.string.contact_access_denied));
            }
        }
    }
}