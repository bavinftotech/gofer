package com.gofercrm.user.gofercrm.streaming.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;

import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;

import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.NavigationActivity;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.databinding.ActivityStreamingBinding;
import com.gofercrm.user.gofercrm.implementor.RecyclerViewClickListener;
import com.gofercrm.user.gofercrm.streaming.player.MusicConstants;
import com.gofercrm.user.gofercrm.streaming.player.SoundService;
import com.gofercrm.user.gofercrm.streaming.ui.adapter.StreamingAdapter;
import com.gofercrm.user.gofercrm.streaming.ui.model.DataSteaming;
import com.gofercrm.user.gofercrm.streaming.ui.model.ResponseStreaming;
import com.gofercrm.user.gofercrm.util.EndlessScrollRecyclerViewListener;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class StreamingActivity extends BaseActivity implements RecyclerViewClickListener {

    private static final String TAG = "StreamingActivity";

    private ActivityStreamingBinding binding;
    private List<DataSteaming> list = new ArrayList<>();
    private StreamingAdapter adapter;

    //pagination
    private String last_id = null;
    private int pageSize = 10;
    public MutableLiveData<Boolean> isLoading = new MutableLiveData<>(false);
    private boolean isStopPlayer = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_streaming);
        binding.setActivity(this);
        setUpHeaderView();
        setUpAdapterView();
        getStreamingList();
        setUpSearchEvent();
    }

    private void setUpHeaderView() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.streaming_audio));
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        adapter = new StreamingAdapter(list, this);
        adapter.setOnRecyclerViewItemClickListener(this);
        binding.rvStreaming.setAdapter(adapter);
    }

    private void setUpSearchEvent() {
        binding.etSearch.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                Util.hideSoftKeyboard(v);
                last_id = null;
                getStreamingList();
                return true;
            }
            return false;
        });
    }

    private void getStreamingList() {
        String q_param = "?page_size=" + pageSize;
        if (binding.etSearch.getText().toString().trim().length() > 0) {
            q_param += "&search_text=" + binding.etSearch.getText().toString().trim();
        }
        if (last_id != null) {
            q_param += "&last_id=" + last_id;
        } else {
            if (list.size() > 0) {
                list.clear();
                adapter.notifyDataSetChanged();
            }
        }
        LogUtils.Print(TAG, "q_param -> " + q_param);
        showProgressBar(true);
        NetworkManager.customJsonObjectGetRequest(
                this, Constants.API_GET_STREAMING + q_param, null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        showProgressBar(false);
                        LogUtils.Print(TAG, "onResponse --> " + response);
                        try {
                            if (Integer.parseInt(response.getString("result")) == 1) {
                                ResponseStreaming streaming = new GsonBuilder().create().fromJson(response.toString(), ResponseStreaming.class);
                                if (streaming.getChannels().size() == pageSize) {
                                    last_id = response.getString("last_id");
                                } else {
                                    //no more list found.
                                    last_id = null;
                                }
                                list.addAll(streaming.getChannels());
                                adapter.notifyDataSetChanged();
                            } else {
                                Util.makeToast(getResources().getString(R.string.server_err));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Util.makeToast(getResources().getString(R.string.server_err));
                        }
                    }

                    @Override
                    public void onError(String message) {
                        showProgressBar(false);
                        LogUtils.Print(TAG, "onError --> " + message);
                        Util.makeToast(message);
                    }
                }, true);
    }

    private void showProgressBar(boolean flag) {
        if (last_id == null) {
            if (flag) {
                if (!binding.sl.isRefreshing())
                    showProgress();
            } else {
                hideProgress();
            }
        } else {
            binding.pbPagination.setVisibility(flag ? View.VISIBLE : View.GONE);
        }
        if (!flag) {
            binding.sl.setRefreshing(false);
        }
        isLoading.setValue(flag);
    }

    /**
     * SET-UP PULL TO REFRESH
     */
    public void refreshList() {
        if (isLoading.getValue() != null && isLoading.getValue()) return;
        isLoading.setValue(false);
        binding.sl.setRefreshing(true);
        last_id = null;
        if (list.size() > 0) {
            list.clear();
            adapter.notifyDataSetChanged();
        }
        getStreamingList();
    }

    /**
     * SET-UP ADAPTER DATA TO RECYCLER VIEW
     */
    private void setUpAdapterView() {
        binding.rvStreaming.addOnScrollListener(new EndlessScrollRecyclerViewListener() {
            @Override
            public void onLoadMore() {
                if (!isLoading.getValue() && last_id != null) {
                    getStreamingList();
                }
            }
        });
    }

    @Override
    public void onItemClick(int flag, int position, View view) {
        Intent intent = new Intent();
        intent.putExtra("DATA", list.get(position));
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        if (isStopPlayer) {
            setResult(RESULT_OK);
            finish();
        } else {
            super.onBackPressed();
        }
    }

    MenuItem menuStreaming;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_streaming, menu);
        menuStreaming = menu.findItem(R.id.item_streaming);
        menuStreaming.setVisible(SoundService.sStateService != MusicConstants.STATE_SERVICE.NOT_INIT);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.item_streaming) {
            Intent startIntent = new Intent(this, SoundService.class);
            startIntent.setAction(MusicConstants.ACTION.STOP_ACTION);
            startService(startIntent);
            isStopPlayer = true;
            menuStreaming.setVisible(false);
        }
        return super.onOptionsItemSelected(item);
    }
}