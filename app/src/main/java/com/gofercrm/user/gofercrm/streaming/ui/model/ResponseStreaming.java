package com.gofercrm.user.gofercrm.streaming.ui.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseStreaming {

    @SerializedName("result")
    private int result;

    @SerializedName("channels")
    private List<DataSteaming> channels;

    @SerializedName("last_id")
    private String lastId;

    public int getResult() {
        return result;
    }

    public List<DataSteaming> getChannels() {
        return channels;
    }

    public String getLastId() {
        return lastId;
    }
}