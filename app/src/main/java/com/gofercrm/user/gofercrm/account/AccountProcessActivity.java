package com.gofercrm.user.gofercrm.account;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;

import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class AccountProcessActivity extends BaseActivity {
    //    ProgressDialog progressDialog;
    String ACCOUNT_ID = "";
    EditText displayName;
    EditText email, company_name, account_code, revenue, currency, description, installed_products;
    Spinner emailType;
    EditText phone;
    Spinner phoneType;
    EditText address;
    Spinner addressType;
    Spinner status;
    Spinner fileas;
    Spinner source;
    Spinner relationship;
    List<String> phoneTypes = new ArrayList<>();
    List<String> emailTypes = new ArrayList<>();
    List<String> addressTypes = new ArrayList<>();
    List<String> statuses = new ArrayList<>();
    List<String> fileaslist = new ArrayList<>();
    List<String> sources = new ArrayList<>();
    List<String> relationships = new ArrayList<>();

    ArrayAdapter<String> emailAdapter;
    ArrayAdapter<String> phoneAdapter;
    ArrayAdapter<String> addressAdapter;
    ArrayAdapter<String> statusAdapter;
    ArrayAdapter<String> fileasAdapter;
    ArrayAdapter<String> sourceAdapter;
    ArrayAdapter<String> relationshipAdapter;
    String PROCESSING_MESSAGE;
    String SUCCESS_MESSAGE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_process);
        Toolbar toolbar = findViewById(R.id.toolbar);

        PROCESSING_MESSAGE = getResources().getString(R.string.saving_account_);
        SUCCESS_MESSAGE = getResources().getString(R.string.account_saved_success);
        displayName = findViewById(R.id.input_lead_displayName);
//        first_name = (EditText) findViewById(R.id.input_lead_firstName);
//        last_name = (EditText) findViewById(R.id.input_lead_lastName);
        email = findViewById(R.id.input_lead_email);

        company_name = findViewById(R.id.input_account_companyname);
        currency = findViewById(R.id.input_account_currency);
        revenue = findViewById(R.id.input_account_revenue);
        account_code = findViewById(R.id.input_account_account_code);
        description = findViewById(R.id.input_account_desc);
        installed_products = findViewById(R.id.input_account_installedProducts);

        displayName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                company_name.setText(displayName.getText());
                account_code.setText(displayName.getText());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        emailType = findViewById(R.id.lead_email_type);
        phone = findViewById(R.id.input_lead_phone);
        phoneType = findViewById(R.id.lead_phone_type);
        address = findViewById(R.id.input_lead_address);
        addressType = findViewById(R.id.lead_address_type);
        status = findViewById(R.id.input_lead_status);
        fileas = findViewById(R.id.input_lead_fileas);
        source = findViewById(R.id.input_lead_source);
        relationship = findViewById(R.id.input_lead_relationship);

        phoneTypes = new ArrayList<>();
        Collections.addAll(phoneTypes, getResources().getStringArray(R.array.arrPhoneType));
        phoneAdapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_item, phoneTypes);
        phoneAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        phoneType.setAdapter(phoneAdapter);

        emailTypes = new ArrayList<>();
        emailTypes.add(getResources().getString(R.string.personal));
        emailTypes.add(getResources().getString(R.string.work));
        emailTypes.add(getResources().getString(R.string.other));
        emailAdapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_item, emailTypes);
        emailAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        emailType.setAdapter(emailAdapter);

        addressTypes = new ArrayList<>();
        addressTypes.add(getResources().getString(R.string.home));
        addressTypes.add(getResources().getString(R.string.work));
        addressTypes.add(getResources().getString(R.string.other));
        addressAdapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_item, addressTypes);
        addressAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        addressType.setAdapter(addressAdapter);

        statuses = new ArrayList<>();
        statuses.add(getResources().getString(R.string.customer));
        statuses.add(getResources().getString(R.string.prospect));
        statuses.add(getResources().getString(R.string.ex_customer));

        statusAdapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_item, statuses);
        statusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        status.setAdapter(statusAdapter);

        fileaslist = new ArrayList<>();
        fileaslist.add(getResources().getString(R.string.immediate_followup));
        fileaslist.add(getResources().getString(R.string.followup_next_month));
        fileaslist.add(getResources().getString(R.string.followup_lease_expiry));
        fileaslist.add(getResources().getString(R.string.unresponsive));

        fileasAdapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_item, fileaslist);
        fileasAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fileas.setAdapter(fileasAdapter);

        sources = new ArrayList<>();
        sources.addAll(Arrays.asList(getResources().getStringArray(R.array.arrSource)));
        sourceAdapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_item, sources);
        sourceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        source.setAdapter(sourceAdapter);

        relationships = new ArrayList<>();
        relationships.addAll(Arrays.asList(getResources().getStringArray(R.array.arrRelationships)));

        relationshipAdapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_item, relationships);
        relationshipAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        relationship.setAdapter(relationshipAdapter);

        Intent intent = getIntent();
        ACCOUNT_ID = intent.getStringExtra("ACCOUNT_ID");
        if (ACCOUNT_ID != null) {
            toolbar.setTitle(getResources().getString(R.string.edit_account));
            PROCESSING_MESSAGE = getResources().getString(R.string.updating_account_);
            SUCCESS_MESSAGE = getResources().getString(R.string.account_updated_success);
            getAccountById(ACCOUNT_ID);
        } else {
            toolbar.setTitle(getResources().getString(R.string.create_account));
        }
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_lead_process, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_save) {
            if (!validate()) {
                onMessage(getResources().getString(R.string.save_failed));
            } else {
                saveAccount();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private JSONObject getData() {
        JSONObject payload = new JSONObject();
        try {
            payload.put("name", displayName.getText().toString());
//            payload.put("first_name", first_name.getText().toString());
//            payload.put("last_name", last_name.getText().toString());

            JSONArray emailArray = new JSONArray();
            JSONObject emailObject = new JSONObject();
            emailObject.put("email", email.getText().toString());
            emailObject.put("type", emailType.getSelectedItem().toString());
            emailArray.put(emailObject);
            payload.put("email_list", emailArray);

            JSONArray phoneArray = new JSONArray();
            JSONObject phoneObject = new JSONObject();
            phoneObject.put("phone", phone.getText().toString());
            phoneObject.put("type", phoneType.getSelectedItem().toString());
            phoneArray.put(phoneObject);
            payload.put("phone_list", phoneArray);

            JSONArray addressArray = new JSONArray();
            JSONObject addressObject = new JSONObject();
            addressObject.put("address", address.getText().toString());
            addressObject.put("type", addressType.getSelectedItem().toString());
            addressArray.put(addressObject);
            payload.put("address_list", addressArray);

            payload.put("status", status.getSelectedItem().toString());
            payload.put("file_as", fileas.getSelectedItem().toString());
            payload.put("source", source.getSelectedItem().toString());
            payload.put("relationship", relationship.getSelectedItem().toString());

            payload.put("company_name", company_name.getText().toString());
            payload.put("currency", currency.getText().toString());
            payload.put("revenue", revenue.getText().toString());
            payload.put("account_code", account_code.getText().toString());
            payload.put("description", description.getText().toString());
            payload.put("installed_products", installed_products.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return payload;
    }

    public void onMessage(String text) {
        Toast.makeText(getBaseContext(), text, Toast.LENGTH_LONG).show();
    }

    private void saveAccount() {
//        progressDialog = new ProgressDialog(AccountProcessActivity.this,
//                R.style.AppTheme_Dark_Dialog);
//        progressDialog.setCanceledOnTouchOutside(false);
//        progressDialog.setIndeterminate(true);
//        progressDialog.setMessage(PROCESSING_MESSAGE);
//        progressDialog.show();
        showProgress();
        JSONObject postData = new JSONObject();
        try {
            if (ACCOUNT_ID != null) {
                postData.put("action", "update");
                postData.put("account_id", ACCOUNT_ID);
            } else {
                postData.put("action", "add");
            }

            postData.put("payload", getData());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        NetworkManager.customJsonObjectRequest(
                AccountProcessActivity.this, Constants.MANAGE_ACCOUNT_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideProgress();
//                        progressDialog.dismiss();
                        processResult(response);
                    }

                    @Override
                    public void onError(String message) {
                        hideProgress();
//                        progressDialog.dismiss();
                        LogUtils.Print("ACCOUNT ERROR", message);
                    }
                }, true);
    }

    private void processResult(JSONObject response) {
        try {
            if (Integer.parseInt(response.getString("result")) == 1) {
                onMessage(SUCCESS_MESSAGE);
            } else {
                onMessage(getResources().getString(R.string.account_save_failed));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getAccountById(String account_id) {
        JSONObject postData = new JSONObject();
        try {
            postData.put("action", "get");
            postData.put("account_id", account_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        NetworkManager.customJsonObjectRequest(
                AccountProcessActivity.this, Constants.MANAGE_ACCOUNT_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processData(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("ACCOUNT PROCESS ERROR", message);
                    }

                }, true);
    }

    private void processData(JSONObject data) {
        try {
            if (Integer.parseInt(data.getString("result")) == 1) {
                JSONObject result_Data = data.getJSONObject("data");
                JSONArray leads = result_Data.getJSONArray("accounts");
                for (int i = 0; i < leads.length(); i++) {

                    JSONObject obj = leads.getJSONObject(i);
                    JSONArray emailArray = obj.getJSONArray("email_list");
                    for (int j = 0; j < emailArray.length(); j++) {

                        JSONObject emailObj = emailArray.getJSONObject(j);
                        email.setText(emailObj.getString("email"));
                        emailType.setSelection(emailAdapter.getPosition(emailObj.getString("type")));
                    }
                    JSONArray phoneArray = obj.getJSONArray("phone_list");
                    for (int j = 0; j < phoneArray.length(); j++) {
                        JSONObject phoneObj = phoneArray.getJSONObject(j);
                        phone.setText(phoneObj.getString("phone"));
                        phoneType.setSelection(phoneAdapter.getPosition(phoneObj.getString("type")));
                    }
                    JSONArray addressArray = obj.getJSONArray("address_list");
                    for (int j = 0; j < addressArray.length(); j++) {
                        JSONObject addressObj = addressArray.getJSONObject(j);
                        address.setText(addressObj.getString("address"));
                        addressType.setSelection(addressAdapter.getPosition(addressObj.getString("type")));
                    }

                    displayName.setText(obj.get("name").toString());
                    company_name.setText(obj.get("company_name").toString());
                    account_code.setText(obj.get("account_code").toString());
                    revenue.setText(obj.get("revenue").toString());
                    currency.setText(obj.get("currency").toString());
                    source.setSelection(sourceAdapter.getPosition(obj.getString("source")));
                    status.setSelection(statusAdapter.getPosition(obj.getString("status")));
                    relationship.setSelection(relationshipAdapter.getPosition(obj.getString("relationship")));
                    fileas.setSelection(fileasAdapter.getPosition(obj.getString("file_as")));
                    description.setText(obj.get("description").toString());
                    installed_products.setText(obj.get("installed_products").toString());
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public boolean validate() {
        boolean valid = true;

        String leadEmail = email.getText().toString();
        String leadName = displayName.getText().toString();
        String leadFirstName = company_name.getText().toString();
        String leadLastName = account_code.getText().toString();
        String leadPhone = phone.getText().toString();

        if (leadEmail.isEmpty() && leadPhone.isEmpty()) {
            onMessage(getResources().getString(R.string.email_phone_empty_err));
            valid = false;
        }

        if (!leadEmail.isEmpty() && !android.util.Patterns.EMAIL_ADDRESS.matcher(leadEmail).matches()) {
            email.setError(getResources().getString(R.string.email_invalid));
            valid = false;
        } else {
            email.setError(null);
        }

        if (leadName.isEmpty()) {
            displayName.setError(getResources().getString(R.string.name_empty_err));
            valid = false;
        } else {
            displayName.setError(null);
        }
        if (leadFirstName.isEmpty()) {
            company_name.setError(getResources().getString(R.string.company_name_empty_err));
            valid = false;
        } else {
            company_name.setError(null);
        }
        if (leadLastName.isEmpty()) {
            account_code.setError(getResources().getString(R.string.account_code_empty_err));
            valid = false;
        } else {
            account_code.setError(null);
        }
        return valid;
    }
}