package com.gofercrm.user.gofercrm.websocket.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataSocketMessage {
    private String type;
    private String error_code;
    private String error_message;
    @SerializedName("payload")
    @Expose
    private Payload payload;
    @SerializedName("sender_name")
    @Expose
    private String senderName;

    @SerializedName("message")
    @Expose
    private Message message; //message seems to be overloaded as an object and as a string

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getErrorCode() {
        return error_code;
    }

    public void setErrorCode(String errorCode) {
        this.error_code = errorCode;
    }

    public String getErrorMessage() {
        return error_message;
    }

    public void setErrorMessage(String errorMessage) {
        this.error_message = errorMessage;
    }

    public Payload getPayload() {
        return payload;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }
}