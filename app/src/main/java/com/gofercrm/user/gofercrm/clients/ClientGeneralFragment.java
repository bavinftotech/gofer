package com.gofercrm.user.gofercrm.clients;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.R;

public class ClientGeneralFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";

    private RecyclerView recyclerView;
    private ClientGeneralAdapter cGAdapter;

    public ClientGeneralFragment() {
    }

    public static ClientGeneralFragment newInstance(int sectionNumber) {
        ClientGeneralFragment fragment = new ClientGeneralFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rv = inflater.inflate(R.layout.general_tab_lead_view, container, false);
        ClientViewActivity activity = (ClientViewActivity) getActivity();
        recyclerView = rv.findViewById(R.id.lead_view_recycle);

        cGAdapter = new ClientGeneralAdapter(activity.getData(), getContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(cGAdapter);
        return rv;
    }
}