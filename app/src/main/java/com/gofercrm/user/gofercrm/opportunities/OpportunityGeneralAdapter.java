package com.gofercrm.user.gofercrm.opportunities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.clients.ClientViewActivity;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.List;

public class OpportunityGeneralAdapter extends RecyclerView.Adapter<OpportunityGeneralAdapter.MyViewHolder> {

    private List<OpportunityView> clientPageElements;
    private Context ctx;

    public OpportunityGeneralAdapter(List<OpportunityView> opportunityViews, Context ctx) {
        this.clientPageElements = opportunityViews;
        this.ctx = ctx;
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lead_view_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final OpportunityView client = clientPageElements.get(position);

        if (client.getType().equals("CONTACT")) {
            holder.label.setText(ctx.getResources().getString(R.string.contact));
            holder.value.setText(client.getLabel());
        }
        if (client.getType().equals("PRODUCT")) {
            holder.label.setText(ctx.getResources().getString(R.string.product));
            holder.value.setText(client.getLabel());
        }
        if (!client.getType().equals("PRODUCT") && !client.getType().equals("CONTACT")) {
            holder.label.setText(client.getLabel());
            holder.value.setText(client.getValue());
        }

        if (client.getLeftDrawable() != 0) {
            holder.leftImage.setImageResource(client.getLeftDrawable());
        }
        if (client.getRightDrawable() != 0) {
            holder.rightImage.setImageResource(client.getRightDrawable());
        }

        if (client.getValue().equals("closed_won") && client.getType().equals("STAGE")) {
            holder.leftImage.setImageDrawable(ctx.getResources().getDrawable(R.drawable.thumb_up_outline));
            holder.leftImage.setColorFilter(ContextCompat.getColor(ctx, R.color.green));

        } else if (client.getValue().equals("closed_lost") && client.getType().equals("STAGE")) {
            holder.leftImage.setImageDrawable(ctx.getResources().getDrawable(R.drawable.thumb_down_outline));
            holder.leftImage.setColorFilter(ContextCompat.getColor(ctx, R.color.rejected));

        } else if (!client.getValue().equals("closed_won") && !client.getValue().equals("closed_lost") && client.getType().equals("STAGE")) {
            holder.leftImage.setImageDrawable(ctx.getResources().getDrawable(R.drawable.checkbox_blank_circle_outline));
            holder.leftImage.setColorFilter(ContextCompat.getColor(ctx, R.color.colorPrimaryDark));
        }

        holder.itemView.setOnClickListener(view -> {
            if (client.getType().equals("CONTACT") && !client.getValue().isEmpty()) {
                Intent intent = new Intent(ctx, ClientViewActivity.class);
                intent.putExtra("CLIENT_ID", (Serializable) client.getValue());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ctx.startActivity(intent);
            } else if (client.getType().equals("EMAIL") && !client.getValue().isEmpty()) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                Uri data = Uri.parse("mailto:" + client.getValue());
                intent.setData(data);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ctx.startActivity(intent);
            } else if (client.getType().equals("ADDRESS") && !client.getValue().isEmpty()) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                Uri data = Uri.parse("geo:" + client.getValue());
                intent.setData(data);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ctx.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return clientPageElements.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView label, value;
        public ImageView leftImage, rightImage;

        public MyViewHolder(View view) {
            super(view);
            label = view.findViewById(R.id.leadView_label_id);
            value = view.findViewById(R.id.lead_view_data_id);
            leftImage = view.findViewById(R.id.lead_img_btn_left);
            rightImage = view.findViewById(R.id.lead_img_btn_right);
        }
    }
}