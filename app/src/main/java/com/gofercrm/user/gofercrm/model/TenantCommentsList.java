package com.gofercrm.user.gofercrm.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TenantCommentsList {

    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("date")
    @Expose
    private Integer date;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("user_name")
    @Expose
    private String userName;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getDate() {
        return date;
    }

    public void setDate(Integer date) {
        this.date = date;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

}