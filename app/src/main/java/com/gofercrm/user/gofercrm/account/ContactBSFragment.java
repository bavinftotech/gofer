package com.gofercrm.user.gofercrm.account;

import android.os.Bundle;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

public class ContactBSFragment extends BottomSheetDialogFragment implements View.OnClickListener {

    private EditText title, first_name, last_name, email, phone, company;
    private ContactBSFragment currentObj;
    private AccountViewActivity activity;

    public ContactBSFragment() {

    }

    public void setCurrentObj(ContactBSFragment currentObj) {
        this.currentObj = currentObj;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        activity = (AccountViewActivity) getActivity();
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.contact_bottom_sheet, container, false);
        Button account_contact_cancel = v.findViewById(R.id.account_contact_cancel);
        account_contact_cancel.setOnClickListener(this);
        Button account_contact_save = v.findViewById(R.id.account_contact_save);
        account_contact_save.setOnClickListener(this);

        title = v.findViewById(R.id.input_account_contact_title);
        first_name = v.findViewById(R.id.input_account_contact_fn);
        last_name = v.findViewById(R.id.input_account_contact_ln);
        email = v.findViewById(R.id.input_account_contact_email);
        phone = v.findViewById(R.id.input_account_contact_phone);
        company = v.findViewById(R.id.input_account_contact_company);
        return v;
    }

    public boolean validate() {
        boolean valid = true;

        String title_v = title.getText().toString();
        String first_name_v = first_name.getText().toString();
        String last_name_v = last_name.getText().toString();
        String email_v = email.getText().toString();
        String phone_V = phone.getText().toString();

        if (title_v.isEmpty()) {
            title.setError(getResources().getString(R.string.title_empty_err));
            valid = false;
        } else {
            title.setError(null);
        }
        if (first_name_v.isEmpty()) {
            first_name.setError(getResources().getString(R.string.first_name_empty_err));
            valid = false;
        } else {
            first_name.setError(null);
        }
        if (last_name_v.isEmpty()) {
            last_name.setError(getResources().getString(R.string.last_name_empty_err));
            valid = false;
        } else {
            last_name.setError(null);
        }
        if (!email_v.isEmpty() && !android.util.Patterns.EMAIL_ADDRESS.matcher(email_v).matches()) {
            email.setError(getResources().getString(R.string.email_invalid));
            valid = false;
        } else {
            email.setError(null);
        }
        if (!phone_V.isEmpty() && !Patterns.PHONE.matcher(phone_V).matches()) {
            phone.setError(getResources().getString(R.string.phone_invalid_err));
            valid = false;
        } else {
            phone.setError(null);
        }
        return valid;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.account_contact_cancel) {
            if (currentObj != null) {
                currentObj.dismiss();
            }
        } else if (view.getId() == R.id.account_contact_save) {
            if (!validate()) {
                Util.onMessageTop(getContext(), getResources().getString(R.string.incorrect_inputs));
            } else {
                saveClient();
            }
        }
    }

    private void saveClient() {
        JSONObject postData = new JSONObject();
        try {
            postData.put("action", "add");
            postData.put("payload", getData());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        NetworkManager.customJsonObjectRequest(
                Objects.requireNonNull(getContext()), Constants.MANAGE_CONTACT_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        processResult(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("CLIENT ERROR", message);
                    }
                }, true);
    }

    private JSONObject getData() {
        JSONObject payload = new JSONObject();
        try {
            payload.put("account_id", activity.getAccount_Id());
            payload.put("name", first_name.getText().toString() + " " + last_name.getText().toString());
            payload.put("first_name", first_name.getText().toString());
            payload.put("last_name", last_name.getText().toString());
            payload.put("title", title.getText().toString());
            payload.put("company_name", company.getText().toString());

            JSONArray emailArray = new JSONArray();
            JSONObject emailObject = new JSONObject();
            emailObject.put("email", email.getText().toString());
            emailObject.put("type", "personal");
            emailArray.put(emailObject);
            payload.put("email_list", emailArray);

            JSONArray phoneArray = new JSONArray();
            JSONObject phoneObject = new JSONObject();
            phoneObject.put("phone", phone.getText().toString());
            phoneObject.put("type", "personal");
            phoneArray.put(phoneObject);
            payload.put("phone_list", phoneArray);
            payload.put("status", "open");
            payload.put("file_as", "Immediate followup");
            payload.put("source", "Google");
            payload.put("relationship", "Lead");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return payload;
    }

    private void processResult(JSONObject response) {
        try {
            if (Integer.parseInt(response.getString("result")) == 1) {
                clearFields();
                Util.onMessageTop(getContext(), getResources().getString(R.string.contact_got_created));
            } else {
                Util.onMessageTop(getContext(), getResources().getString(R.string.client_save_failed));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void clearFields() {
        title.setText("");
        first_name.setText("");
        last_name.setText("");
        email.setText("");
        phone.setText("");
        company.setText("");
    }
}