package com.gofercrm.user.gofercrm.chat.ui.main.addcontact;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.chat.ui.main.SelectableAdapter;
import com.gofercrm.user.gofercrm.chat.ui.main.addcontact.model.DataContact;
import com.gofercrm.user.gofercrm.util.Util;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class AddContactAdapter extends SelectableAdapter<AddContactAdapter.ViewHolder> {
    private List<DataContact> mArrayList;
    private Context mContext;
    private ViewHolder.ClickListener clickListener;

    public AddContactAdapter(Context context, List<DataContact> arrayList, ViewHolder.ClickListener clickListener) {
        setHasStableIds(true);
        this.mArrayList = arrayList;
        this.mContext = context;
        this.clickListener = clickListener;
    }

    // Create new views
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.row_add_contact, null);
        return new ViewHolder(itemLayoutView, clickListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        DataContact chat = mArrayList.get(position);
        viewHolder.tvName.setText(chat.getName());
        if (chat.getEmail() != null && !chat.getEmail().isEmpty()) {
            viewHolder.tvEmail.setVisibility(View.VISIBLE);
            viewHolder.tvEmail.setText(chat.getEmail());
        } else {
            viewHolder.tvEmail.setVisibility(View.GONE);
        }
        if (chat.getPhone() != null && !chat.getPhone().isEmpty()) {
            viewHolder.tvPhone.setVisibility(View.VISIBLE);
            viewHolder.tvPhone.setText(chat.getPhone());
        } else {
            viewHolder.tvPhone.setVisibility(View.GONE);
        }
        if (chat.getDistance() != 0) {
            viewHolder.tvDistance.setText(String.format("%.2f", Util.metersToMiles(chat.getDistance())) + " " +
                    mContext.getResources().getString(R.string.miles));
        } else {
            viewHolder.tvDistance.setText("0 " + mContext.getResources().getString(R.string.mile));
        }
        if (chat.getAvatarUrl() == null || chat.getAvatarUrl().equals("")) {
            if (chat.getName() != null && !chat.getName().isEmpty()) {
                viewHolder.tvInitial.setText(String.valueOf(chat.getName().charAt(0)));
                viewHolder.tvInitial.setVisibility(View.VISIBLE);
                viewHolder.userPhoto.setVisibility(View.GONE);
            } else {
                viewHolder.tvInitial.setVisibility(View.GONE);
                viewHolder.userPhoto.setVisibility(View.VISIBLE);
            }
        } else {
            viewHolder.tvInitial.setVisibility(View.GONE);
            viewHolder.userPhoto.setVisibility(View.VISIBLE);
            Glide.with(mContext)
                    .load(mArrayList.get(position).getAvatarUrl())
                    .placeholder(R.drawable.ic_account_circle)
                    .into(viewHolder.userPhoto);
        }

        switch (mArrayList.get(position).getFriend()) {
            case 1://Existing Friend
                viewHolder.icAddConnection.setVisibility(View.GONE);
                viewHolder.icRemoveConnection.setVisibility(View.VISIBLE);
                viewHolder.tvRemove.setVisibility(View.GONE);
                viewHolder.ivAccept.setVisibility(View.GONE);
                viewHolder.ivRemove.setVisibility(View.GONE);
                break;
            case 2://Waiting Response on your connection request
                viewHolder.tvRemove.setVisibility(View.VISIBLE);
                viewHolder.tvRemove.setText(mContext.getResources().getString(R.string.withdraw));
                viewHolder.icRemoveConnection.setVisibility(View.GONE);
                viewHolder.icAddConnection.setVisibility(View.GONE);
                viewHolder.ivAccept.setVisibility(View.GONE);
                viewHolder.ivRemove.setVisibility(View.GONE);
                break;
            case 3://Accept or Reject Incoming Request
                viewHolder.tvRemove.setVisibility(View.GONE);
                viewHolder.tvRemove.setText(mContext.getResources().getString(R.string.reject));
                viewHolder.icRemoveConnection.setVisibility(View.GONE);
                viewHolder.icAddConnection.setVisibility(View.GONE);
                viewHolder.ivAccept.setVisibility(View.VISIBLE);
                viewHolder.ivRemove.setVisibility(View.VISIBLE);
                break;
            case 0:
                viewHolder.icRemoveConnection.setVisibility(View.GONE);
                viewHolder.icAddConnection.setVisibility(View.VISIBLE);
                viewHolder.tvRemove.setVisibility(View.GONE);
                viewHolder.ivAccept.setVisibility(View.GONE);
                viewHolder.ivRemove.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvRemove;
        public TextView tvName, tvEmail, tvPhone, tvDistance;
        public ImageView userPhoto, icRemoveConnection, icAddConnection, ivAccept, ivRemove;
        public TextView tvInitial;
        private ClickListener listener;
        private RelativeLayout rl_photo;

        public ViewHolder(View itemLayoutView, ClickListener listener) {
            super(itemLayoutView);
            this.listener = listener;
            icRemoveConnection = itemLayoutView.findViewById(R.id.icRemoveConnection);
            icRemoveConnection.setOnClickListener(this);
            icAddConnection = itemLayoutView.findViewById(R.id.icAddConnection);
            icAddConnection.setOnClickListener(this);
            ivAccept = itemLayoutView.findViewById(R.id.ivAccept);
            ivAccept.setOnClickListener(this);
            tvRemove = itemLayoutView.findViewById(R.id.tvRemove);
            tvRemove.setOnClickListener(this);
            ivRemove = itemLayoutView.findViewById(R.id.ivRemove);
            ivRemove.setOnClickListener(this);
            userPhoto = itemLayoutView.findViewById(R.id.iv_user_photo);
            tvInitial = itemLayoutView.findViewById(R.id.user_Initial);
            tvName = itemLayoutView.findViewById(R.id.tv_user_name);
            tvEmail = itemLayoutView.findViewById(R.id.tvEmail);
            tvPhone = itemLayoutView.findViewById(R.id.tvPhone);
            tvDistance = itemLayoutView.findViewById(R.id.tvDistance);
            rl_photo = itemLayoutView.findViewById(R.id.rl_photo);
            rl_photo.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v == ivAccept) {
                listener.onItemClicked(getAdapterPosition(), 1);
            } else if (v == tvRemove || v == ivRemove || v == icRemoveConnection ||
                    v == icAddConnection) {
                listener.onItemClicked(getAdapterPosition(), 0);
            }else if (v == rl_photo) {
                listener.onItemClicked(getAdapterPosition(), 2);
            }
        }

        public interface ClickListener {
            void onItemClicked(int position, int type);
        }
    }
}