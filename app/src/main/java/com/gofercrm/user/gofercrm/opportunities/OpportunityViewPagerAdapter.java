package com.gofercrm.user.gofercrm.opportunities;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import org.jetbrains.annotations.NotNull;

public class OpportunityViewPagerAdapter extends FragmentPagerAdapter {

    public OpportunityViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @NotNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return OpportunityGeneralFragment.newInstance(0);
            case 1:
                return OpportunityCommentsFragment.newInstance(1);
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}