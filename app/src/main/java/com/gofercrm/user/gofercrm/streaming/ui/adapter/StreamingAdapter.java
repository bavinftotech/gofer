package com.gofercrm.user.gofercrm.streaming.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.databinding.RowStreamingBinding;
import com.gofercrm.user.gofercrm.implementor.RecyclerViewClickListener;
import com.gofercrm.user.gofercrm.streaming.ui.StreamingActivity;
import com.gofercrm.user.gofercrm.streaming.ui.model.DataSteaming;

import java.util.List;

public class StreamingAdapter extends RecyclerView.Adapter<StreamingAdapter.ViewHolder> {

    private List<DataSteaming> list;
    private StreamingActivity activity;

    public StreamingAdapter(List<DataSteaming> list, StreamingActivity activity) {
        this.list = list;
        this.activity = activity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RowStreamingBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.row_streaming, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.setData(list.get(position));
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private RowStreamingBinding binding;

        public ViewHolder(RowStreamingBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view == itemView) {
                if (getAdapterPosition() != RecyclerView.NO_POSITION) {
                    if (listener != null) {
                        listener.onItemClick(0, getAdapterPosition(), view);
                    }
                }
            }
        }
    }

    private RecyclerViewClickListener listener;

    public void setOnRecyclerViewItemClickListener(RecyclerViewClickListener clickListener) {
        listener = clickListener;
    }
}