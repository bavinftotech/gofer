package com.gofercrm.user.gofercrm.task.view.tab.comments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.comments.CommentAttachmentActivity;
import com.gofercrm.user.gofercrm.comments.CommentsAdapter;
import com.gofercrm.user.gofercrm.entity.Comment;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.SharedPreferenceData;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TaskCommentsFragment extends Fragment {

    private static final int REQUEST_CODE = 100;

    RecyclerView recyclerView;
    EditText commentText;
    CommentsAdapter commentAdapter;
    List<Comment> commentsData = new ArrayList<>();
    String task_name = "Comments";
    String task_id = "";
    ImageButton sendComment;
    private ImageButton btnAttach;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_task_comments, container, false);
        Bundle bundle = getArguments();
        commentsData = (List<Comment>) bundle.getSerializable("COMMENTS");
        task_name = bundle.getString("TASK_NAME");
        task_id = bundle.getString("TASK_ID");

        recyclerView = view.findViewById(R.id.comments_recycler);
        commentText = view.findViewById(R.id.commentsText);
        if (commentsData == null) {
            commentsData = new ArrayList<>();
        }
        commentAdapter = new CommentsAdapter(commentsData, getActivity());
        RecyclerView.LayoutManager mLayoutManager =
                new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(commentAdapter);

        if (commentsData.size() > 2) {
            recyclerView.smoothScrollToPosition(commentsData.size() - 1);
        }

        sendComment = view.findViewById(R.id.btn_update_comment);
        sendComment.setOnClickListener(v -> {
                    String comment = commentText.getText().toString().trim();
                    if (comment.isEmpty()) {
                        Util.makeToast(getResources().getString(R.string.comment_empty));
                        return;
                    }
                    updateComment(task_id, comment);

                    Comment objComment = new Comment(
                            SharedPreferenceData.getInstance(getActivity()).loginSharedPref.getString("USER_FORMATTED_NAME", "") + ": " + comment,
                            String.valueOf(Util.getCurrentTimeMillis()),
                            "", "");
                    commentsData.add(objComment);
                    commentText.setText("");
                    commentAdapter.notifyDataSetChanged();
                    recyclerView.post(() -> recyclerView.smoothScrollToPosition(commentAdapter.getItemCount() - 1));
                }
        );

        btnAttach = view.findViewById(R.id.btnAttach);
        btnAttach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(getActivity(), CommentAttachmentActivity.class);
                intent1.putExtra("attachment_entity_type", "task");
                intent1.putExtra("attachment_entity_id", task_id);
                startActivityForResult(intent1, REQUEST_CODE);
            }
        });
        return view;
    }

    public void updateComment(String task_id, String comments) {
        if (task_id == null || task_id.isEmpty() || comments == null || comments.isEmpty()) {
            return;
        }
//        sendComment.setEnabled(false);
        JSONObject postData = new JSONObject();
        try {
            postData.put("action", "update");
            postData.put("user_task_id", task_id);
            JSONObject commentReq = new JSONObject();
            commentReq.put("comments", comments);
            postData.put("payload", commentReq);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        NetworkManager.customJsonObjectRequest(
                getActivity(), Constants.TASK_LIST_URL, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        sendComment.setEnabled(true);
//                        processResult(response);
                    }

                    @Override
                    public void onError(String message) {
//                        sendComment.setEnabled(true);
                        LogUtils.Print("TASK  COMMENT ERROR", message);
                    }
                }, true);
    }

    private void processResult(JSONObject response) {
        try {
            if (Integer.parseInt(response.getString("result")) == 1) {
                commentText.setText("");
                manipulateComments(response);
                Util.onMessage(getActivity(), getResources().getString(R.string.comment_updates_success));
            } else {
                Util.onMessage(getActivity(), getResources().getString(R.string.comment_updates_fail));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void manipulateComments(JSONObject response) {
        JSONObject dataObj;
        JSONArray commentsArray;
        Comment comment;
        try {
            dataObj = response.getJSONObject("data");
            if (dataObj != null) {
                commentsArray = dataObj.getJSONArray("comments");
                if (commentsArray.length() >= 1) {
                    JSONObject obj = (JSONObject) commentsArray.get(commentsArray.length() - 1);
                    comment = new Comment(obj.getString("comment"), obj.getString("date"),
                            obj.has("url") ? obj.getString("url") : "",
                            obj.has("content_type") ? obj.getString("content_type") : "");
                    commentsData.add(comment);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null && data.hasExtra("DATA")) {
                    Comment comment = (Comment) data.getSerializableExtra("DATA");
                    commentsData.add(comment);
                    commentAdapter.notifyItemInserted(commentsData.size() - 1);
                    if (commentsData.size() > 2) {
                        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                recyclerView.smoothScrollToPosition(commentsData.size() - 1);
                            }
                        }, 200);
                    }
                }
            }
        }
    }
}