package com.gofercrm.user.gofercrm.opportunityclientlead.list.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.implementor.RecyclerViewClickListener;
import com.gofercrm.user.gofercrm.opportunities.Opportunity;
import com.gofercrm.user.gofercrm.opportunityclientlead.add.ManageOpportunityClientActivity;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class OpportunityLeadClientAdapter extends RecyclerView.Adapter<OpportunityLeadClientAdapter.OpportunityListViewHolder> {

    private List<Opportunity> opportunities;
    private Activity activity;

    String edit_pattern = "MMM dd, yyyy, \nhh:mm:ss a";
    SimpleDateFormat edditDateFormat = new SimpleDateFormat(edit_pattern);

    public OpportunityLeadClientAdapter(List<Opportunity> opportunities, Activity activity) {
        setHasStableIds(true);
        this.opportunities = opportunities;
        this.activity = activity;
    }

    @NotNull
    @Override
    public OpportunityListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.content_opportunity_client_lead, parent, false);
        return new OpportunityListViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(OpportunityListViewHolder holder, final int position) {
        final Opportunity _oppor = opportunities.get(position);
        holder.tvName.setText(_oppor.getName());
        holder.tvProperty.setText(_oppor.getPropertyName());
        holder.tvInvoiceAmount.setText("$" + _oppor.getSaleValue());
        if (_oppor.getClosedDate() != null) {
            String date = edditDateFormat.format(new Date(_oppor.getClosedDate() * 1000));
            holder.tvDate.setText(date);
        }
        if (_oppor.getOpenDate() != null) {
            String date = edditDateFormat.format(new Date(_oppor.getOpenDate() * 1000));
            holder.tvCreatedDate.setText(date);
        }
        if (_oppor.getRenewalDate() != null) {
            String date = edditDateFormat.format(new Date(_oppor.getRenewalDate() * 1000));
            holder.tvUpdatedDate.setText(date);
        }
        holder.ivCopy.setVisibility(_oppor.getStage() != null &&
                (_oppor.getStage().equals("closed_lost") || _oppor.getStage().equals("closed_won")) ?
                View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public int getItemCount() {
        return opportunities.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public class OpportunityListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvName, tvProperty, tvInvoiceAmount, tvDate, tvCreatedDate, tvUpdatedDate;
        private ImageView ivEdit, ivCopy;

        public OpportunityListViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.tvName);
            tvProperty = view.findViewById(R.id.tvProperty);
            tvInvoiceAmount = view.findViewById(R.id.tvInvoiceAmount);
            tvDate = view.findViewById(R.id.tvDate);
            tvCreatedDate = view.findViewById(R.id.tvCreatedDate);
            tvUpdatedDate = view.findViewById(R.id.tvUpdatedDate);
            ivEdit = view.findViewById(R.id.ivEdit);
            ivCopy = view.findViewById(R.id.ivCopy);

            ivEdit.setOnClickListener(this);
            ivCopy.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view == ivEdit) {
                Intent leadIntent = new Intent(activity, ManageOpportunityClientActivity.class);
                leadIntent.putExtra("DATA", opportunities.get(getAdapterPosition()));
                leadIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivityForResult(leadIntent, 228);
            } else if (view == ivCopy) {
                if (getAdapterPosition() != RecyclerView.NO_POSITION) {
                    if (listener != null) {
                        listener.onItemClick(1, getAdapterPosition(), view);
                    }
                }
            }
        }
    }

    private static RecyclerViewClickListener listener;

    public static void setOnRecyclerViewItemClickListener(RecyclerViewClickListener clickListener) {
        listener = clickListener;
    }
}