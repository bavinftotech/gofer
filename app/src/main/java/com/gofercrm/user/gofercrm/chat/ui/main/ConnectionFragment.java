package com.gofercrm.user.gofercrm.chat.ui.main;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.LauncherActivity;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.chat.ui.main.model.Chat;
import com.gofercrm.user.gofercrm.chat.video.single.VideoChatViewActivity;
import com.gofercrm.user.gofercrm.implementor.DialogButtonClickListener;
import com.gofercrm.user.gofercrm.util.DialogUtils;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.SharedPreferenceData;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.gofercrm.user.gofercrm.websocket.SharedObserver;
import com.gofercrm.user.gofercrm.websocket.WsManager;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static java.util.Comparator.comparing;
import static java.util.Comparator.nullsLast;
import static java.util.Comparator.reverseOrder;

public class ConnectionFragment extends Fragment implements ChatAdapter.ClickListener {
    private static final String TAG = "ConnectionFragment";
    private static final String ARG_SECTION_NUMBER = "section_number";
    private RecyclerView mRecyclerView;
    private RelativeLayout rlIndicator;
    private RelativeLayout activity_main;
    private ChatAdapter mAdapter;
    private TextView tv_selection;
    private List<Chat> data = new ArrayList<>();
    private List<Chat> listAll = new ArrayList<>();
    private EditText etSearch;
    private ProgressBar progressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        SharedObserver.getSocketObserver().subscribe(s -> {
            processTyping(s);
        });
        super.onCreate(savedInstanceState);
    }

    public static ConnectionFragment newInstance(int sectionNumber) {
        ConnectionFragment fragment = new ConnectionFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ccc, null, false);
        progressBar = view.findViewById(R.id.progressBar);
        tv_selection = view.findViewById(R.id.tv_selection);
        rlIndicator = view.findViewById(R.id.rlIndicator);
        activity_main = view.findViewById(R.id.activity_main);
        View viewIndicator = view.findViewById(R.id.viewIndicator);
        mRecyclerView = view.findViewById(R.id.recyclerView);
        TextInputLayout tlSearch = view.findViewById(R.id.tlSearch);
        tlSearch.setVisibility(View.VISIBLE);
        etSearch = view.findViewById(R.id.etSearch);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mAdapter = new ChatAdapter(getContext(), data, this, 1);
        mRecyclerView.setAdapter(mAdapter);
        getContacts();
        setUpSearchEvent();

        WsManager.setOnUserStatusChangeListener(new UserStatusChangeListener() {
            @Override
            public void onUserStatusChanges(int type, String strId) {
                if (data != null && data.size() > 0) {
                    for (int i = 0; i < data.size(); i++) {
                        if (data.get(i).get_Id().equals(strId)) {
                            data.get(i).setLastChat(type == 1 ? "Available" : "Busy");
                        }
                    }
                }
            }

            @Override
            public void onUpdateHappen() {
                getContacts();
            }
        });

        tlSearch.setEndIconOnClickListener(view1 -> {
            etSearch.setText("");
            Util.hideSoftKeyboard(etSearch);
            searchConnections();
        });
        return view;
    }

    private void setUpSearchEvent() {
        etSearch.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                Util.hideSoftKeyboard(v);
                searchConnections();
                return true;
            }
            return false;
        });
    }

    private void searchConnections() {
        data.clear();
        if (etSearch.getText().toString().trim().isEmpty()) {
            data.addAll(listAll);
            onLoaded();
        } else {
            for (int i = 0; i < listAll.size(); i++) {
                /*if (listAll.get(i).getName().toLowerCase().contains(etSearch.getText().toString().trim().toLowerCase())) {
                    data.add(listAll.get(i));
                }*/
                StringBuilder SrchStr = new StringBuilder();
                if (listAll.get(i).getName() != null) {
                    SrchStr.append(" ").append(listAll.get(i).getName());
                }
                if (listAll.get(i).getShortName() != null) {
                    SrchStr.append(" ").append(listAll.get(i).getShortName());
                }
                if (listAll.get(i).getVanityName() != null) {
                    SrchStr.append(" ").append(listAll.get(i).getVanityName());
                }
                if (listAll.get(i).getEmail() != null) {
                    SrchStr.append(" ").append(listAll.get(i).getEmail());
                }
                if (SrchStr.toString().trim().toLowerCase().contains(etSearch.getText().toString().trim().toLowerCase())) {
                    //if (listAll.get(i).getName().toLowerCase().contains(etSearch.getText().toString().trim().toLowerCase())))

                    {
                        data.add(listAll.get(i));
                    }
                }
            }
            onLoaded();
        }
    }

    private void onLoaded() {
        data.sort(comparing(
                Chat::getTime,
                nullsLast(reverseOrder())
        ));
        mAdapter = new ChatAdapter(getContext(), data, this, 1);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClicked(int position) {
        navigateToConversation(position);
    }

    private void navigateToConversation(int position) {
        if (data.get(position).getUnReadCount() > 0) {
            data.get(position).setUnReadCount(0);
            mAdapter.notifyItemChanged(position);
        }
        Intent intent = new Intent(getContext(), Conversation.class);
        String id = data.get(position).get_Id();
        if (ChatPagerAdapter.intent != null && ChatPagerAdapter.intent.getBooleanExtra(LauncherActivity.IS_FROM_SHARE, false)) {
            intent.putExtra(LauncherActivity.IS_FROM_SHARE, ChatPagerAdapter.intent.getBooleanExtra(LauncherActivity.IS_FROM_SHARE, false));
            intent.putExtra(LauncherActivity.SHARE_URI, ChatPagerAdapter.intent.getStringExtra(LauncherActivity.SHARE_URI));
        }
        intent.putExtra("_ID", (Serializable) id);
        intent.putExtra("_TYPE", (Serializable) 1);
        intent.putExtra("_STATUS", data.get(position).getLastChat());
        intent.putExtra("_NAME", data.get(position).getName());
        intent.putExtra("_IMAGE", data.get(position).getImage());
        intent.putExtra("_TIMEZONE", data.get(position).getTimeZone());
        if (data.get(position).getTranslateLanguage() != null)
            intent.putExtra("_LANG", data.get(position).getTranslateLanguage());
        if (data.get(position).getMuteNotification() != 0L)
            intent.putExtra("_MUTE_NOT", data.get(position).getMuteNotification());
        if (data.get(position).getTime() != null &&
                data.get(position).getTime() != 0L)
            intent.putExtra("_LAST_SEEN", data.get(position).getTime());
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_ACTIVITY_NEW_TASK);
        getContext().startActivity(intent);
        ChatPagerAdapter.intent = null;
    }

    @Override
    public void onItemClicked(int position, int type) {
        switch (type) {
            case 1:
                //accept
                openConfirmationDialog(getResources().getString(R.string.allow_request_confirmation), 1, position);
                break;
            case 0:
                //reject, withdraw, remove
                switch (data.get(position).getFriend()) {
                    case 1://Existing Friend
                        openConfirmationDialog(getResources().getString(R.string.disconnect_request_confirmation)
                                + " " + data.get(position).getName(), 0, position);
                        break;
                    case 2://Waiting Response on your connection request
                        openConfirmationDialog(getResources().getString(R.string.withdraw_request_confirmation), 0, position);
                        break;
                    case 3://Accept or Reject Incoming Request
                        openConfirmationDialog(getResources().getString(R.string.reject_request_confirmation), 0, position);
                        break;
                }
                break;
            case 2:
                //call
                if (data.get(position).getPhone() != null &&
                        !data.get(position).getPhone().equals("")) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + data.get(position).getPhone()));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
                    Snackbar.make(activity_main,
                            getResources().getString(R.string.no_phone_is_associated_connection),
                            Snackbar.LENGTH_LONG).setAction(
                            getResources().getString(R.string.action), null).show();
                }
                break;
            case 3:
                //message
                if (data.get(position).getPhone() != null &&
                        !data.get(position).getPhone().equals("")) {
                    try {
                        Intent smsIntent = new Intent(android.content.Intent.ACTION_VIEW);
                        smsIntent.setType("vnd.android-dir/mms-sms");
                        smsIntent.putExtra("address", data.get(position).getPhone());
                        smsIntent.putExtra("sms_body", "");
                        startActivity(smsIntent);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Snackbar.make(activity_main, getResources().getString(R.string.no_phone_is_associated_lead), Snackbar.LENGTH_LONG)
                                .setAction(getResources().getString(R.string.action), null).show();
                    }
                } else {
                    Snackbar.make(activity_main,
                            getResources().getString(R.string.no_phone_is_associated_connection),
                            Snackbar.LENGTH_LONG).setAction(
                            getResources().getString(R.string.action), null).show();
                }
                break;
            case 4:
                //video
                if (!data.get(position).getLastChat().equals("Available")) {
                    Util.onMessage(getActivity(), data.get(position).getName() + " " +
                            getResources().getString(R.string.is_not_available));
                    return;
                }
                isUserEligibleForVideoCall(position);
                break;
        }
    }

    private void isUserEligibleForVideoCall(int position) {
        String url = Constants.VIDEO_ELIGIBILITY;
        JSONObject postData = new JSONObject();
        try {
            postData.put("userid", SharedPreferenceData.getInstance(getActivity()).getLoggedInUserID());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        NetworkManager.customJsonObjectRequest(
                getActivity(), url, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Intent intent = new Intent(getActivity(), VideoChatViewActivity.class);
                        intent.putExtra("_NAME", data.get(position).getName());
                        intent.putExtra("_USER_ID", SharedPreferenceData.getInstance(getActivity()).getLoggedInUserID());
                        intent.putExtra("_LOGIN_USER_NAME", data.get(position).getName());
                        intent.putExtra("_OPO_USER_ID", data.get(position).get_Id());
                        intent.putExtra("_CHANNEL_NAME", SharedPreferenceData.getInstance(getActivity()).getLoggedInUserID() + data.get(position).get_Id());
                        intent.putExtra("_FROM", 1);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }

                    @Override
                    public void onError(String message) {
                        Util.onMessage(getActivity(), message);
                        LogUtils.Print("ERRROR=", message);
                    }
                }, true);
    }

    private void openConfirmationDialog(String strMsg, int type, int position) {
        DialogUtils.showConfirmationDialog(getActivity(),
                new DialogButtonClickListener() {
                    @Override
                    public void onPositiveButtonClick() {
                        removeAddUserFromList(type, position);
                    }

                    @Override
                    public void onNegativeButtonClick() {

                    }
                }, strMsg);
    }

    /**
     * @param type     1 add, 0 - remove
     * @param position
     */
    private void removeAddUserFromList(int type, int position) {
        NetworkManager.customJsonObjectRequest(getActivity(),
                ((type == 1) ? Constants.ADD_USER : Constants.REMOVE_USER) +
                        "?userid=" + data.get(position).get_Id(), null,
                new VolleyResponseListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtils.Print("RESPONSE", response.toString());
                        try {
                            if (response.has("result") && Integer.parseInt(response.getString("result")) == 1) {
                                if (type == 1) {
                                    //add in list
                                    data.get(position).setFriend(1);
                                    if (listAll.contains(data.get(position))) {
                                        listAll.get(listAll.indexOf(data.get(position))).setFriend(1);
                                    }
                                    mAdapter.notifyDataSetChanged();
                                } else {
                                    //remove from list
                                    if (data.size() > position) {
                                        if (listAll.contains(data.get(position))) {
                                            listAll.remove(position);
                                        }
                                        data.remove(position);
                                        mAdapter.notifyDataSetChanged();
                                    }
                                }
                            } else {
                                Util.onMessage(getActivity(), getResources().getString(R.string.server_err));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Util.onMessage(getActivity(), getResources().getString(R.string.server_err));
                        }
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("UPDATE PROFILE ERROR", message);
                    }
                }, false);
    }

    @Override
    public boolean onItemLongClicked(int position) {
//        toggleSelection(position);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    private void toggleSelection(int position) {
        mAdapter.toggleSelection(position);
        if (mAdapter.getSelectedItemCount() > 0) {
            tv_selection.setVisibility(View.VISIBLE);
        } else
            tv_selection.setVisibility(View.GONE);

        getActivity().runOnUiThread(() -> tv_selection.setText(
                getResources().getString(R.string.delete) +
                        " (" + mAdapter.getSelectedItemCount() + ")"));
    }

    public void getContacts() {
        progressBar.setVisibility(View.VISIBLE);
        NetworkManager.customJsonObjectGetRequest(
                getContext(), Constants.FRIENDS_FETCH_URL + "?contact_type_filter=person", null,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressBar.setVisibility(View.GONE);
                        processData(response);
                    }

                    @Override
                    public void onError(String message) {
                        progressBar.setVisibility(View.GONE);
                        LogUtils.Print("ERRROR=", message);
                    }
                }, true);
    }

    private void processData(JSONObject response) {
        //LogUtils.Print(TAG, "response Connection -->" + response);
        try {
            if (Integer.parseInt(response.getString("result")) == 1) {
                data = new ArrayList<>();
                listAll.clear();
                String name = "";
                String short_name = "";
                String vanity_name = "";
                JSONArray users = response.getJSONArray("users");
                JSONObject result_obj;
                for (int i = 0; i < users.length(); i++) {
                    result_obj = users.getJSONObject(i);
                    if (result_obj.has("contact_type")
                            && result_obj.getString("contact_type").equals("person")) {

                        Chat chat = new Chat();
                        //chat.setLastChat(result_obj.getString("status"));
                        if (result_obj.has("status") && result_obj.getString("status") != null) {
                            chat.setLastChat(result_obj.getString("status"));
                        } else {
                            chat.setLastChat("Busy");
                        }
                        if (result_obj.has("avatar_url") && result_obj.getString("avatar_url") != null) {
                            chat.setImage(result_obj.getString("avatar_url"));
                        } else {
                            chat.setImage("room_image");
                        }
                        if (result_obj.has("id")) {
                            chat.set_Id(result_obj.getString("id"));
                        }

                        if (result_obj.has("friend")) {
                            chat.setFriend(result_obj.getInt("friend"));
                        }
                        if (result_obj.has("phone")) {
                            chat.setPhone(result_obj.getString("phone"));
                        }
                        if (result_obj.has("email")) {
                            chat.setEmail(result_obj.getString("email"));
                        }
                        if (result_obj.has("last_seen")) {
                            chat.setmTime(result_obj.getLong("last_seen"));
                        }

                        if (result_obj.has("first_name")) {
                            name = result_obj.getString("first_name");
                            chat.setName(name);
                        }
                        if (result_obj.has("last_name")) {
                            name = name + " " + result_obj.getString("last_name");
                            chat.setName(name);
                        }
                        if (result_obj.has("name")) {
                            short_name = result_obj.getString("name");
                            chat.setShortName(short_name);
                        }
                        if (result_obj.has("vanity_name")) {
                            vanity_name = result_obj.getString("vanity_name");
                            chat.setVanityName(vanity_name);
                        }

                        if (result_obj.has("is_active")) {
                            chat.setOnline(result_obj.getBoolean("is_active"));
                        }

                        if (result_obj.has("message_stats_dict")) {
                            JSONObject jsonObject = result_obj.getJSONObject("message_stats_dict");
                            if (jsonObject.has("incoming_unread_count")) {
                                chat.setUnReadCount(jsonObject.getInt("incoming_unread_count"));
                            }
                        }
                        if (result_obj.has("translation_settings_dict")) {
                            JSONObject jsonObject = result_obj.getJSONObject("translation_settings_dict");
                            if (jsonObject.has(chat.get_Id())) {
                                JSONArray jsonArray = jsonObject.getJSONArray(chat.get_Id());
                                if (jsonArray.length() > 1) {
                                    chat.setTranslateLanguage(jsonArray.get(1).toString());
                                }
                            }
                        }
                        if (result_obj.has("mute_chat_notification_dict")) {
                            JSONObject jsonObject = result_obj.getJSONObject("mute_chat_notification_dict");
                            if (jsonObject.has(chat.get_Id())) {
                                chat.setMuteNotification(jsonObject.getLong(chat.get_Id()));
                            }
                        }
                        if (result_obj.has("timeZone")) {
                            chat.setTimeZone(result_obj.getString("timeZone"));
                        }

                        data.add(chat);
                    }
                }
                onLoaded();
                listAll.addAll(data);
                if (data.size() == 0) {
                    openIndicatorView();
                } else {
                    rlIndicator.setVisibility(View.GONE);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void openIndicatorView() {
        rlIndicator.setOnClickListener(v -> rlIndicator.setVisibility(View.GONE));
        rlIndicator.setVisibility(View.VISIBLE);
    }

    private void processTyping(String response) {
        try {
            JSONObject input = new JSONObject(response);
            if (input.has("type") && input.getString("type").equals("contact_logout")) {
                String user_id = input.has("user_id") ? input.getString("user_id") : "";
                for (Chat user : data) {
                    if (user.get_Id().equals(user_id)) {
                        user.setOnline(false);
                        /*if (getActivity() != null)
                            getActivity().runOnUiThread(() -> {
                                onLoaded();
                            });*/
                        break;
                    }
                }
            } else if (input.has("type") && input.getString("type").equals("contact_login")) {
                String user_id = input.has("user_id") ? input.getString("user_id") : "";
                for (Chat user : data) {
                    if (user.get_Id().equals(user_id)) {
                        user.setOnline(true);
                        if (getActivity() != null)
                            getActivity().runOnUiThread(() -> {
                                onLoaded();
                            });
                        break;
                    }
                }
            } else if (input.has("type") && input.getString("type").equals("message")) {
                String user_id = input.has("sender_id") ? input.getString("sender_id") : "";
                for (Chat user : data) {
                    if (user.get_Id().equals(user_id)) {
                        user.setMsg_count(1);
                        if (getActivity() != null)
                            getActivity().runOnUiThread(() -> {
                                onLoaded();
                            });
                        break;
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}