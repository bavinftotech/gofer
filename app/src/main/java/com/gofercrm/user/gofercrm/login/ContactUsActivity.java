package com.gofercrm.user.gofercrm.login;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.util.ValidationUtil;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

public class ContactUsActivity extends BaseActivity implements View.OnClickListener {

    private ImageView ivClose;
    private EditText etName, etEmail, etPhone, etMessage;
    private TextInputLayout tlName, tlEmail, tlPhone, tlMessage;
    private Button btnSendMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_contact_us);
        findViewById();
    }

    private void findViewById() {
        ivClose = findViewById(R.id.ivClose);
        ivClose.setOnClickListener(this);
        etName = findViewById(R.id.etName);
        etEmail = findViewById(R.id.etEmail);
        etPhone = findViewById(R.id.etPhone);
        etMessage = findViewById(R.id.etMessage);
        tlName = findViewById(R.id.tlName);
        tlEmail = findViewById(R.id.tlEmail);
        tlPhone = findViewById(R.id.tlPhone);
        tlMessage = findViewById(R.id.tlMessage);
        btnSendMessage = findViewById(R.id.btnSendMessage);
        btnSendMessage.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == ivClose) {
            onBackPressed();
        } else if (view == btnSendMessage) {
            if (isValidInput()) {
                submitContactUs();
            }
        }
    }

    private void submitContactUs() {
        JSONObject postData = new JSONObject();
        try {
            postData.put("email", etEmail.getText().toString().trim());
            postData.put("phone", etPhone.getText().toString().trim());
            postData.put("message", etMessage.getText().toString().trim());
            postData.put("name", etName.getText().toString().trim());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        LogUtils.Print("submitContactUs", "" + postData);
        showProgress();
        NetworkManager.customJsonObjectRequest(
                this, Constants.CONTACT_US, postData,
                new VolleyResponseListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //LogUtils.Print("onResponse", "" + response);
                        Util.onMessage(ContactUsActivity.this, getResources().getString(R.string.contact_us_add));
                        hideProgress();
                        onBackPressed();
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print("onError", "" + message);
                        hideProgress();
                    }
                }, true);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Util.hideSoftKeyboard(etEmail);
        Util.hideSoftKeyboard(etMessage);
        Util.hideSoftKeyboard(etName);
        Util.hideSoftKeyboard(etPhone);
    }

    /**
     * CHECK VALIDATION
     *
     * @return
     */
    private boolean isValidInput() {
        if (ValidationUtil.checkEmptyView(etName, tlName,
                getResources().getString(R.string.name_empty_err_))) {
            etName.requestFocus();
            return false;
        } else if (ValidationUtil.checkEmailView(etEmail, tlEmail,
                getResources().getString(R.string.email_empty_err),
                getResources().getString(R.string.email_invalid))) {
            etEmail.requestFocus();
            return false;
        } else {
            return true;
        }
    }
}