package com.gofercrm.user.gofercrm.chat.ui.main.model;

import java.io.Serializable;

public class Member implements Serializable {

    private String _Id;
    private String mName;
    private String email;
    private String mImage;
    private int role;
    private boolean mute_notifications;

    public String get_Id() {
        return _Id;
    }

    public void set_Id(String _Id) {
        this._Id = _Id;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmImage() {
        return mImage;
    }

    public void setmImage(String mImage) {
        this.mImage = mImage;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public boolean isMute_notifications() {
        return mute_notifications;
    }

    public void setMute_notifications(boolean mute_notifications) {
        this.mute_notifications = mute_notifications;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}