package com.gofercrm.user.gofercrm.chat.ui.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.chat.ui.main.model.Chat;
import com.gofercrm.user.gofercrm.util.Util;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class ChatAdapter extends SelectableAdapter<ChatAdapter.ViewHolder> {
    private List<Chat> mArrayList;
    private Context mContext;
    private ClickListener clickListener;
    private int from;

    public ChatAdapter(Context context, List<Chat> arrayList, ClickListener clickListener, int from) {
        setHasStableIds(true);
        this.mArrayList = arrayList;
        this.mContext = context;
        this.clickListener = clickListener;
        this.from = from;
    }

    // Create new views
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.list_item_chat, null);
        return new ViewHolder(itemLayoutView, clickListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        Chat chat = mArrayList.get(position);
        viewHolder.tvName.setText(mArrayList.get(position).getName());
        if (isSelected(position)) {
            viewHolder.checked.setChecked(true);
            viewHolder.checked.setVisibility(View.VISIBLE);
        } else {
            viewHolder.checked.setChecked(false);
            viewHolder.checked.setVisibility(View.GONE);
        }
        if (mArrayList.get(position).getTime() != null &&
                mArrayList.get(position).getTime() != 0L) {
            viewHolder.tvTime.setText(Util.timeAgo(mArrayList.get(position).getTime()));
            viewHolder.tvTime.setVisibility(View.VISIBLE);
        } else {
            viewHolder.tvTime.setVisibility(View.GONE);
        }
        if (chat.getImage() != null && chat.getImage().equals("room_image")) {
            if (chat.getName() != null && !mArrayList.get(position).getName().isEmpty()) {
                viewHolder.tvInitial.setText(String.valueOf(chat.getName().charAt(0)));
                viewHolder.tvInitial.setVisibility(View.VISIBLE);
                viewHolder.userPhoto.setVisibility(View.GONE);
            }
        } else if (chat.getImage() != null && !chat.getImage().isEmpty()) {
            viewHolder.tvInitial.setVisibility(View.GONE);
            viewHolder.userPhoto.setVisibility(View.VISIBLE);
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .circleCropTransform()
                    .placeholder(R.drawable.ic_account_circle)
                    .error(R.drawable.ic_account_circle);
            Glide.with(mContext).load(mArrayList.get(position).getImage()).apply(options).into(viewHolder.userPhoto);
        }

        if (mArrayList.get(position).getOnline()) {
            viewHolder.onlineView.setVisibility(View.VISIBLE);
        } else {
            viewHolder.onlineView.setVisibility(View.INVISIBLE);
        }
        if (mArrayList.get(position).getMsg_count() > 0) {
            viewHolder.unread_message_count.setVisibility(View.VISIBLE);
            viewHolder.unread_message_count.setText("1");
        }

        switch (mArrayList.get(position).getFriend()) {
            case 1://Existing Friend
                viewHolder.tvAccept.setVisibility(View.INVISIBLE);
                viewHolder.icRemoveConnection.setVisibility(View.VISIBLE);
//                viewHolder.tvRemove.setText(mContext.getResources().getString(R.string.remove));

                viewHolder.tvRemove.setVisibility(View.GONE);
                viewHolder.ivAccept.setVisibility(View.GONE);
                viewHolder.ivRemove.setVisibility(View.GONE);
                if (from == 1)
                    viewHolder.llButtons.setVisibility(View.VISIBLE);
                break;
            case 2://Waiting Response on your connection request
                viewHolder.tvAccept.setVisibility(View.INVISIBLE);
                viewHolder.tvRemove.setVisibility(View.VISIBLE);
                viewHolder.tvRemove.setText(mContext.getResources().getString(R.string.withdraw));

                viewHolder.icRemoveConnection.setVisibility(View.GONE);
                viewHolder.ivAccept.setVisibility(View.GONE);
                viewHolder.ivRemove.setVisibility(View.GONE);
                if (from == 1)
                    viewHolder.llButtons.setVisibility(View.GONE);
                break;
            case 3://Accept or Reject Incoming Request
                viewHolder.tvAccept.setVisibility(View.INVISIBLE);
                viewHolder.tvRemove.setVisibility(View.GONE);
                viewHolder.tvAccept.setText(mContext.getResources().getString(R.string.accept));
                viewHolder.tvRemove.setText(mContext.getResources().getString(R.string.reject));

                viewHolder.icRemoveConnection.setVisibility(View.GONE);
                viewHolder.ivAccept.setVisibility(View.VISIBLE);
                viewHolder.ivRemove.setVisibility(View.VISIBLE);
                if (from == 1)
                    viewHolder.llButtons.setVisibility(View.GONE);
                break;
            default:
                viewHolder.ivAccept.setVisibility(View.GONE);
                viewHolder.ivRemove.setVisibility(View.GONE);
                viewHolder.tvAccept.setVisibility(View.INVISIBLE);
                viewHolder.tvRemove.setVisibility(View.GONE);
                viewHolder.icRemoveConnection.setVisibility(View.GONE);
                if (from == 1)
                    viewHolder.llButtons.setVisibility(View.GONE);
                break;
        }

        if (from == 1) {
            viewHolder.tvEmail.setText(mArrayList.get(position).getEmail() != null ?
                    mArrayList.get(position).getEmail() : "");
            if (mArrayList.get(position).getUnReadCount() > 0) {
                viewHolder.tvUnReadyCnt.setText("" + mArrayList.get(position).getUnReadCount());
                viewHolder.tvUnReadyCnt.setVisibility(View.VISIBLE);
            } else {
                viewHolder.tvUnReadyCnt.setVisibility(View.GONE);
            }
        } else {
            viewHolder.tvEmail.setText("");
            viewHolder.tvUnReadyCnt.setVisibility(View.GONE);
        }

        if (from == 2 && mArrayList.get(position).getRoom_type() != null) {
            switch (mArrayList.get(position).getRoom_type()) {
                case "client":
                    viewHolder.ivRoomType.setImageResource(R.drawable.user);
                    break;
                case "regular":
                    viewHolder.ivRoomType.setImageResource(R.drawable.cinema);
                    break;
                case "team":
                    viewHolder.ivRoomType.setImageResource(R.drawable.group);
                    break;
            }
        }

        viewHolder.tvLastChat.setText(mArrayList.get(position).getLastChat().equals("Available") ?
                mContext.getResources().getString(R.string.available) :
                mContext.getResources().getString(R.string.busy));
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        public LinearLayout llButtons;
        public ImageView ivAccept;
        public ImageView ivRemove;
        public ImageView icRemoveConnection;
        public ImageView ivCall, ivMessage, ivVideo;
        public TextView tvAccept;
        public TextView tvEmail;
        public TextView tvUnReadyCnt;
        public TextView tvRemove;
        public TextView tvName;
        public TextView tvTime;
        public TextView tvLastChat;
        public ImageView userPhoto;
        public ImageView ivRoomType;
        public TextView tvInitial;
        public boolean online = false;
        private final View onlineView;
        public CheckBox checked;
        private ClickListener listener;
        private TextView unread_message_count;
        //private final View selectedOverlay;

        public ViewHolder(View itemLayoutView, ClickListener listener) {
            super(itemLayoutView);
            this.listener = listener;
            llButtons = itemLayoutView.findViewById(R.id.llButtons);
            ivCall = itemLayoutView.findViewById(R.id.ivCall);
            ivMessage = itemLayoutView.findViewById(R.id.ivMessage);
            ivVideo = itemLayoutView.findViewById(R.id.ivVideo);
            tvEmail = itemLayoutView.findViewById(R.id.tvEmail);
            tvUnReadyCnt = itemLayoutView.findViewById(R.id.tvUnReadyCnt);
            ivCall.setOnClickListener(this);
            ivMessage.setOnClickListener(this);
            ivVideo.setOnClickListener(this);
            tvEmail.setOnLongClickListener(this);
            if (from == 1) {
                llButtons.setVisibility(View.VISIBLE);
                tvEmail.setVisibility(View.VISIBLE);
                tvUnReadyCnt.setVisibility(View.VISIBLE);
            } else {
                llButtons.setVisibility(View.GONE);
                tvEmail.setVisibility(View.GONE);
                tvUnReadyCnt.setVisibility(View.GONE);
            }
            ivRoomType = itemLayoutView.findViewById(R.id.ivRoomType);
            if (from == 2) {
                ivRoomType.setVisibility(View.VISIBLE);
            } else {
                ivRoomType.setVisibility(View.GONE);
            }

            ivAccept = itemLayoutView.findViewById(R.id.ivAccept);
            ivRemove = itemLayoutView.findViewById(R.id.ivRemove);
            icRemoveConnection = itemLayoutView.findViewById(R.id.icRemoveConnection);

            tvAccept = itemLayoutView.findViewById(R.id.tvAccept);
            tvRemove = itemLayoutView.findViewById(R.id.tvRemove);
            tvName = itemLayoutView.findViewById(R.id.tv_user_name);
            tvInitial = itemLayoutView.findViewById(R.id.user_Initial);
            //selectedOverlay = (View) itemView.findViewById(R.id.selected_overlay);
            tvTime = itemLayoutView.findViewById(R.id.tv_time);
            tvLastChat = itemLayoutView.findViewById(R.id.tv_last_chat);
            userPhoto = itemLayoutView.findViewById(R.id.iv_user_photo);
            onlineView = itemLayoutView.findViewById(R.id.online_indicator);
            checked = itemLayoutView.findViewById(R.id.chk_list);
            unread_message_count = itemLayoutView.findViewById(R.id.unread_message_count);
            itemLayoutView.setOnClickListener(this);
            ivAccept.setOnClickListener(this);
            tvAccept.setOnClickListener(this);
            tvRemove.setOnClickListener(this);
            ivRemove.setOnClickListener(this);
            icRemoveConnection.setOnClickListener(this);
//            itemLayoutView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v == tvAccept || v == ivAccept) {
                listener.onItemClicked(getAdapterPosition(), 1);
            } else if (v == tvRemove || v == ivRemove || v == icRemoveConnection) {
                listener.onItemClicked(getAdapterPosition(), 0);
            } else if (v == ivCall) {
                listener.onItemClicked(getAdapterPosition(), 2);
            } else if (v == ivMessage) {
                listener.onItemClicked(getAdapterPosition(), 3);
            } else if (v == ivVideo) {
                listener.onItemClicked(getAdapterPosition(), 4);
            } else if (listener != null) {
                listener.onItemClicked(getAdapterPosition());
            }
        }

        @Override
        public boolean onLongClick(View view) {
            if (view == tvEmail) {
                Util.copyDataOnClipboard(mContext, mArrayList.get(getAdapterPosition()).getEmail());
                Util.onMessage(mContext, mContext.getResources().getString(R.string.email_copied));
            } else {
                if (listener != null) {
                    return listener.onItemLongClicked(getAdapterPosition());
                }
            }
            return false;
        }
    }

    public interface ClickListener {
        void onItemClicked(int position);

        boolean onItemLongClicked(int position);

        default void onItemClicked(int position, int type) {
        }

        boolean onCreateOptionsMenu(Menu menu);
    }
}