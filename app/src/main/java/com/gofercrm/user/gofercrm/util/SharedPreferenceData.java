package com.gofercrm.user.gofercrm.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class SharedPreferenceData {

    private static Context mCtx;
    private static SharedPreferenceData mInstance;
    public static SharedPreferences loginSharedPref;
    private SharedPreferences.Editor editor;

    private SharedPreferenceData(Context context) {
        mCtx = context;
        loginSharedPref = context.getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
        editor = loginSharedPref.edit();
    }

    public static synchronized SharedPreferenceData getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPreferenceData(context);
        }
        return mInstance;
    }

    public String getString(String key) {
        return loginSharedPref.getString(key, "");
    }

    public void putString(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public boolean getBoolean(String key) {
        return loginSharedPref.getBoolean(key, false);
    }

    public void putBoolean(String key, boolean value) {
        editor.putBoolean(key, value);
        editor.commit();
    }

    public String getLoggedInUserID() {
        return loginSharedPref.getString("USER_ID", "");
    }

    public String getClientConnectionID() {
        return loginSharedPref.getString("CLIENT_CONNECTION_ID", "");
    }

    public String getPrefObj() {
        return loginSharedPref.getString("PERSONAL_DICT", "");
    }

    public String getEventData() {
        return loginSharedPref.getString("EVENT_DATA", "");
    }

    public String getUserName() {
        return loginSharedPref.getString("USER_NAME", "");
    }

    //public int getCurrentAccountLevel() {
    //    return loginSharedPref.getInt("ACCOUNT_LEVEL", 0);
    //}

    public boolean isFullAccessAccount() {
        return (loginSharedPref.getBoolean("ACCESS_LEAD", false) &&
                loginSharedPref.getBoolean("ACCESS_CONTACT", false) &&
                loginSharedPref.getBoolean("ACCESS_TASK", false) &&
                loginSharedPref.getBoolean("ACCESS_ACCOUNT", false) &&
                loginSharedPref.getBoolean("ACCESS_OPPORTUNITY", false));
    }

    public boolean isLeadContactAccessAvailable() {
        return (loginSharedPref.getBoolean("ACCESS_LEAD", false) &&
                loginSharedPref.getBoolean("ACCESS_CONTACT", false));
    }

    public String getProviderId() {
        return loginSharedPref.getString("PROVIDERID", "");
    }

    public String getProvider() {
        return loginSharedPref.getString("PROVIDER", "");
    }

    public boolean isUserActive() {
        return loginSharedPref.getString("STATUS", "").equals("Available");
    }

    public void setFCMToken(String token) {
        editor.putString("FCM_TOKEN", token);
        editor.commit();
    }

    public String getFCMToken() {
        return loginSharedPref.getString("FCM_TOKEN", "");
    }

    public boolean isTenantAdmin() {
        return loginSharedPref.getBoolean("IS_TENANT_ADMIN", false);
    }

    public void setIsUserActive(String status) {
        editor.putString("STATUS", status);
        editor.commit();
    }

    public void setEventData(String data) {
        editor.putString("EVENT_DATA", data);
        editor.commit();
    }

//    public void setList(int from, List<DataScheduleNotification> list) {
//        Gson gson = new Gson();
//        String json = gson.toJson(list);
//        editor.putString(from == Constants.fromPottyNotification ?
//                Constants.pottyNotification : Constants.scheduleNotification, json);
//        editor.commit();
//    }
//
//    public List<DataScheduleNotification> getList(int from) {
//        List<DataScheduleNotification> arrayItems = new ArrayList<>();
//        String serializedObject = sharedPreferences.getString(from == Constants.fromPottyNotification ?
//                Constants.pottyNotification : Constants.scheduleNotification, null);
//        if (serializedObject != null) {
//            Gson gson = new Gson();
//            Type type = new TypeToken<List<DataScheduleNotification>>() {
//            }.getType();
//            arrayItems = gson.fromJson(serializedObject, type);
//        }
//        return arrayItems;
//    }
}