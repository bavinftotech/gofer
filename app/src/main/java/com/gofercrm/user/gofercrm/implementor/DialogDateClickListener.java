package com.gofercrm.user.gofercrm.implementor;

public interface DialogDateClickListener {
    void onDateClick(String date);
    void onCancelClick();
}
