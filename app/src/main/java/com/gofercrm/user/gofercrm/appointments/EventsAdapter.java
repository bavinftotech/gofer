package com.gofercrm.user.gofercrm.appointments;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.util.Util;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.EventViewHolder> {

    private List<JSONObject> events;
    private Context ctx;
    private boolean isSummary;

    public EventsAdapter(List<JSONObject> events, Context ctx, boolean isSummary) {
        this.events = events;
        this.ctx = ctx;
        this.isSummary = isSummary;
    }

    @NotNull
    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.events_list, parent, false);
        return new EventViewHolder(itemView);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(@NotNull EventsAdapter.EventViewHolder holder, final int position) {
        final JSONObject event = events.get(position);
        try {
            if (event.has("summary") && !event.getString("summary").isEmpty()) {
                holder.summary.setVisibility(View.VISIBLE);
                holder.summary_value.setText(event.getString("summary"));
            } else {
                holder.summary.setVisibility(View.GONE);
            }

            if (event.has("start") && !event.getString("start").isEmpty()) {
                holder.time_l.setVisibility(View.VISIBLE);
//                holder.time_value.setText(event.getString("start") + " to " + event.getString("end"));
                String date_txt = Util.zFormatStringToDate(event.getString("start")) + " to " + Util.zFormatStringToDate(event.getString("end"));
                holder.time_value.setText(date_txt);
            } else {
                holder.time_l.setVisibility(View.GONE);
            }
            if (event.has("location") && !event.getString("location").isEmpty() && !event.getString("location").equals("undefined")) {
                holder.location.setVisibility(View.VISIBLE);
                holder.location_value.setText(event.getString("location"));
                Linkify.addLinks(holder.location_value, Linkify.ALL);
            } else {
                holder.location.setVisibility(View.GONE);
            }
            if (!isSummary) {
                if (event.has("description") && !event.getString("description").isEmpty() && event.getString("description") != "undefined") {
                    holder.desc.setVisibility(View.VISIBLE);
                    holder.desc_value.setText(event.getString("description"));
                    Linkify.addLinks(holder.desc_value, Linkify.ALL);
                } else {
                    holder.desc.setVisibility(View.GONE);
                }

                if (event.has("htmlLink") && !event.getString("htmlLink").isEmpty() && event.getString("htmlLink") != "undefined") {
                    holder.html_link.setVisibility(View.VISIBLE);
                    holder.html_link_value.setText(event.getString("htmlLink"));
                    Linkify.addLinks(holder.html_link_value, Linkify.ALL);
                } else {
                    holder.html_link.setVisibility(View.GONE);
                }

                if (event.has("hangoutLink") && !event.getString("hangoutLink").isEmpty() && event.getString("hangoutLink") != "undefined") {
                    holder.hangout.setVisibility(View.VISIBLE);
                    holder.hangout_value.setText(event.getString("hangoutLink"));
                    Linkify.addLinks(holder.hangout_value, Linkify.ALL);
                } else {
                    holder.hangout.setVisibility(View.GONE);
                }

                if (event.has("attendees")) {
                    String emails = "";
                    JSONArray attendees_list = event.getJSONArray("attendees");
                    for (int i = 0; i < attendees_list.length(); i++) {
                        if (attendees_list.getJSONObject(i).has("email")) {
                            emails = emails + "," + attendees_list.getJSONObject(i).getString("email");
                        }
                    }

                    holder.attendess.setVisibility(View.VISIBLE);
                    holder.attendees_value.setText(emails);
                    Linkify.addLinks(holder.attendees_value, Linkify.ALL);
                } else {
                    holder.attendess.setVisibility(View.GONE);
                }
            }
            holder.itemView.setOnClickListener(view -> {
                Intent intent = new Intent(ctx, NewEventActivity.class);
                intent.putExtra("EVENT_OBJ", event.toString());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ctx.startActivity(intent);
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    public class EventViewHolder extends RecyclerView.ViewHolder {
        public TextView summary_value,
                time_value,
                location_value,
                hangout_value,
                html_link_value,
                desc_value,
                attendees_value;

        public LinearLayout summary, time_l, desc, location, hangout, html_link, attendess;

        public EventViewHolder(View view) {
            super(view);
            //Layouts
            summary = view.findViewById(R.id.event_summary);
            time_l = view.findViewById(R.id.event_time);
            desc = view.findViewById(R.id.event_desc);
            location = view.findViewById(R.id.event_location);
            hangout = view.findViewById(R.id.event_hangout);
            html_link = view.findViewById(R.id.event_htmlLink);
            attendess = view.findViewById(R.id.event_attendees);

            //Elements
            summary_value = view.findViewById(R.id.event_summary_value_id);
            time_value = view.findViewById(R.id.event_time_value_id);
            hangout_value = view.findViewById(R.id.event_hangout_value_id);
            html_link_value = view.findViewById(R.id.event_htmlLink_value_id);
            desc_value = view.findViewById(R.id.event_desc_value_id);
            location_value = view.findViewById(R.id.event_location_value_id);
            attendees_value = view.findViewById(R.id.event_attendess_value_id);
        }
    }
}