package com.gofercrm.user.gofercrm.task;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gofercrm.user.gofercrm.Constants;
import com.gofercrm.user.gofercrm.R;
import com.gofercrm.user.gofercrm.appointments.Contact;
import com.gofercrm.user.gofercrm.databinding.ActivityTaskUserFilterBinding;
import com.gofercrm.user.gofercrm.implementor.RecyclerViewClickListener;
import com.gofercrm.user.gofercrm.BaseActivity;
import com.gofercrm.user.gofercrm.task.adapter.TaskUserFilterAdapter;
import com.gofercrm.user.gofercrm.util.LogUtils;
import com.gofercrm.user.gofercrm.util.Util;
import com.gofercrm.user.gofercrm.volleynetworkutil.NetworkManager;
import com.gofercrm.user.gofercrm.volleynetworkutil.VolleyResponseListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TaskUserFilterActivity extends BaseActivity implements RecyclerViewClickListener {
    private static final String TAG = "TaskUserFilterActivity";
    private List<Contact> connectionsAll = new ArrayList<>();
    private List<Contact> connections = new ArrayList<>();
    private TaskUserFilterAdapter oAdapter;

    private String strUID;
    private ActivityTaskUserFilterBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_task_user_filter);
        binding.setActivity(this);
        getDataFromIntent();
        setUpView();
        getConnections();
    }

    private void setUpView() {
        oAdapter = new TaskUserFilterAdapter(connections, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        binding.rv.setLayoutManager(mLayoutManager);
        binding.rv.setItemAnimator(new DefaultItemAnimator());
        binding.rv.setAdapter(oAdapter);
        oAdapter.setOnRecyclerViewItemClickListener(this);
        binding.tvCancel.setOnClickListener(view -> {
            Util.hideSoftKeyboard(view);
            onBackPressed();
        });

        binding.etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                connections.clear();
                if (charSequence.toString().length() > 0) {
                    for (int j = 0; j < connectionsAll.size(); j++) {
                        if (connectionsAll.get(j).getName().toLowerCase().startsWith(charSequence.toString().toLowerCase())) {
                            connections.add(connectionsAll.get(j));
                        }
                    }
                    oAdapter.notifyDataSetChanged();
                } else {
                    connections.addAll(connectionsAll);
                    oAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    /**
     * GET DATA FROM INTENT
     */
    private void getDataFromIntent() {
        if (getIntent() != null && getIntent().hasExtra("DATA")) {
            strUID = getIntent().getStringExtra("DATA");
        }
    }

    private void getConnections() {
        NetworkManager.customJsonObjectGetRequest(
                this, Constants.GET_CONNECTIONS_PICKLIST_URL, null,
                new VolleyResponseListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        processConnections(response);
                    }

                    @Override
                    public void onError(String message) {
                        LogUtils.Print(TAG, "Message -> " + message);
                    }

                }, false);
    }

    private void processConnections(JSONObject data) {
        try {
            connections.clear();
            connectionsAll.clear();
            String id = "";
            String name = "";
            String email = "";
            if (Integer.parseInt(data.getString("result")) == 1) {
                JSONArray users = data.getJSONArray("users");
                for (int i = 0; i < users.length(); i++) {
                    JSONObject obj = users.getJSONObject(i);
                    email = obj.has("email") ? obj.getString("email") : "";
                    id = obj.getString("id");
                    name = obj.getString("name");
                    if (name != null) {
                        name = Util.capitalize(name);
                    }
                    Contact contact = new Contact(id, name, email, "");
                    if (strUID != null && !strUID.equals("")) {
                        if (id.equals(strUID)) {
                            contact.setSelect(true);
                        }
                    }
                    connectionsAll.add(contact);
                    connections.add(contact);
                }
                onLoaded();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onLoaded() {
        if (connections.size() > 0) {
            binding.rv.setVisibility(View.VISIBLE);
            oAdapter.notifyDataSetChanged();
            binding.noRecords.setVisibility(View.GONE);
        } else {
            binding.noRecords.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onItemClick(int flag, int position, View view) {
        Intent intent = new Intent();
        intent.putExtra("DATA", connections.get(position).getId());
        setResult(RESULT_OK, intent);
        finish();
    }
}